/**
 * Created by amit on 4/24/18.
 */

import express from 'express';
import cors from "cors";
import React from 'react';
import {renderToString} from 'react-dom/server'
import {Provider} from 'react-redux';
import {push} from 'react-router-redux';
import {matchPath, StaticRouter,Switch} from 'react-router-dom';
import serialize from "serialize-javascript";
import { SheetsRegistry } from 'react-jss/lib/jss';
import JssProvider from 'react-jss/lib/JssProvider';
import PropTypes from 'prop-types';
import {
  MuiThemeProvider,
  createMuiTheme,
  createGenerateClassName,
} from '@material-ui/core/styles';
import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import FormLabel from '@material-ui/core/FormLabel';
import FormControl from '@material-ui/core/FormControl';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormHelperText from '@material-ui/core/FormHelperText';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import Checkbox from '@material-ui/core/Checkbox';

import configureStore, {history} from '../shared/config';
import {App, routes} from '../shared';
import "source-map-support/register";
import Html from './Html'
import green from '@material-ui/core/colors/green';


const port = process.env.PORT || 5000;
const server = express();
server.use(cors());
server.use(express.static('public'));


server.get("*", (req, res, next) => {
   
    const {store, sagaMiddleware} = configureStore();
    let pageType = false;
    var cookieValue = req.headers.cookie // get cookie value form the request
   
    var langValue = 'EN'
    
    if(cookieValue){
        langValue=cookieValue.split('=');
        for(let i=0 ;i<langValue.length;i++){
            if(langValue[i]=='EN' || langValue[i]=='AR'){
                langValue = langValue[i];
                break;
            }
        }
        // langValue=langValue?langValue:"EN"
     }
    
   // console.log("new lang",langValue)


    // console.log(req.url);
    // We created an array of promises because we need our state to be initialized after all the actions are performed
    const promises = routes.reduce((total, route) => {
        const matched = matchPath(req.url, route) !== null;
      //  console.log('matched ---------', matched, req.url);
        const condition =
            matched &&      // check if requested url exists in routes or not
            route.component &&                // check if there exist any component on the requested route
            route.component.initialAction;    // check if there exist any static initializing function in component
        // console.log("api url condition :: ", condition);
        // console.log('initial action   --- ', route.component.initialAction({url: req.url}));

        if (condition) {
            // This sync the requested url with the redux Store routes.
            total.push(Promise.resolve(store.dispatch(push(req.url))));
            // We call the initialAction of the component containing requested url, so that it can initialize the state
            // Catch this action in reducer and change the app state.
            // total.push(Promise.resolve(store.dispatch(route.component.initialAction({url: req.url}))));

            // const responsePromise = route.component.initialAction({url: req.url, store});
          pageType = route.component.initialAction({url: req.url, store,lang:langValue});

            // total.push(Promise.resolve(responsePromise));
            
        }

        return total;
    }, []);     // Default value []

    // Wait for sagas to finish.
    if(!pageType)
        promises.push(Promise.resolve(sagaMiddleware.done));

    Promise.all(promises).then(() => {
        // When all promises are completed
       
        const context = {history};
        const sheetsRegistry = new SheetsRegistry();
        const sheetsManager = new Map();
        const generateClassName = createGenerateClassName();
        const markup = renderToString(
            <Provider store={store}>
            <JssProvider  registry={sheetsRegistry} generateClassName={generateClassName}>
              <MuiThemeProvider sheetsManager={sheetsManager}>
                <StaticRouter location={req.url} context={context}>
                    <App />
                </StaticRouter>
              </MuiThemeProvider>
            </JssProvider>
            </Provider>
        );
        const css = sheetsRegistry.toString();
       
        const initialData = store.getState();       // Initialized data which we got from resolving our promises.
            //console.log(markup); return;
        // TODO : Fetch relevant data from the initialData here and pass it in the Html Function. Example below
        res.send(Html({title: "", body: markup, initialData: serialize(initialData),css:css}));

    }).catch(next);
});

server.listen(port, () => {
    console.log(`Serving at http://localhost:${port}`);
});

