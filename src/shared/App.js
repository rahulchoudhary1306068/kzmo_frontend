import React , {Component} from 'react';
import { Route, Router, Switch } from 'react-router-dom';

import { history } from './config';
import routes from './routes';

import Header from './components/header'
import Footer from './components/footer'
import ErrorBoundary from './components/error_page'

const App = (props) => (
    <div className="app-container">
        <Header history={history}/>
            <ErrorBoundary history={history}>
                
                <Switch>
                    <Router  history={history} >
                        <div>
                            {
                                routes.map( (route, index) =>
                                <Route key={index} exact={route.exact} path={route.path} component={route.component}/>)
                            }
                        </div>
                    </Router>
                </Switch>
            </ErrorBoundary>    
        <Footer history={history}/>
    </div>
);

export default App;

// export default class App extends Component {

// //     render() {
        
        
// //         return (
// //             <div className="app-container">
// //             <Header history={history}/>
// //                 <ErrorBoundary history={history}>
                    
                    
// //                     <Router  history={history} >
// //                             <Switch>>
// //                                 {
// //                                     routes.map( (route, index) =>
// //                                     <Route key={index} exact={route.exact} path={route.path} component={route.component}/>)
// //                                 }
// //                             </Switch>
// //                     </Router>   
                    
// //                 </ErrorBoundary>    
// //             <Footer history={history}/>
// //         </div>
// //         )
// //     }
// // }