const env = 'prod'; //prod //dev

let apiType = "";
let microSiteValue = "";
if(env == "dev"){
	apiType = "test-";
	microSiteValue = ".test";
}

export const base_URL = "https://" + apiType + "api.kzmo.com/api/v11/";
export const api_KEY = "d42121c70dda5edfgd1df6633fdb36c0";

export const base_url_SM_API = "https://" + apiType + "sm.kzmo.com/api/v11/";

export const login_base_URL = "https://" + apiType + "developer.kzmo.com/api/v1/user/";
export const login_api_KEY = "c04d979305fe8969b34b09c3f223a54a";
export const token_URL = "https://" + apiType + "auth.kzmo.com/token.php";

export const base_url_shopclues = "http://api.shopclues.com/api/v11/";
export const api_key_shopclues = "d42121c70dda5edfgd1df6633fdb36c0";

export const delivery_detail_base_url = "https://www.shopclues.com/nss.php";

export const userProfile = "https://" + apiType + "api.kzmo.com/api/v11/";

export const userProfile_key = "d42121c70dda5edfgd1df6633fdb36c0"

export const paymentGateway_URL = "https://" + apiType + "api.kzmo.com/api/v11/atom/DoPayment/initiatePayment"
export const platform = "D";
export const omniture_mcid = "";
export const COD_Payment_Option_Id = 61;
export const returnUrlSuccess = "http://" + apiType + "www.kzmo.com/thankyou/order_id/";
export const returnUrlFail = "http://" + apiType + "www.kzmo.com/orderfail/order_id/";

export const developerUrl ="https://" + apiType + "developer.kzmo.com/api/v2/"
export const SADA_Payment_Option_Id = 10001

export const base_URL_SM = "https://" + apiType + "api.kzmo.com/api/v11/atom/";
export const base_URL_CODOTP = "https://" + apiType + "api.kzmo.com/api/v11/atom/";

export const image_cdn_path = "https://cdn.kzmo.com/";

export const developerUrlSec = "https://" + apiType + "developer.kzmo.com/api/";

export const client_id = "01RX33H3LTQIUZXSAPJ1AFUU34K1X6OW";
export const client_secret = "_X7HMB78MMV&!VPAIQH9^_1HIDFZ8+WI75X8RI+33^RBI";
export const REFRESH_TOKEN_URL = "https://" + apiType +"auth.kzmo.com/refreshToken.php";
export const MicroSiteUrl = microSiteValue + ".kzmo.com";
export const GA_ID = 'UA-132330581-1';  //
