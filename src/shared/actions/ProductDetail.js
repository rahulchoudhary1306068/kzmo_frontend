export const PRODUCTDETAIL_ACTION = 'PRODUCTDETAIL_ACTION';
export const PRODUCTDETAIL_ACTION_RESULT = 'PRODUCTDETAIL_ACTION_RESULT';
export const PRODUCTDETAIL_ACTION_ERROR = 'PRODUCTDETAIL_ACTION_ERROR';
export const PRODUCTDETAIL_ACTION_RETRY = 'PRODUCTDETAIL_ACTION_RETRY';

export const productdetailAction = (data) => {
    return {
        type: PRODUCTDETAIL_ACTION,
        payload: data
    }
};

export const PINCODE_ACTION = 'PINCODE_ACTION';
export const PINCODE_ACTION_RESULT = 'PINCODE_ACTION_RESULT';
export const PINCODE_ACTION_ERROR = 'PINCODE_ACTION_ERROR';
export const PINCODE_ACTION_RETRY = 'PINCODE_ACTION_RETRY';

export const deliveryDetailAction = (data) => {
    return {
        type: PINCODE_ACTION,
        payload: data
    }
};

export const ADD_PRODUCT_REVIEW_ACTION = 'ADD_PRODUCT_REVIEW_ACTION';

export const addProductReviewAction = (data) => {
	return {
        type: ADD_PRODUCT_REVIEW_ACTION,
        payload: data
    }
}

export const GET_PRODUCT_IMAGE_ACTION = 'GET_PRODUCT_IMAGE_ACTION';

export const getProductImageAction = (data) => {
    return {
        type: GET_PRODUCT_IMAGE_ACTION,
        payload: data
    }
}

export const GETNEARSTORE_ACTION = 'GETNEARSTORE_ACTION';
export const GETNEARSTORE_ACTION_RESULT = 'GETNEARSTORE_ACTION_RESULT';
export const GETNEARSTORE_ACTION_ERROR = 'GETNEARSTORE_ACTION_ERROR';
export const GETNEARSTORE_ACTION_RETRY = 'GETNEARSTORE_ACTION_RETRY';

export const getNearStoreLocation = (data) => {
    return {
        type: GETNEARSTORE_ACTION,
        payload: data
    }
};

export const GET_SIBLING_ID_ACTION = 'GET_SIBLING_ID_ACTION';
export const GET_SIBLING_ID_RESULT = 'GET_SIBLING_ID_RESULT';
export const GET_SIBLING_ID_ERROR = 'GET_SIBLING_ID_ERROR';
export const GET_SIBLING_ID_RETRY = 'GET_SIBLING_ID_RETRY';

export const getSiblingIdAction = (data) => {
    return {
        type: GET_SIBLING_ID_ACTION,
        payload: data
    }
};

// action for edd - dinesh
export const GETEDD_ACTION =  'GETEDD_ACTION';
export const GETEDD_ACTION_RESULT = 'GETEDD_ACTION_RESULT';
export const GETEDD_ACTION_ERROR = 'GETEDD_ACTION_ERROR';
export const GETEDD_ACTION_RETRY = 'GETEDD_ACTION_RETRY';

export const getEddAction = (data) => {
    return {
        type: GETEDD_ACTION,
        payload: data
    }
};