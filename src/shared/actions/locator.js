export const LOCATOR_ACTION = 'LOCATOR_ACTION';
export const LOCATOR_ACTION_RESULT = 'LOCATOR_ACTION_RESULT';
export const LOCATOR_ACTION_ERROR = 'LOCATOR_ACTION_ERROR';
export const LOCATOR_ACTION_RETRY = 'LOCATOR_ACTION_RETRY';

export const locatorAction = (data) => {
    return {
        type: LOCATOR_ACTION,
        payload: data
    }
};

// mall list actions
export const MALL_ACTION = 'MALL_ACTION';
export const MALL_ACTION_RESULT = 'MALL_ACTION_RESULT';
export const MALL_ACTION_ERROR = 'MALL_ACTION_ERROR';
export const MALL_ACTION_RETRY = 'MALL_ACTION_RETRY';

export const mallAction = (data) => {
    return {
        type: MALL_ACTION,
        payload: data
    }
};