export const ORDER_ACTION = 'ORDER_ACTION';
export const ORDER_ACTION_RESULT = 'ORDER_ACTION_RESULT';
export const ORDER_ACTION_ERROR = 'ORDER_ACTION_ERROR';
export const ORDER_ACTION_RETRY = 'ORDER_ACTION_RETRY';

export const userOrderAction = (data) => {
    return {
        type: ORDER_ACTION,
        payload: data
    }
};

export const ORDER_DETAIL_ACTION = 'ORDER_DETAIL_ACTION';
export const ORDER_DETAIL_RESULT = 'ORDER_DETAIL_ACTION_RESULT';
export const ORDER_DETAIL_ERROR = 'ORDER_DETAIL_ACTION_ERROR';
export const ORDER_DETAIL_RETRY = 'ORDER_DETAIL_ACTION_RETRY';

export const orderDetailAction = (data) => {
    return {
        type: ORDER_DETAIL_ACTION,
        payload: data
    }
};

export const USER_INFO_ACTION = 'USER_INFO_ACTION';
export const USER_INFO_RESULT = 'USER_INFO_RESULT';
export const USER_INFO_ERROR = 'USER_INFO_ERROR';
export const USER_INFO_RETRY = 'USER_INFO_RETRY';

export const userInfoAction = (data) => {
    return {
        type: USER_INFO_ACTION,
        payload: data
    }
};

export const EDIT_USER_INFO_ACTION = 'EDIT_USER_INFO_ACTION';

export const editUserInfoAction = (data) => {
    return {
        type: EDIT_USER_INFO_ACTION,
        payload: data
    }
};

export const CHANGE_USER_PASSWORD_ACTION = 'CHANGE_USER_PASSWORD_ACTION';

export const changeUserPasswordAction = (data) => {
    return {
        type: CHANGE_USER_PASSWORD_ACTION,
        payload: data
    }
};

export const REWARD_POINTS_ACTION = 'REWARD_POINTS_ACTION';
export const REWARD_POINTS_RESULT = 'REWARD_POINTS_RESULT';
export const REWARD_POINTS_ERROR = 'REWARD_POINTS_ERROR';
export const REWARD_POINTS_RETRY = 'REWARD_POINTS_RETRY';

export const rewardPointsAction = (data) => {
    return {
        type: REWARD_POINTS_ACTION,
        payload: data
    }
};

export const WISHLIST_ACTION = 'WISHLIST_ACTION';
export const WISHLIST_ACTION_RESULT = 'WISHLIST_ACTION_RESULT';
export const WISHLIST_ACTION_ERROR = 'WISHLIST_ACTION_ERROR';
export const WISHLIST_ACTION_RETRY = 'WISHLIST_ACTION_RETRY';

export const wishlistAction = (data) => {
    return {
        type: WISHLIST_ACTION,
        payload: data
    }
};

export const WISHLIST_REMOVE_ACTION = 'WISHLIST_REMOVE_ACTION';

export const wishlistRemoveAction = (data) => {
    return {
        type: WISHLIST_REMOVE_ACTION,
        payload: data
    }
};

export const ADD_TO_CART_FROM_WISHLIST_ACTION = 'ADD_TO_CART_FROM_WISHLIST_ACTION';

export const addToCartFromWishlistAction = (data) => {
    return {
        type: ADD_TO_CART_FROM_WISHLIST_ACTION,
        payload: data
    }
};

export const CANCEL_ORDER_ACTION = 'CANCEL_ORDER_ACTION';

export const cancelOrderAction = (data) => {
    return {
        type: CANCEL_ORDER_ACTION,
        payload: data
    }
};

//dinesh for cancel order from order detail pageType
export const CANCEL_ORDER_FROM_DETAIL_ACTION = 'CANCEL_ORDER_FROM_DETAIL_ACTION';

export const cancelOrderFromDetailAction = (data) => {
return {
type: CANCEL_ORDER_FROM_DETAIL_ACTION,
payload: data
}
};

export const RATE_PURCHASE_ACTION = 'RATE_PURCHASE_ACTION';
export const RATE_PURCHASE_ACTION_RESULT = 'RATE_PURCHASE_ACTION_RESULT';
export const RATE_PURCHASE_ACTION_ERROR = 'RATE_PURCHASE_ACTION_ERROR';
export const RATE_PURCHASE_ACTION_RETRY = 'RATE_PURCHASE_ACTION_RETRY';

export const ratePurchaseAction = (data) => {
    return {
        type: RATE_PURCHASE_ACTION,
        payload: data
    }
};

export const ORDER_REVIEW_SUBMIT_ACTION = 'ORDER_REVIEW_SUBMIT_ACTION';
export const ORDER_REVIEW_SUBMIT_ACTION_RESULT = 'ORDER_REVIEW_SUBMIT_ACTION_RESULT';
export const ORDER_REVIEW_SUBMIT_ACTION_ERROR = 'ORDER_REVIEW_SUBMIT_ACTION_ERROR';
export const ORDER_REVIEW_SUBMIT_ACTION_RETRY = 'ORDER_REVIEW_SUBMIT_ACTION_RETRY';

export const orderReviewSubmitAction = (data) => {
    return {
        type: ORDER_REVIEW_SUBMIT_ACTION,
        payload: data
    }
};

export const HELP_ISSUE_ACTION = 'HELP_ISSUE_ACTION';
export const HELP_ISSUE_ACTION_RESULT = 'HELP_ISSUE_ACTION_RESULT';
export const HELP_ISSUE_ACTION_ERROR = 'HELP_ISSUE_ACTION_ERROR';
export const HELP_ISSUE_ACTION_RETRY = 'HELP_ISSUE_ACTION_RETRY';

export const helpIssueAction = (data) => {
    return {
        type: HELP_ISSUE_ACTION,
        payload: data
    }
};

export const HELP_SUB_ISSUE_ACTION = 'HELP_SUB_ISSUE_ACTION';
export const HELP_SUB_ISSUE_ACTION_RESULT = 'HELP_SUB_ISSUE_ACTION_RESULT';
export const HELP_SUB_ISSUE_ACTION_ERROR = 'HELP_SUB_ISSUE_ACTION_ERROR';
export const HELP_SUB_ISSUE_ACTION_RETRY = 'HELP_SUB_ISSUE_ACTION_RETRY';

export const helpSubIssueAction = (data) => {
    return {
        type: HELP_SUB_ISSUE_ACTION,
        payload: data
    }
};

export const HELP_SUB_SUB_ISSUE_ACTION = 'HELP_SUB_SUB_ISSUE_ACTION';
export const HELP_SUB_SUB_ISSUE_ACTION_RESULT = 'HELP_SUB_SUB_ISSUE_ACTION_RESULT';
export const HELP_SUB_SUB_ISSUE_ACTION_ERROR = 'HELP_SUB_SUB_ISSUE_ACTION_ERROR';
export const HELP_SUB_SUB_ISSUE_ACTION_RETRY = 'HELP_SUB_SUB_ISSUE_ACTION_RETRY';

export const helpSubSubIssueAction = (data) => {
    return {
        type: HELP_SUB_SUB_ISSUE_ACTION,
        payload: data
    }
};

export const SUBMIT_NEED_HELP_ACTION = 'SUBMIT_NEED_HELP_ACTION';
export const SUBMIT_NEED_HELP_ACTION_RESULT = 'SUBMIT_NEED_HELP_ACTION_RESULT';
export const SUBMIT_NEED_HELP_ACTION_ERROR = 'SUBMIT_NEED_HELP_ACTION_ERROR';
export const SUBMIT_NEED_HELP_ACTION_RETRY = 'SUBMIT_NEED_HELP_ACTION_RETRY';

export const submitNeedHelpAction = (data) => {
    return {
        type: SUBMIT_NEED_HELP_ACTION,
        payload: data
    }
};

export const ORDER_STATUS_ACTION = 'ORDER_STATUS_ACTION';
export const ORDER_STATUS_ACTION_RESULT = 'ORDER_STATUS_ACTION_RESULT';
export const ORDER_STATUS_ACTION_ERROR = 'ORDER_STATUS_ACTION_ERROR';
export const ORDER_STATUS_ACTION_RETRY = 'ORDER_STATUS_ACTION_RETRY';

export const orderStatusAction = (data) => {
    return {
        type: ORDER_STATUS_ACTION,
        payload: data
    }
};

export const RETURN_LIST_ACTION = 'RETURN_LIST_ACTION';
export const RETURN_LIST_ACTION_RESULT = 'RETURN_LIST_ACTION_RESULT';
export const RETURN_LIST_ACTION_ERROR = 'RETURN_LIST_ACTION_ERROR';
export const RETURN_LIST_ACTION_RETRY = 'RETURN_LIST_ACTION_RETRY';

export const returnListAction = (data) => {
    return {
        type: RETURN_LIST_ACTION,
        payload: data
    }
};

export const CANCEL_RETURN_ACTION = 'CANCEL_RETURN_ACTION';

export const cancelReturnAction = (data) => {
    return {
        type: CANCEL_RETURN_ACTION,
        payload: data
    }
};

export const RETURN_DETAIL_ACTION = 'RETURN_DETAIL_ACTION';
export const RETURN_DETAIL_ACTION_RESULT = 'RETURN_DETAIL_ACTION_RESULT';
export const RETURN_DETAIL_ACTION_ERROR = 'RETURN_DETAIL_ACTION_ERROR';
export const RETURN_DETAIL_ACTION_RETRY = 'RETURN_DETAIL_ACTION_RETRY';

export const returnDetailAction = (data) => {
    return {
        type: RETURN_DETAIL_ACTION,
        payload: data
    }
};

export const REFUND_DETAIL_ACTION = 'REFUND_DETAIL_ACTION';
export const REFUND_DETAIL_ACTION_RESULT = 'REFUND_DETAIL_ACTION_RESULT';
export const REFUND_DETAIL_ACTION_ERROR = 'REFUND_DETAIL_ACTION_ERROR';
export const REFUND_DETAIL_ACTION_RETRY = 'REFUND_DETAIL_ACTION_RETRY';

export const refundDetailAction = (data) => {
    return {
        type: REFUND_DETAIL_ACTION,
        payload: data
    }
};

export const CREATE_RETURN_DETAIL_ACTION = 'CREATE_RETURN_DETAIL_ACTION';
export const CREATE_RETURN_DETAIL_RESULT = 'CREATE_RETURN_DETAIL_RESULT';
export const CREATE_RETURN_DETAIL_ERROR = 'CREATE_RETURN_DETAIL_ERROR';
export const CREATE_RETURN_DETAIL_RETRY = 'CREATE_RETURN_DETAIL_RETRY';

export const createReturnDetailAction = (data) => {
    return {
        type: CREATE_RETURN_DETAIL_ACTION,
        payload: data
    }
};

export const SUBMIT_RETURN_ACTION = 'SUBMIT_RETURN_ACTION';
export const SUBMIT_RETURN_RESULT = 'SUBMIT_RETURN_RESULT';
export const SUBMIT_RETURN_ERROR = 'SUBMIT_RETURN_ERROR';
export const SUBMIT_RETURN_RETRY = 'SUBMIT_RETURN_RETRY';

export const submitReturnAction = (data) => {
    return {
        type: SUBMIT_RETURN_ACTION,
        payload: data
    }
};