export const UserProfile_ACTION = 'UserProfile_ACTION';
export const UserProfile_ACTION_RESULT = 'UserProfile_ACTION_RESULT';
export const UserProfile_ACTION_ERROR = 'UserProfile_ACTION_ERROR';
export const UserProfile_ACTION_RETRY = 'UserProfile_ACTION_RETRY';

export const EditProfile_ACTION = 'EditProfile_ACTION';
export const RemoveProfile_ACTION = 'RemoveProfile_ACTION';
export const RemoveProfile_ACTION_RESULT = 'RemoveProfile_ACTION_RESULT';
export const RemoveProfile_ACTION_ERROR = 'RemoveProfile_ACTION_ERROR';
export const RemoveProfile_ACTION_RETRY = 'RemoveProfile_ACTION_RETRY';


export const UserBank_ACTION = 'UserBank_ACTION';
export const UserBank_ACTION_RESULT = 'UserBank_ACTION_RESULT';
export const UserBank_ACTION_ERROR = 'UserBank_ACTION_ERROR';
export const UserBank_ACTION_RETRY = 'UserBank_ACTION_RETRY';

export const UserBank_ADD_ACTION = 'UserBank_ADD_ACTION';
export const UserBank_ADD_ACTION_RESULT = 'UserBank_ADD_ACTION_RESULT';
export const UserBank_ADD_ACTION_ERROR = 'UserBank_ADD_ACTION_ERROR';
export const UserBank_ADD_ACTION_RETRY = 'UserBank_ADD_ACTION_RETRY';


export const UserBank_EDIT_ACTION = 'UserBank_EDIT_ACTION';
export const UserBank_EDIT_ACTION_RESULT = 'UserBank_EDIT_ACTION_RESULT';
export const UserBank_EDIT_ACTION_ERROR = 'UserBank_EDIT_ACTION_ERROR';
export const UserBank_EDIT_ACTION_RETRY = 'UserBank_EDIT_ACTION_RETRY';


export const UserBank_All_Bank_ACTION = 'UserBank_All_Bank_ACTION';
export const UserBank_All_Bank_ACTION_RESULT = 'UserBank_All_Bank_ACTION_RESULT';
export const UserBank_All_Bank_ACTION_ERROR = 'UserBank_All_Bank_ACTION_ERROR';
export const UserBank_All_Bank_ACTION_RETRY = 'UserBank_All_Bank_ACTION_RETRY';


export const userProfileAction = (data) => {
    return {
        type: UserProfile_ACTION,
        payload: data
    }
};

export const editProfileAction = (data) => {
    return {
        type: EditProfile_ACTION,
        payload: data
    }
};

export const removeProfileAction = (data) => {
    return {
        type: RemoveProfile_ACTION,
        payload: data
    }
};

export const userBankAction = (data) => {
    return {
        type: UserBank_ACTION,
        payload: data
    }
};

export const addBankAction = (data) => {
    return {
        type: UserBank_ADD_ACTION,
        payload: data
    }
};


export const editBankAction = (data) => {
    return {
        type: UserBank_EDIT_ACTION,
        payload: data
    }
};


export const allBankAction = (data) => {
    return {
        type: UserBank_All_Bank_ACTION,
        payload: data
    }
};


