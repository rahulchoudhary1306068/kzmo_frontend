export const HOME_ACTION = 'HOME_ACTION';
export const HOME_ACTION_RESULT = 'HOME_ACTION_RESULT';
export const HOME_ACTION_ERROR = 'HOME_ACTION_ERROR';
export const HOME_ACTION_RETRY = 'HOME_ACTION_RETRY';

export const homeAction = (data) => {
    return {
        type: HOME_ACTION,
        payload: data
    }
};

export const HOME_SECTION = 'HOME_SECTION';
export const HOME_SECTION_RESULT = 'HOME_SECTION_RESULT';
export const HOME_SECTION_ERROR = 'HOME_SECTION_ERROR';
export const HOME_SECTION_RETRY = 'HOME_SECTION_RETRY';

export const sectionAction = (data) => {
    return {
        type: HOME_SECTION,
        payload: data
    }
};