export const Checkout_ACTION = 'Checkout_ACTION';
export const Checkout_ACTION_RESULT = 'Checkout_ACTION_RESULT';
export const Checkout_ACTION_ERROR = 'Checkout_ACTION_ERROR';
export const Checkout_ACTION_RETRY = 'Checkout_ACTION_RETRY';

export const checkoutAction = (data) => {
    return {
        type: Checkout_ACTION,
        payload: data
    }
};

export const PaymentOption_ACTION = 'PaymentOption_ACTION';
export const PaymentOption_ACTION_RESULT = 'PaymentOption_ACTION_RESULT';
export const PaymentOption_ACTION_ERROR = 'PaymentOption_ACTION_ERROR';
export const PaymentOption_ACTION_RETRY = 'PaymentOption_ACTION_RETRY';

export const PaymentOptionAction = (data) => {
    return {
        type: PaymentOption_ACTION,
        payload: data
    }
};
export const placeOrder_ACTION = 'placeOrder_ACTION';
export const placeOrder_ACTION_RESULT = 'placeOrder_ACTION_RESULT';
export const placeOrder_ACTION_ERROR = 'placeOrder_ACTION_ERROR';
export const placeOrder_ACTION_RETRY = 'placeOrder_ACTION_RETRY';

export const placeOrderAction = (data) => {
    return {
        type: placeOrder_ACTION,
        payload: data
    }
};
export const codOTP_ACTION = 'codOTP_ACTION';
export const codOTP_ACTION_RESULT = 'codOTP_ACTION_RESULT';
export const codOTP_ACTION_ERROR = 'codOTP_ACTION_ERROR';
export const codOTP_ACTION_RETRY = 'codOTP_ACTION_RETRY';

export const codOTPAction = (data) => {
    return {
        type: codOTP_ACTION,
        payload: data
    }
};


export const orderConfirm_ACTION = 'orderConfirm_ACTION';
export const orderConfirm_ACTION_RESULT = 'orderConfirm_ACTION_RESULT';
export const orderConfirm_ACTION_ERROR = 'orderConfirm_ACTION_ERROR';
export const orderConfirm_ACTION_RETRY = 'orderConfirm_ACTION_RETRY';

export const orderConfirmAction = (data) => {
    return {
        type: orderConfirm_ACTION,
        payload: data
    }
};