export const HEADER_ACTION = 'HEADER_ACTION';
export const HEADER_ACTION_RESULT = 'HEADER_ACTION_RESULT';
export const HEADER_ACTION_ERROR = 'HEADER_ACTION_ERROR';
export const HEADER_ACTION_RETRY = 'HEADER_ACTION_RETRY';

export const headerAction = (data) => {
    return {
        type: HEADER_ACTION,
        payload: data
    }
};

export const HEADER_SUBMENU = 'HEADER_SUBMENU';
export const HEADER_SUBMENU_RESULT = 'HEADER_SUBMENU_RESULT';
export const HEADER_SUBMENU_ERROR = 'HEADER_SUBMENU_ERROR';
export const HEADER_SUBMENU_RETRY = 'HEADER_SUBMENU_RETRY';

export const subMenuAction = (data) => {
    return {
        type: HEADER_SUBMENU,
        payload: data
    }
};

export const HEADER_SEARCH = 'HEADER_SEARCH';
export const HEADER_SEARCH_RESULT = 'HEADER_SEARCH_RESULT';
export const HEADER_SEARCH_ERROR = 'HEADER_SEARCH_ERROR';
export const HEADER_SEARCH_RETRY = 'HEADER_SEARCH_RETRY';

export const headerSearchAction = (data) => {
    return {
        type: HEADER_SEARCH,
        payload: data
    }
};

export const Country_ACTION = 'Country_ACTION';
export const Country_ACTION_RESULT = 'Country_ACTION_RESULT';
export const Country_ACTION_ERROR = 'Country_ACTION_ERROR';
export const Country_ACTION_RETRY = 'Country_ACTION_RETRY';

export const CountryAction = (data) => {
    return {
        type: Country_ACTION,
        payload: data
    }
};

export const CITY_ACTION = 'CITY_ACTION';
export const CITY_ACTION_RESULT = 'CITY_ACTION_RESULT';
export const CITY_ACTION_ERROR = 'CITY_ACTION_ERROR';
export const CITY_ACTION_RETRY = 'CITY_ACTION_RETRY';

export const CityAction = (data) => {
    return {
        type: CITY_ACTION,
        payload: data
    }
};

export const STORE_ACTION = 'STORE_ACTION';
export const STORE_ACTION_RESULT = 'STORE_ACTION_RESULT';
export const STORE_ACTION_ERROR = 'STORE_ACTION_ERROR';
export const STORE_ACTION_RETRY = 'STORE_ACTION_RETRY';

export const StoreAction = (data) => {
    return {
        type: STORE_ACTION,
        payload: data
    }
};

export const UPDATE_LANG_ACTION = 'UPDATE_LANG_ACTION';
export const UPDATELANG_ACTION_RESULT = 'UPDATELANG_ACTION_RESULT';

export const UpdateLanguage = (data) => {
    return {
        type: UPDATE_LANG_ACTION,
        payload: data
    }
};

export const BrandMenu_ACTION = 'BrandMenu_ACTION';
export const BrandMenu_ACTION_RESULT = 'BrandMenu_ACTION_RESULT';
export const BrandMenu_ACTION_ERROR = 'BrandMenu_ACTION_ERROR';
export const BrandMenu_ACTION_RETRY = 'BrandMenu_ACTION_RETRY';