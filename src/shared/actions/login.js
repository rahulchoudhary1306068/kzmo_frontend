export const Login_ACTION = 'Login_ACTION';
export const Login_ACTION_RESULT = 'Login_ACTION_RESULT';
export const Login_ACTION_ERROR = 'Login_ACTION_ERROR';
export const Login_ACTION_RETRY = 'Login_ACTION_RETRY';
export const OTPVerify_ACTION = 'OTPVerify_ACTION';
export const OTPVerify_ACTION_RESULT = 'OTPVerify_ACTION_RESULT';
export const OTPVerify_ACTION_ERROR = 'OTPVerify_ACTION_ERROR';
export const OTPVerify_ACTION_RETRY = 'OTPVerify_ACTION_RETRY';

export const Showlogin_ACTION = 'Showlogin_ACTION';
export const ResendOTP_ACTION = 'ResendOTP_ACTION';
export const ResendOTP_ACTION_RESULT = 'ResendOTP_ACTION_RESULT';
export const ResendOTP_ACTION_ERROR = 'ResendOTP_ACTION_ERROR';
export const ResendOTP_ACTION_RETRY = 'ResendOTP_ACTION_RETRY';
export const LoginOTP_ACTION = 'LoginOTP_ACTION';
export const LoginOTP_ACTION_RESULT = 'LoginOTP_ACTION_RESULT';
export const LoginOTP_ACTION_ERROR = 'LoginOTP_ACTION_ERROR';
export const LoginOTP_ACTION_RETRY = 'LoginOTP_ACTION_RETRY';
export const InitailState_ACTION = 'InitailState_ACTION';
export const ForgotOTP_ACTION = 'ForgotOTP_ACTION';
export const ForgotOTP_ACTION_RESULT = 'ForgotOTP_ACTION_RESULT';
export const ForgotOTP_ACTION_ERROR = 'ForgotOTP_ACTION_ERROR';
export const ForgotOTP_ACTION_RETRY = 'ForgotOTP_ACTION_RETRY';
export const ForgotVerifyOTP_ACTION = 'ForgotVerifyOTP_ACTION';
export const ForgotVerifyOTP_ACTION_RESULT = 'ForgotVerifyOTP_ACTION_RESULT';
export const ForgotVerifyOTP_ACTION_ERROR = 'ForgotVerifyOTP_ACTION_ERROR';
export const ForgotVerifyOTP_ACTION_RETRY = 'ForgotVerifyOTP_ACTION_RETRY';
export const UpdatePassword_ACTION = 'UpdatePassword_ACTION';
export const UpdatePassword_ACTION_RESULT = 'UpdatePassword_ACTION_RESULT';
export const UpdatePassword_ACTION_ERROR = 'UpdatePassword_ACTION_ERROR';
export const UpdatePassword_ACTION_RETRY = 'UpdatePassword_ACTION_RETRY';
export const InvalidToken_ACTION = 'InvalidToken_ACTION';
export const InvalidToken_ACTION_RESULT = 'InvalidToken_ACTION_RESULT'

export const LoginAction = (data) => { 
    return {
        type: Login_ACTION,
        payload: data
    }
};

export const showLogin = (data) => { 
    return {
        type: Showlogin_ACTION,
        payload: data
    }
};

export const OTPVerifyAction = (data) => { 
    return {
        type: OTPVerify_ACTION,
        payload: data
    }
};
export const ResendOTPAction = (data) => { 
    return {
        type: ResendOTP_ACTION,
        payload: data
    }
};
export const LoginOTPAction = (data) => { 
    return {
        type: LoginOTP_ACTION,
        payload: data
    }
};

export const InitailStateAction = (data) => { 
    return {
        type: data,
        
    }
};
export const ForgotOTPAction = (data) => { 
    return {
        type: ForgotOTP_ACTION,
        payload: data
    }
};
export const ForgotVerifyOTPAction = (data) => { 
    return {
        type: ForgotVerifyOTP_ACTION,
        payload: data
    }
};
export const UpdatePasswordAction = (data) => { 
    return {
        type: UpdatePassword_ACTION,
        payload: data
    }
};

export const InvalidTokenAction = (data) => { 
    return {
        type: InvalidToken_ACTION,
        payload: data
    }
};