export const addtocart_ACTION = 'addtocart_ACTION';
export const addtocart_ACTION_RESULT = 'addtocart_ACTION_RESULT';
export const addtocart_ACTION_ERROR = 'addtocart_ACTION_ERROR';
export const addtocart_ACTION_RETRY = 'addtocart_ACTION_RETRY';

export const getcart_ACTION = 'getcart_ACTION';
export const getcart_ACTION_RESULT = 'getcart_ACTION_RESULT';
export const getcart_ACTION_ERROR = 'getcart_ACTION_ERROR';
export const getcart_ACTION_RETRY = 'getcart_ACTION_RETRY';

export const deleteproductfromcart_ACTION = 'deleteproductfromcart_ACTION';
export const deleteproductfromcart_ACTION_RESULT = 'deleteproductfromcart_ACTION_RESULT';
export const deleteproductfromcart_ACTION_ERROR = 'deleteproductfromcart_ACTION_ERROR';
export const deleteproductfromcart_ACTION_RETRY = 'deleteproductfromcart_ACTION_RETRY';

export const cartcouponcode_ACTION = 'cartcouponcode_ACTION';
export const cartcouponcode_ACTION_RESULT = 'cartcouponcode_ACTION_RESULT';
export const cartcouponcode_ACTION_ERROR = 'cartcouponcode_ACTION_ERROR';
export const cartcouponcode_ACTION_RETRY = 'cartcouponcode_ACTION_RETRY';

export const addToWishlist_ACTION = 'addToWishlist_ACTION';
export const addToWishlist_ACTION_RESULT = 'addToWishlist_ACTION_RESULT';
export const addToWishlist_ACTION_ERROR = 'addToWishlist_ACTION_ERROR';
export const addToWishlist_ACTION_RETRY = 'addToWishlist_ACTION_RETRY';

export const addtocartAction = (data) => {
    return {
        type: addtocart_ACTION,
        payload: data
    }
};

export const getcartAction = (data) => {
    return {
        type: getcart_ACTION,
        payload: data
    }
};

export const deleteproductfromcartAction = (data) => {
    return {
        type: deleteproductfromcart_ACTION,
        payload: data
    }
};

export const cartcouponcodeAction = (data) => {
    return {
        type: cartcouponcode_ACTION,
        payload: data
    }
};

export const addToWishlistAction = (data) => {
    return {
        type: addToWishlist_ACTION,
        payload: data
    }
};