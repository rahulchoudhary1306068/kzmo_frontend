import React, { Component } from 'react';
import {connect} from 'react-redux';

// import "react-responsive-carousel/lib/styles/carousel.min.css";
import local, {deafultLanguage} from '../../localization';
import {getter} from '../../commonfunction';

class Brand extends Component {
    constructor(props){
        super(props);
        this.state = {
            language: ''
        };
    }
  
    static initialAction({url, store}) {
       // store.dispatch(productListAction());
       return true;
    }
    componentDidMount(){
        this.setState({
            language: getter('language') ? getter('language') : deafultLanguage
        })
    }
    componentWillReceiveProps(nextProps){
        if(nextProps.languageReducer.value){
            this.setState({
                    language: nextProps.languageReducer.value
            })  
        }
    }
    render() {
        return (
            <div className="brand_outer">
                <h1 className="category_name text-left">Exclusive Brands</h1>
                <div className="brand_sec">
                    <img src="/img/adidas.png" />
                    <img src="/img/adidas.png" />
                    <img src="/img/adidas.png" />
                    <img src="/img/adidas.png" />
                    <img src="/img/adidas.png" />
                    <img src="/img/adidas.png" />
                    <img src="/img/adidas.png" />
                    <img src="/img/adidas.png" />
                    <img src="/img/adidas.png" />
                    <img src="/img/adidas.png" />
                </div>
                <div className="brand_list">
                    <h2 className="category_name text-left">Brands (A-Z)</h2>
                    <div className="alphabetic_sort">
                        <a href="Javascript:void(0);">#</a>
                        <a href="Javascript:void(0);">A</a>
                        <a href="Javascript:void(0);">B</a>
                        <a href="Javascript:void(0);">C</a>
                        <a href="Javascript:void(0);">D</a>
                        <a href="Javascript:void(0);">E</a>
                        <a href="Javascript:void(0);">F</a>
                        <a href="Javascript:void(0);">G</a>
                        <a href="Javascript:void(0);">H</a>
                        <a href="Javascript:void(0);">I</a>
                        <a href="Javascript:void(0);">J</a>
                        <a href="Javascript:void(0);">K</a>
                        <a href="Javascript:void(0);">L</a>
                        <a href="Javascript:void(0);">M</a>
                        <a href="Javascript:void(0);">N</a>
                        <a href="Javascript:void(0);">O</a>
                        <a href="Javascript:void(0);">P</a>
                        <a href="Javascript:void(0);">Q</a>
                        <a href="Javascript:void(0);">R</a>
                        <a href="Javascript:void(0);">S</a>
                        <a href="Javascript:void(0);">T</a>
                        <a href="Javascript:void(0);">U</a>
                        <a href="Javascript:void(0);">V</a>
                        <a href="Javascript:void(0);">W</a>
                        <a href="Javascript:void(0);">X</a>
                        <a href="Javascript:void(0);">Y</a>
                        <a href="Javascript:void(0);">Z</a>
                    </div>
                    <div className="row">
                        <div className="col">
                            <h6>A</h6>
                            <ul>
                                <li><a href="Javascript:void(0);">A Peace Treaty</a></li>
                                <li><a href="Javascript:void(0);">A.P.C.</a></li>
                                <li><a href="Javascript:void(0);">A.W.A.K.E.</a></li>
                                <li><a href="Javascript:void(0);">Adidas</a></li>
                                <li><a href="Javascript:void(0);">AGMES</a></li>
                                <li><a href="Javascript:void(0);">A Peace Treaty</a></li>
                                <li><a href="Javascript:void(0);">A Peace Treaty</a></li>
                                <li><a href="Javascript:void(0);">A Peace Treaty</a></li>
                                <li><a href="Javascript:void(0);">A Peace Treaty</a></li>
                                <li><a href="Javascript:void(0);">A Peace Treaty</a></li>
                                <li><a href="Javascript:void(0);">A.P.C.</a></li>
                                <li><a href="Javascript:void(0);">A.W.A.K.E.</a></li>
                                <li><a href="Javascript:void(0);">Adidas</a></li>
                                <li><a href="Javascript:void(0);">AGMES</a></li>
                                <li><a href="Javascript:void(0);">A Peace Treaty</a></li>
                                <li><a href="Javascript:void(0);">A Peace Treaty</a></li>
                                <li><a href="Javascript:void(0);">A Peace Treaty</a></li>
                                <li><a href="Javascript:void(0);">A Peace Treaty</a></li>
                            </ul>
                            <h6>B</h6>
                            <ul>
                                <li><a href="Javascript:void(0);">A Peace Treaty</a></li>
                                <li><a href="Javascript:void(0);">A.P.C.</a></li>
                                <li><a href="Javascript:void(0);">A.W.A.K.E.</a></li>
                                <li><a href="Javascript:void(0);">Adidas</a></li>
                                <li><a href="Javascript:void(0);">AGMES</a></li>
                                <li><a href="Javascript:void(0);">A Peace Treaty</a></li>
                                <li><a href="Javascript:void(0);">A Peace Treaty</a></li>
                                <li><a href="Javascript:void(0);">A Peace Treaty</a></li>
                                <li><a href="Javascript:void(0);">A Peace Treaty</a></li>
                                <li><a href="Javascript:void(0);">A Peace Treaty</a></li>
                                <li><a href="Javascript:void(0);">A.P.C.</a></li>
                                <li><a href="Javascript:void(0);">A.W.A.K.E.</a></li>
                                <li><a href="Javascript:void(0);">Adidas</a></li>
                                <li><a href="Javascript:void(0);">AGMES</a></li>
                                <li><a href="Javascript:void(0);">A Peace Treaty</a></li>
                                <li><a href="Javascript:void(0);">A Peace Treaty</a></li>
                                <li><a href="Javascript:void(0);">A Peace Treaty</a></li>
                                <li><a href="Javascript:void(0);">A Peace Treaty</a></li>
                            </ul>
                        </div>
                        <div className="col">
                            <h6>C</h6>
                            <ul>
                                <li><a href="Javascript:void(0);">A Peace Treaty</a></li>
                                <li><a href="Javascript:void(0);">A.P.C.</a></li>
                                <li><a href="Javascript:void(0);">A.W.A.K.E.</a></li>
                                <li><a href="Javascript:void(0);">Adidas</a></li>
                                <li><a href="Javascript:void(0);">AGMES</a></li>
                                <li><a href="Javascript:void(0);">A Peace Treaty</a></li>
                                <li><a href="Javascript:void(0);">A Peace Treaty</a></li>
                                <li><a href="Javascript:void(0);">A Peace Treaty</a></li>
                                <li><a href="Javascript:void(0);">A Peace Treaty</a></li>
                                <li><a href="Javascript:void(0);">A Peace Treaty</a></li>
                                <li><a href="Javascript:void(0);">A.P.C.</a></li>
                                <li><a href="Javascript:void(0);">A.W.A.K.E.</a></li>
                                <li><a href="Javascript:void(0);">Adidas</a></li>
                                <li><a href="Javascript:void(0);">AGMES</a></li>
                                <li><a href="Javascript:void(0);">A Peace Treaty</a></li>
                                <li><a href="Javascript:void(0);">A Peace Treaty</a></li>
                                <li><a href="Javascript:void(0);">A Peace Treaty</a></li>
                                <li><a href="Javascript:void(0);">A Peace Treaty</a></li>
                            </ul>
                        </div>
                        <div className="col">
                            <h6>D</h6>
                            <ul>
                                <li><a href="Javascript:void(0);">A Peace Treaty</a></li>
                                <li><a href="Javascript:void(0);">A.P.C.</a></li>
                                <li><a href="Javascript:void(0);">A.W.A.K.E.</a></li>
                                <li><a href="Javascript:void(0);">Adidas</a></li>
                                <li><a href="Javascript:void(0);">AGMES</a></li>
                                <li><a href="Javascript:void(0);">A Peace Treaty</a></li>
                                <li><a href="Javascript:void(0);">A Peace Treaty</a></li>
                                <li><a href="Javascript:void(0);">A Peace Treaty</a></li>
                                <li><a href="Javascript:void(0);">A Peace Treaty</a></li>
                                <li><a href="Javascript:void(0);">A Peace Treaty</a></li>
                                <li><a href="Javascript:void(0);">A.P.C.</a></li>
                                <li><a href="Javascript:void(0);">A.W.A.K.E.</a></li>
                                <li><a href="Javascript:void(0);">Adidas</a></li>
                                <li><a href="Javascript:void(0);">AGMES</a></li>
                                <li><a href="Javascript:void(0);">A Peace Treaty</a></li>
                                <li><a href="Javascript:void(0);">A Peace Treaty</a></li>
                                <li><a href="Javascript:void(0);">A Peace Treaty</a></li>
                                <li><a href="Javascript:void(0);">A Peace Treaty</a></li>
                            </ul>
                        </div>
                    </div>
                    
                </div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    console.log(state);
    return {
        example: state.example,
        languageReducer: state.languageReducer
    }
}

export default connect(mapStateToProps)(Brand);

