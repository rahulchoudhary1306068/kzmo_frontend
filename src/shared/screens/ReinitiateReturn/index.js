import React, { Component } from 'react';
import {connect} from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import local, {deafultLanguage} from '../../localization';
import {getter} from '../../commonfunction';


const styles = theme => ({
  root: {
    '&$checked': {
      color: '#e4b122'
    }
  },
  checked: {},
  
});

class ReinitiateReturn extends Component {
    constructor(props){
        super(props);
        this.state = {
            language: ''
        };
    }
    static initialAction({url, store}) {
       return true;
    }

   componentDidMount(){
        this.setState({
            language: getter('language') ? getter('language') : deafultLanguage
        })
   }
   componentWillReceiveProps(nextProps){
        if(nextProps.languageReducer.value){
            this.setState({
                    language: nextProps.languageReducer.value
            })  
        }
   }
    render() {
        const { classes } = this.props;
        
        return (
           <div className="row" style={{marginBottom:"60px"}}>
      <div className="col col-md-12 thankyou_pg">
        <span className="heading">{local.thankyou[this.state.language]}</span>
        <span className="sub_heading">{local.reinitiateReturnHeading[this.state.language]}</span>
        <p>{local.reinitiateReturnMessage[this.state.language]}</p>
        <div className="thankyou_pg_row">
          <a href="/">{local.cartContinueShopping[this.state.language]}</a>
        </div>
        {/*<div className="thankyou_pg_row">
          <a href="#!" className="inverse">View Pickup Adress</a>
        </div>*/}
      </div>
     </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        example: state.example,
        languageReducer: state.languageReducer
    }
}
export default connect(mapStateToProps)(ReinitiateReturn);