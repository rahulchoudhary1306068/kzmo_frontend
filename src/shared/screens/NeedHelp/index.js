import React, { Component } from 'react';
import {connect} from 'react-redux';
import {
  MuiThemeProvider,
  createMuiTheme,
  createGenerateClassName,
} from '@material-ui/core/styles';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Sidebar from '../../components/account_sidebar';
// import "react-responsive-carousel/lib/styles/carousel.min.css";
import local, {deafultLanguage} from '../../localization';
import {getter} from '../../commonfunction';


class NeedHelp extends Component {
    constructor(props){
        super(props);
        this.state = {
            language: ''
        };
    }
    static initialAction({url, store}) {
       // store.dispatch(productListAction());
       return true;
    }

   componentDidMount(){
        this.setState({
            language: getter('language') ? getter('language') : deafultLanguage
        })
   }
   componentWillReceiveProps(nextProps){
        if(nextProps.languageReducer.value){
            this.setState({
                    language: nextProps.languageReducer.value
            })  
        }
   }
    render() {
        return (
            <div className="dashboard_outer">
                <h1 className="category_name text-left">
                <a href="#"><svg className="icon icon-left-arrow"><use xlinkHref="/img/icon-sprite.svg#icon-left-arrow"></use></svg></a>
                {local.addrMyAccount[this.state.language]}</h1>
                <div className="row">
                    <div className="col-md-3 left_sec">
                      <Sidebar />
                    </div>
                    <div className="col-md-9 right_sec points_outer needhelp_outer order_outer">
                        <div className="value_container">
                            <div className="inner text-center p-0">
                                <p className="m-0">{local.helpCallOurCustomer[this.state.language]} </p>
                                <div className="value">0124 441 4888</div>
                            </div>
                        </div>
                        <ul className="list_outer mt-md-5">
                            <li>
                                <p className="m-0 value">{local.helpTrackOrder[this.state.language]}</p>
                                <form>
                                    <Button
                                        variant="contained"
                                        color="primary"
                                    >
                                        {local.helpCheckMyOrders[this.state.language]}
                                    </Button>
                                </form>
                            </li>
                            <li>
                                <p className="m-0 value">{local.helpTrackOrder[this.state.language]}</p>
                                <form>
                                    <Button
                                        variant="contained"
                                        color="primary"
                                    >
                                        {local.helpCheckMyReturns[this.state.language]}
                                    </Button>
                                </form>
                            </li>
                        </ul> 
                    </div>
                </div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    console.log(state);
    return {
        example: state.example,
        languageReducer: state.languageReducer
    }
}

export default connect(mapStateToProps)(NeedHelp);
