import React, { Component } from 'react';
import {connect} from 'react-redux';

// import "react-responsive-carousel/lib/styles/carousel.min.css";
import { Carousel } from 'react-responsive-carousel';
import PropTypes from 'prop-types';
import {
  MuiThemeProvider,
  createMuiTheme,
  createGenerateClassName,
} from '@material-ui/core/styles';
import { withStyles } from '@material-ui/core/styles';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import FormGroup from '@material-ui/core/FormGroup';
import FormHelperText from '@material-ui/core/FormHelperText';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Switch from '@material-ui/core/Switch';
import Button from '@material-ui/core/Button';
import {notify} from 'react-notify-toast';

import { productListAction,productidbyseonameAction } from '../../actions/productList';
import { StoreAction } from '../../actions/header';

import Loader from "../../components/loader";
import { getter, languageForAPI,calculateDiscount , setter } from '../../commonfunction';

import { addtocartAction, addToWishlistAction } from '../../actions/cart';
import { InitailStateAction } from '../../actions/login';

import { showLogin } from '../../actions/login';
import { image_cdn_path,siteUrl } from '../../constant';
import local, {deafultLanguage} from '../../localization';
import ReactGA from 'react-ga';

const FastList =require("./stack");
const staticFilter = {
    price: 'fq',
    discount_percentage: "df",
    product_rating: "pr",
    merchant_rating: "mr",
    show_brand: "show_brand",
    product_amount_available: "availability"
};
const styles = theme => ({
    root: {
    '&$checked': {
      color: '#e4b122'
    }
  },
  checked: {},
});
const Filters = (props) => (
    <div className="col-md-4 col-lg-3 filter_outer">
        <h3 className="filter_heading">
            <a className="d-sm-inline-block d-md-none cross_icon" href="JavaScript:void(0);" ><svg className="icon icon-multiply"><use xlinkHref="/img/icon-sprite.svg#icon-multiply"></use></svg></a>
            {local.commonFilters[props.language]} 
            
            { Object.keys(props.selectedFilters).length !== 0 && props.selectedFilters.constructor === Object 
                && <a className="float-right" href="JavaScript:void(0);" onClick={() => props.resetFilters()}>{local.commonReset[props.language]}</a>
            }
            </h3>
        <div className="d-sm-block d-md-none applid_outer">
        {
            Object.keys(props.selectedFilters).length !== 0 && props.selectedFilters.constructor === Object &&
            <h3 className="filter_heading">{local.commonApplied[props.language]}
            </h3>
        }
            <ul class="category_breadcrumbs d-block">
                {
                    Object.keys(props.selectedFilters).map(key => (
                        Object.keys(props.selectedFilters[key]).map(filterKey => (
                            <li>{props.selectedFilters[key][filterKey].name} <a href="JavaScript:void(0);" onClick={() => props.removeFilter(key, filterKey)}><svg className="icon icon-multiply"><use xlinkHref="/img/icon-sprite.svg#icon-multiply"></use></svg></a></li>
                        ))
                    ))
                }
            </ul>
        </div>
        <div id="filter_accordion" className="filter_list">
            {
                Object.keys(props.filters).map((filterKey, index) => ( 
                    props.filters[filterKey] && filterKey != "product_amount_available" && filterKey != "is_cod" && <div className="card">
                        <div className="card-header" id={"brand_heading"+index}>
                            <h5 className="mb-0" onClick={() => props.manageAccordian(index)}>
                                <button className="btn btn-link text-left" data-toggle="collapse" data-target={ "#filter_collapse" + index } aria-expanded="true" aria-controls={ "filter_collapse" + index }>
                                    {props.filterLabels[filterKey]} <span className=" custom_plus" style={{display: "inline"}}>+</span>
                                    <span className=" custom_minus" style={{display: "none"}}>-</span>
                                </button>
                            </h5>
                        </div>
                        <div id={ "filter_collapse" + index } className="collapse" aria-labelledby={"brand_heading"+index} data-parent={"#filter_accordion"}>
                            <div className="card-body">
                                {/*<input type="search" name="search" className="brand_srch" placeholder="Search" value={props.filters[filterKey][0].searchValue ? props.filters[filterKey][0].searchValue : ""} onChange={(e) => props.updateFilterSearchVal(e.target.value, filterKey)} />*/}
                                <ul className="unstyled centered">
                                    {
                                        props.filters[filterKey].map((item, ind) => {
                                            if(true){
                                                return(
                                                    <li className={(!props.filters[filterKey][0].searchValue || item.name.toLowerCase().indexOf(props.filters[filterKey][0].searchValue.toLowerCase()) != -1) ? "": "hide-filter"}>
                                                        <input className="checkbox_list"  id={ filterKey + "-checkbox-" + ind } type="checkbox" checked={item.is_selected}  onClick={() => props.selectFilter(filterKey, item.id, item.name)} />
                                                        <label for={ filterKey + "-checkbox-" + ind }>{item.name}</label>
                                                    </li>
                                                )
                                            }
                                        })
                                    }
                                </ul>
                            </div>
                        </div>
                    </div>
                ))
            }
        </div>
    </div>
);

const SortBy = (props) => (
    <ul className="col text-right sorting_Sec d-none d-md-block">
        <li>{local.commonSortBy[props.language]} </li>
        {/*<li className={props.activeSort == "score_desc" ? "active" : ""} onClick={() => props.setSortBy('score', 'desc')}><a href="JavaScript:void(0);">Relevance</a></li>*/}
        <li className={props.activeSort == "sort_price_desc" ? "active" : ""} onClick={() => props.setSortBy('sort_price', 'desc')}><a href="JavaScript:void(0);">{local.commonHighPrice[props.language]}</a></li>
        <li className={props.activeSort == "sort_price_asc" ? "active" : ""} onClick={() => props.setSortBy('sort_price', 'asc')}><a href="JavaScript:void(0);">{local.commonLowPrice[props.language]}</a></li>
        <li className={props.activeSort == "popularity_desc" ? "active" : ""} onClick={() => props.setSortBy('popularity', 'desc')}><a href="JavaScript:void(0);">{local.commonPopularity[props.language]}</a></li>
        {/*<li className={props.activeSort == "bestsellers_desc" ? "active" : ""} onClick={() => props.setSortBy('bestsellers', 'desc')}><a href="JavaScript:void(0);">Best Selling</a></li>*/}
        <li className={props.activeSort == "newarrivals_desc" ? "active" : ""} onClick={() => props.setSortBy('newarrivals', 'desc')}><a href="JavaScript:void(0);">{local.commonNewArrivals[props.language]}</a></li>
    </ul>
);

const AppliedFilters = (props) => (
    <div>
        <div className="d-inline-block">
            {/* <span className="filter_value">{local.commonFilterMall[props.language]} <span id="open-store" className="value">{props.selectedStore.name} </span></span> */}
            {/* <FormGroup className="d-inline-block toggle_outer common_tooltip_outer">
                <FormControlLabel
                  control={
                    <Switch
                      classes={{
                        switchBase: "iOSSwitchBase",
                        bar: "iOSBar",
                        icon: "iOSIcon",
                        iconChecked: "iOSIconChecked",
                        checked: "iOSChecked",
                      }}
                      disableRipple
                      checked={props.storeFilterToggle}
                      value="checkedB" 
                      /*onClick={() => props.toggleStoreFilter()}
                    />
                  }
                />
                <span className="common_tooltip down">{local.orderComingSoon[props.language]}</span>
            </FormGroup> */}
        </div>
        <ul className="category_breadcrumbs d-none d-md-inline-block">
            {
                Object.keys(props.selectedFilters).map(key => (
                    Object.keys(props.selectedFilters[key]).map(filterKey => (
                        <li>{props.selectedFilters[key][filterKey].name} <a href="JavaScript:void(0);" onClick={() => props.removeFilter(key, filterKey)}><svg className="icon icon-multiply"><use xlinkHref="/img/icon-sprite.svg#icon-multiply"></use></svg></a></li>
                    ))
                ))
            }
        </ul>
        <div className="modal store_outer" id="check_stores">
            <div className="modal-dialog">
                <div className="modal-content">
                    <div className="modal-body">
                        <button type="button" id="store-close" className="close" data-dismiss="modal">&times;</button>
                        <h2 className="text-center">{local.commonNearestMall[props.language]}</h2>
                        <p className="text-center">{local.commonNearYou[props.language]}</p>
                        <form>
                            <FormControl className="formControl d-block slect_outer mb-4 mt-5">
                              <InputLabel shrink htmlFor="city-placeholder">
                                {local.commonSelectCity[props.language]}
                              </InputLabel>
                              <Select
                                value={props.selectedCity}
                                input={<Input name="select_city" id="city-placeholder" />}
                                displayEmpty
                                name="select_city"
                                className="selectEmpty" 
                                onChange={(e) => props.selectCity(e.target.value)}
                              >
                                {
                                    props.cityData.response && props.cityData.response.map(city => (
                                        <MenuItem value={city.city}>{city.city}</MenuItem>
                                    ))
                                }
                              </Select>
                                <Button variant="contained" color="primary" type="button" onClick={() => props.getStores()}>
                                    {local.commonSearch[props.language]}
                                </Button>
                            </FormControl>
                            <div className="input_radio d-flex w-100 align-items-center">
                                    <RadioGroup
                                        aria-label="Address Type"
                                        name="bAddressType"
                                        value={props.selectedStore.id}
                                        onChange={(e) => props.selectStore(e)}
                                    >
                                    {
                                        props.storeList && props.storeList.data && props.storeList.data.map(store => (
                                            <FormControlLabel value={store.id} control={<Radio
                                                                          /*classes={{root: classes.root, checked: classes.checked}}*/
                                                                        />} label={store.address_line1} labelPlacement="end"
                                            />
                                        ))
                                    }
                                    </RadioGroup>
                                </div>
                                <Button variant="contained" color="primary" type="button" onClick={() => props.applyStoreFilter()}>
                                    {local.logContinue[props.language]}
                                </Button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
);

const ProductList = (props) => (
    <div className="product_list">
        <div className="data-cont row">
            { 
                props.products && props.products.length ? (props.products.map((product, prodIndex) => (
                    <div className="img-cont col-6 col-md-6 col-lg-4">
                        <div className="img_outer">
                            <a href="JavaScript:void(0);" onClick={() => props.goToProductPage(product.seo_name)}><img src={product.image_url_1 ? image_cdn_path+product.image_url_1[0] : product.image_url320} /></a>
                            <div className="size-cont">
                                <div className="sizes">
                                    {
                                        product.option_size && product.option_size.length && product.option_size.map((option, sizeOptionIndex) => (
                                            <a href="JavaScript:void(0);" className={(product.selectedSizeIndex != undefined && product.selectedSizeIndex == sizeOptionIndex) ? "active" : ""} onClick={() => props.selectProductSize(prodIndex, sizeOptionIndex)}>{option}</a>
                                        ))
                                    }
                                </div>
                                <div className="icons">
                                    <a href="JavaScript:void(0);" onClick={() => props.addToWishlist(product.product_id)}><svg className="icon icon-favorite-heart-button"><title>{local.commonAddWishList[props.language]}</title><use xlinkHref="/img/icon-sprite.svg#icon-favorite-heart-button"></use></svg></a>
                                    <a href="JavaScript:void(0);" onClick={() => props.addProductToCart(prodIndex)}><svg className="icon icon-Combined-Shape" alt="amit"><use alt="amit2" xlinkHref="/img/icon-sprite.svg#icon-Combined-Shape"></use></svg></a>
                                    {/*<a href="#"><svg className="icon icon-overlay-view"><use xlinkHref="/img/icon-sprite.svg#icon-overlay-view"></use></svg></a>*/}
                                </div>
                            </div>
                        </div>
                        <div className="img-info">
                            <div className="prod-name"><a href="JavaScript:void(0);" onClick={() => props.goToProductPage(product.seo_name)}>{product.product}</a></div>
                            <div className="price-cont">
                                <span className="brand">{product.brand}</span>
                            </div>
                            <div className="price-cont d-flex">
                                 <span className="price"> {local.wishliSar[props.language]} {product.price}</span> 
                                {parseInt(product.list_price) > parseInt(product.price) && <span  className="strike">{local.wishliSar[props.language]} {product.list_price}</span>}
                                {/*calculateDiscount(product.price,product.discount_percentage)?"price amit":"price"*/}
                                                                
                                {parseInt(product.list_price) > parseInt(product.price) &&  <span className="offer"> {calculateDiscount(product.price,product.list_price)}% {local.off[props.language]}</span> }
                                
                            </div>
                        </div>
                    </div>
                ))) : (<div>{local.commonNoProductFound[props.language]}</div>)
            }
        </div>
        { props.currentPage < props.totalPages && <a href="JavaScript:void(0);" className="show_more" onClick={() => props.loadMore()}>{local.rewardShowMore[props.language]}</a> }
    </div>
);

const ListContainer = (props) => (
    <div className="product_listing">
       <div className="row m-0">
            <Filters 
                filters = {props.data.response.filters} 
                filterLabels = {props.data.response.filterLabels} 
                selectFilter = {props.selectFilter} 
                manageAccordian = {props.manageAccordian} 
                updateFilterSearchVal = {props.updateFilterSearchVal} 
                resetFilters = {props.resetFilters} 
                selectedFilters = {props.selectedFilters} 
                removeFilter = {props.removeFilter}
                language={props.language}
                currentSelectedFilter={props.currentSelectedFilter}
            />
            <div className="col-md-8 col-lg-9">
                {props.data.metadata && <p className="category_name">{props.data.metadata.category}</p>}
                <div className="row align-items-lg-end">
                    {props.data.metadata && <p className="col subcategory text-left">{props.data.metadata.plp_product_title_h1}</p>}
                    <SortBy setSortBy={props.setSortBy} activeSort={props.activeSort} language={props.language}/>
                </div>
                <div className="d-sm-flex d-md-none row sort_filter_outer">
                    <div className="col-6 text-left">
                        <div className="dropdown sortdropdown">
                            <button className="dropdown-toggle btn-link" id="sort_link" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <svg class="icon icon-sort"><use xlinkHref="/img/icon-sprite.svg#icon-sort"></use></svg> {local.commonSortBy[props.language]}
                            </button>
                            <div className="dropdown-menu" aria-labelledby="sort_link">
                                <ul className="col sorting_Sec" >
                                    <li className={props.activeSort == "sort_price_desc" ? "active" : ""} onClick={() => props.setSortBy('sort_price', 'desc')}><a href="JavaScript:void(0);">{local.commonHighPrice[props.language]}</a></li>
                                    <li className={props.activeSort == "sort_price_asc" ? "active" : ""} onClick={() => props.setSortBy('sort_price', 'asc')}><a href="JavaScript:void(0);">{local.commonLowPrice[props.language]}</a></li>
                                    <li className={props.activeSort == "popularity_desc" ? "active" : ""} onClick={() => props.setSortBy('popularity', 'desc')}><a href="JavaScript:void(0);">{local.commonPopularity[props.language]}</a></li>
                                    <li className={props.activeSort == "newarrivals_desc" ? "active" : ""} onClick={() => props.setSortBy('newarrivals', 'desc')}><a href="JavaScript:void(0);">{local.commonNewArrivals[props.language]}</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div className="col-6 text-right">
                        <button type="button" className="btn-link filter_btn">
                            <svg class="icon icon-controls"><use xlinkHref="/img/icon-sprite.svg#icon-controls"></use></svg> {local.commonFilters[props.language]}
                        </button>
                    </div>
                </div>
                <AppliedFilters 
                    selectedFilters = {props.selectedFilters} 
                    removeFilter = {props.removeFilter} 
                    storeFilterToggle={props.storeFilterToggle} 
                    toggleStoreFilter={props.toggleStoreFilter} 
                    cityData={props.cityData} 
                    selectedCity={props.selectedCity} 
                    selectCity={props.selectCity} 
                    getStores={props.getStores} 
                    storeList={props.storeList} 
                    selectedStore={props.selectedStore} 
                    selectStore={props.selectStore} 
                    applyStoreFilter={props.applyStoreFilter}
                    language={props.language}
                />
                <ProductList 
                    products={props.data.response.products} 
                    loadMore={() => props.loadMore()} 
                    currentPage={props.currentPage} 
                    totalPages={props.totalPages} 
                    goToProductPage={props.goToProductPage} 
                    addToWishlist={props.addToWishlist}
                    language={props.language} 
                    selectProductSize={props.selectProductSize} 
                    addProductToCart={props.addProductToCart}
                />
            </div>
       </div>

    </div>
);

class ProductListing extends Component {

    constructor(props) {
        super(props);
        this.state = {
            pageType: 'plp',
            productList: "",
            currentPage: 1,
            totalPages: 1,
            sortBy: {
                type: '',
                dir: ''
            },
            activeSort: '',
            selectedFilters: {},
            storeFilterToggle: false,
            cityData: {},
            selectedCity: "Riyadh",
            storeList: {},
            selectedStore: {
                id: "",
                name: ""
            },
            language: '',
            showLoading: false,
            currentSelectedFilter:-1,
            selectedFilterIndexes:new FastList(),
            productIdSeoName:{data:{}},
            object_id:""
            
        };
    }

    static initialAction({url, store, lang}) {
        let data = {
            catId: "",
            page: 1,
            paramStr: "",
            pageType: "plp",
            lang_cod : lang
        }

        if(url.indexOf("/search") != -1){
            data.pageType = "search";
            let catIdArr = url.split("?q=");
            data.catId = catIdArr[1];
        }
        else{
            let catIdArr = url.split("/ct/");
            data.catId = catIdArr[1];
        }
        store.dispatch(productListAction(data));
        // return true;
    }

    componentDidMount(){
        ReactGA.pageview('ProductListing');
        this.setState({
            language: getter('language') ? getter('language') : deafultLanguage
        });

        if(this.props.location.pathname.indexOf("/search") != -1){
            this.setState({pageType: 'search'},() => {
                this.getProductList(window.location.href.split("q=")[1]);
            });
        }
        else{
            this.props.dispatch(productidbyseonameAction({"seo_name":window.location.href.split("/ct/")[1].replace(".html",""),"lang":languageForAPI()}));
            
        }

         
        if(this.props.cityData && this.props.cityData.data){
            this.setState({cityData: this.props.cityData.data});
        }
    }

    componentWillReceiveProps(nextProps){
        if(nextProps.languageReducer.value){
            this.setState({
                language: nextProps.languageReducer.value
            })  
        }

        if((nextProps.match.params.category_id != this.props.match.params.category_id) || (nextProps.location.search != this.props.location.search)){
            this.props.dispatch(productidbyseonameAction({"seo_name":window.location.href.split("/ct/")[1].replace(".html",""),"lang":languageForAPI()}));
            this.setState({currentPage: 1, totalPages: 1, sortBy: {type: '', dir: ''}, activeSort: '', selectedFilters: {}, storeFilterToggle: false});
        }

        if(nextProps.productList != this.props.productList){
            this.setState({showLoading: nextProps.productList.isFetching})
            if(nextProps.productList.result){
                this.setProductData(nextProps);
                this.props.dispatch(InitailStateAction("Initail_PRODUCT_LIST_ACTION"));
            }
        }

        if(nextProps.cityData.data != this.props.cityData.data){
            this.setState({cityData: nextProps.cityData.data});
        }

        if(nextProps.storeList.data != this.props.storeList.data){
            this.setState({storeList: nextProps.storeList.data})
        }

        if(nextProps.cartData != this.props.cartData && nextProps.cartData.addResult){
            notify.show(local.wishliAddedInCart[this.state.language],"success");
        }

        if(getter('userId')){
            let data = JSON.parse(getter("wishListItem"));
            if(data!== undefined && data!==null && data!==""){
                if (!(Object.keys(data).length === 0 && data.constructor === Object)) {
                   this.props.dispatch(addToWishlistAction(data));  
                }
            } 
        }
        if(nextProps.productIdSeoName!=this.props.productIdSeoName){
            this.setState({productIdSeoName:nextProps.productIdSeoName.data},()=>{
                if(Object.keys(nextProps.productIdSeoName.data).length>0){
                    this.setState({object_id:nextProps.productIdSeoName.data.data[0].object_id},()=>{
                        if(this.props.location.pathname.indexOf("/search") != -1){
                            this.setState({pageType: 'search'},() => {
                                this.getProductList(nextProps.productIdSeoName.data.data[0].object_id);
                            });
                        }
                        else{
                            this.getProductList(nextProps.productIdSeoName.data.data[0].object_id);
                        }
                    });
                }
            });
        }

    }

    setProductData(nextProps){
        let data;
        if(this.state.pageType == "plp"){
            data = nextProps.productList;
        }
        else{
            data = nextProps.productList;
            data.data = {
                response: nextProps.productList.data
            };
        }

        if(this.state.currentPage == 1){
            this.setState({productList: data});
            let totalPages = Math.ceil((data.data.response.products_count)/(data.data.response.items_per_page));
            this.setState({totalPages: totalPages});
        }
        else{
            let productList = this.state.productList;
            productList.data.response.products = productList.data.response.products.concat(data.data.response.products);
            this.setState({productList: productList});
        }
    }

    getProductList(catId){
        let str = "";
        let fsrc = "";
        for (let i = 0; i < Object.keys(this.state.selectedFilters).length; i++) {
            let key = Object.keys(this.state.selectedFilters)[i];
            let keyArr = Object.keys(this.state.selectedFilters[key]);
            if(keyArr.length){
                if(fsrc){
                    fsrc = fsrc + "," + key;
                }
                else{
                    fsrc = "&fsrc=" + key; 
                }

                keyArr.map(filtKey => {
                    str = str + "&" + key + "[]=" + filtKey;
                });
            }
        }
        if(this.state.sortBy.type){
            str = str + "&sort_by=" + this.state.sortBy.type + "&sort_order=" + this.state.sortBy.dir;
        }

        str = str + fsrc;
        
        let data = {
            catId: "",
            page: this.state.currentPage,
            paramStr: str,
            pageType: this.state.pageType,
            lang_code : languageForAPI()
        }

        if(this.props.location.pathname.indexOf("/search/") != -1){
            let search = this.props.location.search.split("q=")[1];
            if(search.indexOf("&") != -1){
                search = search.split("&")[0];
            }
            data["catId"] = search;
        }
        else{
            //data["catId"] = this.props.match.params.category_id;
            data["catId"] = catId;
        }

        //alert(JSON.stringify(data));

        this.props.dispatch(productListAction(data));
    }

    loadMoreProducts(){
        this.setState({
            currentPage: this.state.currentPage + 1
        }, () => {
           this.getProductList(this.state.object_id); 
       });
    }

    setSortBy(type, dir){
        this.setState({activeSort: type+"_"+dir});

        let temp = {
            type: type,
            dir: dir
        };
        this.setState({sortBy: temp},
            () => {
                this.setState({
                    currentPage: 1
                },
                () => {
                    this.getProductList(this.state.object_id);
                })
            }
        );

    }

    selectFilter(key, id, name){
        if(this.state.pageType == "search" && staticFilter[key]){
            key = staticFilter[key];
        }

        let temp = {...this.state.selectedFilters};
        if(!temp[key]){
            temp[key] = {};
        }
        if(temp[key][id]){
            delete temp[key][id];
        }
        else{
            temp[key][id] = {name: name};
        }

        this.setState({selectedFilters: temp},
            () => {
                this.setState({currentPage: 1},
                    () => {
                        this.getProductList(this.state.object_id);
                    }
                );
            }
        );
    }

    removeFilter(parentKey, childKey){
        let allFilters= this.state.selectedFilterIndexes;
        if(allFilters.length>=1){
            let item = allFilters.pop(); 
            this.closeModals(item);
        }
        
        this.setState({selectedFilterIndexes:allFilters});
        let temp = this.state.selectedFilters;
        delete temp[parentKey][childKey];
        if(Object.keys(temp[parentKey]).length===0)
        {
            delete temp[parentKey];
        }
        this.setState({selectedFilters: temp},
            () => {
                this.setState({currentPage: 1},
                    () => {
                        this.getProductList(this.state.object_id);
                    }
                );
            }
        );
    }

    goToProductPage(seoName){
        // this.props.history.push("/pd/"+prodId);
        window.open(siteUrl+"/pd/"+seoName+".html");

    }

    manageAccordian(index){
        if(index!==this.state.currentSelectedFilter){
            this.setState({currentSelectedFilter:index});
            let selectedFilters=this.state.selectedFilterIndexes;
            selectedFilters=[...selectedFilters,index];
            this.setState({selectedFilterIndexes:selectedFilters});
        }
        for(let i = 0; i < Object.keys(this.state.productList.data.response.filters).length; i++){
            let elem = document.getElementById("brand_heading"+i);
            if(elem){
                let minus = elem.getElementsByClassName("custom_minus")[0];
                let plus = elem.getElementsByClassName("custom_plus")[0];
                if(i == index){
                    if(minus.style.display == "none"){
                        minus.style.display = "inline";
                        plus.style.display = "none";
                    }
                    else{
                        minus.style.display = "none";
                        plus.style.display = "inline";
                    }
                }
                else{
                    minus.style.display = "none";
                    plus.style.display = "inline";
                }
            }
        }
    }

    handleChange = name => event => {
        this.setState({ [name]: event.target.checked });
    };

    toggleStoreFilter(){
        this.setState({storeFilterToggle: !this.state.storeFilterToggle},
            () => {
                if(this.state.storeFilterToggle){
                    document.getElementById("open-store").click();
                }
                else{
                    this.setState({selectedStore: {}});
                }
            });
    }

    selectCity(cityName){
        this.setState({selectedCity: cityName});
    }

    getStores(){
        this.props.dispatch(StoreAction(this.state.selectedCity));
    }

    selectStore(e){
        let temp = {
            id: e.target.value,
            name: ""
        };

        this.state.storeList.data.map(store => {
            if(store.id == e.target.value){
                temp.name = store.address_line1
            }
        });

        this.setState({selectedStore: temp});
    }

    applyStoreFilter(){
        document.getElementById("store-close").click();
    }

    addToWishlist(prodId){
        let data = {};
        data[prodId] = {
            product_id: prodId,
            amount: 1,
            cart_converted_id: 13,
            language: this.state.language
        };
        if(getter('userId')){  
            this.props.dispatch(addToWishlistAction(data));
        }
        else{
            setter("wishListItem",JSON.stringify(data));
            this.props.dispatch(showLogin());
        }
    }

    updateFilterSearchVal(value, filterKey){
        let prodList = {...this.state.productList.data};
        prodList.response.filters[filterKey][0]["searchValue"] = value;
        this.setState({productList: prodList});
    }

    resetFilters(){
        this.setState({selectedFilters: {}},
            () => {this.getProductList(this.state.object_id);}
        );
        if(this.state.currentSelectedFilter!==-1){
            this.closeModals(this.state.currentSelectedFilter);
        }
    }

    closeModals(index){
        let elem = document.getElementById("brand_heading"+index);
        if(elem){ 
            let minus = elem.getElementsByClassName("custom_minus")[0];
            let plus = elem.getElementsByClassName("custom_plus")[0];
            document.getElementById("filter_collapse"+index).setAttribute("class","collapse");
            minus.style.display="none";
            plus.style.display="inline";
        }
    }

    selectProductSize(prodIndex, sizeOptionIndex){
        let productList = {...this.state.productList};
        productList.data.response.products[prodIndex]["selectedSizeIndex"] = sizeOptionIndex;
        this.setState({productList: productList});
    }

    addProductToCart(prodIndex){
        let error = false;
        let message = "";

        let selectedSizeOption = {};
        let requiredProduct = this.state.productList.data.response.products[prodIndex];

        if(requiredProduct.tracking == "O"){
            if(typeof requiredProduct.selectedSizeIndex === "undefined"){
                error = true;
                message = local.wishliSelectSize[this.state.language];
            }
            else{
                let selectedSizeIndex = requiredProduct.selectedSizeIndex;
                let selectedSize = requiredProduct["option_size"][selectedSizeIndex];
                requiredProduct["o_size"].map(size => {
                    let temp = size.split("_");
                    let option = temp[0].split("-");
                    if(temp[1] == selectedSize){
                        selectedSizeOption[option[0]] = option[1];
                        // break;
                    }
                });
            }
        }

        if(!error){
            let obj = Object.assign({}, selectedSizeOption);
            let data = {};
            let temp = {
                "product_id": requiredProduct.product_id,
                "amount": 1,
                "product_options": obj
            }
            data[requiredProduct.product_id] = temp;
            console.log(data);
            this.props.dispatch(addtocartAction(data));
        }
        else{
            notify.show(message,"error");
        }
    }

    render() {

        if(this.state.productList){
            return (
                <div>
                    <ListContainer 
                        data = {this.state.productList.data} 
                        loadMore = {() => this.loadMoreProducts()} 
                        currentPage = {this.state.currentPage} 
                        totalPages = {this.state.totalPages} 
                        setSortBy = {(type, dir) => this.setSortBy(type, dir)} 
                        selectFilter = {(key, id, name) => this.selectFilter(key, id, name)} 
                        selectedFilters = {this.state.selectedFilters} 
                        removeFilter = {(parentKey, childKey) => this.removeFilter(parentKey, childKey)} 
                        activeSort = {this.state.activeSort} 
                        goToProductPage = {(prodId) => this.goToProductPage(prodId)} 
                        manageAccordian={(index) => this.manageAccordian(index)} 
                        storeFilterToggle={this.state.storeFilterToggle} 
                        toggleStoreFilter={() => this.toggleStoreFilter()} 
                        cityData={this.state.cityData} 
                        selectedCity={this.state.selectedCity} 
                        selectCity={(cityName) => this.selectCity(cityName)} 
                        getStores={() => this.getStores()} 
                        storeList={this.state.storeList} 
                        selectedStore={this.state.selectedStore} 
                        selectStore={(e) => this.selectStore(e)} 
                        applyStoreFilter={() => this.applyStoreFilter()} 
                        addToWishlist={(prodId) => this.addToWishlist(prodId)} 
                        updateFilterSearchVal={(value, filterKey) => this.updateFilterSearchVal(value, filterKey)} 
                        resetFilters={() => this.resetFilters()}
                        language={this.state.language} 
                        selectProductSize={(prodIndex, sizeOptionIndex) => this.selectProductSize(prodIndex, sizeOptionIndex)} 
                        addProductToCart={(prodIndex) => this.addProductToCart(prodIndex)}
                        currentSelectedFilter={this.state.currentSelectedFilter}
                    />
                    {this.state.showLoading && <Loader />}
                </div>
            );
        }
        else{
            return (
                <Loader />
            );
        }
    }
}

function mapStateToProps(state) {
    return {
        productList: state.productListReducer,
        cityData: state.cityReducer ,
        storeList: state.storeReducer,
        languageReducer: state.languageReducer,
        cartData: state.cartReducer,
        productIdSeoName: state.productIdReducer
    }
}

export default connect(mapStateToProps)(withStyles(styles)(ProductListing));
