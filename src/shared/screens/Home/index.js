import React, { Component } from 'react';
import {connect} from 'react-redux';

// import "react-responsive-carousel/lib/styles/carousel.min.css";
import { Carousel } from 'react-responsive-carousel';

import Loader from "../../components/loader";

import { homeAction } from '../../actions/home';

import {getter,languageForAPI, calculateDiscount} from '../../commonfunction';
import local, {deafultLanguage} from '../../localization';
import {GA_ID} from '../../constant';
import ReactGA from 'react-ga';

const Section1 = (props) => (
    <div className="section header_banner" id="top-banner" style={{position: "relative"}}>
        <Carousel showStatus={false} showIndicators={false} showThumbs={false} showArrows={true} infiniteLoop={true} autoPlay={true} onChange={(index) => props.onTopBannnerChange(index)} selectedItem={props.topBannerShownSlide}>
            {
                props.data.map(slideData => (
                    <div className="carouseldata-cont" onClick={() => props.viewDetails(slideData.object_type, slideData.seo_name,slideData.link_url)}>
                        <img src={slideData.banner_url} />
                        <div className="text-cont">
                            <div className="title">
                                {slideData.title}
                            </div>
                            {/*<div className="info">  
                                The FW18/19 collections 
                            </div>*/}
                            {/*<a href="JavaScript:void(0);" onClick={() => props.viewDetails(slideData.object_type, slideData.object_id)} className="view">{local.homeViewCollection[props.language]}</a>*/}
                        </div>
                        
                    </div>
                ))
            }
        </Carousel>
        <div>
            {
                props.data.map((slideData, index) => (
                    index == props.topBannerShownSlide && <div>
                        {
                            slideData.new_banners && slideData.new_banners.map((data) => { 
                               return <div className={props.getPlusArrorPos(data.custom_two.split(",")[1]) + " plus-cont"} style={{top: data.custom_two.split(",")[0]+"%", left: data.custom_two.split(",")[1]+"%"}}>
                                    <div className="plus-data-cont">
                                        <svg className="icon icon-plus">
                                        <use xlinkHref="img/icon-sprite.svg#icon-plus"></use></svg>
                                        <div className="data-cont text-left" style={{top: "136px", right: props.getPlusLeftPos(data.custom_two.split(",")[1])}}>
                                            <div className="data-cont-inner" >
                                                <div className={props.getPlusArrorPos(data.custom_two.split(",")[1]) == "left" ? "inner left" : "inner right"}>
                                                    <div className="bannerimg_outer"><a href="JavaScript:void(0);" onClick={() => props.viewDetails(data.object_type, data.seo_name,data.link_url)} ><img src={data.product_info.image_url} /></a></div>
                                                    <div className="bannercontn_outer">
                                                        <div className="prod-name"><a href="JavaScript:void(0);" onClick={() => props.viewDetails(data.object_type, data.seo_name,data.link_url)} >{data.product_info.name}</a></div>
                                                        <div className="price-cont">
                                                            <span className="brand">{data.product_info.brand}</span>
                                                            {data.product_info.brand && <span className="saperator">|</span>}
                                                            
                                                            <span className="price">{data.product_info.price} {local.homeSAR[props.language]}</span>
                                                        </div>
                                                        <div className="btn-cont">
                                                            <div className="btn" onClick={() => props.viewDetails(data.object_type, data.seo_name,data.link_url)}>{local.homeView[props.language]}</div>
                                                            {/*<div className="btn">Buy</div>*/}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            
                        } ) }
                    </div>
                ))
            }
        </div>
    </div>
);

const Section2 = (props) => (
    <div className="section collection_block row mx-0 text-center">
        {
            props.data.map(data => (
                <div className="img-cont col-sm-12 col-md-4 px-0" onClick={() => props.viewDetails(data.object_type, data.seo_name,data.link_url)}>
                    <img src={data.banner_url} />
                    <div className="info">
                        <div className="title">
                            {data.title}
                        </div>
                        {/*<a href="JavaScript:void(0);" className="view-details" onClick={() => props.viewDetails(data.object_type, data.seo_name,data.link_url)}>{local.homeSeeCollection[props.language]}</a>*/}
                    </div>
                </div>
            ))
        }
    </div>
);

const Section3 = (props) => (
    <div className="section just_arrived">
        <div className="img-cont" onClick={() => props.viewDetails(props.data[0].object_type, props.data[0].seo_name,props.data[0].link_url)}>
            <img src={props.data[0].banner_url} />
            <div className="info">
                <div className="title text-left">{props.data[0].title}</div>
                { props.data[0].extra_text && <div className="des">{props.data[0].extra_text}</div> }
                {/*<a href="JavaScript:void(0);" className="view-details" onClick={() => props.viewDetails(props.data[0].object_type, props.data[0].object_id)}>{local.homeCheckAll[props.language]}</a>*/}
            </div>
        </div>
        <div className="sub-cont">
            <div id="just_arrived_loop" class="just_arrived_loop owl-carousel owl-theme">
                {
                    props.data.slice(1, props.data.length).map((item, index) => (
                        <div class="item">
                            <img src={item.banner_url} />
                            <div className="hover_srch">
                                <a className={"hover_icon" + index} href="JavaScript:void(0);"><svg class="icon icon-hover-view"><use xlinkHref="/img/icon-sprite.svg#icon-hover-view"></use></svg></a>
                            </div>
                        </div>
                    ))
                }
            </div>
        </div>

    </div>
);

const Section4 = (props) => (
    <div className="section look_book_sec" onClick={() => props.viewDetails(props.data[0].object_type, props.data[0].seo_name,props.data[0].link_url)}>
        <img className="img-fluid" src={props.data[0].banner_url} />
        <div className="text-cont">
            <div className="title" style={{color: "#5b6269"}}>
                {props.data[0].title}
            </div>
            {props.data[0].extra_text && <div className="info">{props.data[0].extra_text}</div>}
            {/*<a href="Javascript:void(0);" className="view" onClick={() => props.viewDetails(props.data[0].object_type, props.data[0].object_id)} style={{color: "#000000", fontWeight: "bold"}}>{local.homeViewCollection[props.language]}</a>*/}
        </div>
    </div>
);

const Section5 = (props) => (
    <div className="section brand_sec row mx-0">
        <div className="brand_sec_loop owl-carousel owl-theme">
            {
                props.data.map(item => (
                    <div className="img-cont col-sm-12 col-md-12 px-0" onClick={() => props.viewDetails(item.object_type, item.seo_name,item.link_url)}>
                        <img className="sec-img" src={item.banner_url} />
                        <div className="info">
                            { item.brand_image && <img src={item.brand_image} /> }
                            <div className="title">{item.title}</div>
                            {item.extra_text && <div className="des">{item.extra_text}</div> }
                            {/*<a href="Javascript:void(0);" className="view" onClick={() => props.viewDetails(item.object_type, item.object_id)}>{local.homeViewCollection[props.language]}</a>*/}
                        </div>
                    </div>
                ))
            }
         </div>
    </div>
);

const Section6 = (props) => (
    <div className="section shop_here_Sec my-0" onClick={() => props.viewDetails(props.data[0].object_type, props.data[0].object_id,props.data[0].link_url)}>
        <img src={props.data[0].banner_url} />
        <div className="info">
            <div className="title">{props.data[0].title}</div>
            <div className="des">{props.data[0].extra_text}</div>
            {/*<a href="Javascript:void(0);" className="view common_tooltip_outer" onClick={() => props.viewDetails(props.data[0].object_type, props.data[0].object_id)}>{local.homeFindOutMore[props.language]} <span className="common_tooltip down">{local.orderComingSoon[props.language]}</span></a>*/}
        </div>
    </div>
);

const Section7 = (props) => (
    <div className="section trending_near">
        <div className="data-cont">
            <div className="title">{local.homeTrendingNearYou[props.language]}</div>
            <div className="img-cont">
                <div className="trending_near_loop owl-carousel owl-theme">
                    {
                        props.data.map(item => (
                            <div className="item">
                                <div className="img_outer">
                                    <img src={item.banner_url} onClick={() => props.viewDetails(item.object_type, item.seo_name,item.link_url)}/>
                                    {/*<div className="size-cont">
                                        <div className="sizes">
                                            <a href="#">S</a>
                                            <a href="#">M</a>
                                            <a href="#">L</a>
                                            <a href="#">XL</a>
                                        </div>
                                        <div className="icons">
                                            <a href="#"><svg className="icon icon-favorite-heart-button"><use xlinkHref="/img/icon-sprite.svg#icon-favorite-heart-button"></use></svg></a>
                                            <a href="#"><svg className="icon icon-Combined-Shape"><use xlinkHref="/img/icon-sprite.svg#icon-Combined-Shape"></use></svg></a>*/}
                                            {/*<a href="#"><svg className="icon icon-overlay-view"><use xlinkHref="/img/icon-sprite.svg#icon-overlay-view"></use></svg></a>*/}
                                        {/*</div>
                                    </div>*/}
                                </div>
                                <div className="img-info">
                                    <div className="prod-name"><a href="JavaScript:void(0);" onClick={() => props.viewDetails(item.object_type, item.seo_name,item.link_url)}>{item.product_name}</a></div>
                                    <div className="price-cont">
                                        <span className="brand">{item.company}</span>
                                    </div>
                                    <div className="price-cont">
                                         <span className="price">{item.product_price} {local.homeSAR[props.language]}</span>
                                         {item.product_list_price > item.product_price && <span className="strike">{item.product_list_price} {local.homeSAR[props.language]}</span>}
                                        {item.product_list_price > item.product_price && <span className="offer">{calculateDiscount(item.product_price,item.product_list_price)}% {local.off[props.language]}</span>}
                                    </div>
                                </div>
                            </div>
                        ))
                    }
                </div>
            </div>
        </div>
    </div>
);

const Section8 = (props) => (
    <div className="section follow_insta">
        <div className="title">{local.homeInstagram[props.language]}</div>
        <div className="img-cont">
            <div className="follow_insta_loop owl-carousel owl-theme">
                {
                    props.data.map(item => (
                        <div className="item">
                            <a href="JavaScript:void(0);" onClick={() => props.viewDetails(item.object_type, item.object_id,item.link_url)}><img src={item.banner_url} /></a>
                        </div>
                    ))
                }
            </div>
        </div>
    </div>
);

const RenderSection = (props) => {
    let data = props.data;
    return(
        data.length && data[0].section == 1 ? (
                <Section1 data={data} viewDetails={props.viewDetails} getPlusLeftPos={props.getPlusLeftPos} getPlusArrorPos={props.getPlusArrorPos} onTopBannnerChange={props.onTopBannnerChange} topBannerShownSlide={props.topBannerShownSlide} language = {props.language}/>) : (
                    data[0].section == 2 ? (<Section2 data={data} viewDetails={props.viewDetails} language = {props.language}/>) : (
                        data[0].section == 3 ? (<Section3 data={data} viewDetails={props.viewDetails} language = {props.language}/>) : (
                            data[0].section == 4 ? (<Section4 data={data} viewDetails={props.viewDetails} language = {props.language} />) : (
                                data[0].section == 5 ? (<Section5 data={data} viewDetails={props.viewDetails}  language = {props.language}/>) : (
                                    data[0].section == 6 ? (<Section6 data={data} viewDetails={props.viewDetails} language = {props.language}/>) : (
                                        data[0].section == 7 ? (<Section7 data={data} viewDetails={props.viewDetails} language = {props.language} />) : (
                                            data[0].section == 8 ? (<Section8 data={data} viewDetails={props.viewDetails} language = {props.language} />) : "")))))))

    )
}


class Home extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            topBannerWidth: 1000,
            sectionData: {},
            topBannerShownSlide: 0,
            language: '',
            showLoader: false
        }
    }
	
    static initialAction({url, store,lang}) {
        store.dispatch(homeAction(lang));
    }

    componentDidMount(){
        this.setState({
            language: getter('language') ? getter('language') : deafultLanguage
        })

        let elm = document.getElementsByClassName("header_banner");
        if(elm && elm.length){
            let bannerWidth = document.getElementsByClassName("header_banner")[0].offsetWidth;
            this.setState({topBannerWidth: bannerWidth});
        }

        if(!this.props.sectionData.Section || !this.props.sectionData.Section.length){
            console.log("909090909090909");
             this.props.dispatch(homeAction(languageForAPI()));
        }
        else{
            this.setState({sectionData: this.props.sectionData}, () => {
                this.setHomeBlocksDisplay();
                this.setOwlCarouselClick();
            });

        }

        this.setTopBannerArrowPosition();
        
        ReactGA.pageview('homepage');
        
    }

    componentWillReceiveProps(nextProps){
        if(nextProps.languageReducer.value){
            this.setState({
                language: nextProps.languageReducer.value
            }, () => {
                $('.just_arrived_loop').owlCarousel('destroy');
                $('.trending_near_loop').owlCarousel('destroy');
                $('.brand_sec_loop').owlCarousel('destroy');
                $('.follow_insta_loop').owlCarousel('destroy');
                this.setHomeBlocksDisplay();
            });  
        }

        if(nextProps.sectionData != this.props.sectionData){
            this.setState({showLoader: nextProps.sectionData.isFetching});
            this.setState({sectionData: nextProps.sectionData}, () => {
                this.setHomeBlocksDisplay();
                this.setOwlCarouselClick();

            });
        }
        
        
    }

    setTopBannerArrowPosition(){
        this.setPosition();
        document.getElementsByTagName('body')[0].onscroll = () => {
            this.setPosition();
        };
    }

    setPosition(){
        let topBanner = document.getElementById("top-banner");
        if(topBanner){
            let domRect = topBanner.getBoundingClientRect().bottom + 28;
            let next = topBanner.getElementsByClassName("control-next");
            let prev = topBanner.getElementsByClassName("control-prev");
            if((domRect - window.innerHeight) > 0){
                next[0].style.position = "fixed";
                prev[0].style.position = "fixed";
            }
            else{
                next[0].style.position = "absolute";
                prev[0].style.position = "absolute";
            }
        }
    }

    setHomeBlocksDisplay(){console.log("homeblock");
        let homeComponent = this;
        console.log(homeComponent.state.language);

        $(document).ready(function(){
            $('.just_arrived_loop').owlCarousel({
                center: true,
                loop:true,
                margin:20,
                dots:false,
                rtl: homeComponent.state.language == "sar"?true:false,
                responsive:{
                    0:{
                        items:2
                    },
                    768:{
                        items:2,
                        mouseDrag : false,
                        center:false
                    },
                    1025:{
                        items:3,
                        mouseDrag : false,
                    }
                }  
            });

            $('.trending_near_loop').owlCarousel({
                center: false,
                loop:true,
                margin:40,
                dots:false,
                nav: true,
                rtl: homeComponent.state.language == "sar"?true:false,
                responsive:{
                    0:{
                        items:2
                    },
                    768:{
                        items:3,
                        mouseDrag : false,

                    },
                    992:{
                        mouseDrag : false,
                        items:4
                    }
                }
            });

            $('.brand_sec_loop').owlCarousel({
                center: false,
                loop:true,
                margin:0, 
                nav: true,
                dots: false,
                autoplay: true,
                autoplayTimeout:3000,
                rtl: homeComponent.state.language == "sar"?true:false,
                responsive:{
                    0:{
                        items:2
                    },
                    768:{
                        items:2
                    },
                    993:{
                        items:2
                    }
                } 
            });

            $('.follow_insta_loop').owlCarousel({
                center: false,
                loop:true,
                margin:0,
                nav: true,
                dots: false,
                autoplay: true,
                rtl: homeComponent.state.language == "sar"?true:false,
                responsive:{
                    0:{
                        items:2
                    },
                    768:{
                        items:4
                    },
                    993:{
                        items:6
                    }
                } 
            });
        });
    }

    setOwlCarouselClick(){
        let homePageRef = this;

        $('#just_arrived_loop').on('click', '.item a', function () {
           let className = $(this).attr('class');
           let ind = parseInt(className.split("hover_icon")[1]);
           homePageRef.state.sectionData.Section.map(section => {
                if(section[0].section == 3){
                   homePageRef.viewDetails(section[ind+1].object_type, section[ind+1].seo_name,section[ind+1].link_url);
                }
           });
        });
    }

    viewDetails(objectType, objectId,link_url=null){
        if(objectType == "C"){
            this.props.history.push("/ct/"+objectId+".html");
        }
        else if(objectType == "P"){
            this.props.history.push("/pd/"+objectId+".html");
        }
        else if(objectType == "I"){
            if(link_url!==null && link_url!==undefined)
            window.location.href=link_url;
        }
    }

    getPlusLeftPos(left){
        let contWidth = this.state.topBannerWidth;
        let per = (contWidth*parseInt(left))/100;
        if(per+(246*2) < contWidth){
            return -246+"px";
        }
        else{
            return 26+"px";
        }
    }

    getPlusArrorPos(left){
        let contWidth = this.state.topBannerWidth;
        let per = (contWidth*parseInt(left))/100;
        if(per+(246*2) < contWidth){
            return "left";
        }
        else{
            return "rigth";
        }
    }

    onTopBannnerChange(index){
        this.setState({topBannerShownSlide: index});
    }

	render() {
        if(this.state.sectionData.Section){
        	return (
        		<div className="home-page">
                {
                    this.state.showLoader && <Loader />
                }
                    {
                        this.state.sectionData.Section.map(section => (
                            <RenderSection 
                                data={section} 
                                viewDetails={(objectType, objectId,link_url) => this.viewDetails(objectType, objectId,link_url)} 
                                getPlusLeftPos={(left) => this.getPlusLeftPos(left)} 
                                getPlusArrorPos={(left) => this.getPlusArrorPos(left)} 
                                onTopBannnerChange={(index) => this.onTopBannnerChange(index)} 
                                topBannerShownSlide={this.state.topBannerShownSlide} 
                                language = {this.state.language}
                            />

                        ))
                    }
        		</div>
        	);
        }
        else{
            return (
                <Loader />
            );
        }
    }
}

function mapStateToProps(state) {
    return {
        sectionData: state.homeReducer,
        languageReducer: state.languageReducer
    }
}

export default connect(mapStateToProps)(Home);
