import React, { Component } from 'react';
import {connect} from 'react-redux';
import { getcartAction } from '../../actions/cart';

// import "react-responsive-carousel/lib/styles/carousel.min.css";
import local, {deafultLanguage} from '../../localization';
import {getter} from '../../commonfunction';
import ReactGA from 'react-ga'

class OrderFail extends Component {
  
    static initialAction({url, store}) {
       // store.dispatch(productListAction());
       return true;
    }
    constructor(props){
        super(props)
        this.state = {
            orderId : null,
            language: ''
        }
    }
    
    componentDidMount(){
        ReactGA.pageview('orderFailPage');
        this.setState({
            language: getter('language') ? getter('language') : deafultLanguage
        })

       // this.props.dispatch(getcartAction())
        console.log(this.props);
        const orderId = Object.keys(this.props.placeOrderReducer.data).length?this.props.placeOrderReducer.data.response.order_id.order_id:null
        this.setState({
            orderId : orderId
        })
        if(this.props.match.params && this.props.match.params.pathParam2){
            this.setState({
                orderId : this.props.match.params.pathParam2
            })
        }
        window.scroll(0,0); 
    }
    
    componentWillReceiveProps(nextProps){
        if(nextProps.languageReducer.value){
            this.setState({
                    language: nextProps.languageReducer.value
            })  
        }
    }

    continueshopping(){
        this.props.history.push('/');
    }
   
    render() {
        return (
            <div className="thankyou_outer text-center">
               <h1>{local.orderFail[this.state.language]}</h1>
               {/*<p>{local.failedOrder[this.state.language]}{this.state.orderId}</p>*/}
                <button className=" continue btn" onClick= {this.continueshopping.bind(this)}>{local.cartContinueShopping[this.state.language]}</button>
            </div>
        );
    }
}

function mapStateToProps(state) {
   
    return {
        placeOrderReducer : state.placeOrderReducer,
        languageReducer: state.languageReducer
    }
}

export default connect(mapStateToProps)(OrderFail);
