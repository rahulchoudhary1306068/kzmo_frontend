import React, { Component } from 'react';
import {connect} from 'react-redux';

import {addtocartAction,deleteproductfromcartAction,cartcouponcodeAction} from '../../actions/cart'
import {notify} from 'react-notify-toast';

import local,{deafultLanguage} from '../../localization';
import { InitailStateAction } from '../../actions/login';
import { getter } from '../../commonfunction';
import { UpdateLanguage } from '../../actions/header';
import Loader from "../../components/loader";
import ReactGA from 'react-ga'

class ShopingCart extends Component {
    static initialAction({url, store}) {
       return true;
    }
    constructor(props){
        super(props)
        this.state = {
            cartData:false,
            couponCode: "",
            placeOrderButtonDisable : "",
            couponCodeError : "",
            language:''
        }

    }
    manageQuantity(index,type,value,subindex){
        let productId = this.state.cartData.response.products[index].product_id;      
       let count = 0;
       
       if(type==1){ //increment
        count = parseInt(value)+1;
        }
        else{ // decrement
            if(parseInt(value) > 1){
                count = parseInt(value)-1;
            }
            else{
                notify.show(local.minCartQty[this.state.language], 'error');
                return;
            }
        }
        if(!count){ // remove product when count is zero
            let itemId = this.state.cartData.response.products[index].variant ? this.state.cartData.response.products[index].variant[subindex].item_id : "";
            var data =   {"product_id":productId,"item_id":itemId}
                 
            this.props.dispatch(deleteproductfromcartAction(data))
            return;
        }
        let productOption=this.state.cartData.response.products[index].variant? this.state.cartData.response.products[index].variant[subindex].details.selected_options_id : {};
        
        
        var data = {productId:{"product_id":productId,"amount":count,"product_options":productOption,"upsert":1}}
        this.props.dispatch(addtocartAction(data))
    }
    removeProduct(index,subindex){
        let productId = this.state.cartData.response.products[index].product_id;
        let itemId = this.state.cartData.response.products[index].variant ? this.state.cartData.response.products[index].variant[subindex].item_id : "";
        
        var data =   {"product_id":productId,"item_id":itemId}
             
        this.props.dispatch(deleteproductfromcartAction(data))
        
        
    }

    editProduct(index){
        this.props.history.push("/pd/"+this.state.cartData.response.products[index].seo_name+".html");
    }

    goToProduct(index){
        this.props.history.push("/pd/"+this.state.cartData.response.products[index].product_id);
    }

    continueShopping(){
        this.props.history.push("/");
    }

    placeOrder(){
        // if(Object.keys(this.state.cartData.response).length && this.state.cartData.response.total >=10000 ){
        //     notify.show(local.cartCashOnDelivery[props.language],'error');
        //     return;
        // }
        if(Object.keys(this.state.cartData.response).length){
            this.props.history.push("/checkout");
        }
            
        else
        {
            notify.show(local.cartAddSomething[this.state.language],'error');
        }

    }

    updateCouponCodevalue(e){
        this.setState({
            [e.target.name]: [e.target.value]
        })
    }
    enterCouponCode(){
        
        if(this.state.couponCode){
            this.props.dispatch(cartcouponcodeAction(this.state.couponCode))
        }
    }
    componentDidMount(){
        ReactGA.pageview('shoppingcart');
        this.setState({
            language: getter('language')?getter('language'):deafultLanguage
        })
       
        if(this.props.cartReducer && Object.keys(this.props.cartReducer.cart).length){
            this.setState({
                cartData: this.props.cartReducer.cart
            })
            // if(this.props.cartReducer.cart.response.total >=10000){
            //     this.setState({
            //         placeOrderButtonDisable : "btn_disable"
            //     })
            // }
            // else{
            //     this.setState({
            //         placeOrderButtonDisable : "false"
            //     })
            // }
        }
       window.scrollTo(0,0);
        
    }
    componentWillReceiveProps(nextProps){
        
        if(nextProps.languageReducer.value){
            this.setState({
                language: nextProps.languageReducer.value
            })  
            
        }
              
        if(nextProps.cartReducer.cart!==this.props.cartReducer.cart){
              this.setState({
                cartData: nextProps.cartReducer.cart,
                
                
            })
            if(nextProps.cartReducer.cart.response.total_discount>0){
                this.setState({
                    couponCodeError : local.cartShoppingCouponCodeMessage[this.state.language]
                })
            }
            else{
                this.setState({
                    couponCodeError : ""
                })
            }
            if(nextProps.cartReducer.cart.response.messages){
                this.setState({
                    couponCodeError : nextProps.cartReducer.cart.response.messages.E[0]
                })
            }
            
            // if(nextProps.cartReducer.cart.response.total >=10000){
            //     this.setState({
            //         placeOrderButtonDisable : "btn_disable"
            //     })
            // }
            else{
                this.setState({
                    placeOrderButtonDisable : "false"
                })
            }
            
        }
    }  
     
    render() {
        if(this.state.cartData){
            return (
                <div className="shopingcart_outer">
                  
                    <h1 className="category_name text-left">{local.cartShoppingCart[this.state.language]}</h1>
                    <div className="row_outer">
                        <div className="left_outer align-top">
                            <ul className="listing_outer">
                                <li className="list_header d-none d-md-block">
                                    <div className="row">
                                        <div className="col-sm-6">{local.cartItem[this.state.language]}</div>
                                        <div className="col-sm-4 text-center">{local.cartQuantity[this.state.language]}</div>
                                        <div className="col-sm-2 text-right">{local.cartSubTotal[this.state.language]}</div>
                                    </div>
                                </li>
                                
                                {Object.keys(this.state.cartData).length >0
                                 ? Object.keys(this.state.cartData.response).length == 0? local.cartNoItemFound[this.state.language]:this.state.cartData.response.products.map((value,index)=>{
                                 if(!value.variant){ // for non varient product
                                      return  <li className="listing">
                                     <div className="row align-items-center">
                                         <div className="details col-md-6 col-sm-12">
                                             <img src={value.icon_images.length>0 ?value.icon_images[0].icon_image_path:""} onClick = {this.editProduct.bind(this,index)} />
                                             <div className="inner">
                                                 <p className="name"><a href="JavaScript:void(0);" onClick = {this.editProduct.bind(this,index)}>{value.name}</a></p>
                                                 {value.variant && Object.keys(value).length && Object.keys(value.details.selected_options).map((data,index)=>{
                                                     return <p className="size_name">{data} : {value.details.selected_options[data]}</p>
                                                 })}
                                                 
                                                 <p className="link_outer">
                                                     {/* <a className="edit_product" href="JavaScript:void(0);" onClick = {this.editProduct.bind(this,index)}>Edit</a> */}
                                                     <a className="remove_product" href="JavaScript:void(0);" onClick={this.removeProduct.bind(this,index)}>{local.cartRemove[this.state.language]}</a>
                                                 </p>
                                             </div>
                                         </div>
                                         <div className="col col-md-4 col-sm-6 quantity">
                                             <div className="control">
                                                 <button onClick={this.manageQuantity.bind(this,index,2,value.quantity)} className="cart-qty-minus btn" type="button" value="-">-</button>
                                                 <input type="text" name={"qty"+index} id={"qty"+index} value={value.quantity} title="Qty" className="input-text qty" />
                                                 <button onClick={this.manageQuantity.bind(this,index,1,value.quantity)} className="cart-qty-plus btn" type="button" value="+">+</button>
                                             </div>
                                         </div>
                                         <div className="col col-md-2 col-sm-6 price text-right"><span>{local.wishliSar[this.state.language]}{value.product_pre_price_total}</span></div>
                                     </div>
                                 </li>
                                 }

                                    return  value.variant.map((v,subindex)=>{
                                        
                                        return  <li className="listing">
                                        <div className="row align-items-center">
                                            <div className="details col-md-6 col-sm-12">
                                                <img src={value.icon_images.length>0 ?value.icon_images[0].icon_image_path:""} onClick = {this.editProduct.bind(this,index)} />
                                                <div className="inner">
                                                    <p className="name"><a href="JavaScript:void(0);" onClick = {this.editProduct.bind(this,index)}>{value.name}</a></p>
                                                    {value.variant && Object.keys(v).length && Object.keys(v.details.selected_options).map((data,index)=>{
                                                        return <p className="size_name">{data} : {v.details.selected_options[data]}</p>
                                                    })}
                                                    
                                                    <p className="link_outer">
                                                        {/* <a className="edit_product" href="JavaScript:void(0);" onClick = {this.editProduct.bind(this,index)}>Edit</a> */}
                                                        <a className="remove_product" href="JavaScript:void(0);" onClick={this.removeProduct.bind(this,index,subindex)}>{local.cartRemove[this.state.language]}</a>
                                                    </p>
                                                </div>
                                            </div>
                                            <div className="col col-md-4 col-sm-6 quantity">
                                                <div className="control">
                                                    <button onClick={this.manageQuantity.bind(this,index,2,v.quantity,subindex)} className="cart-qty-minus btn" type="button" value="-">-</button>
                                                    <input type="text" name={"qty"+index} id={"qty"+index} value={v.quantity} title="Qty" className="input-text qty" />
                                                    <button onClick={this.manageQuantity.bind(this,index,1,v.quantity,subindex)} className="cart-qty-plus btn" type="button" value="+">+</button>
                                                </div>
                                            </div>
                                            <div className="col col-md-2 col-sm-6 price text-right"><span>{local.wishliSar[this.state.language]}{v.product_pre_price_total}</span></div>
                                        </div>
                                    </li>
                                    })
                                    
                                }):local.cartNoItemFound[this.state.language]}
                               
                                </ul>
                        </div>
                        <div className="right_outer align-top">
                            <div className="right_sec">
                                <span>
                                    <input onKeyPress = {this.enterCouponCode.bind(this)} onChange={e=>this.updateCouponCodevalue(e)} name="couponCode" className="code_input" value= {this.couponCode} type="text" placeholder={local.cartEnterCouponCode[this.state.language]} />
                                    <button onClick={this.enterCouponCode.bind(this)} className="btn" title="" type="button"> {local.applyButton[this.state.language]}
                                        {/* <svg className="icon icon-right-arrow"><use xlinkHref="/img/icon-sprite.svg#icon-right-arrow"></use></svg> */}
                                    </button>
                                    <p>{this.state.couponCodeError}</p>
                                </span>
                                <div className="row sub_totl">

                                    <p className="col col-sm-8">{local.cartSubTotal[this.state.language]}: </p>
                                    <p className="col text-right">{Object.keys(this.state.cartData).length?Object.keys(this.state.cartData.response).length>0?this.state.cartData.response.subtotal:"":""}</p>

                                </div>
                                {Object.keys(this.state.cartData).length?Object.keys(this.state.cartData.response).length? 
                                        <div className="row sub_totl">
                                        <p className="col col-sm-8">{local.cartDiscount[this.state.language]}: </p>
                                        <p className="col text-right">{this.state.cartData.response.total_discount}</p>
                                    </div>
                               :"":"" }
                                
                                <div className="row grand_totl">

                                    <p className="col col-sm-8">{local.cartGrandTotal[this.state.language]}:</p>
                                    <p className="col text-right">{Object.keys(this.state.cartData).length?Object.keys(this.state.cartData.response).length>0?this.state.cartData.response.total:"":""}</p>

                                </div>
                                <div className="row sub_totl">
                                        <p className="col vatmsg col-sm-8">{local.VatMessage[this.state.language]}</p>
                                        
                                    </div>
                                <button onClick={this.placeOrder.bind(this)} className={"place_order w-100 btn"+" "+ this.state.placeOrderButtonDisable}>{local.proceedToCheckOut[this.state.language]}</button>
                                <p className="accpt">{local.cartWeAccept[this.state.language]}</p>
                                <div className="paymnt_list">
                                    <img src="/img/temp/barbour.png" />
                                    <img src="/img/temp/bitmap1.png" />
                                    <img src="/img/temp/bitmap.png" />
                                </div>
                            </div>
                            <button onClick={this.continueShopping.bind(this)} className="continue w-100 btn">{local.cartContinueShopping[this.state.language]}</button>
                        </div>
                    </div>
                </div>
            );
        }
        else{
            return (
                <Loader/>    
            )
        }
    }
}

function mapStateToProps(state) {
    
    return {
        cartReducer : state.cartReducer,
        languageReducer : state.languageReducer
    }
}

export default connect(mapStateToProps)(ShopingCart);
