import React, { Component } from 'react';
import {connect} from 'react-redux';
import {
  MuiThemeProvider,
  createMuiTheme,
  createGenerateClassName,
} from '@material-ui/core/styles';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Sidebar from '../../components/account_sidebar';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import FormLabel from '@material-ui/core/FormLabel';
import TextField from '@material-ui/core/TextField';
import FormGroup from '@material-ui/core/FormGroup';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControlLabel from '@material-ui/core/FormControlLabel';

// import "react-responsive-carousel/lib/styles/carousel.min.css";
import local, {deafultLanguage} from '../../localization';
import {getter} from '../../commonfunction';


const styles = theme => ({
  root: {
    '&$checked': {
      color: '#e4b122'
    }
  },
  checked: {},
  
});



const mui = createMuiTheme({
  overrides: {
    MuiFormLabel: {
      root: {
        color: "#b1b1b1"
      },
      focused: {
        color: "black !important"
      },
    }
  }
});

class ReturnRequest extends Component {
    constructor(props){
        console.log("======>ReturnRequest<=======");
        console.log(props);
        super(props);
        this.state = {
            language: ''
        };
    }
    static initialAction({url, store}) {
       // store.dispatch(productListAction());
       return true;
    }

   componentDidMount(){
        this.setState({
            language: getter('language') ? getter('language') : deafultLanguage
        })
   }
   componentWillReceiveProps(nextProps){
        if(nextProps.languageReducer.value){
            this.setState({
                    language: nextProps.languageReducer.value
            })  
        }
   }
    render() {
        const { classes } = this.props;
        
        return (
            <div className="dashboard_outer">
                <h1 className="category_name text-left">
                <a href="#"><svg className="icon icon-left-arrow"><use xlinkHref="/img/icon-sprite.svg#icon-left-arrow"></use></svg></a>
                {local.addrMyAccount[this.state.language]}</h1>
                <div className="row">
                    <div className="col-md-3 left_sec">
                      <Sidebar />
                    </div>
                    <div className="col-md-9 right_sec  return_outer order_outer">
                        <h2 className="subheading">Return Request</h2>
                        <p className="subcontent mb-0">Please let us know how you want us to handle the return by selecting the options shown below</p>
                        <ul className="listing_outer">
                            <li className="listing">
                                <div className="details">
                                    <p className="mb-3 ordr_detail">
                                        Order ID: <a href="Javascript:void(0);" >154110268</a>
                                        <span>Order Placed on Fri, Sep 28 2018, 2:59 PM</span>
                                        <span>Delivered on Fri, Oct 11 2018, 5:40 PM</span>
                                    </p>
                                    <img src="/img/perfume.jpg" />
                                    <div className="inner">
                                        <p className="name"><a href="JavaScript:void(0);" >perfume</a></p>
                                        <p className="color_name">Total amount: $45</p>
                                        <p className="size_name">Qty: 1</p>
                                    </div>
                                </div>
                            </li>
                        </ul>
                        <ul className="form_list">
                            <li className="mb-4">
                                <h4><span>1</span> Reason for return:</h4>
                                 <form>
                                    <FormControl className="formControl d-block slect_outer mb-4 mt-4">
                                        <InputLabel shrink htmlFor="city-placeholder">
                                            Reason for return
                                        </InputLabel>
                                        <Select
                                                value="Reason 1"
                                                input={<Input name="select_reason" id="" />}
                                                displayEmpty
                                                name="select_reason"
                                                className="selectEmpty" 
                                            >
                                                <MenuItem value="Reason 1">Wrong product</MenuItem>        
                                                <MenuItem value="Reason 2">Quality issue</MenuItem>        
                                                <MenuItem value="Reason 3">Defective product</MenuItem>        
                                                <MenuItem value="Reason 3">Damaged</MenuItem>        
                                                <MenuItem value="Reason 3">Missing accesssories</MenuItem>        
                                                <MenuItem value="Reason 3">Wrong size</MenuItem>        
                                        </Select>
                                    </FormControl>
                                    <div className="d-flex align-items-baseline custmflex">
                                         <FormControl className="formControl d-block slect_outer mb-4 mt-4">
                                            <InputLabel shrink htmlFor="city-placeholder">
                                                Sub reason
                                            </InputLabel>
                                            <Select
                                                    value="Reason 1"
                                                    input={<Input name="selectsub_reason" id="" />}
                                                    displayEmpty
                                                    name="select_reason"
                                                    className="selectEmpty" 
                                                >
                                                    <MenuItem value="Reason 1">Passed expiry date</MenuItem>        
                                                    <MenuItem value="Reason 2">Product appears used</MenuItem>        
                                                    <MenuItem value="Reason 3">Not genuine product</MenuItem>        
                                            </Select>
                                        </FormControl>
                                        <span className="reason ml-4">
                                            Product looks used.
                                        </span>
                                    </div>
                                    <MuiThemeProvider theme={mui}>
                                        <TextField
                                            id="feedback"
                                            className="input_outer textField mt-1"
                                            label="Comments"
                                            multiline
                                            rowsMax="4"
                                            fullWidth
                                        />
                                    </MuiThemeProvider>
                                </form>
                            </li>
                            <li className="mb-4">
                                <h4><span>2</span> Pickup address</h4>
                                <div className="d-flex mt-4 align-items-baseline custmflex">
                                    <div className="address col-sm-7">
                                            <p>Rakesh gaur </p>
                                            <p>3rd floor UDB landmark complex Plot No A-6 Basant Bahar Tonk Road </p>
                                            <p>7300083928</p>
                                            <a href="Javascript:void(0);" data-toggle="modal" data-target="#check_address">Change Address</a>

                                    </div>
                                    <div className="verify">
                                        <img src="/img/tick.png" />
                                        This address is eligible for pickup.
                                    </div>
                                    <div className="notverify">
                                        <img src="/img/cross.png" />
                                        This address is ineligible for pickup.
                                    </div>
                                </div>
                            </li>
                            <li className="mb-4 d-flex flex-wrap">
                                <div className="col-md-6 col-sm-12">
                                    <h4><span>3</span> Do you want a refund or replacement?</h4>
                                    <div className="input_radio d-sm-flex w-100 align-items-center">
                                        <RadioGroup
                                            aria-label="Return Type"
                                            name="returnType"
                                            value={this.state.addressType}
                                            onChange = {e=>this.updateState(e)}
                                            helperText = {this.state.addressTypeError}
                                            className="radio_outer"
                                        >
                                            <FormControlLabel
                                              value="replacement"
                                              control={
                                                <Radio
                                                  classes={{root: classes.root, checked: classes.checked}}
                                                />
                                              }
                                              label="Replacement"
                                              labelPlacement="end"
                                            />
                                            <FormControlLabel
                                              value="refund"
                                              control={
                                                <Radio
                                                  classes={{root: classes.root, checked: classes.checked}}
                                                />
                                              }
                                              label="Refund"
                                              labelPlacement="end"
                                            />
                                        </RadioGroup>
                                    </div>
                                    <div className="radio_contnt mt-3 mb-3">
                                        <ul>
                                            <li>Replacement is subject to availability of product.</li>
                                            <li>In case product is not available, you will be refunded.</li>
                                        </ul>
                                    </div>
                                   
                                </div>
                                <div className="mode_outer col-md-6 col-sm-12">
                                    <h4>Mode of refund</h4>
                                    <div className="input_radio d-sm-flex w-100 align-items-center">
                                        <RadioGroup
                                            aria-label="Return Mode"
                                            name="returnMode"
                                            value={this.state.addressType}
                                            onChange = {e=>this.updateState(e)}
                                            helperText = {this.state.addressTypeError}
                                            className="radio_outer"
                                        >
                                            <FormControlLabel
                                              value="Refund in KZMO points"
                                              control={
                                                <Radio
                                                  classes={{root: classes.root, checked: classes.checked}}
                                                />
                                              }
                                              label="Refund in KZMO points" 
                                              labelPlacement="end"
                                            />
                                            <div className="content w-100">
                                                <ul>
                                                    <li>Refund within 24 Hours</li>
                                                    <li>Will get credited to KZMO Account</li>
                                                </ul>
                                            </div>
                                            <FormControlLabel
                                              value="Refund in Bank Account/Debit Card/Credit Card/Wallets"
                                              control={
                                                <Radio
                                                  classes={{root: classes.root, checked: classes.checked}}
                                                />
                                              }
                                              label="Refund in Bank Account/Debit Card/Credit Card/Wallets"
                                              labelPlacement="end"
                                            />
                                        </RadioGroup>
                                    </div>
                                </div>
                                 <Button
                                        variant="contained"
                                        color="primary"
                                    >
                                Submit
                                </Button>
                            </li>
                        </ul>
                    </div>
                </div>
                {/*modal*/}
                
            </div>
        );
    }1
}

function mapStateToProps(state) {
    console.log(state);
    return {
        example: state.example,
        languageReducer: state.languageReducer
    }
}
export default connect(mapStateToProps)(withStyles(styles)(ReturnRequest));

