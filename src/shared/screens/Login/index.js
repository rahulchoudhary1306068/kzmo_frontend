import React, { Component } from 'react';
import {connect} from 'react-redux';

import {LoginAction, OTPVerifyAction, ResendOTPAction, InitailStateAction, LoginOTPAction, ForgotOTPAction, ForgotVerifyOTPAction, UpdatePasswordAction} from '../../actions/login'
import {
  MuiThemeProvider,
  createMuiTheme,
  createGenerateClassName,
} from '@material-ui/core/styles';
import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';

import Button from '@material-ui/core/Button';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';

import FacebookLogin from 'react-facebook-login/dist/facebook-login-render-props'
import { GoogleLogin } from 'react-google-login';
import {notify} from 'react-notify-toast';

import Loader from "../../components/loader";
import local, {deafultLanguage} from '../../localization';
import {getter} from '../../commonfunction';

import {checkuserName,checkRequired,checkPhonenumber,checkconfirmPassword,checkEmail,checkPassword} from '../../commonfunction'
import { UpdatePassword } from '../../sagas/login';

const styles = theme => ({
    root: {
      '&$checked': {
        color: '#e4b122'
      }
    },
    checked: {},
    
  });
const mui = createMuiTheme({
  overrides: {
    MuiFormLabel: {
      root: {
        color: "#b1b1b1"
      },
      focused: {
        color: "black !important"
      },
    }
  }
});

class Login extends Component {
    constructor(props){
        super(props)
        this.state = {
            mobileSignup : "",
            mobileSignupError : "",
            emailSignup : "",
            emailSignupError : "",
            passwordSignup : "",
            passwordSignupError : "",
            confirmpasswordSignup : "",
            confirmpasswordSignupError : "",
            userNameLogin : "",
            userNameLoginError : "",
            passwordLogin : "",
            passwordLoginError : "",
            otpTimer : 30,
            otpValue : "",
            otpValueError : "",
            otpFor : "" ,//1 for login and 2 for signup and 3 forgot passwod
            createPasswordFor : "", //1 for signup 2 for forgot password
            newPassword : "",
            newPasswordError : "",
            confirmPassword : "",
            confirmPasswordError : "",
            userNameForgot : "",
            userNameForgotError : "",
            otpintervalClear : "",
            resendOTPshow : false,
            toggleLoginButton : false,
            showOTP : false,
            LoginError : "",
            linkEmail : "",
            linkedEmailArray : [],
            showLinkEmail : false,
            ForgotResendOTP : false,
            ForgotPasswordID : "",
            language: '',
            loginData: {},
            LoginOTPData: {},
            ForgotOTPData: {},
            ForgotVerifyOTPData: {},
            UpdatePasswordData: {},
            OTPResendData: {}
        }
    }

    handleToggleLoginButton(){
        this.setState({
            toggleLoginButton : !this.state.toggleLoginButton,
            passwordLoginError : "",
            passwordLogin : "",
            
        })
    }

    handleLogin(event){console.log("in login");
        event.preventDefault();
        let error = false;
        let vuserName = checkuserName(this.state.userNameLogin, this.state.language);
        if(!vuserName[0]){
            error = true;
            this.setState({
                userNameLoginError : vuserName[1]
            })
        }
        let vPassword  = checkPassword(this.state.passwordLogin, this.state.language)
        if(!vPassword[0]){
            error = true;
            this.setState({
                passwordLoginError : vPassword[1]
            })
        }
        if(!error){
            // temp line 
            this.setState({
                createPasswordFor: "",
                showOTP : false
            })
            let data ={"user_login":this.state.userNameLogin,
            "password":this.state.passwordLogin,
            "key":"c04d979305fe8969b34b09c3f223a54a"}
            this.props.dispatch(LoginAction(data));       
        }
    }

    handleLoginByOTP(event){
        event.preventDefault();
        let error = false;
        let vuserName = checkuserName(this.state.userNameLogin, this.state.language);
        if(!vuserName[0]){
            error = true;
            this.setState({
                userNameLoginError : vuserName[1]
            })
        }
         
         if(!error){
            var data = {'key':'c04d979305fe8969b34b09c3f223a54a','user_login':this.state.userNameLogin
            ,'link_email':this.state.link_email
        }
        this.props.dispatch(LoginOTPAction(data))
           // this.props.dispatch(LoginAction(data))
        //     this.setState({
        //         otpFor :1
        //     })
        //     $('.modal').modal('hide');
        //     $("#otp_popup").modal("show");
        //     this.state.otpintervalClear = setInterval(()=>{ console.log("call timeout",this.state.otpTimer)
        //         if(this.state.otpTimer > 0){
        //             this.setState({
        //                 otpTimer : this.state.otpTimer-1
        //             }) 
        //         }
        //         else{
        //             this.setState({
        //                 resendOTPshow:true
        //         })
        //             clearInterval(this.state.otpintervalClear);
        //         }
        //     },1000)
            console.log("call login otp here api here")
        }
    }

    handleSignupByOTP(event){
        event.preventDefault();
        let error = false;
        let vsignuser = checkPhonenumber(this.state.mobileSignup, this.state.language)
        if(!vsignuser[0]){
            error = true;
            this.setState({
                mobileSignupError : vsignuser[1]
            })
        }
        let vemail = checkEmail(this.state.emailSignup, this.state.language)
        if(!vemail[0]){
            error = true;
            this.setState({
                emailSignupError : vemail[1]
            })
        }
         
        if(!error){
            this.setState({
                showOTP : true
            })
            var data = {'key':'c04d979305fe8969b34b09c3f223a54a','email':this.state.emailSignup, 
                phone:this.state.mobileSignup,'register_with_password':0,
                    'signup':true}
                this.props.dispatch(LoginAction(data))
        }
    }

    showOTPPopUp(value){
        this.setState({
            LoginError : "",
            otpValue : "",
            showLinkEmail : false

        })
        this.setState({
            otpFor :value,
            //showOTP : false
        })
         $('.modal').modal('hide');
            $("#otp_popup").modal("show");
            this.state.otpintervalClear = setInterval(()=>{ 
                if(this.state.otpTimer > 0){
                    this.setState({
                        otpTimer : this.state.otpTimer-1
                    }) 
                }
                else{
                    this.setState({
                        resendOTPshow:true
                })
                    clearInterval(this.state.otpintervalClear);
                }
            },1000)
    }

    otpVerify(event){
        event.preventDefault();
        let error = false;
        let votp = checkRequired(this.state.otpValue)
        if(!votp[0]){
            error = true;
            this.setState({
                otpValueError : votp[1]
            })
        }
        if(!error){
            if(this.state.otpFor==1){
                var data = {
                    'key':'c04d979305fe8969b34b09c3f223a54a',
                    'user_login':this.state.userNameLogin,
                    'otp':this.state.otpValue,
                    'link_email':this.state.link_email,
                    'platform': "D"
                }
                this.props.dispatch(LoginAction(data))
                console.log("process for login");
            }
            else if(this.state.otpFor==2){
                let data = {
                    'key':'c04d979305fe8969b34b09c3f223a54a',
                    'user_login':this.state.mobileSignup,
                    'otp':this.state.otpValue,
                    'platform': "D"
                }
                this.props.dispatch(OTPVerifyAction(data));
            }
            else{
                // verify otp here and refirect to create password screen
                let data = {
                    'key':'c04d979305fe8969b34b09c3f223a54a',
                    "user_login":this.state.userNameForgot,
                    "otp":this.state.otpValue,
                    'platform': "D"
                }
                this.props.dispatch(ForgotVerifyOTPAction(data))
                // $('.modal').modal('hide');
                // $("#createpassword_popup").modal("show");
                // this.setState({
                //     createPasswordFor : 2
                // })
                console.log(" for forgot password")
            }
        }
    }

    changePopup(id,event){
        event.preventDefault();
        clearInterval(this.state.otpintervalClear);
        this.setState({
            LoginError : "",
            emailSignup : "",
            mobileSignup : "",
            email : "",
            resendOTPshow : false,
            otpTimer :30
            
        })
        $('.modal').modal('hide');
        $("#"+id).modal("show");
    }
    
    login(){
        this.props.dispatch(LoginAction());
    }

    handleClose(id){
        this.setState({
            LoginError : "",
            showLinkEmail : false
        })
        if(!this.props.LoginReducer.showLogin){
            this.props.toggleLogin();
            $('#'+id).modal('hide');
        }  
    }

    updateState(e){
        this.setState({
            [e.target.name+"Error"] : ""
        })
        this.setState({
            [e.target.name]:e.target.value,
        })
        this.setState({
            LoginError : "",
           
        })
        if(e.target.name!=='otpValue'){
            this.setState({
               link_email:"",
               linkedEmailArray:[]
            })
        }    
    }

    updateStateRadio(e){
        this.setState({
            [e.target.name]:e.target.value,
        })    
    }

    showCreatePassword(event){
        event.preventDefault();
        let error = false;
        let vuserName = checkPhonenumber(this.state.mobileSignup, this.state.language)
        if(!vuserName[0]){
            error = true;
            this.setState({
                mobileSignupError : vuserName[1]
            })
        }
        let vemail = checkEmail(this.state.emailSignup, this.state.language)
        if(!vemail[0]){
            error = true;
            this.setState({
                emailSignupError : vemail[1]
            })
        }
        
        if(!error){ 
            $('.modal').modal('hide');
            $("#createpassword_popup").modal("show");
            this.setState({
                createPasswordFor : 1
            })
                console.log("show create password popup for signup")
        }
    }

    signupRequest(event){
        event.preventDefault();
        let error = false;
        let vnewpassword = checkPassword(this.state.newPassword, this.state.language)
        if(!vnewpassword[0]){
            error = true;
            this.setState({
                newPasswordError : vnewpassword[1]
            })
        }
        let vconfirmpassword = checkconfirmPassword(this.state.confirmPassword,this.state.newPassword, this.state.language)
        if(!vconfirmpassword[0]){
            error = true;
            this.setState({
                confirmPasswordError : vconfirmpassword[1]
            })
        }
        
        if(!error){ 
            if(this.state.createPasswordFor == 1){
                this.setState({
                    showOTP : false
                })
                var data = {'key':'c04d979305fe8969b34b09c3f223a54a','email':this.state.emailSignup, 
                phone:this.state.mobileSignup,'register_with_password':1,
                 'password':this.state.newPassword,'signup':true}
                 this.props.dispatch(LoginAction(data))
            }
                
            else{
                let data ={'key':'c04d979305fe8969b34b09c3f223a54a','user_login':this.state.userNameForgot, 'user_id':this.state.ForgotPasswordID, 'password':this.state.newPassword }
                this.props.dispatch(UpdatePasswordAction(data))
                console.log("forgot password request")
            }   
        }
    }

    forgotPassword(event){
        event.preventDefault();
        let error = false;
        let vuserName = checkuserName(this.state.userNameForgot, this.state.language)
        if(!vuserName[0]){
            error = true;
            this.setState({
                userNameForgotError : vuserName[1]
            })
        }
         
        if(!error){
            let data = {'user_login':this.state.userNameForgot}
            this.props.dispatch(ForgotOTPAction(data))
            this.setState({
                otpFor :3
            })
                   
        }
    }

    resendOTP(event){
        event.preventDefault();
        //write resend logic here
        if(this.state.otpFor ==2 ){ // for signup
            let data = {"user_login":this.state.mobileSignup}
            this.props.dispatch(ResendOTPAction(data))
        }
        else if(this.state.otpFor ==1){
            //login
            var data = {'key':'c04d979305fe8969b34b09c3f223a54a','user_login':this.state.userNameLogin
            ,'link_email':this.state.link_email,'type':'login'
            }
        }    
        else { // for forgot password we are using different action for Resend
            this.setState({
                ForgotResendOTP : true
            })
            let data ={"user_login": this.state.userNameForgot}
            this.props.dispatch(ForgotOTPAction(data));
            return;
        }
        this.props.dispatch(ResendOTPAction(data))
    }

    RestartTimer(){
        this.state.otpintervalClear = setInterval(()=>{ 
            if(this.state.otpTimer > 0){
                this.setState({
                    otpTimer : this.state.otpTimer-1
                }) 
            }
            else{
                this.setState({
                    resendOTPshow:true
            })
                clearInterval(this.state.otpintervalClear);
            }
        },1000)
        this.setState({
                otpTimer:30,
                resendOTPshow:false
        })
    }

    facebookResponse = (e) => {
        console.log("google call");
        alert(e)
        console.log(e)
    };

    googleResponse = (e) => {
        console.log(e)
    };

    onFailure = (error) => {
      
    }

    toggleEmailLink (){
        this.setState({
            showLinkEmail : ! this.state.showLinkEmail,
            link_email : "",
            linkedEmailArray :[]
        })
    }

    componentDidMount(){
        this.setState({
            language: getter('language') ? getter('language') : deafultLanguage
        })
    }

    componentWillReceiveProps(nextProps){
        if(nextProps.languageReducer.value){
            this.setState({
                language: nextProps.languageReducer.value
            })  
        }

        if(nextProps.ForgotOTPReducer != this.props.ForgotOTPReducer){
            this.setState({ForgotOTPData: nextProps.ForgotOTPReducer});

            if(Object.keys(nextProps.ForgotOTPReducer.data).length){    
                if(nextProps.ForgotOTPReducer.data.status =='success'){
                    //show otpPOPup
                    if(this.state.ForgotResendOTP){
                        this.setState({
                            ForgotResendOTP : false
                        })
                        this.RestartTimer();
                    }
                    else{
                        this.showOTPPopUp(3);
                    }
                }
                else{
                    this.setState({
                        userNameForgotError : nextProps.ForgotOTPReducer.data.extra.errors[0].message
                    })
                }
                this.props.dispatch(InitailStateAction('InitailStateForgotOTP_ACTION')); 
            }
        }

        if(nextProps.UpdatePasswordReducer != this.props.UpdatePasswordReducer){
            this.setState({UpdatePasswordData: nextProps.UpdatePasswordReducer});

            if(Object.keys(nextProps.UpdatePasswordReducer.data).length){
                if(nextProps.UpdatePasswordReducer.data.status =='success'){
                   //hide password Popup
                   this.setState({
                       ForgotPasswordID :""
                   })

                   notify.show(nextProps.UpdatePasswordReducer.data.message);
                   $('.modal').modal('hide');
                   this.props.toggleLogin();
                }
                else{
                    this.setState({
                        LoginError : nextProps.UpdatePasswordReducer.data.extra.errors[0].message
                    })
                }
                this.props.dispatch(InitailStateAction('InitailStateUpdatePassword_ACTION'));
            }
        }

        if(nextProps.ForgotVerifyOTPReducer != this.props.ForgotVerifyOTPReducer){
            this.setState({ForgotVerifyOTPData: nextProps.ForgotVerifyOTPReducer});

            if(Object.keys(nextProps.ForgotVerifyOTPReducer.data).length){
                if(nextProps.ForgotVerifyOTPReducer.data.status =='success'){
                    //show otpPOPup
                    $('.modal').modal('hide');
                    $("#createpassword_popup").modal("show");
                    this.setState({
                        createPasswordFor : 2,
                        ForgotPasswordID : nextProps.ForgotVerifyOTPReducer.data.data.user_id
                    })
                }
                else{
                    this.setState({
                        otpValueError : nextProps.ForgotVerifyOTPReducer.data.extra.errors[0].message
                    })
                }
                this.props.dispatch(InitailStateAction('InitailStateForgotVerifyOTP_ACTION')); 
            }
        }
    
        if (nextProps.LoginReducer != this.props.LoginReducer){
            this.setState({loginData: nextProps.LoginReducer});

            if(nextProps.LoginReducer.data.status=='fail'){
                this.setState({
                    LoginError : nextProps.LoginReducer.data.extra.errors[0]['message']
                    //  confirmPasswordError : nextProps.LoginReducer.data.extra.errors[0]['message']
                })
                
                this.props.dispatch(InitailStateAction('InitailStateLogin_ACTION'));
            }

            if(nextProps.LoginReducer.data.status=='success'){
                console.log("call for singup",this.state.showOTP);
                if(this.state.showOTP){
                    this.showOTPPopUp(2);
                }
                else{
                    $('.modal').modal('hide');
                    this.props.toggleLogin();
                }
				this.props.dispatch(InitailStateAction('InitailStateLogin_ACTION'));
			}
        }

        if(nextProps.OTPReducer !== this.props.OTPReducer && Object.keys (nextProps.OTPReducer.data).length){
            if(nextProps.OTPReducer.data.status == "success"){
                $('.modal').modal('hide');
                this.props.toggleLogin();
            }
            else{
                this.setState({
                    LoginError:nextProps.OTPReducer.data.extra.errors[0].message
                })
            }
        }

        if(nextProps.OTPResendReducer != this.state.OTPResendReducer){
            this.setState({OTPResendData: nextProps.OTPResendReducer});

            if(Object.keys (nextProps.OTPResendReducer.data).length){
                if(nextProps.OTPResendReducer.data.status == "success"){
                    //show otp send successfully message
                    this.RestartTimer();
                }
                else{
                    this.setState({
                        LoginError:nextProps.OTPResendReducer.data.extra.errors[0].message
                    })
                }

                this.props.dispatch(InitailStateAction('InitailStateResendOTP_ACTION')) 
            }
        }

        if(nextProps.LoginOTPReducer != this.props.LoginOTPReducer){
            this.setState({LoginOTPData: nextProps.LoginOTPReducer});

            if(Object.keys(nextProps.LoginOTPReducer.data).length){
                if(nextProps.LoginOTPReducer.data.status == 'fail'){
                    if(nextProps.LoginOTPReducer.data.extra.errors[0].key =='link_email'){
                        //show email list
                        let value = JSON.parse(nextProps.LoginOTPReducer.data.extra.errors[1].value)
                        this.setState({
                            link_email : value[0],
                            linkedEmailArray :value,
                            showLinkEmail : true
                        })
                    }
                    else{
                        this.setState({
                            LoginError : nextProps.LoginOTPReducer.data.extra.errors[0].message
                        })
                    }
                }
                else{
                    this.showOTPPopUp(1);
                    // show Otp popup
                }

                this.props.dispatch(InitailStateAction('InitailStateLoginOTP_ACTION'));
            }
        }
    }

    render() {
        const { classes } = this.props;

        return (
            <div>
                <div data-backdrop="static" data-keyboard="false" className="modal  login_popup_outer" id="register_popup">
                    <div className="modal-dialog">
                        <div className="modal-content">
                            <div className="modal-body">
                               {this.props.showCrossButton && <button  type="button" className="close"  onClick = {this.handleClose.bind(this,'register_popup')}>&times;</button> } 
                                <div className="row m-0">
                                    <div className="col-md-7 left_sec">

                                        <h2 className="login_title text-center">{local.logCreateAccountWith[this.state.language]}</h2>
                                        <div className="text-center icon_sec">
                                        <span className="common_tooltip_outer">
                                            <span className="common_tooltip top">{local.orderComingSoon[this.state.language]}</span>
                                            <FacebookLogin
                                                 appId="309386579860155"
                                                 autoLoad={false}
                                                 fields="name,email,picture"
                                                 callback={this.facebookResponse}
                                                  render={renderProps => (
                                                    <button className="btn" onClick={renderProps.onClick}><svg className="icon icon-facebook-1"><use xlinkHref="/img/icon-sprite.svg#icon-facebook-1"></use></svg> Facebook</button>
                                                  )}
                                            />
                                        </span>
                                        <span className="common_tooltip_outer">
                                            <span className="common_tooltip top">{local.orderComingSoon[this.state.language]}</span>
                                            <GoogleLogin
                                                clientId="578292974093-0sufj22rvnicl19129gvre72m681317k.apps.googleusercontent.com"
                                                onSuccess={this.googleResponse}
                                                onFailure={this.onFailure}
                                                render={renderProps => (
                                                    <button className="btn" onClick={renderProps.onClick}><svg className="icon icon-google"><use xlinkHref="/img/icon-sprite.svg#icon-google"></use></svg> Google</button>
                                                  )}
                                            />
                                        </span>
                                            <div>{local.logOr[this.state.language]}</div>
                                        </div>
                                        <div className="form_outer" autoComplete="off">
                                            <MuiThemeProvider theme={mui}>
                                                <TextField
                                                  id="phone_number"
                                                  className="input_outer"
                                                  label={local.logEnterMobileNumber[this.state.language]}
                                                  autoFocus
                                                  fullWidth
                                                  helperText=""
                                                  name = "mobileSignup"
                                                  onChange = {e=>this.updateState(e)}
                                                  helperText = {this.state.mobileSignupError}

                                                />
                                                <TextField
                                                  id="email_input"
                                                  label={local.footerEmailAddress[this.state.language]}
                                                  className="input_outer"
                                                  type="email"
                                                  fullWidth
                                                  name = "emailSignup"
                                                  onChange = {e=>this.updateState(e)}
                                                  helperText={this.state.emailSignupError}
                                                />
                                            </MuiThemeProvider>
                                            <div>
                                                 <Button
                                                    variant="contained"
                                                    color="primary"
                                                    className="send_otp"
                                                    onClick = {this.handleSignupByOTP.bind(this)}
                                                >
                                                    {local.logSendOTP[this.state.language]}
                                                </Button>
                                                <span className="buffer_text">{local.logOr[this.state.language]}</span>
                                                <a
                                                    variant="contained"
                                                    color="primary"
                                                    className="create_pass d-inline-block text-right"
                                                    onClick = {this.showCreatePassword.bind(this)}
                                                    href ="JavaScript:void(0);"
                                                >
                                                    {local.logCreatePassword[this.state.language]}
                                                </a>
                                                <p className="LoginCommonError">{this.state.LoginError}</p>
                                            </div>
                                            
                                        </div>
                                        <div className="content">
                                            <p>{local.logPrivacyPolicy[this.state.language]}</p>
                                        </div>
                                    </div>
                                    <div className="col-md-5 right_sec text-right">
                                        <div className="text-center">
                                            <h2>{local.logHaveAccount[this.state.language]}</h2>
                                            <a href="#" onClick = {this.changePopup.bind(this,'login_popup')}>{local.logLoginNow[this.state.language]}</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div data-backdrop="static" data-keyboard="false" className="modal login_popup_outer" id="otp_popup">
                                <div className="modal-dialog">
                                    <div className="modal-content">
                                        <div className="modal-body">

                                        {this.props.showCrossButton && <button type="button" className="close" onClick = {this.handleClose.bind(this,'otp_popup')} >&times;</button>}
                                            <div className="row m-0">
                                                <div className="col-md-7 left_sec verification_outer">

                                                    <h2 className="login_title text-center">{local.logVerification[this.state.language]}</h2>
                                                    <div className="verification_inner">
                                                        <div className="content">
                                                            <p>{local.logEnterOTP[this.state.language]}{this.state.userNameLogin?this.state.userNameLogin:this.state.mobileSignup?this.state.mobileSignup:this.state.userNameForgot}</p>
                                                        </div>
                                                        <div className="form_outer" autoComplete="off">
                                                            <MuiThemeProvider theme={mui}>
                                                                <TextField
                                                                  id="otp"
                                                                  name = "otpValue"
                                                                  className="input_outer"
                                                                  label={local.checkEnterOtp[this.state.language]}
                                                                  autoFocus
                                                                  fullWidth
                                                                  helperText={this.state.otpValueError}
                                                                  onChange = {e =>this.updateState(e)}
                                                                />
                                                            </MuiThemeProvider>
                                                            <div>
                                                                <Button
                                                                    variant="contained"
                                                                    color="primary"
                                                                    onClick = {this.otpVerify.bind(this)}
                                                                >
                                                                    {local.logContinue[this.state.language]}
                                                                </Button>
                                                                {this.state.resendOTPshow && (
                                                                     <span className="time_span float-right" ><a href="#" onClick = {this.resendOTP.bind(this)}>{local.logResendOTP[this.state.language]}</a></span>
                                                                )}
                                                               {!this.state.resendOTPshow && (
                                                                   <span className="time_span float-right">{this.state.otpTimer}{local.logSecondsLeft[this.state.language]}</span>
                                                               )}
                                                                
                                                             <p className="LoginCommonError">{this.state.LoginError}</p>
                                                            </div>
                                                            
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="col-md-5 right_sec text-right">
                                                    <div className="text-center">
                                                        <h2>{local.logHaveAccount[this.state.language]}</h2>
                                                        <a href="#" onClick = {this.changePopup.bind(this,'login_popup')}>{local.logLoginNow[this.state.language]}</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                </div>

                <div data-backdrop="static" data-keyboard="false" className="modal  login_popup_outer" id="login_popup">
                    <div className="modal-dialog">
                        <div className="modal-content">
                            <div className="modal-body">

                            {this.props.showCrossButton && <button type="button" className="close" onClick = {this.handleClose.bind(this,'login_popup')} >&times;</button>}
                                <div className="row m-0">
                                 {!this.state.showLinkEmail &&  <div className="col-md-7 left_sec">
                                    
                                        <h2 className="login_title text-center">{local.logLoginWith[this.state.language]}</h2>
                                        <div className="text-center icon_sec">
                                        <span className="common_tooltip_outer">
                                            <span className="common_tooltip top">{local.orderComingSoon[this.state.language]}</span>
                                        
                                        <FacebookLogin
                                                 appId="309386579860155"
                                                 autoLoad={false}
                                                 fields="name,email,picture"
                                                 callback={this.facebookResponse}
                                                  render={renderProps => (
                                                    <button className="btn"  onClick={renderProps.onClick}><svg className="icon icon-facebook-1"><use xlinkHref="/img/icon-sprite.svg#icon-facebook-1"></use></svg> Facebook</button>
                                                  )}
                                            />
                                        </span>
                                        <span className="common_tooltip_outer">
                                            <span className="common_tooltip top">{local.orderComingSoon[this.state.language]}</span>

                                            <GoogleLogin
                                                clientId="578292974093-0sufj22rvnicl19129gvre72m681317k.apps.googleusercontent.com"
                                                onSuccess={this.googleResponse}
                                                onFailure={this.onFailure}
                                                render={renderProps => (
                                                    <button className="btn"  onClick={renderProps.onClick}><svg className="icon icon-google"><use xlinkHref="/img/icon-sprite.svg#icon-google"></use></svg> Google</button>
                                                  )}
                                            />
                                        </span>
                                            <div>{local.logOr[this.state.language]}</div>
                                        </div>
                                        <div className="form_outer" autoComplete="off">
                                        <MuiThemeProvider theme={mui}>
                                            <TextField
                                              required
                                              id="userNameLogin"
                                              name = "userNameLogin"
                                              className="input_outer"
                                              label={local.logEmailOrMobile[this.state.language]}
                                              autoFocus
                                              fullWidth
                                              helperText={this.state.userNameLoginError}
                                              value = {this.state.userNameLogin}
                                              onChange = {e => this.updateState(e)}
                                            />
                                            { this.state.toggleLoginButton && 
                                                <TextField
                                                    required
                                                    id="passwordLogin"
                                                    name = "passwordLogin"
                                                    label={local.logEnterPassword[this.state.language]}
                                                    className="input_outer"
                                                    type="password"
                                                    fullWidth
                                                    helperText={this.state.passwordLoginError}
                                                    value = {this.state.passwordLogin}
                                                    onChange = {e => this.updateState(e)}
                                                />
                                            }
                                            </MuiThemeProvider>

                                            {this.state.toggleLoginButton && 
                                                <Button
                                                    variant="contained"
                                                    color="primary"
                                                    type="button"
                                                    onClick = {this.handleLogin.bind(this)}
                                                    fullWidth>
                                                    {local.logContinue[this.state.language]}
                                                </Button>
                                            }
                                            { !this.state.toggleLoginButton && 
                                                <Button
                                                    variant="contained"
                                                    color="primary"
                                                    type="button"
                                                    onClick = {this.handleLoginByOTP.bind(this)}
                                                    fullWidth>
                                                    {local.logSendOTP[this.state.language]}
                                                </Button>
                                            }
                                            <p className="LoginCommonError">{this.state.LoginError}</p>
                                            
                                        </div>
                                        <div className="content">
                                            { this.state.toggleLoginButton && 
                                                 <a href="JavaScript:void(0);" onClick = {this.handleToggleLoginButton.bind(this)}>{local.logWithOTP[this.state.language]}</a>
                                            }
                                            { !this.state.toggleLoginButton && 
                                             <a href="JavaScript:void(0);" onClick = {this.handleToggleLoginButton.bind(this)}>{local.logWithPassword[this.state.language]}</a>
                                            }
                                            
                                            
                                            <a href="JavaScript:void(0);"  onClick = {this.changePopup.bind(this,'forgotpassword_popup')} className="float-right">{local.logForgotPassword[this.state.language]}</a>
                                        </div>
                                        
                                        </div> }

                                 {this.state.showLinkEmail &&   <div className="col-md-7 left_sec">

                                        <h2 className="login_title text-center">{local.logUpdateYourAccount[this.state.language]}</h2>
                                        <p className="sub_desp text-center">{local.logEnterMobile[this.state.language]}</p>
                                        <p className="sub_desp text-center">{local.logSelectDesires[this.state.language]}</p>
                                        <div className="form_outer" autoComplete="off">
                                        {this.state.linkedEmailArray.map((val,index)=>{
                                            return <RadioGroup
                                            aria-label="Link Email"
                                            name="link_email"
                                            value={this.state.link_email}
                                            onChange={e=>this.updateStateRadio(e)}
                                            className= "d-inline-block"
                                        >
                                                <FormControlLabel
                                                    value={val}
                                                    control={<Radio
                                                            classes={{root: classes.root, checked: classes.checked}}
                                                            />}

                                                    label={val}
                                                    labelPlacement="end"
                                                    
                                                />
                                                
                                            
                                        
                                        </RadioGroup>
                                    })}
                                            <div className="btn_outer">
                                                <Button
                                                    variant="contained"
                                                    color="primary"
                                                    onClick = {this.handleLoginByOTP.bind(this)}
                                                >
                                                    {local.logContinue[this.state.language]}
                                                </Button>
                                                <a href="JavaScript:void(0);" onClick = {this.toggleEmailLink.bind(this)} className="create_pass float-right">{local.logBackToLogin[this.state.language]}</a>
                                            </div>
                                        </div>
                                    </div>
                                     }
                                    <div className="col-md-5 right_sec text-right">
                                        <div className="text-center">
                                            <h2>{local.logNewHere[this.state.language]}</h2>
                                            <a href="#" onClick = {this.changePopup.bind(this,'register_popup')}>{local.logCreateNewAccount[this.state.language]}</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div data-backdrop="static" data-keyboard="false" className="modal  login_popup_outer" id="createpassword_popup">
                    <div className="modal-dialog">
                        <div className="modal-content">
                            <div className="modal-body">

                            {this.props.showCrossButton && <button  type="button" className="close"  onClick = {this.handleClose.bind(this,'createpassword_popup')}>&times;</button>}
                                <div className="row m-0">
                                    <div className="col-md-7 left_sec">

                                        <h2 className="login_title text-center">{local.logCreatePassword[this.state.language]}</h2>
                                        
                                        <div className="form_outer" autoComplete="off">
                                                <MuiThemeProvider theme={mui}>
                                                    <TextField
                                                      required
                                                      id="new_password"
                                                      className="input_outer"
                                                      label={local.logNewPassword[this.state.language]}
                                                      type = "password"
                                                      autoFocus
                                                      fullWidth
                                                      helperText=""
                                                      name = "newPassword"
                                                      onChange = {e=>this.updateState(e)}
                                                      helperText = {this.state.newPasswordError}

                                                    />
                                                    <TextField
                                                      required
                                                      id="confirmpassword"
                                                      label={local.logConfirmPassword[this.state.language]}
                                                      className="input_outer"
                                                      type="password"
                                                      fullWidth
                                                      name = "confirmPassword"
                                                      onChange = {e=>this.updateState(e)}
                                                      helperText={this.state.confirmPasswordError}
                                                    />
                                                </MuiThemeProvider>
                                            <div>
                                                <Button
                                                    variant="contained"
                                                    color="primary"
                                                    onClick = {this.signupRequest.bind(this)}
                                                    fullWidth
                                                >
                                                    {local.logContinue[this.state.language]}
                                                </Button>
                                                <p className="LoginCommonError">{this.state.LoginError}</p>
                                            </div>
                                            
                                        </div>
                                        {/* <div className="content">
                                            <p>By Registerings you accept our User Agreement and Privacy Policy</p>
                                        </div> */}
                                    </div>
                                    <div className="col-md-5 right_sec text-right">
                                        <div className="text-center">
                                            <h2>{local.logHaveAccount[this.state.language]}</h2>
                                            <a href="#" onClick = {this.changePopup.bind(this,'login_popup')}>{local.logLoginNow[this.state.language]}</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div data-backdrop="static" data-keyboard="false" className="modal  login_popup_outer" id="forgotpassword_popup">
                    <div className="modal-dialog">
                        <div className="modal-content">
                            <div className="modal-body">

                            {this.props.showCrossButton && <button type="button" className="close" onClick = {this.handleClose.bind(this,'forgotpassword_popup')} >&times;</button>}
                                <div className="row m-0">
                                    <div className="col-md-7 left_sec">

                                        <h2 className="login_title text-center">{local.logForgotPassword[this.state.language]}</h2>
                              
                                        <div className="form_outer" autoComplete="off">
                                            <MuiThemeProvider theme={mui}>
                                                <TextField
                                                  required
                                                  id="userNameForgot"
                                                  name = "userNameForgot"
                                                  className="input_outer"
                                                  label={local.logEmailOrMobile[this.state.language]}
                                                  autoFocus
                                                  fullWidth
                                                  helperText={this.state.userNameForgotError}
                                                  value = {this.state.userNameForgot}
                                                  onChange = {e => this.updateState(e)}
                                                />
                                             </MuiThemeProvider>
                                            <Button
                                                variant="contained"
                                                color="primary"
                                                type="button"
                                                onClick = {this.forgotPassword.bind(this)}
                                                fullWidth>
                                                {local.logSendOTP[this.state.language]}
                                            </Button>
                                            <p className="LoginCommonError">{this.state.LoginError}</p>
                                        </div>
                                       
                                    </div>
                                    <div className="col-md-5 right_sec text-right">
                                        <div className="text-center">
                                            <h2>{local.logHaveAccount[this.state.language]}</h2>
                                            <a href="#" onClick = {this.changePopup.bind(this,'login_popup')}>{local.logLoginNow[this.state.language]}</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                {(this.state.loginData.isFetching || this.state.LoginOTPData.isFetching || this.state.ForgotOTPData.isFetching || this.state.ForgotVerifyOTPData.isFetching || this.state.UpdatePasswordData.isFetching || this.state.OTPResendData.isFetching) && <Loader />}
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        OTPReducer : state.OTPReducer,
        OTPResendReducer : state.OTPResendReducer,
        LoginOTPReducer : state.LoginOTPReducer,
        ForgotOTPReducer : state.ForgotOTPReducer,
        ForgotVerifyOTPReducer : state.ForgotVerifyOTPReducer,
        UpdatePasswordReducer : state.UpdatePasswordReducer,
        languageReducer: state.languageReducer,
        LoginReducer: state.LoginReducer
    }
}

export default connect(mapStateToProps)(withStyles(styles)(Login));
