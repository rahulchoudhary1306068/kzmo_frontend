import React, { Component } from 'react';
import {connect} from 'react-redux';

// import "react-responsive-carousel/lib/styles/carousel.min.css";
import {
  MuiThemeProvider,
  createMuiTheme,
  createGenerateClassName,
} from '@material-ui/core/styles';
import { withStyles } from '@material-ui/core/styles';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import Button from '@material-ui/core/Button';
//import {PinMap} from '../../components/pinMap'
//import Map from '../../components/map';
import LocatorMap from '../../components/locator';
import {notify} from 'react-notify-toast';
import local, {deafultLanguage} from '../../localization';
import {getter,languageForAPI,convertTimeToAM} from '../../commonfunction';
import { StoreAction } from '../../actions/header';
import { locatorAction,mallAction} from '../../actions/locator';
var latLongData;
class SelectLocator extends Component {
    static initialAction({url, store,lang}) {
       store.dispatch(locatorAction());
       return true;
    }
    constructor(props){
        super(props)
        this.state = {
           
            select_city: "",
            select_brand: "",
            select_mall:"",
            language: '',
            cityData: {},
            brandMenuData: {},
            mallData:{},
            mallList:{},
            searchBtnDisable:"btn_disable",
            locatorData:{},
            locatorList:{},
            showMap:true,
            lat : 23.7224509,
            long : 46.7893653,
            address:""
        }
    }

    getMallList(){

            this.props.dispatch(mallAction({
                lang_code:languageForAPI()
            }));

    }
    goToMicrosite(link){
      if(link != ''){
        window.open("http://"+link+".kzmo.com");
      }
    }
    validate(){
        //console.log('dinesh in in');
        //console.log(this.state);
        //console.log(this.props);
        if(this.state.select_city == '' && this.state.select_brand == '' && this.state.select_mall == ''){
            this.setState({searchBtnDisable: "btn_disable"});
        }else{
            this.setState({searchBtnDisable : "false"});
        }
        this.props.dispatch(locatorAction({city_id:this.state.select_city,
                                            company_id:this.state.select_brand,
                                            mall_id:this.state.select_mall,
                                            lang_code:languageForAPI()
                                        }));
    }

    handleChange = event => {
        if(event.target.value != ''){
            this.setState({searchBtnDisable : "false"});
        }
        this.setState({ [event.target.name]: event.target.value });
    };
    componentDidMount(){
        this.setState({
            language: getter('language') ? getter('language') : deafultLanguage
        })

        if(this.props.cityData && this.props.cityData.data){
            this.setState({cityData: this.props.cityData.data},
                () => {
                    this.props.dispatch(StoreAction(this.state.selectedCity));
            });
        }

        this.getMallList();

    }
    componentWillReceiveProps(nextProps){
        if(nextProps.languageReducer.value){
            this.setState({
                language: nextProps.languageReducer.value
            })
        }
        if(nextProps.cityData.data != this.props.cityData.data){
            this.setState({cityData: nextProps.cityData.data},
                () => {
                    this.props.dispatch(StoreAction(this.state.selectedCity));
            });
        }


        //console.log(nextProps);
        if(Object.keys(nextProps.brandMenuReducer.data).length){
          if(nextProps.brandMenuReducer.data){
              this.setState({
                  brandMenuData : nextProps.brandMenuReducer.data,
              })
          }
       }

        if(Object.keys(nextProps.mallData.data).length){
            //console.log('i am in componentWillReceiveProps - dinesh goyal',Object.keys(nextProps.mallData.data).length);
            if(nextProps.mallData.data){
                this.setState({
                    mallList : nextProps.mallData.data,
                })
            }
        }
        if(Object.keys(nextProps.locatorData.data).length){
          //console.log('i am in componentWillReceiveProps - dinesh goyal',Object.keys(nextProps.locatorData.data).length);
            if(nextProps.locatorData.data){
                this.setState({
                    locatorList : nextProps.locatorData.data,
                },()=>{this.setAddress()});
                //console.log(this.state.locatorList,'screen componentWillReceiveProps dinesh');
                //this.setAddress(this.state.locatorList);
            }
       }

    }

    getCurrentLocation = ()=>{
        if (window.navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(
                (position)=> {
                    this.setState({
                        lat : position.coords.latitude,
                        long : position.coords.longitude,

                    })

                },
                function() {notify.show("Geolocation not available.","error")}
            );

        }

        this.setState({
            showMap : true
        })
        //console.log(showMap,'dinesh');
    }

    updateLatLongValue = (data)=>{
        latLongData = data;
        return;
   }
   setAddress=()=>{
     
   }

    render() {
      
        return (

            <div className="locator_outer checkout_outer">
                <h1 className="category_name text-left">{local.headerStoreLocator[this.state.language]}</h1>
                <p className="detail mt-3">
                    Our seamless online and in-store experiences allow you to shop, make returns, and earn rewards on all your purchases across brands online or in-store.
                </p>
                <div className="d-flex align-items-center mt-4">
                    <form className="row align-items-end">
                        <FormControl className="formControl d-block slect_outer col-sm-3">
                          <InputLabel shrink htmlFor="city-placeholder">
                            {local.commonSelectCity[this.state.language]}
                          </InputLabel>
                          <Select
                            value={this.state.select_city}
                            onChange={this.handleChange}
                            input={<Input name="select_city" id="city-placeholder" />}
                            displayEmpty
                            name="select_city"
                            className="selectEmpty"
                          >
                            {
                            this.state.cityData.response && this.state.cityData.response.map(city => (
                                <MenuItem value={city.id}>{city.city}</MenuItem>
                            ))

                        }
                          </Select>
                        </FormControl>
                        <FormControl className="formControl d-block slect_outer col-sm-3">
                          <InputLabel shrink htmlFor="brand-placeholder">
                            Select Brand
                          </InputLabel>
                          <Select
                            value={this.state.select_brand}
                            onChange={this.handleChange}
                            input={<Input name="select_brand" id="brand-placeholder" />}
                            displayEmpty
                            name="select_brand"
                            className="selectEmpty"
                          >
                           {
                            this.state.brandMenuData.response && this.state.brandMenuData.response.map(brand => (
                                <MenuItem value={brand.brand_id}>{brand.brand_name}</MenuItem>
                            ))

                        }
                          </Select>
                        </FormControl>

                        <FormControl className="formControl d-block slect_outer col-sm-3">
                          <InputLabel shrink htmlFor="mall-placeholder">
                            Select Mall
                          </InputLabel>
                          <Select
                            value={this.state.select_mall}
                            onChange={this.handleChange}
                            input={<Input name="select_mall" id="mall-placeholder" />}
                            displayEmpty
                            name="select_mall"
                            className="selectEmpty"
                          >
                           {
                            this.state.mallList.data && this.state.mallList.data.map(mall => (
                                <MenuItem value={mall.mall_id}>{mall.mall_name}</MenuItem>
                            ))

                        }
                          </Select>
                        </FormControl>

                        <Button
                            variant="contained"
                            color="primary"
                            className={"continue col-sm-3 "+ this.state.searchBtnDisable}
                            onClick={() => {this.validate()}}
                        >
                            Search
                        </Button>
                    </form>
                </div>
                <div className="row mt-md-5 mt-4">
                {/*this.state.storeDataMap.length?<PinMap storeValue = {this.state.storeDataMap} ref={instance => { this.child = instance;}} />:"" */}

                    <div className="col-md-6 left_block">
                        {/*<div className="mb-3 d-sm-inline-block d-md-none">
                            <span className="locatn_sec"><svg className="icon icon-gps"><use xlinkHref="/img/icon-sprite.svg#icon-gps"></use></svg>
                                <a onClick={this.getCurrentLocation.bind(this)} href="Javascript:void(0);">{local.addrUseCurrenLocation[this.state.language]}</a>
                            </span>
                        </div>*/}
                          <div className="map_section position-relative">
                              <div className="address_detail position-absolute">
                                  <h6>Zara</h6>
                                  <p>Western Ring Road, Exit 27, Al Badee District، Alawali, Riyadh 14924, Saudi Arabia</p>
                                  <p>+966 11 217 9691</p>
                                  <p>Opens at 04:00 pm</p>
                                  <div className="mt-2">
                                      <a href="Javascript:void(0);">Shop Here</a>
                                      <a href="Javascript:void(0);">{local.checkDirection[this.state.language]}</a>
                                  </div>
                                </div>
                              {/*<img src="/img/map.jpg" />*/}
                              {this.state.showMap && <LocatorMap
                                                        updateLatLong={this.updateLatLongValue}
                                                        address = {this.state.locatorList}
                                                        lat =  {this.state.lat}
                                                        lng = {this.state.long}
                                                         />}


                          </div>
                    </div>
                    <div className="col-md-6 right_block">
                    <div className="loatn_outer pb-3 mb-3 d-none d-md-block">
                        <span className="locatn_sec"><svg className="icon icon-gps"><use xlinkHref="/img/icon-sprite.svg#icon-gps"></use></svg>
                            <a onClick={this.getCurrentLocation.bind(this)} href="Javascript:void(0);">{local.addrUseCurrenLocation[this.state.language]}</a>
                        </span>
                    </div>
                    <ul className="store_list" style={{'height': 480 ,'overflow-y': 'scroll'}}>
                    {
                        this.state.locatorList.data && (this.state.locatorList.data != "No Data Found") ? ( this.state.locatorList.data.map(locations => (

                            <li>
                                <span><svg className="icon icon-location-pointer"><use xlinkHref="/img/icon-sprite.svg#icon-location-pointer"></use></svg></span>
                                <div className="d-inline-block align-top">
                                    <h6 className="brand_name">{locations.brand_name} - {locations.name}, {locations.city_name}</h6>
                                    <p className="address">{locations.address}</p>
                                    <p className="address">{convertTimeToAM(locations.from_time)} - {convertTimeToAM(locations.to_time)}</p>

                                    <div className="contct">
                                        <span>
                                            <svg className="icon icon-phone-receiver"><use xlinkHref="/img/icon-sprite.svg#icon-phone-receiver"></use></svg>
                                            <a href="Javascript:void(0);">{locations.phone}</a>
                                        </span>
                                        <span>
                                            <svg className="icon icon-shopping-cart"><use xlinkHref="/img/icon-sprite.svg#icon-shopping-cart"></use></svg>
                                            <a href="Javascript:void(0);" onClick={()=>this.goToMicrosite(locations.seo)}>Shop Here</a>
                                        </span>
                                        {/*<span>
                                            <svg className="icon icon-compass"><use xlinkHref="/img/icon-sprite.svg#icon-compass"></use></svg>
                                            <a href="Javascript:void(0);">{local.checkDirection[this.state.language]}</a>
                                        </span>*/}
                                    </div>
                                </div>
                            </li>
                          ))) : (<div><div className="pt-2">{local.commonNoStoreFound[this.state.language]}</div>
                        <div className="">{local.commonNoStoreNextFound[this.state.language]}</div></div>
                          )
                        }
                        </ul>
                    </div>
                </div>
            </div>
        );
    }
}

function mapStateToProps(state) {

    return {
        example: state.example,
        languageReducer: state.languageReducer,
        cityData: state.cityReducer,
        brandMenuReducer : state.brandMenuReducer,
        mallData : state.mallReducer,
        locatorData : state.locatorReducer

    }
}

export default connect(mapStateToProps)(withStyles ()(SelectLocator));
