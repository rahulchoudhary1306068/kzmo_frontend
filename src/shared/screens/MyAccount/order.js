import React, { Component } from 'react';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import {
  MuiThemeProvider,
  createMuiTheme,
  createGenerateClassName,
} from '@material-ui/core/styles';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import TextField from '@material-ui/core/TextField';
import FormGroup from '@material-ui/core/FormGroup';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Sidebar from '../../components/account_sidebar';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';

import Loader from "../../components/loader";
import { months, days, getUserInfo,getDayIndex } from '../../commonfunction';


import { userOrderAction, cancelOrderAction,
 returnListAction,cancelReturnAction } from '../../actions/myAccount';
import { InvalidTokenAction } from '../../actions/login';
import local, {deafultLanguage} from '../../localization';
import {getter} from '../../commonfunction';

import { orderInvoiceStatus,base_URL,api_KEY } from '../../constant';
import {statusesForEDD} from '../../commonConstant';
import {notify} from 'react-notify-toast';

const mui = createMuiTheme({
  overrides: {
    MuiFormLabel: {
      root: {
        color: "#b1b1b1"
      },
      focused: {
        color: "black !important"
      },
    }
  }
});

const styles = theme => ({
  root: {
    '&$checked': {
      color: '#e4b122'
    }
  },
  checked: {},

});

const OrderList = (props) => (
    <div>
        <ul className="nav nav-tabs" role="tablist">
          <li className="nav-item">
            <a className="nav-link active" href="#order_detail" role="tab" data-toggle="tab" >{local.orderOrders[props.language]}</a>
          </li>
          <li className="nav-item">
            <a className="nav-link" href="#return_detail" role="tab" data-toggle="tab" onClick={() => props.getReturnList()}>{local.orderReturn[props.language]}</a>
            {/*<a className="nav-link" href="Javascript:void(0)" role="tab" >My Returns <span className="common_tooltip down">Coming soon</span></a>*/}
          </li>
        </ul>
        <div className="tab-content">
            <div role="tabpanel" className="tab-pane fade in active show" id="order_detail">
                <ul className="listing_outer">
                    {
                        props.orderList.data && (props.orderList.data.summary ? (props.orderList.data.summary.map((data, index) => {
                            let date = new Date(parseInt(data.timestamp)*1000);
                            let day = date.getDate();
                            let month = date.getMonth();
                            let year = date.getFullYear();
                            let weekDay = getDayIndex(date.getDay());

                            let deliveryTimestamp = new Date(parseInt(data.pdd_edd.edd)*1000);
                            let deliveryTime = days[getDayIndex(deliveryTimestamp.getDay())] + ", " + deliveryTimestamp.getDate() + " " + months[deliveryTimestamp.getMonth()] + " " + deliveryTimestamp.getFullYear();
                            console.log(data.pdd_edd.edd,"====Time Stamps===");
                            data.showTrackOrder = false;

                            return(
                                <li className="listing">
                                    <div className="d-flex">
                                        <div className="buffer_prop details">

                                            <p className="mb-0 ordr_detail">{local.orderId[props.language]}:<a href="Javascript:void(0);" onClick={() => props.goToOrderDetail(data.order_id)}>{data.order_id}</a></p>
                                            <p className="mb-3 placing_dte">{local.orderPlacedOn[props.language]}{days[weekDay] + ", " + day + " " + months[month] + " " + year}</p>
                                            {
                                                Object.keys(props.orderList.data.details[index]).map((detailKey, detailIndex) => {
                                                    let prodData = props.orderList.data.details[index][detailKey];
                                                    return(
                                                        <div>
                                                            <img src={prodData.product_image_path} onClick={() => props.goToProduct(prodData.seo_name)} />
                                                            <div className="inner">
                                                                <p className="name"><a href="JavaScript:void(0);" onClick={() => props.goToProduct(prodData.seo_name)}>{prodData.product_name}</a></p>
                                                                {
                                                                    prodData.product_options && typeof(prodData.product_options) == "object" && prodData.product_options.map(option => (
                                                                        <p className="color_name">{option.option_name}: {option.variant_name}</p>
                                                                    ))
                                                                }
                                                            </div>
                                                        </div>
                                                    )
                                                })
                                            }


                                            {props.validateEDDStatus(data.status) && data.pdd_edd.edd && <p className="font-weight-bold mb-0 mt-3">{parseInt(new Date().getTime())-86400000 <= parseInt(data.pdd_edd.edd)*1000 ? "Delivery expected by" : "Delivered on" } {deliveryTime}</p>}
                                            <p className="mb-0">{data.status_name}</p>
                                        </div>
                                        <div className="inner_right text-right">
                                            <p>
                                                {data.trackOrderFlag == 1 && <a className="links_detail" href="JavaScript:void(0);" onClick={() => props.goToTrackOrder(data.order_id)}>{local.footerTrackOrder[props.language]}</a>}
                                                {data.allow_cancellation == 1 && <a className="links_detail" data-toggle="modal" data-target={"#cancel_order"+index} href="JavaScript:void(0);">{local.orderCancel[props.language]}</a>}
                                                {data.allow_return == "Y" && props.checkStatus(data.status) && <a className="links_detail" href="JavaScript:void(0);" onClick={() => props.goToCreateReturn(index)}>{local.orderReturnItem[props.language]}</a>}
                                                <a style={{display: "none"}} id="select-return-item-popup-link" href="Javascript:void(0);" data-toggle="modal" data-target="#select_product"> </a>
                                            </p>
                                            <p className="m-0">
                                            { props.checkStatus(data.status) &&
                                                <a className="links_detail" href="JavaScript:void(0);" onClick={() => props.downloadInvoice(data.order_id,data.status)}>{local.orderInvoice[props.language]}{/*<span className="common_tooltip down">{local.orderComingSoon[props.language]}</span>*/}</a>
                                            }
                                                <a className="links_detail" href="JavaScript:void(0);" onClick={() => props.goToNeedHelp(data.order_id)}>{local.orderNeedHelp[props.language]}</a>
                                            </p>
                                        </div>
                                    </div>
                                    {
                                        data.showTrackOrder ? (<ul className="trackorder_outer d-flex justify-content-between mt-sm-5 mb-3 position-relative one-third">
                                            <li className="complete d-flex flex-sm-column">{local.orderOrdered[props.language]}</li>
                                            <li className="complete align-items-sm-center d-flex flex-sm-column">{local.orderPacked[props.language]}</li>
                                            <li className="active align-items-sm-center d-flex flex-sm-column">{local.orderShipped[props.language]}</li>
                                            <li className="disable align-items-sm-end d-flex flex-sm-column">{local.orderDelivered[props.language]}</li>
                                        </ul>) : ""
                                    }
                                    {data.allow_cancellation == 1 && <CancelOrderPopup data={data} index={index} orderList={props.orderList} selectCancelReason={props.selectCancelReason} cancelOrder={props.cancelOrder} language={props.language} validateEDDStatus={props.validateEDDStatus}/>}
                                </li>
                            )
                        })) : (<div>{props.orderList.data.msg}</div>))
                    }
                    {/*<li className="listing">
                        <div className="d-flex">
                            <div className="buffer_prop details">
                                <p className="mb-0 ordr_detail">Order ID: <a href="Javascript:void(0);">157818879</a></p>
                                <p className="mb-3 placing_dte">Order Placed on Fri, 28 Sep 2018</p>
                                <img src="/img/temp/section8.png" />
                                <div className="inner">
                                    <p className="name"><a href="JavaScript:void(0);">Paisley Patch Dress</a></p>
                                    <p className="color_name">Color: Amber</p>
                                    <p className="size_name">Size: EU 44</p>
                                </div>
                                <p className="font-weight-bold mb-0 mt-3">Delivered on Fri, Oct 11th '18</p>
                                <p className="mb-0">Package was handed directly to the customer. </p>
                            </div>
                            <div className="inner_right text-right">
                                <p><a className="links_detail" href="JavaScript:void(0);">Review Product</a>
                                <a className="links_detail" href="JavaScript:void(0);">Return Item</a></p>
                                <a className="links_detail" href="JavaScript:void(0);">Need Help?</a>
                            </div>
                        </div>
                    </li>*/}

                </ul>
                { props.orderCurrentPage < props.orderTotalPage && <a href="JavaScript:void(0);" className="show_more" onClick={() => props.loadMoreOrders()}>{local.rewardShowMore[props.language]}</a> }
            </div>
            <ReturnList
                returnList={props.returnList}
                language={props.language}
                confirmCancel={props.confirmCancel}
                uncheckConfirmCalcel={props.uncheckConfirmCalcel}
                cancelReturn={props.cancelReturn}
                goToReturnDetail={props.goToReturnDetail}
                goToProduct={props.goToProduct}
                goToNeedHelp={props.goToNeedHelp}
            />
            <ChooseReturnItem
                slectedReturnItems={props.slectedReturnItems}
                itemIdToReturn={props.itemIdToReturn}
                selectItemToReturn={props.selectItemToReturn}
                classes={props.classes}
                openCreateReturn={props.openCreateReturn}
            />
        </div>
        {/*popup cancel order*/}
        {/*Cancel order */}

    </div>
);

const ReturnList = (props) => {
    return (
        <div role="tabpanel" className="tab-pane fade" id="return_detail">
            <ul className="listing_outer">
                {
                    props.returnList.return_requests && (props.returnList.return_requests.length > 0 ? (props.returnList.return_requests.map((returnRequest, index) => {
                        let returnDate = new Date(parseInt(returnRequest.timestamp)*1000);
                        let formatedReturnDate = days[getDayIndex(returnDate.getDay())] + ", " + returnDate.getDate() + " " + months[returnDate.getMonth()] + " " + returnDate.getFullYear();
                        return(
                            <li className="listing">
                                <div className="d-flex">
                                    <div className="buffer_prop details">
                                        <p className="mb-0 ordr_detail">

                                            <span>{local.orderId[props.language]}:<a href="Javascript:void(0);">{returnRequest.order_id}</a></span>
                                            <span className="pl-4">{local.orderReturnId[props.language]}<a href="Javascript:void(0); "  onClick={() => props.goToReturnDetail(returnRequest.order_id, returnRequest.return_id)}>{returnRequest.return_id}</a></span>

                                        </p>
                                        <p className="mb-3 placing_dte">{local.orderReturnRequest[props.language]}{formatedReturnDate}</p>
                                        <img src={returnRequest.product_image.image_path[0]} onClick={() => props.goToProduct(returnRequest.seo_name)} />
                                        <div className="inner">
                                            <p className="name"><a href="JavaScript:void(0);" onClick={() => props.goToProduct(returnRequest.seo_name)}>{returnRequest.product}</a></p>
                                            {
                                                returnRequest.product_options && typeof(returnRequest.product_options) == "object" && returnRequest.product_options.map(option => (
                                                    <p className="color_name">{option.option_name}: {option.variant_name}</p>
                                                ))
                                            }
                                            {/*<p className="color_name">Total Amount: $45</p>
                                            <p className="size_name">Qty: 1</p>*/}
                                        </div>
                                        <p className="font-weight-bold mb-0 mt-3">{returnRequest.status_text}</p>
                                    </div>
                                    <div className="inner_right text-right">

                                        {returnRequest.cancel_return_link == "Y" && <p><a className="links_detail" data-toggle="modal" data-target={"#cancel_return" + index} href="JavaScript:void(0);" onClick={() => props.uncheckConfirmCalcel(index)}>{local.orderCancelReturn[props.language]}</a></p>}
                                        <a className="links_detail" href="JavaScript:void(0);" onClick={() => props.goToNeedHelp(returnRequest.order_id)}>{local.orderNeedHelp[props.language]}</a>
                                    </div>
                                </div>
                                <CancelReturnPopup
                                    data={returnRequest}

                                    language={props.language}

                                    index={index}
                                    confirmCancel={props.confirmCancel}
                                    cancelReturn={props.cancelReturn}
                                />
                                {/*<div className="innerorder_detail mt-4 pt-4">
                                    <h5>Return Information</h5>
                                    <ul className="return_detail">
                                        <li className="d-flex justify-content-between"><p>Return reason:</p><p className="buffer-color">Size is incorrect</p></li>
                                        <li className="d-flex justify-content-between"><p>You have asked for: </p><p className="buffer-color">Refund</p></li>
                                        <li className="d-flex justify-content-between"><p>How will return shipped:  </p><p className="buffer-color">KZMO to pick this order</p></li>
                                        <li className="d-flex justify-content-between"><p>Return ID: </p><p className="buffer-color">157845677</p></li>
                                        <li className="d-flex justify-content-between"><p>Return created on: </p><p className="buffer-color">28 Sep 2108</p></li>
                                        <li className="d-flex justify-content-between"><p>Return status: </p><p className="buffer-color">Request initiated</p></li>
                                    </ul>
                                </div>
                                <div className="needhelp_outer mt-4 pt-4">
                                    <form>
                                        <MuiThemeProvider theme={mui}>
                                           <TextField
                                              id="comment"
                                              className="input_outer textField mt-1"
                                              label="Write new comment"
                                              multiline
                                              rowsMax="10"
                                              rows="5"
                                              fullWidth
                                              variant="outlined"
                                            />
                                        </MuiThemeProvider>
                                        <Button variant="contained" color="primary" type="button" >
                                            Add comment
                                        </Button>
                                    </form>
                                    <h2 className="mb-4 mt-3">Recent Comments</h2>
                                    <ul className="comment_list">
                                        <li>
                                            <p>The size is incorrect for me , hense I am returning the product.</p>
                                            <div>Rakesh gaur <span>05:13 pm, 3 Oct, 2018</span></div>
                                        </li>
                                        <li>
                                            <p>The size is incorrect for me , hense I am returning the product.</p>
                                            <div>Rakesh gaur <span>05:13 pm, 3 Oct, 2018</span></div>
                                        </li>
                                        <li>
                                            <p>The size is incorrect for me , hense I am returning the product.</p>
                                            <div>Rakesh gaur <span>05:13 pm, 3 Oct, 2018</span></div>
                                        </li>
                                    </ul>
                                </div>*/}
                            </li>
                        )
                    })) : (<div>{local.orderNoReturnRequest[props.language]}</div>))
                }

                {/*<li className="listing">
                    <div className="d-flex">
                        <div className="buffer_prop details">
                            <p className="mb-0 ordr_detail">
                                <span>Order ID: <a href="Javascript:void(0);">157818879</a></span>
                                <span className="pl-4">Return ID: <a href="Javascript:void(0);">157818879</a></span>
                            </p>
                            <p className="mb-3 placing_dte">Return requested on Fri, 28 Sep 2018</p>
                            <img src="/img/temp/section8.png" />
                            <div className="inner">
                                <p className="name"><a href="JavaScript:void(0);">Paisley Patch Dress</a></p>
                                <p className="color_name">Total Amount: $45</p>
                                <p className="size_name">Qty: 1</p>
                            </div>
                            <p className="font-weight-bold mb-0 mt-3">Return request completed </p>
                        </div>
                        <div className="inner_right text-right">
                            <a className="links_detail" href="JavaScript:void(0);">Need Help?</a>
                        </div>
                    </div>
                </li>*/}
            </ul>
        </div>
    )
}

const CancelOrderPopup = (props) => {
    let date = new Date(parseInt(props.data.timestamp)*1000);
    let day = date.getDate();
    let month = date.getMonth();
    let year = date.getFullYear();

    let deliveryTimestamp = new Date(parseInt(props.data.pdd_edd.edd)*1000);
    let deliveryTime = days[getDayIndex(deliveryTimestamp.getDay())] + ", " + deliveryTimestamp.getDate() + " " + months[deliveryTimestamp.getMonth()] + " " + deliveryTimestamp.getFullYear();

    let cancelReason = props.orderList.data.reasons;
    return(
        <div className="modal canceloder_outer order_outerpopup" id={"cancel_order"+props.index}>
            <div className="modal-dialog">
                <div className="modal-content">
                    <div className="modal-body">
                        <button type="button" id={"store-close" + props.index} className="close" data-dismiss="modal">&times;</button>
                        <h2 className="text-center mb-4">{local.orderCancel[props.language]}</h2>
                        <p>{local.orderId[props.language]}:{props.data.order_id}   <span>{local.orderPlacedOn[props.language]} {day + " " + months[month] + " " + year}</span></p>

                        {
                            Object.keys(props.orderList.data.details[props.index]).map((detailKey, detailIndex) => {
                                let prodData = props.orderList.data.details[props.index][detailKey];
                                return(
                                    <div className="details">
                                        <img src={prodData.product_image_path} />
                                        <div className="inner">
                                            <p className="name"><a href="JavaScript:void(0);">{prodData.product_name}</a></p>
                                            <p className="color_name">{local.orderTotalAmount[props.language]}{prodData.price}</p>
                                            <p className="size_name">{local.orderOty[props.language]}{prodData.amount}</p>
                                        </div>
                                    </div>
                                )
                            })
                        }
                        {/*<p className="font-weight-bold mb-0 mt-3">Delivered on Fri, Oct 11th '18</p>
                        <p className="mb-0">Your item has been shipped.</p>*/}
                        {props.validateEDDStatus(props.data.status) && props.data.pdd_edd.edd && <p className="font-weight-bold mb-0 mt-3">{new Date().getTime() < parseInt(props.data.pdd_edd.edd)*1000 ? "Delivery expected by" : "Delivered on" } {deliveryTime}</p>}
                        <p className="mb-0">{props.data.status_name}</p>
                        <form>
                            <FormControl className="formControl d-block slect_outer mb-4 mt-5">
                              <InputLabel shrink htmlFor="city-placeholder">
                                {local.orderSelectReason[props.language]}
                              </InputLabel>
                              <Select
                                value={props.data.cancelReasonId}
                                input={<Input name="select_reason" id="cancel-placeholder" />}
                                displayEmpty
                                name="select_reason"
                                className="selectEmpty"
                                onChange={(e) => props.selectCancelReason(e.target.value, props.index)}
                              >
                                {
                                    cancelReason.map(reason => (
                                        <MenuItem value={reason.reason_id}>{reason.reason}</MenuItem>
                                    ))
                                }
                              </Select>
                            </FormControl>
                            <Button type="button" className="cancel_order_btn" variant="contained" color="primary" type="button" disabled={!props.data.cancelReasonId} onClick={() => props.cancelOrder(props.index)}>
                                {local.orderSubmit[props.language]}
                            </Button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    )
}

const CancelReturnPopup = (props) => {
    let returnDate = new Date(parseInt(props.data.timestamp)*1000);
    let formatedReturnDate = returnDate.getDate() + " " + months[returnDate.getMonth()] + " " + returnDate.getFullYear();
    return(
        <div className="modal canceloder_outer order_outerpopup" id={"cancel_return"+props.index}>
            <div className="modal-dialog">
                <div className="modal-content">
                    <div className="modal-body">

                        <button type="button" id={"cancel-return-close"+props.index} className="close" data-dismiss="modal">&times;</button>
                        <h2 className="text-center mb-4">{local.orderCancelReturnRequest[props.language]} </h2>
                        <p>{local.orderId[props.language]}:{props.data.order_id}   <span>{local.orderPlacedOn[props.language]} {formatedReturnDate}</span></p>


                        <div className="details">
                            <img src="/img/temp/section8.png" />
                            <div className="inner">

                                <p className="name"><a href="JavaScript:void(0);">{props.data.product}</a></p>
                                {
                                    props.data.product_options && typeof(props.data.product_options) == "object" && props.data.product_options.map(option => (
                                        <p className="color_name">{option.option_name}: {option.variant_name}</p>
                                    ))
                                }
                                {/*<p className="color_name">Total Amount: $45</p>
                                <p className="size_name">Qty: 1</p>*/}

                            </div>
                        </div>
                        <form>
                            <FormGroup className="w-100 input_checkbox">
                                <FormControlLabel
                                  control={
                                    <Checkbox />
                                  }
                                  label={local.orderConfirmToCancel[props.language]}
                                  name = "confirmcancelSS"
                                  checked={props.data.confirmCancel}
                                  onChange={(e) => props.confirmCancel(e, props.index)}
                                />
                            </FormGroup>

                            <Button class="cancel_order_btn" variant="contained" color="primary" type="button" disabled={!props.data.confirmCancel} onClick={() => props.cancelReturn(props.data.return_id, props.data.order_id, props.index)}>
                                {local.orderSubmit[props.language]}

                            </Button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    )
}

const ChooseReturnItem = (props) => {
    const { classes } = props.classes;
    return(
        <div className="modal  choose_produt canceloder_outer address_outerpopup order_outerpopup" id="select_product">
            <div className="modal-dialog">
                <div className="modal-content">
                    <div className="modal-body">
                        <button type="button" id="" className="close" data-dismiss="modal">&times;</button>
                        <h2 className="text-center">Select a product to return</h2>
                        {
                            props.slectedReturnItems && <form>
                                <RadioGroup
                                    aria-label="Address Type"
                                    name="returnType"
                                    value={props.itemIdToReturn}
                                    onChange = {e=>props.selectItemToReturn(e.target.value)}
                                    helperText = ""
                                    className="radio_outer"
                                >
                                    {
                                        Object.keys(props.slectedReturnItems).map((key) => {
                                            let item = props.slectedReturnItems[key];
                                            return(
                                                <FormControlLabel
                                                  value={item.order_id + '/' + item.item_id}
                                                  control={
                                                    <Radio
                                                      classes={{root: classes.root, checked: classes.checked}}
                                                    />
                                                  }
                                                  label={
                                                     <div className="details mb-4">
                                                        <img src={item.product_image_path} />
                                                        <div className="inner">
                                                            <p className="name"><a href="JavaScript:void(0);" >{item.product}</a></p>
                                                            <p className="color_name"> Order ID: <a href="Javascript:void(0);" >{item.order_id}</a></p>
                                                        </div>
                                                    </div>
                                                  }
                                                  labelPlacement="end"
                                                />
                                            )
                                        })
                                    }
                                </RadioGroup>
                                <button type="button" onClick={() => props.openCreateReturn(props.itemIdToReturn,true)}>Continue</button>
                                
                            </form>
                        }
                    </div>
                </div>
            </div>
        </div>
    )
}

class MyOrder extends Component {
    constructor(props) {
        super(props);
        this.state = {
            userId: "",
            orderList: {},
            orderCurrentPage: 1,
            orderTotalPage: 1,
            returnList: {},
            cancelReturnData: {},
            language: '',
            slectedReturnItems: "",
            itemIdToReturn: ""
        }
    }

    componentDidMount(){

        this.setState({
            language: getter('language') ? getter('language') : deafultLanguage
        })

        let userId = localStorage.getItem('userId');
        this.setState({userId: userId}, () => {
            this.getOrders();
        });
    }

    componentWillReceiveProps(nextProps){

        if(nextProps.languageReducer.value){
            this.setState({
                language: nextProps.languageReducer.value
            })
        }

        if(nextProps.orderList != this.props.orderList){
            if(nextProps.orderList.message && nextProps.orderList.message.status == "failed"){
                if(nextProps.orderList.message.extra == "_api_param_invalid_token" || nextProps.orderList.message.extra == "_api_param_invalid_auth" || nextProps.orderList.message.extra == "_api_param_expired_token"){
                    this.props.dispatch(InvalidTokenAction());
                }
            }
            else{console.log("nextProps.orderList", nextProps.orderList);
                if(this.state.orderCurrentPage == 1){
                    this.setState({orderList: nextProps.orderList});
                    this.setState({orderTotalPage: Math.ceil(Number(nextProps.orderList.data.total_orders)/10)});
                }
                else{
                    let orderList = {...this.state.orderList};
                    orderList.error = nextProps.orderList.error;
                    orderList.isFetching = nextProps.orderList.isFetching;
                    orderList.message = nextProps.orderList.message;
                    orderList.retry = nextProps.orderList.retry;
                    orderList.result = nextProps.orderList.result;
                    if(nextProps.orderList.result){
                        orderList.data.details = orderList.data.details.concat(nextProps.orderList.data.details);
                        orderList.data.summary = orderList.data.summary.concat(nextProps.orderList.data.summary);
                    }
                    console.log("orderListorderList", orderList);
                    this.setState({orderList: orderList});
                }
            }
        }

        if(nextProps.returnList != this.props.returnList){
            if(nextProps.returnList.message && nextProps.returnList.message.status == "failed"){
                if(nextProps.returnList.message.extra == "_api_param_invalid_token" || nextProps.returnList.message.extra == "_api_param_invalid_auth"){
                    this.props.dispatch(InvalidTokenAction());
                }
            }
            else{
                this.setState({returnList: nextProps.returnList});
            }
        }
    }

    getOrders(){
        this.props.dispatch(userOrderAction(this.state.orderCurrentPage));
    }

    selectCancelReason(val, index){
        let orderList = {...this.state.orderList};
        orderList.data.summary[index]["cancelReasonId"] = val;
        this.setState({orderList: orderList});
    }

    cancelOrder(index){
        let data = {
            "order_id": this.state.orderList.data.summary[index].order_id,
            "user_id": this.state.userId,
            "reasons": this.state.orderList.data.summary[index].cancelReasonId,
            "comment": ""
        }
        document.getElementById("store-close" + index).click()
        this.props.dispatch(cancelOrderAction(data));
    }

    goToProduct(prodId){
        this.props.history.push('/pd/' + prodId+".html");
    }

    goToOrderDetail(orderId){
        this.props.history.push('/myaccount/order/' + orderId);
    }

    goToNeedHelp(orderId){
        this.props.history.push('/myaccount/need-help/' + orderId);
    }

    goToTrackOrder(orderId){
        this.props.history.push('/myaccount/track-order/' + orderId);
    }

    getReturnList(){
        if(!(this.state.returnList.data && this.state.returnList.data.return_requests)){
            this.props.dispatch(returnListAction());
        }
    }

    confirmCancel(e, index){
        let returnList = this.state.returnList;
        returnList.data.return_requests[index]["confirmCancel"] = e.target.checked;
        this.setState({returnList: returnList});
    }

    uncheckConfirmCalcel(index){
        let returnList = this.state.returnList;
        returnList.data.return_requests[index]["confirmCancel"] = false;
        this.setState({returnList: returnList},  () => {

        });
    }

    cancelReturn(returnId, orderId, index){
        let data = {
            returnId: returnId,
            orderId: orderId
        }
        document.getElementById("cancel-return-close" + index).click();
        this.props.dispatch(cancelReturnAction(data))
    }

    goToReturnDetail(orderId, returnId){
        this.props.history.push("/myaccount/return/" + orderId + "/" + returnId);
    }

    goToCreateReturn(index){
        let orderSummery = this.state.orderList.data.summary;
        let orderDetail = this.state.orderList.data.details[index];
        if(Object.keys(orderDetail).length > 1){
            this.setState({slectedReturnItems: orderDetail});
            this.setState({itemIdToReturn: ""});
            document.getElementById("select-return-item-popup-link").click();
            console.log("orderDetail", orderDetail);
        }
        else{
            let itemID = Object.keys(orderDetail)[0];
            this.openCreateReturn(orderSummery[index].order_id + "/" + itemID);
        }
    }

    openCreateReturn(url,flag=false){
        if(!this.state.itemIdToReturn && flag)
        {
            notify.show(local.returnProductMessage[this.state.language]);
        }
        else
        {
            this.props.history.push("create-return/" + url);   
        }
    }

    selectItemToReturn(id){
        this.setState({itemIdToReturn: id});
    }

    loadMoreOrders(){
        this.setState({orderCurrentPage: this.state.orderCurrentPage + 1}, () => {
            this.getOrders();
        });
    }

    downloadInvoice(orderId,status){
        if(orderInvoiceStatus.indexOf(status) > -1) {
            window.open(base_URL + "order_invoice?key="+api_KEY+"&order_id=" + orderId , "_self");
        }else {
            console.log('Invalid Order Status');
            return "JavaScript:void(0);";
        }
    }

    checkStatus(status) {
        if(orderInvoiceStatus.indexOf(status) > -1) {
            return true;
        }
        return false;
    }
    validateEDDStatus(status) {
    if(statusesForEDD.indexOf(status) > -1) {
    return true;
    }
    return false;
    }
    render() {
        return (
            <div className="col-md-9 right_sec order_outer mobile_order_detail">
                <OrderList
                    orderList={this.state.orderList}
                    selectCancelReason={(val, index) => this.selectCancelReason(val, index)}
                    cancelOrder={(index) => this.cancelOrder(index)}
                    goToProduct={(prodId) => this.goToProduct(prodId)}
                    goToOrderDetail={(orderId) => this.goToOrderDetail(orderId)}
                    goToNeedHelp={(orderId) => this.goToNeedHelp(orderId)}
                    downloadInvoice={(orderId,status) => this.downloadInvoice(orderId,status)}
                    checkStatus={(status) => this.checkStatus(status)}
                    goToTrackOrder={(orderId) => this.goToTrackOrder(orderId)}
                    getReturnList={() => this.getReturnList()}
                    returnList={this.props.returnList.data}
                    language={this.state.language}
                    confirmCancel={(e, index) => this.confirmCancel(e, index)}
                    uncheckConfirmCalcel={(index) => this.uncheckConfirmCalcel(index)}
                    cancelReturn={(returnId, orderId, index) => this.cancelReturn(returnId, orderId, index)}
                    goToReturnDetail={(orderId, returnId) => this.goToReturnDetail(orderId, returnId)}
                    goToCreateReturn={(index) => this.goToCreateReturn(index)}
                    slectedReturnItems={this.state.slectedReturnItems}
                    itemIdToReturn={this.state.itemIdToReturn}
                    selectItemToReturn={(id) => this.selectItemToReturn(id)}
                    classes={this.props}
                    openCreateReturn={(url,flag) => this.openCreateReturn(url,flag)}
                    orderTotalPage = {this.state.orderTotalPage}
                    orderCurrentPage = {this.state.orderCurrentPage}
                    loadMoreOrders = {() => this.loadMoreOrders()}
                    validateEDDStatus = {(status) => this.validateEDDStatus(status)}

                />
                {(this.state.orderList.isFetching || this.state.returnList.isFetching) && <Loader />}
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        orderList: state.orderReducer,
        returnList: state.returnListReducer,
        languageReducer: state.languageReducer
    }
}

export default connect(mapStateToProps)(withStyles(styles)(MyOrder));
