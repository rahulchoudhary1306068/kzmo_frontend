import React from 'react';
import {connect} from 'react-redux';

import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';

import {
  MuiThemeProvider,
  createMuiTheme,
  createGenerateClassName,
} from '@material-ui/core/styles';

import Loader from "../../components/loader";

import TextField from '@material-ui/core/TextField';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import Button from '@material-ui/core/Button';
import FormControlLabel from '@material-ui/core/FormControlLabel';

import { userInfoAction, editUserInfoAction, changeUserPasswordAction } from '../../actions/myAccount';

import { getUserLoginToken, getUserTTLToken, createMD5, checkRequired, checkPhonenumber, checkPassword, checkconfirmPassword } from '../../commonfunction';
import { InvalidTokenAction } from '../../actions/login';
import local, {deafultLanguage} from '../../localization';
import {getter} from '../../commonfunction';

const styles = theme => ({
  root: {
    '&$checked': {
      color: '#e4b122'
    }
  },
  checked: {},
  
});

const mui = createMuiTheme({
  overrides: {
    MuiFormLabel: {
      root: {
        color: "#b1b1b1"
      },
      focused: {
        color: "black !important"
      },
    }
  }
});

class Profile extends React.Component {
	constructor(props) {
        super(props);
        this.state = {
            userInfo: {},
            userData: {},
						userId: "",
						firstNameError : "",
						lastnameError : "",
						genderError : "",
						phoneError: "",
						emailError : "",
						newPassError : "",
						confirmPassError : "",
						currentPassError : "",
						language: ''
        };
    }

    componentDidMount(){

			this.setState({
				language: getter('language') ? getter('language') : deafultLanguage
			})

    	let userId = localStorage.getItem('userId');
    	this.setState({userId: userId});
        this.props.dispatch(userInfoAction(userId));
    }

    componentWillReceiveProps(nextProps){

			if(nextProps.languageReducer.value){
				this.setState({
						language: nextProps.languageReducer.value
				}) 
			}
      if(nextProps.userInfo != this.props.userInfo){
			if(nextProps.userInfo.data.status === 'failed'){
				if(nextProps.userInfo.data.extra === '_api_param_invalid_token' || nextProps.userInfo.data.extra == "_api_param_invalid_auth"){		
					this.props.dispatch(InvalidTokenAction());
				}
			}
			else{
				this.setState({userInfo: nextProps.userInfo});
            	this.setState({userData: nextProps.userInfo.data.user_data})
			}

          
      }
    }

    updateState(e){
    	let userData = {...this.state.userData};
    	userData[e.target.name] = e.target.value;
        this.setState({userData : userData,[e.target.name+"Error"] : ""});
    }

    updateProfile(){
		let error = false;
		let Fname = checkRequired(this.state.userData.firstname, this.state.language)
        if(!Fname[0]){
            error =  true;
            this.setState({
              firstnameError : Fname[1]
            })
				}

		let Lname = checkRequired(this.state.userData.lastname, this.state.language)
        if(!Lname[0]){
            error =  true;
            this.setState({
              lastnameError : Lname[1]
            })
		}

		let vgender = checkRequired(this.state.userData.gender, this.state.language)
        if(!vgender[0]){
            error =  true;
            this.setState({
              genderError : vgender[1]
            })
		}

		let vphone = checkPhonenumber(this.state.userData.phone, this.state.language)
        if(!vphone[0]){
            error =  true;
            this.setState({
                phoneError : vphone[1]
            })
		}

		if(!error){
			let data = {
				"firstname": this.state.userData.firstname,
				"lastname": this.state.userData.lastname,
				"phone": this.state.userData.phone,
				"gender": this.state.userData.gender,
				"user_id": this.state.userId
			}
			this.props.dispatch(editUserInfoAction(data));
		}
	}

    changePassword(){
		let error = false;
		let CurrentPass = checkRequired(this.state.userData.currentPass, this.state.language)
        if(!CurrentPass[0]){
            error =  true;
            this.setState({
              currentPassError : CurrentPass[1]
            })
		}
		let NewPass = checkPassword(this.state.userData.newPass, this.state.language)
        if(!NewPass[0]){
            error =  true;
            this.setState({
              newPassError : NewPass[1]
            })
		}
		let ConfirmPass = checkconfirmPassword(this.state.userData.confirmPass,this.state.userData.newPass, this.state.language)
        if(!ConfirmPass[0]){
            error =  true;
            this.setState({
              confirmPassError : ConfirmPass[1]
            })
		}
    	if(!error){
    		let data = {
    			"passwordc": createMD5(this.state.userData.currentPass),
    			"password1": createMD5(this.state.userData.newPass),
    			"password2": createMD5(this.state.userData.confirmPass),
    			"user_id": this.state.userId,
    			"ttl": getUserTTLToken(),
    			"token": getUserLoginToken()
    		}
    		this.props.dispatch(changeUserPasswordAction(data));
    	}
    }

    render() {
    	const { classes } = this.props;

    	if(!this.state.userInfo.isFetching){
    		return (
	    		<div className="col-md-9 right_sec profile_outer">
			        <h2 className="subheading">{local.profPersonelDetails[this.state.language]}</h2>
			        <form className="row">
			            <MuiThemeProvider theme={mui}>
			                <TextField
			                  id="firstname"
			                  name = "firstname"
			                  className="input_outer col-sm-6"
			                  label={local.addrFirstName[this.state.language]}
			                  type="text" 
			                  value={this.state.userData.firstname}
							  onChange = {e=>this.updateState(e)}
							  helperText={this.state.firstnameError}
			                />
			                
			                <TextField
			                  id="lastname"
			                  className="input_outer col-sm-6"
			                  label={local.addrLastName[this.state.language]}
			                  type="text"
			                  name = "lastname" 
			                  value={this.state.userData.lastname}
							  onChange = {e=>this.updateState(e)}
							  helperText={this.state.lastnameError}
			                />
			                <TextField
			                  id="email"
			                  className="input_outer col-sm-6"
			                  label={local.addrEmailAddress[this.state.language]}
			                  type="email"
			                  name = "email" 
			                  disabled={true}  
			                  value={this.state.userData.email}
							  onChange = {e=>this.updateState(e)}
							  helperText={this.state.emailError}
			                />
			                <TextField
			                  id="phone"
			                  className="input_outer col-sm-6"
			                  label={local.addrPhoneNumber[this.state.language]}
			                  type="text"
			                  name = "phone" 
			                  value={this.state.userData.phone}
							  onChange = {e=>this.updateState(e)}
							  helperText={this.state.phoneError}
			                /> 
			            </MuiThemeProvider>
			            <RadioGroup
			                aria-label="Gender Type"
			                name="gender" 
			                id="gender" 
			                value={this.state.userData.gender}
							onChange = {e=>this.updateState(e)}
							helperText={this.state.genderError}
			                className="w-100 radio_outer"
			            >
			                <FormControlLabel
			                  value="M"
			                  control={
			                    <Radio
			                      classes={{root: classes.root, checked: classes.checked}}
			                    />
			                  }
			                  label={local.addrMale[this.state.language]}
			                  labelPlacement="end"
			                />
			                <FormControlLabel
			                  value="F"
			                  control={
			                    <Radio
			                      classes={{root: classes.root, checked: classes.checked}}
			                    />
			                  }
			                  label={local.addrFemale[this.state.language]}
			                  labelPlacement="end"
			                />
			            </RadioGroup>
			            <Button type="button" variant="contained" color="primary" className="update" onClick={() => this.updateProfile()}>
			                {local.profUpdate[this.state.language]}
			            </Button>
			        </form>
			        <h2 className="subheading buffer_bod pt-4">{local.profUpdatePassword[this.state.language]}</h2>
			        <form className="row">
			            <MuiThemeProvider theme={mui}>
			                <div className="w-100">
			                  <TextField
			                    id="currentPass"
			                    name = "currentPass"
			                    className="input_outer col-sm-6"
			                    label={local.addrCurrentPassword[this.state.language]}
			                    type="password" 
			                    value={this.state.userData.currentPass} 
													onChange = {e=>this.updateState(e)}
													helperText={this.state.currentPassError}
			                  />
			                </div>
			                <TextField
			                  id="newPass"
			                  className="input_outer col-sm-6"
			                  label={local.addrNewPassword[this.state.language]}
			                  type="password"
			                  name = "newPass" 
			                  value={this.state.userData.newPass}
												onChange = {e=>this.updateState(e)}
												helperText={this.state.newPassError}
			                />
			                <TextField
			                  id="confirmPass"
			                  className="input_outer col-sm-6"
			                  label={local.addrConfirmPassword[this.state.language]}
			                  type="password"
			                  name = "confirmPass" 
			                  value={this.state.userData.confirmPass}
												onChange = {e=>this.updateState(e)}
												helperText={this.state.confirmPassError}
			                /> 
			            </MuiThemeProvider>
			            <Button
			                variant="contained"
			                color="primary"
			                className="update" 
			                onClick={() => this.changePassword()}
			            >
			                {local.profUpdate[this.state.language]}
			            </Button>
			        </form>
			    </div>
			)
    	}
    	else{
    		return(
    			<Loader />
    		)
    	}
    	
    }	
}

function mapStateToProps(state) {
    return {
				userInfo: state.userInfoReducer,
				languageReducer : state.languageReducer
    }
}

export default connect(mapStateToProps)(withStyles(styles)(Profile));
