import React, { Component } from 'react';
import {connect} from 'react-redux';
import { MuiThemeProvider, createMuiTheme, createGenerateClassName } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import { notify } from 'react-notify-toast';

import Sidebar from '../../components/account_sidebar';
import Loader from "../../components/loader";
import local, {deafultLanguage} from '../../localization';

import { months, days, checkRequired, checkCharacterCount, getter, getUserInfo } from "../../commonfunction";

import { ratePurchaseAction, orderReviewSubmitAction } from '../../actions/myAccount';

const mui = createMuiTheme({
  overrides: {
    MuiFormLabel: {
      root: {
        color: "#b1b1b1"
      },
      focused: {
        color: "black !important"
      },
    }
  }
});

const Reviews = (props) => (
    <div className="col-md-9 right_sec rate_outer order_outer">
        <ul className="nav nav-tabs" role="tablist">
          <li className="nav-item">
            <a className="nav-link active" href="#your_review" role="tab" data-toggle="tab">{local.reviewYourReview[props.language]}</a>
          </li>
          <li className="nav-item">
            <a className="nav-link" href="#pending_review" role="tab" data-toggle="tab">{local.reviewPendingReview[props.language]}</a>
          </li>
        </ul>
        <div className="tab-content">
            {props.reviews && <CompletedReviews reviews={props.reviews.complete} language={props.language}/>}
            {props.reviews && <PendingReviews reviews={props.reviews.pending} setFeedbackData={props.setFeedbackData} language={props.language}/>}
        </div>
        <SubmitReview 
            writeReview={props.writeReview}
            titleError = {props.titleError}
            messageError = {props.messageError} 
            updateWriteReview = {props.updateWriteReview} 
            submitReview={props.submitReview}
            language={props.language}
        />
    </div>
);

const CompletedReviews = (props) => (
    <div role="tabpanel" className="tab-pane fade in active show" id="your_review">
        <ul className="listing_outer">
            {
                props.reviews.feedbacks ? (
                    props.reviews.feedbacks.complete_feedback.map(feedback => {
                        let date = new Date(feedback.creation_date);
                        let createdDate = feedback.creation_date ? ("Reviewed on " + days[date.getDay()-1] + ", " + months[date.getMonth()] + " " + date.getDate() + " " + date.getFullYear() + " " + date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds()) : "";
                        return(
                            <li className="listing">
                                <div className="d-flex">
                                    <div className="details">
                                        <p className="mb-3 ordr_detail">{createdDate}</p>
                                        <img src={feedback.product_image_path} />
                                        <div className="inner">
                                            <p className="name">{feedback.product_image_path.product_name}</p>
                                            <div className="overall_review_rating">
                                                <svg className={feedback.product_rating >= 1 ? "icon icon-star active" : "icon icon-star"}><use xlinkHref="/img/icon-sprite.svg#icon-star"></use></svg>
                                                <svg className={feedback.product_rating >= 2 ? "icon icon-star active" : "icon icon-star"}><use xlinkHref="/img/icon-sprite.svg#icon-star"></use></svg>
                                                <svg className={feedback.product_rating >= 3 ? "icon icon-star active" : "icon icon-star"}><use xlinkHref="/img/icon-sprite.svg#icon-star"></use></svg>
                                                <svg className={feedback.product_rating >= 4 ? "icon icon-star active" : "icon icon-star"}><use xlinkHref="/img/icon-sprite.svg#icon-star"></use></svg>
                                                <svg className={feedback.product_rating >= 5 ? "icon icon-star active" : "icon icon-star"}><use xlinkHref="/img/icon-sprite.svg#icon-star"></use></svg>
                                            </div>
                                            <p className="size_name">{feedback.review}</p>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        )
                    })
                ) : (<div>{local.reviewNoCompletedReview[props.language]}</div>)
            }
            {/*<li className="listing">
                <div className="d-flex">
                    <div className="details">
                        <p className="mb-3 ordr_detail">Reviewed on Fri, Oct 17th '18, 14:52:56</p>
                        <img src="/img/temp/section8.png" />
                        <div className="inner">
                            <p className="name">Paisley Patch Dress</p>
                            <div className="overall_review_rating">
                                <svg className="icon icon-star active"><use xlinkHref="/img/icon-sprite.svg#icon-star"></use></svg>
                                <svg className="icon icon-star"><use xlinkHref="/img/icon-sprite.svg#icon-star"></use></svg>
                                <svg className="icon icon-star"><use xlinkHref="/img/icon-sprite.svg#icon-star"></use></svg>
                                <svg className="icon icon-star"><use xlinkHref="/img/icon-sprite.svg#icon-star"></use></svg>
                                <svg className="icon icon-star"><use xlinkHref="/img/icon-sprite.svg#icon-star"></use></svg>
                            </div>
                            <p className="size_name">I loved the dress. Fabric is super soft and feels premium. I Recommend this to everyone.</p>
                        </div>
                    </div>
                </div>
            </li>*/}
        </ul>
    </div>
);

const PendingReviews = (props) => (
    <div role="tabpanel" className="tab-pane fade" id="pending_review">
        <ul className="listing_outer">
            {
                props.reviews.feedbacks && props.reviews.feedbacks.pending_feedback ? (
                    props.reviews.feedbacks.pending_feedback.map(feedback => {
                            return(
                                <li className="listing">
                                    <div className="d-flex">
                                        <div className="details">
                                            <img src={feedback.product_image_path} />
                                            <div className="inner">
                                                <p className="name">{feedback.product_name}</p>
                                                <p className="color_name">{local.orderId[props.language]}{feedback.order_id}</p>
                                                <p className="size_name">{local.reviewOrderPlacedOn[props.language]}</p>
                                                <a className="review_product" href="JavaScript:void(0);" data-toggle="modal" data-target="#write_review_popup" onClick={() => props.setFeedbackData(feedback)}>{local.reviewProduct[props.language]}</a>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            )
                        })
                    ) : (<div>{local.reviewNoPendingReview[props.language]}</div>)
            }
        </ul>
    </div>
);

const SubmitReview = (props) => (
    <div className="modal reviewwrite" id="write_review_popup">
        <div className="modal-dialog">
            <div className="modal-content">
                <div className="modal-body">
                    <button type="button" id="review-close-btn" className="close" data-dismiss="modal">&times;</button>
                    <div className="row m-0">
                        <div className="col-md-8 left_sec">
                            <h2 className="login_title text-center">{local.reviewWriteReview[props.language]}</h2>
                            <div className="rating_sec text-center">
                                <svg className={props.writeReview.rating >= 1 ? "icon icon-shape active" : "icon icon-shape"} onClick={() => props.updateWriteReview('rating', 1)}><use xlinkHref="/img/icon-sprite.svg#icon-shape"></use></svg>
                                <svg className={props.writeReview.rating >= 2 ? "icon icon-shape active" : "icon icon-shape"} onClick={() => props.updateWriteReview('rating', 2)}><use xlinkHref="/img/icon-sprite.svg#icon-shape"></use></svg>
                                <svg className={props.writeReview.rating >= 3 ? "icon icon-shape active" : "icon icon-shape"} onClick={() => props.updateWriteReview('rating', 3)}><use xlinkHref="/img/icon-sprite.svg#icon-shape"></use></svg>
                                <svg className={props.writeReview.rating >= 4 ? "icon icon-shape active" : "icon icon-shape"} onClick={() => props.updateWriteReview('rating', 4)}><use xlinkHref="/img/icon-sprite.svg#icon-shape"></use></svg>
                                <svg className={props.writeReview.rating >= 5 ? "icon icon-shape active" : "icon icon-shape"} onClick={() => props.updateWriteReview('rating', 5)}><use xlinkHref="/img/icon-sprite.svg#icon-shape"></use></svg>
                            </div>
                            <div>
                                <form autoComplete="off">
                                    <MuiThemeProvider theme={mui}>
                                        <TextField
                                          id="review_heading"
                                          className="input_outer"
                                          label="Review heading*"
                                          autoFocus
                                          fullWidth 
                                          value={props.writeReview.title} 
                                          name ='title'
                                          helperText ={props.titleError}
                                          onChange={(e) => props.updateWriteReview('title', e.target.value)}
                                        />
                                        <TextField
                                          id="feedback"
                                          className="input_outer textField"
                                          label="Feedback*"
                                          multiline
                                          rowsMax="10"
                                          rows="5"
                                          fullWidth
                                          variant="outlined"
                                          helperText={props.messageError + " " + ((50-props.writeReview.message.length) > 0 ? ((50-props.writeReview.message.length) + " remaining") : "")} 
                                          value={props.writeReview.message} 
                                          name = 'message'
                                          onChange={(e) => props.updateWriteReview('message', e.target.value)}
                                        />
                                    </MuiThemeProvider>
                                    <Button
                                        variant="contained"
                                        color="primary"
                                        type="submit"
                                        fullWidth 
                                        type="button" 
                                        onClick={() => props.submitReview()}
                                    >
                                        {local.orderSubmit[props.language]}
                                    </Button>
                                </form>
                            </div>
                        </div>
                        <div className="col-md-4 right_sec">
                            <h3>{local.reviewHowToWrite[props.language]}</h3>
                            <ul>
                                <li>{local.reviewFocusOnProduct[props.language]}</li>
                                <li>{local.reviewProAndCons[props.language]}</li>
                                <li>{local.reviewSimpleCrisp[props.language]}</li>
                                <li>{local.reviewBestUsage[props.language]}</li>
                                <li>{local.reviewComparison[props.language]}</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
);


class RatePurchase extends Component {
    constructor(props) {
        super(props);
        this.state = {
            reviews: "",
            writeReview: {
                rating: 3,
                title: "",
                message: ""
            },
            messageError : "(Please make sure your reviews contains at least 50 characters.)",
            titleError : "",
            currentFeedbackData: {},
            submitReviewState: {},
            language: ''
        };
    }

    componentDidMount(){

        this.setState({
            language: getter('language') ? getter('language') : deafultLanguage
        })

        let userId = localStorage.getItem('userId');
        this.setState({userId: userId});
        this.props.dispatch(ratePurchaseAction({"user_id": userId, "type": "completed"}));
        this.props.dispatch(ratePurchaseAction({"user_id": userId, "type": "pending"}));
    }

    componentWillReceiveProps(nextProps){

        if(nextProps.languageReducer.value){
            this.setState({
                    language: nextProps.languageReducer.value
            })  
        }

        if(nextProps.reviews != this.props.reviews){
            // setTimeout(() => {
                this.setState({reviews: nextProps.reviews});
            // },1000);
        }

        if(nextProps.submitReviewState != this.props.submitReviewState){
            this.setState({submitReviewState: nextProps.submitReviewState});
            if(nextProps.submitReviewState.result){
                notify.show("Review submitted successfully.", "success");
            }
        }
    }

    setFeedbackData(feedbackData){
        this.setState({currentFeedbackData: feedbackData});
    }

    updateWriteReview(key, val){
        let writeReview = {...this.state.writeReview};
        writeReview[key] = val;
        this.setState({writeReview: writeReview});
        if(key=='title'){
            this.setState({
                titleError:''
            })
        }
        else if(key=='message'){
            this.setState({
                messageError : "(Please make sure your reviews contains at least 50 characters.)",
            })
        }
    }

    submitReview(){
        let error = false;
        let rating = checkRequired(this.state.writeReview.rating)
        if(!rating[0]){
            error =  true;
            
        }
        let message = checkCharacterCount(this.state.writeReview.message,50,"(Please make sure your reviews contains at least 50 characters.)")
        if(!message[0]){
            error =  true;
            this.setState({
             messageError : message[1]
            })
        }
        let title = checkRequired(this.state.writeReview.title)
        if(!title[0]){
            error =  true;
            this.setState({
              titleError : title[1]
            })
        }
        if(!error){
            document.getElementById("review-close-btn").click();
            if(getter('userId')){
                let userInfo = getUserInfo();
                let data = {
                    "order_id": this.state.currentFeedbackData.order_id,
                    "product_rating": this.state.writeReview.rating,
                    "review": this.state.writeReview.title,
                    "product_id": this.state.currentFeedbackData.product_id,
                    "product_quality": 0,
                    "platform": "D",
                    "shipping_cost": "",
                    "value_for_money": "",
                    "review_merchant": this.state.writeReview.message
                }
                this.props.dispatch(orderReviewSubmitAction(data));
            }
            else{
                this.props.dispatch(showLogin());
            }

        }
       
    }
   
    render() {
        if(!this.state.reviews.isFetching && !this.state.submitReviewState.isFetching){
            return (
                <Reviews 
                    reviews={this.state.reviews.data} 
                    writeReview={this.state.writeReview} 
                    titleError = {this.state.titleError} 
                    messageError = {this.state.messageError} 
                    updateWriteReview = {(key, val) => this.updateWriteReview(key, val)} 
                    submitReview={() => this.submitReview()} 
                    setFeedbackData={(feedbackData) => this.setFeedbackData(feedbackData)}
                    language={this.state.language}
                />
            )
        }
        else{
            return (
                <Loader />
            )
        }
    }
}

function mapStateToProps(state) {
    return {
        reviews: state.getReviewsReducer,
        submitReviewState: state.submitOrderReviewsReducer,
        languageReducer: state.languageReducer
    }
}

export default connect(mapStateToProps)(RatePurchase);
