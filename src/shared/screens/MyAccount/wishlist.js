import React, { Component } from 'react';
import {connect} from 'react-redux';
import Sidebar from '../../components/account_sidebar';

import { wishlistAction, wishlistRemoveAction, addToCartFromWishlistAction } from '../../actions/myAccount';

import Loader from "../../components/loader";
import {getter} from '../../commonfunction';
import local, {deafultLanguage} from '../../localization';
import { notify } from 'react-notify-toast';
const Wishlist = (props) => (
    <div className="col-md-9 right_sec wishlist_outer order_outer">
        <h2 className="subheading">{local.wishliMyWishList[props.language]}</h2>
        <ul className="listing_outer">
            {
                props.wishlist.data && props.wishlist.data.response && props.wishlist.data.response.products && props.wishlist.data.response.products.length ? (props.wishlist.data.response.products.map((item, index) => (
                    <li className="listing">
                        <div className="row align-items-center">
                            <div className="details col-md-6 col-sm-12">
                                <img src={item.image.image_url} onClick={() => props.goToProductPage(item.seo_name)}/>
                                <div className="inner">
                                    <p className="name"><a href="JavaScript:void(0);" onClick={() => props.goToProductPage(item.seo_name)}>{item.name}</a></p>
                                    {/*<p className="color_name">Color: Amber</p>
                                    <p className="size_name">Size: EU 44</p>*/}
                                    <div className="links">
                                        <a className="add_product" href="JavaScript:void(0);" onClick={() => props.addToCart(item)}>{local.wishliAddToCart[props.language]}</a>
                                        <a className="remove_product" href="JavaScript:void(0);" onClick={() => props.removeWishlist(item.product_id)}>{local.wishliRemove[props.language]}</a>
                                    </div>
                                </div>
                            </div>
                            <div className="col col-md-3 col-sm-6  quantity">
                                <div className="control">
                                    <button className="cart-qty-minus btn" type="button" value="-" onClick={() => item.amount && item.amount > 1 && props.updateQty(item.amount - 1, index)}>-</button>
                                    <input type="text" name="qty" id="qty" value={ item.amount ? item.amount : 1 } title="Qty" className="input-text qty" />
                                    <button className="cart-qty-plus btn" type="button" value="+" onClick={() => props.updateQty(item.amount ? item.amount + 1  : 2, index)}>+</button>
                                </div>
                            </div>
                            <div className="col col-md-3 col-sm-6  price text-right"><span>{local.wishliSar[props.language]}{item.price}</span></div>
                        </div>
                    </li>
                ))) : (<div>{local.wishliNoItem[props.language]}</div>)
            }
        </ul>
    </div>
);


class MyWishlist extends Component {
    constructor(props) {
        super(props);
        this.state = {
            wishlist: "",
            language: '',
            wishlistRemovalFlag:false
        };
    }

    componentDidMount(){

        this.setState({
            language: getter('language') ? getter('language') : deafultLanguage
        })

        let userId = localStorage.getItem('userId');
        this.setState({userId: userId});
        this.props.dispatch(wishlistAction(userId));
    }

    componentWillReceiveProps(nextProps){

        if(nextProps.languageReducer.value){
            this.setState({
                    language: nextProps.languageReducer.value
            })  
        }

        if(nextProps.wishlist != this.props.wishlist){
            if(nextProps.wishlist.data.status===200 && this.state.wishlistRemovalFlag)
            {    
                notify.show(local.removeWishlistMessage[this.state.language],"success");
                this.setState({wishlistRemovalFlag:false});
            }
            this.setState({wishlist: nextProps.wishlist});
        }
    }

    updateQty(qty, index){
        let wishlist = {...this.state.wishlist};
        console.log(wishlist);
        wishlist.data.response.products[index]["amount"] = qty;
        this.setState({wishlist: wishlist});
    }

    removeWishlist(prodId){
        let data = {
            "user_id": this.state.userId,
            "cart_service_id": 2,
            "platform":"D",
            "product_id": prodId,
            "return_wishlist":1
        }
        this.setState({wishlistRemovalFlag:true});
        this.props.dispatch(wishlistRemoveAction(data));
    }

    addToCart(product){
        if(product.tracking == "B"){
            let obj = {};
            let data = {};
            let temp = {
                "product_id": product.product_id,
                "amount": product.amount ? product.amount : 1,
                "product_options": obj,
                "upsert": 1
            }
            data[product.product_id] = temp;

            let addParam = {
                "user_id": this.state.userId,
                "platform": "D",
                "cart_service_id": 1,
                "return_cart": 1,
                "get_cod_fee" :1, 
                "product_data": data
            }

            let removeParam = {
                "user_id": this.state.userId,
                "cart_service_id": 2,
                "platform":"D",
                "product_id": product.product_id,
                "return_wishlist":1
            }

            let param = {
                "addToCart": addParam,
                "removeFromWishlist": removeParam
            }

            this.props.dispatch(addToCartFromWishlistAction(param));
        }
        else{
            this.goToProductPage(product.seo_name);
        }
    }

    goToProductPage(productId){
        this.props.history.push("/pd/" + productId+".html");
    }
   
    render() {
        if(!this.state.wishlist.isFetching){
            return (
                <Wishlist 
                    wishlist={this.state.wishlist} 
                    updateQty={(qty, index) => this.updateQty(qty, index)} 
                    removeWishlist={(prodId) => this.removeWishlist(prodId)} 
                    addToCart={(product) => this.addToCart(product)}
                    goToProductPage={(productId) => this.goToProductPage(productId)}
                    language={this.state.language}
                />
            );
        }
        else{
            return(
                <Loader />
            )
        }
    }
}

function mapStateToProps(state) {
    return {
        wishlist: state.getWishlistReducer,
        languageReducer: state.languageReducer
    }
}

export default connect(mapStateToProps)(MyWishlist);
