import React, { Component } from 'react';
import {connect} from 'react-redux';
import Sidebar from '../../components/account_sidebar';
import Profile from './profile';
import MyKzmoPoint from './rewardPoints';
import MyOrder from './order';
import AddressBook from './addressBook';
import MyWishlist from './wishlist';
import RatePurchase from './ratePurchase';
import local, {deafultLanguage} from '../../localization';
import {getter} from '../../commonfunction';

class MyAccount extends Component {
    constructor(props){
        super(props);
        this.state = {
            viewType: "",
            language: ''
        }
    }

    static initialAction({url, store}) {
       return true;
    }

    componentDidMount(){
        this.setState({
            language: getter('language') ? getter('language') : deafultLanguage
        })

        if(!localStorage.getItem('userId')){
            this.props.history.push("/");
        }
        let viewType = this.props.match.params.viewType;
        this.setState({viewType: viewType});
    }

    componentWillReceiveProps(nextProps){
        if(nextProps.languageReducer.value){
            this.setState({
                language: nextProps.languageReducer.value
            }) 
        }
        if(nextProps.match.params.viewType != this.props.match.params.viewType){
            this.setState({viewType: nextProps.match.params.viewType});
        }
    }

    setView(view){
        this.props.history.push("/myaccount/" + view);
    }

    goToMyaccount(){
        this.props.history.push("/myaccount/profile");
    }
   
    render() {

        return (
            <div className="dashboard_outer">
                <h1 className="category_name text-left">
                <a href="Javascript:void(0);" onClick={() => this.goToMyaccount()}>{local.addrBackAccount[this.state.language]}</a>
                <span>{local.addrMyAccount[this.state.language]}</span></h1>
                <div className="row">
                    <div className={this.state.viewType ? "col-md-3 left_sec d-none d-md-block" : "col-md-3 left_sec"}>
                      <Sidebar viewType={this.state.viewType} setView={(view) => this.setView(view)} />
                    </div>
                    {this.state.viewType == "profile" ? (<Profile />) : (
                        this.state.viewType == "reward-points" ? (<MyKzmoPoint />) : (
                            this.state.viewType == "orders" ? (<MyOrder history={this.props.history} />) : (
                               this.state.viewType == "wishlist" ? (<MyWishlist history={this.props.history} />) : (
                                    this.state.viewType == "rate-purchase" ? (<RatePurchase />) : (
                                        this.state.viewType == "address-book" ? (<AddressBook />) : (<Profile />)
                        )))))
                    }
                    
                </div>
            </div>
        );
    }
}
function mapStateToProps(state) {
    return {
				languageReducer : state.languageReducer
    }
}
export default connect(mapStateToProps)(MyAccount);
