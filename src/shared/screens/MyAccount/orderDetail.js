import React, { Component } from 'react';
import {connect} from 'react-redux';

import { MuiThemeProvider, createMuiTheme, createGenerateClassName } from '@material-ui/core/styles';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import TextField from '@material-ui/core/TextField';
import FormGroup from '@material-ui/core/FormGroup';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import Button from '@material-ui/core/Button';
import FormControlLabel from '@material-ui/core/FormControlLabel';

import Sidebar from '../../components/account_sidebar';
import Loader from "../../components/loader";
import { getter, months, days,getDayIndex} from '../../commonfunction';
import { InvalidTokenAction } from '../../actions/login';
import { notify } from 'react-notify-toast';

import { orderDetailAction, helpIssueAction, helpSubIssueAction, helpSubSubIssueAction, submitNeedHelpAction,
  orderStatusAction,cancelOrderFromDetailAction } from '../../actions/myAccount';
import local, {deafultLanguage} from '../../localization';
import { orderInvoiceStatus,base_URL,api_KEY } from '../../constant';
import {statusesForEDD} from '../../commonConstant';
const mui = createMuiTheme({
  overrides: {
    MuiFormLabel: {
      root: {
        color: "#b1b1b1"
      },
      focused: {
        color: "black !important"
      },
    }
  }
});

const Details = (props) => {
    //console.log('in details - dinesh',props);
    let orderInfo = props.details.order_info;
    console.log(orderInfo,'in details - dinesh');
    let date = new Date(parseInt(orderInfo.timestamp)*1000);
    let orderDate = days[getDayIndex(date.getDay())] + ", " + date.getDate() + " " + months[date.getMonth()] + " " + date.getFullYear();

   let deliveryTimestamp = new Date(parseInt(orderInfo.order_delivery_info.edd)*1000);
    let deliveryTime = days[getDayIndex(deliveryTimestamp.getDay())] + ", " + deliveryTimestamp.getDate() + " " + months[deliveryTimestamp.getMonth()] + " " + deliveryTimestamp.getFullYear();

    return (
        <div className="col-md-9 right_sec order_outer mobile_order_detail">
            <div className="listing_outer">
                <div className="listing">
                    <div className="d-flex">
                        <div className="buffer_prop details">
                            {<p className="mb-0 ordr_detail">{local.orderId[props.language]} {orderInfo.order_id}</p>}
                            <p className="mb-3 placing_dte">{local.orderPlacedOn[props.language]} {orderDate}</p>
                            {
                                Object.keys(orderInfo.items).map(itemKey => {
                                    let prodInfo = orderInfo.items[itemKey];
                                    return(
                                        <div>
                                            <img src={prodInfo.image_path} onClick={() => props.goToProductPage(prodInfo.product_seo_name)}/>
                                            <div className="inner">
                                                <p className="name"><a href="JavaScript:void(0);"  onClick={() => props.goToProductPage(prodInfo.product_seo_name)}>{prodInfo.product_name}</a></p>
                                                {
                                                    prodInfo.product_options && prodInfo.product_options.map(option => (
                                                       <p className="color_name">{option.option_name}: {option.variant_name}</p>
                                                    ))
                                                }
                                            </div>
                                        </div>
                                    )
                                })
                            }
                            {props.validateEDDStatus(orderInfo.status) && orderInfo.order_delivery_info.edd && <p className="font-weight-bold mb-0 mt-3">{new Date().getTime() < parseInt(orderInfo.order_delivery_info.edd)*1000 ? "Delivery expected by " : "Delivered on " } {deliveryTime}</p>}
                            <p className="mb-0">{orderInfo.status_description}</p>
                        </div>
                        <div className="inner_right text-right">
                            <p>
                                {/*<a className="links_detail" href="JavaScript:void(0);" onClick={() => props.goToTrackOrder(orderInfo.order_id)}>{local.footerTrackOrder[props.language]}</a>*/}
                                { orderInfo.allow_cancellation == 1  &&
                              <a className="links_detail" data-toggle="modal" data-target="#cancel_order" href="JavaScript:void(0);">{local.orderCancel[props.language]}</a>
                            }
                            </p>
                            <p className="m-0">
                            { props.checkStatus(orderInfo.status) &&
                                <a className="links_detail" href="JavaScript:void(0);" onClick={() => props.downloadInvoice(orderInfo.order_id,orderInfo.status)}>{local.orderInvoice[props.language]} {/*<span className="common_tooltip down">{local.orderComingSoon[props.language]}</span>*/}</a>
                            }
                                {props.viewType != "needHelp" && <a className="links_detail" href="JavaScript:void(0);" onClick={() => props.goToNeedHelp(orderInfo.order_id)}>{local.orderNeedHelp[props.language]}</a>}
                            </p>
                        </div>
                    </div>
                {orderInfo.allow_cancellation == 1 && <CancelOrderPopup orderList={orderInfo} language={props.language} selectCancelReason={props.selectCancelReason} cancelOrder={props.cancelOrder} validateEDDStatus={props.validateEDDStatus}/>}
                {
          <div className="modal canceloder_outer address_outerpopup order_outerpopup" id="need_help_modal">
                <div className="modal-dialog">
                    <div className="modal-content">
                        <div className="modal-body">
                            <button id="close-bank-popup" type="button" className="close" data-dismiss="modal">&times;</button>
                            <p className="text-center" dangerouslySetInnerHTML={{__html: props.modalMessage}} >
                            </p>                            
                        </div>
                    </div>
                </div>
            </div>
    }
                </div>

                <RenderView
                    viewType = {props.viewType}
                    issueData = {props.issueData}
                    issueId = {props.issueId}
                    selectIssue = {props.selectIssue}
                    subIssueData = {props.subIssueData}
                    subIssueId = {props.subIssueId}
                    selectSubIssue = {props.selectSubIssue}
                    subSubIssueData = {props.subSubIssueData}
                    subSubIssueId = {props.subSubIssueId}
                    selectSubSubIssue = {props.selectSubSubIssue}
                    submitNeedHelp = {props.submitNeedHelp}
                    helpText = {props.helpText}
                    changeHelpText = {props.changeHelpText}
                    orderStatus = {props.orderStatus}
                    orderInfo = {orderInfo}
                    language = {props.language}
                    showLoader = {props.showLoader}
                    isEmpty = {props.isEmpty}
                />
            </div>
        </div>


    );
};

const RenderView = (props) => {
    if(props.viewType == "needHelp") {
        return <NeedHelp
            issueData = {props.issueData}
            issueId = {props.issueId}
            selectIssue = {props.selectIssue}
            subIssueData = {props.subIssueData}
            subIssueId = {props.subIssueId}
            selectSubIssue = {props.selectSubIssue}
            subSubIssueData = {props.subSubIssueData}
            subSubIssueId = {props.subSubIssueId}
            selectSubSubIssue = {props.selectSubSubIssue}
            submitNeedHelp = {props.submitNeedHelp}
            helpText = {props.helpText}
            changeHelpText = {props.changeHelpText}
            language = {props.language}
            showLoader = {props.showLoader}
        />
    }
    else if(props.viewType == "trackOrder"){
        return <TrackOrder
                orderStatus = {props.orderStatus}
                language = {props.language}
            />
    }
    else if(props.viewType == "orderDetail") {
        return <OrderInfo
            orderInfo = {props.orderInfo}
            language = {props.language}
            isEmpty = {props.isEmpty}
        />
    }
    else{
        return "";
    }
};

const NeedHelp = (props) => (
    <div className="needhelp_outer mt-4">
        <h2 className="mt-4">{local.orderTellUsIssue[props.language]}</h2>
        <form>
            <FormControl className="formControl d-block slect_outer mb-4 mt-5">
                <InputLabel shrink htmlFor="city-placeholder">
                    {local.orderSelectReason[props.language]}
                </InputLabel>
                {
                    props.issueData && <Select
                        value={props.issueId}
                        input={<Input name="select_reason" id="cancel-placeholder" />}
                        displayEmpty
                        name="select_reason"
                        className="selectEmpty"
                        onChange={(e) => props.selectIssue(e.target.value)}
                    >
                        {
                            props.issueData.map(issue => (
                                <MenuItem value={issue.issue_id}>{issue.name}</MenuItem>
                            ))
                        }
                    </Select>
                }
            </FormControl>
            {
                props.subIssueData && props.subIssueData.length > 0 && <FormControl className="formControl d-block slect_outer mb-4 mt-5">
                    <InputLabel shrink htmlFor="city-placeholder">
                        {local.orderSelectSubReason[props.language]}
                    </InputLabel>
                    {
                        <Select
                            value={props.subIssueId}
                            input={<Input name="select_reason" id="cancel-placeholder" />}
                            displayEmpty
                            name="select_reason"
                            className="selectEmpty"
                            onChange={(e) => props.selectSubIssue(e.target.value)}
                        >
                            {
                                props.subIssueData.map(subIssue => (
                                    <MenuItem value={subIssue.issue_id}>{subIssue.name}</MenuItem>
                                ))
                            }
                        </Select>
                    }
                </FormControl>
            }
            {
                props.subSubIssueData && props.subSubIssueData.length > 0 && <FormControl className="formControl d-block slect_outer mb-4 mt-5">
                    <InputLabel shrink htmlFor="city-placeholder">
                        {local.orderSelectSubSubReason[props.language]}
                    </InputLabel>
                    {
                        <Select
                            value={props.subSubIssueId}
                            input={<Input name="select_reason" id="cancel-placeholder" />}
                            displayEmpty
                            name="select_reason"
                            className="selectEmpty"
                            onChange={(e) => props.selectSubSubIssue(e.target.value)}
                        >
                            {
                                props.subSubIssueData.map(subSubIssue => (
                                    <MenuItem value={subSubIssue.issue_id}>{subSubIssue.name}</MenuItem>
                                ))
                            }
                        </Select>
                    }
                </FormControl>
            }
            <MuiThemeProvider theme={mui}>
                <TextField
                    id="feedback"
                    className="input_outer textField mt-1"
                    label={local.orderMessage[props.language]}
                    multiline
                    rowsMax="10"
                    rows="5"
                    fullWidth
                    variant="outlined"
                    value={props.helpText}
                    onChange={(e) => props.changeHelpText(e.target.value)}
                />
            </MuiThemeProvider>
            <Button variant="contained" color="primary" type="button"  onClick={() => props.submitNeedHelp()}>
                {local.orderSubmit[props.language]}
            </Button>
            {props.showLoader && <Loader />}
        </form>
    </div>
);

const TrackOrder = (props) => {
    let processing = 0;
    let shipping = 0;
    let delivery = 0;
    let lineWidth = "";
    let approvedClass = "active";
    let processingClass = "disable";
    let shippingClass = "disable";
    let deliveryClass = "disable";

    let statusInfo = props.orderStatus.status_info;
    statusInfo && statusInfo.map(info => {
        if(info.status == 'L' || info.status == '104' || info.status == 'A' || info.status == 'H' || info.status == 'C' || info.status == '117'){
            processing = 1;
            lineWidth = "one-third";
            approvedClass = "";
            processingClass = "active";
        }

        if(info.status == 'A' || info.status == 'H' || info.status == 'C' || info.status == '117'){
            shipping = 1;
            lineWidth = "half";
            processingClass = "";
            shippingClass = "active";
        }

        if(info.status == 'H' || info.status == 'C' || info.status == '117'){
            delivery = 1;
            lineWidth = "full";
            shippingClass = "";
            deliveryClass = "";
        }
    });

    return (
        <div>
            <ul className={"trackorder_outer d-flex justify-content-between mt-sm-5 mb-3 position-relative " + lineWidth}>
                <li className={"d-flex flex-sm-column " + approvedClass}>{local.orderApproved[props.language]}</li>
                <li className={"align-items-sm-center d-flex flex-sm-column " + processingClass}>{local.orderProcessed[props.language]}</li>
                <li className={"align-items-sm-center d-flex flex-sm-column " + shippingClass}>{local.orderShipped[props.language]}</li>
                <li className={"align-items-sm-end d-flex flex-sm-column " + deliveryClass}>{local.orderDelivered[props.language]}</li>
            </ul>
            <div className="text-sm-right mt-4"><a className="links_detail" href="JavaScript:void(0);">{local.orderShowDetails[props.language]}</a></div>
        </div>
        );
};


const CancelOrderPopup = (props) => {

    let date = new Date(parseInt(props.orderList.timestamp)*1000);
    let day = date.getDate();
    let month = date.getMonth();
    let year = date.getFullYear();

    let deliveryTimestamp = new Date(parseInt(props.orderList.order_delivery_info.edd)*1000);
    let deliveryTime = days[getDayIndex(deliveryTimestamp.getDay())] + ", " + deliveryTimestamp.getDate() + " " + months[deliveryTimestamp.getMonth()] + " " + deliveryTimestamp.getFullYear();

    let cancelReason = props.orderList.reasons;
    let orderInfo = props.orderList.order_info;
    console.log('dinesh goyal - ',props.orderList);
    return(
        <div className="modal canceloder_outer order_outerpopup" id="cancel_order">
            <div className="modal-dialog">
                <div className="modal-content">
                    <div className="modal-body">
                        <button type="button" id="store-close" className="close" data-dismiss="modal">&times;</button>
                        <h2 className="text-center mb-4">{local.orderCancel[props.language]}</h2>
                        <p>{local.orderId[props.language]}:{props.orderList.order_id}   <span>{local.orderPlacedOn[props.language]} {day + " " + months[month] + " " + year}</span></p>

                        {

                          Object.keys(props.orderList.items).map(itemKey => {
                              let prodData = props.orderList.items[itemKey];
                                return(
                                    <div className="details">
                                        <img src={prodData.product_image_path} />
                                        <div className="inner">
                                            <p className="name"><a href="JavaScript:void(0);">{prodData.product_name}</a></p>
                                            <p className="color_name">{local.orderTotalAmount[props.language]}{prodData.price}</p>
                                            <p className="size_name">{local.orderOty[props.language]}{prodData.amount}</p>
                                        </div>
                                    </div>
                                )
                            })
                        }
                        {/*<p className="font-weight-bold mb-0 mt-3">Delivered on Fri, Oct 11th '18</p>
                        <p className="mb-0">Your item has been shipped.</p>*/}
                        {props.validateEDDStatus(props.orderList.status) && props.orderList.order_delivery_info.edd && <p className="font-weight-bold mb-0 mt-3">{new Date().getTime() < parseInt(props.orderList.order_delivery_info.edd)*1000 ? "Delivery expected by" : "Delivered on" } {deliveryTime}</p>}
                        <p className="mb-0">{props.orderList.status_name}</p>
                        {<form>
                            <FormControl className="formControl d-block slect_outer mb-4 mt-5">
                              <InputLabel shrink htmlFor="city-placeholder">
                                {local.orderSelectReason[props.language]}
                              </InputLabel>
                              <Select
                                value={props.orderList.cancelReasonId}
                                input={<Input name="select_reason" id="cancel-placeholder" />}
                                displayEmpty
                                name="select_reason"
                                className="selectEmpty"
                                onChange={(e) => props.selectCancelReason(e.target.value)}
                              >
                                {
                                    cancelReason.map(reason => (
                                        <MenuItem value={reason.reason_id}>{reason.reason}</MenuItem>
                                    ))
                                }
                              </Select>
                            </FormControl>
                            <Button type="button" className="cancel_order_btn" variant="contained" color="primary" type="button" disabled={!props.orderList.cancelReasonId} onClick={() => props.cancelOrder(props.index)}>
                                {local.orderSubmit[props.language]}
                            </Button>
                        </form>}
                    </div>
                </div>
            </div>
        </div>
    )
}

const OrderInfo = (props) => (
    <div className="innerorder_detail mt-4 pt-4">
        <div className="row">
            <div className="col-lg-8  col-md-6 buffer_border">
                <h5>{local.orderDeliveryAddr[props.language]}</h5>
                <p>{!props.isEmpty(props.orderInfo.firstname)?props.orderInfo.firstname:" "}{ + "  " + !props.isEmpty(props.orderInfo.lastname)?props.orderInfo.lastname:""}</p>
                <p>{!props.isEmpty(props.orderInfo.s_address)?props.orderInfo.s_address:" "}{ + "  " + !props.isEmpty(props.orderInfo.s_address_2)?props.orderInfo.s_address_2:" "}{ + "  " + !props.isEmpty(props.orderInfo.s_city)?props.orderInfo.s_city:" "}{ + "  " + !props.isEmpty(props.orderInfo.s_country_descr)?props.orderInfo.s_country_descr:" "}</p>
                <p>{!props.isEmpty(props.orderInfo.s_phone)?props.orderInfo.s_phone:""}</p>
            </div>
            <div className="col-lg-4 col-md-6">
                <h5>{local.orderPaymentInfo[props.language]}</h5>
                <ul>
                    <li className="d-flex justify-content-between"><p>{local.cartSubTotal[props.language]}:</p><p className="text-right buffer-color">{props.orderInfo.subtotal} {local.homeSAR[props.language]}</p></li>
                    <li className="d-flex justify-content-between"><p>{local.orderShippingCost[props.language]}: </p><p className="text-right buffer-color">{props.orderInfo.shipping_cost} {local.homeSAR[props.language]}</p></li>
                    {props.orderInfo.cod_fee && <li className="d-flex justify-content-between"><p>{local.orderCodFee[props.language]}: </p><p className="text-right buffer-color">{props.orderInfo.cod_fee} {local.homeSAR[props.language]}</p></li>}
                    <li className="d-flex justify-content-between"><p>{local.cartGrandTotal[props.language]}:  </p><p className="text-right buffer-color">{props.orderInfo.total} {local.homeSAR[props.language]}</p></li>
                    <li className="d-flex justify-content-between"><p>{local.orderPaymentMode[props.language]}: </p><p className="text-right buffer-color">{props.orderInfo.payment_method.method}</p></li>
                </ul>
            </div>
        </div>
    </div>
);

class OrderDetail extends Component {
    constructor(props) {
        super(props);
        this.state = {
            userId: "",
            orderId: "",
            orderDetail: "",
            viewType: "",
            needHelp: {
                issueData: "",
                issueId: "",
                subIssueData: "",
                subIssueId: "",
                subSubIssueData: "",
                subSubIssueId: "",
                helpText: "",
                language: ''
            },
            showLoader : false,
            orderStatus: {},
            modalMessage: ""
        }
    }

    static initialAction({url, store}) {
        return true;
    }

    componentDidMount(){
        this.setState({
            language: getter('language') ? getter('language') : deafultLanguage
        })
        let userId = localStorage.getItem('userId');
        this.setState({userId: userId});
        let orderId = this.props.match.params.orderId;
        this.setState({orderId: orderId}, () => {
            this.setViewType();
        });

        this.props.dispatch(orderDetailAction(orderId));
    }

    componentWillReceiveProps(nextProps){
      console.log('componentWillReceiveProps dinesh', nextProps);
        if(nextProps.languageReducer.value){
            this.setState({
                language: nextProps.languageReducer.value
            })
        }
        if(nextProps.orderDetail != this.props.orderDetail){
            this.setState({orderDetail: nextProps.orderDetail});
        }

        if(nextProps.helpIssueData != this.props.helpIssueData){
            let needHelp = {...this.state.needHelp};
            needHelp.issueData = nextProps.helpIssueData;
            this.setState({needHelp: needHelp});
        }

        if(nextProps.helpSubIssueData != this.props.helpSubIssueData){
            let needHelp = {...this.state.needHelp};
            needHelp.subIssueData = nextProps.helpSubIssueData;
            this.setState({needHelp: needHelp});
        }

        if(nextProps.helpSubSubIssueData != this.props.helpSubSubIssueData){
            let needHelp = {...this.state.needHelp};
            needHelp.subSubIssueData = nextProps.helpSubSubIssueData;
            this.setState({needHelp: needHelp});
        }

        if(nextProps.submitNeedHelpData != this.props.submitNeedHelpData && nextProps.submitNeedHelpData.result){
            this.setState({showLoader: false,showModal:true,modalMessage:nextProps.submitNeedHelpData.data},()=>{
                $("#need_help_modal").modal();
            });
            let needHelp = {...this.state.needHelp};
            needHelp.issueId = "";
            needHelp.issueId = "";
            needHelp.subIssueData = "";
            needHelp.subIssueId = "";
            needHelp.subSubIssueData = "";
            needHelp.subSubIssueId = "";
            needHelp.helpText = "";
            this.setState({needHelp: needHelp});
        }

        if(nextProps.orderStatus != this.props.orderStatus){
            if(nextProps.orderStatus.error){
                this.props.dispatch(InvalidTokenAction());
            }
            this.setState({orderStatus: nextProps.orderStatus});
        }
    }

    setViewType(){
        let pathName = this.props.location.pathname;
        if(pathName.indexOf("/myaccount/order") != -1){
            this.setState({viewType: "orderDetail"});
        }
        else if(pathName.indexOf("/myaccount/need-help") != -1){
            this.setState({viewType: "needHelp"});
            this.props.dispatch(helpIssueAction(this.state.orderId));
        }
        else if(pathName.indexOf("/myaccount/track-order") != -1){
            this.setState({viewType: "trackOrder"});
            this.props.dispatch(orderStatusAction(this.state.orderId));
        }
    }

    setView(view){
        this.props.history.push("/myaccount/" + view);
    }

    selectIssue(issueId){
        let needHelp = {...this.state.needHelp};
        needHelp.issueId = issueId;
        this.setState({needHelp: needHelp}, () => {
            this.props.dispatch(helpSubIssueAction(issueId));
        });
    }

    selectSubIssue(subIssueId){
        let needHelp = {...this.state.needHelp};
        needHelp.subIssueId = subIssueId;
        this.setState({needHelp: needHelp}, () => {
            this.props.dispatch(helpSubSubIssueAction(subIssueId));
        });
    }

    selectSubSubIssue(subSubIssueId){
        let needHelp = {...this.state.needHelp};
        needHelp.subSubIssueId = subSubIssueId;
        this.setState({needHelp: needHelp});
    }

    submitNeedHelp(){
        this.setState({showLoader: true});
        let data = {
            issueId: this.state.needHelp.issueId,
            subIssueId: this.state.needHelp.subIssueId,
            subSubIssueId: this.state.needHelp.subSubIssueId,
            helpText: this.state.needHelp.helpText,
            orderId: this.state.orderId
        }
        this.props.dispatch(submitNeedHelpAction(data));
    }
    //dinesh
    selectCancelReason(val){
       //console.log('selectCancelReason', this.state, val);

        let orderDetail = {...this.state.orderDetail};
        orderDetail.data.order_info["cancelReasonId"] = val;
        this.setState({orderDetail: orderDetail});

    }

    cancelOrder(){
      //console.log('cancelOrder -dinesh', this.state);
        let data = {
            "order_id": this.state.orderId,
            "user_id": this.state.userId,
            "reasons": this.state.orderDetail.data.order_info.cancelReasonId,
            "comment": ""
        }
        document.getElementById("store-close").click()
        this.props.dispatch(cancelOrderFromDetailAction(data));
    }

    changeHelpText(val){
        let needHelp = {...this.state.needHelp};
        needHelp.helpText = val;
        this.setState({needHelp: needHelp});
    }

    goToTrackOrder(orderId){
        this.props.history.push('/myaccount/track-order/' + orderId);
    }

    goToNeedHelp(orderId){
        this.props.history.push('/myaccount/need-help/' + orderId);
    }

    downloadInvoice(orderId,status){
        if(orderInvoiceStatus.indexOf(status) > -1) {
            window.open(base_URL + "order_invoice?key="+api_KEY+"&order_id=" + orderId , "_self");
        }else {
            console.log('Invalid Order Status');
            return "JavaScript:void(0);";
        }
    }

    checkStatus(status) {
        if(orderInvoiceStatus.indexOf(status) > -1) {
            return true;
        }
        return false;
    }

    goToProductPage(productId){
        this.props.history.push('/pd/' + productId+".html");
    }

    validateEDDStatus(status) {
        if(statusesForEDD.indexOf(status) > -1) {
            return true;
        }
        return false;
    }
    isEmpty(val){
        return (val === undefined || val == null || val == "") ? true : false;
    }
    render() {
        if(!this.state.orderDetail.isFetching && this.state.needHelp.issueData.isFetching !== true && this.state.orderStatus.isFetching !== true){
            return (
                <div className="dashboard_outer">
                    <h1 className="category_name text-left">
                    <a href="#"><svg className="icon icon-left-arrow"><use xlinkHref="/img/icon-sprite.svg#icon-left-arrow"></use></svg></a>
                    {local.addrMyAccount[this.state.language]}</h1>
                    <div className="row">
                        <div className="col-md-3 left_sec">
                            <Sidebar
                                viewType="orders"
                                setView={(view) => this.setView(view)}
                            />
                        </div>
                        {
                            this.state.orderDetail.data &&
                            <Details
                                details = {this.state.orderDetail.data}
                                viewType = {this.state.viewType}
                                issueData = {this.state.needHelp.issueData.data}
                                issueId = {this.state.needHelp.issueId}
                                selectIssue = {(issueId) => this.selectIssue(issueId)}
                                subIssueData = {this.state.needHelp.subIssueData.data}
                                subIssueId = {this.state.needHelp.subIssueId}
                                selectSubIssue = {(subIssueId) => this.selectSubIssue(subIssueId)}
                                subSubIssueData = {this.state.needHelp.subSubIssueData.data}
                                subSubIssueId = {this.state.needHelp.subSubIssueId}
                                selectSubSubIssue = {(subSubIssueId) => this.selectSubSubIssue(subSubIssueId)}
                                submitNeedHelp = {() => this.submitNeedHelp()}
                                helpText = {this.state.needHelp.helpText}
                                changeHelpText = {(val) => this.changeHelpText(val)}
                                orderStatus = {this.state.orderStatus.data}
                                goToTrackOrder = {(orderId) => this.goToTrackOrder(orderId)}
                                language = {this.state.language}
                                goToProductPage = {(productId) => this.goToProductPage(productId)}
                                downloadInvoice={(orderId,status) => this.downloadInvoice(orderId,status)}
                                checkStatus={(status) => this.checkStatus(status)}
                                cancelOrder={() => this.cancelOrder()}
                                selectCancelReason={(val) => this.selectCancelReason(val)}
                                goToNeedHelp={(orderId) => this.goToNeedHelp(orderId)}
                                validateEDDStatus = {(status) => this.validateEDDStatus(status)}
                                showLoader = {this.state.showLoader}
                                modalMessage = {this.state.modalMessage}
                                isEmpty = {(value)=> this.isEmpty(value)}
                                
                            />
                        }
                    </div>
                    {!(this.state.needHelp.subIssueData.isFetching !== true && this.state.needHelp.subSubIssueData.isFetching !== true) && <Loader />}
                </div>
            );
        }
        else{
            return (
                <Loader />
            );
        }
    }
}

function mapStateToProps(state) {
    return {
        orderDetail: state.orderDetailReducer,
        helpIssueData: state.helpIssueReducer,
        helpSubIssueData: state.helpSubIssueReducer,
        helpSubSubIssueData: state.helpSubSubIssueReducer,
        submitNeedHelpData: state.submitNeedHelpReducer,
        orderStatus: state.orderStatusReducer,
        languageReducer: state.languageReducer
    }
}

export default connect(mapStateToProps)(OrderDetail);