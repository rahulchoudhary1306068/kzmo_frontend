import React, { Component } from 'react';
import {connect} from 'react-redux';

import {
  MuiThemeProvider,
  createMuiTheme,
  createGenerateClassName,
} from '@material-ui/core/styles';
import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import FormLabel from '@material-ui/core/FormLabel';
import Button from '@material-ui/core/Button';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import Map from '../../components/map';
import {notify} from 'react-notify-toast';
import { showLogin, InitailStateAction, InvalidTokenAction } from '../../actions/login';
import { userProfileAction, editProfileAction, removeProfileAction } from '../../actions/userprofile';
import { getter, checkRequired, checkPhonenumber, checkPincode } from '../../commonfunction';
import Loader from '../../components/loader';
import local, {deafultLanguage} from '../../localization';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import Input from '@material-ui/core/Input';
import MenuItem from '@material-ui/core/MenuItem';
import FormGroup from '@material-ui/core/FormGroup';
import {CityAction} from '../../actions/header';
const styles = theme => ({
  root: {
    '&$checked': {
      color: '#e4b122'
    }
  },
  checked: {},
  
});
const mui = createMuiTheme({
  overrides: {
    MuiFormLabel: {
      root: {
        color: "#b1b1b1"
      },
      focused: {
        color: "black !important"
      },
    }
  }
});
var latLongData;
class AddressBook extends Component {
  
    static initialAction({url, store}) {
       
       return true;
    }
      
    constructor(props){
        super(props)
        this.state = {
            showMap : false,
            value : 'home',
            lat : 23.7224509,
            long : 46.7893653,
            profileData : false,
            showAddressForm : true,
            name:"",
            houseNo:"",
            city:"",
            phoneNumber:"",
            addressType : "home",
            nameError:"",
            houseNoError:"",
            cityError:"",
            phoneNumberError:"",
            addressTypeError:"",
            showEditAdress : false,
            editProfileId : false,
            showLoader : true,
            pinCode :"",
            pinCodeError :"",
            lastName : "",
            language : '',
            cityData:{data:{response:""}},
            selectedCity:"",
            selectedCityId:"",
            countryDefaultValue : "SA",

        }
        
    }
    selectCity(cityId){
        let selCity = this.state.cityData.data.response.filter((city)=> cityId===city.id);
        this.setState({selectedCity: selCity[0].city});
        this.setState({selectedCityId: selCity[0].id});
    }
    componentDidMount(){
        
        this.setState({
            language: getter('language') ? getter('language') : deafultLanguage
        })

        if(!getter('userId')){
            
            this.props.dispatch(showLogin());
        }
        else{ 
            this.props.dispatch(userProfileAction()); 
             
        }
        this.props.dispatch(CityAction(this.state.countryDefaultValue)); 
        //this.props.dispatch(userProfileAction());
        
        window.scrollTo(0,0)
    }
    componentWillReceiveProps(nextProps){

        if(nextProps.languageReducer.value){
            this.setState({
                language: nextProps.languageReducer.value
            })   
        }
        // this.addProfile()
        if(nextProps.UserProfileReducer.data.response){
            //console.log("profile data", nextProps.UserProfileReducer.data);
            this.setState({
                profileData : nextProps.UserProfileReducer.data,
                
            })
            if(nextProps.UserProfileReducer.data.response.length>1 && nextProps.UserProfileReducer.data.response.message==null){
                console.log(nextProps.UserProfileReducer.data)
                this.setState({
                    showAddressForm : false,
                    showMap : false,
                    phoneNumber : "",
                    houseNo : "",
                    city:"",
                    name : "",
                    lastName : "",
                    pinCode:"",
                    editProfileId : false,
                    showEditAdress : false,
                    address_type :'home'

                })
            }
            else{
                console.log("show error");
            }
            this.setState({
                showLoader: false
            })
        }
        if(Object.keys(nextProps.RemoveProfileReducer.data).length){
            if(nextProps.RemoveProfileReducer.data.status=='Successful'){
                notify.show(nextProps.RemoveProfileReducer.data.msg,"success")
            
            }
            else{
                if(nextProps.RemoveProfileReducer.data.extra==='_api_param_invalid_token'){
							
                    this.props.dispatch(InvalidTokenAction());
                }
            }
            this.props.dispatch(InitailStateAction('InitailStateRemoveProfile_ACTION'));
        }

        if(nextProps.cityData!==this.props.cityData)
        {
            this.setState({cityData:nextProps.cityData});
        }

    }
    removeAddress(profileID){
        this.props.dispatch(removeProfileAction(profileID))
    }
    editAddress(profileObject){
        
        console.log(profileObject)
        this.toggleEditAdress()
        this.setState({
            showAddressForm : true,
            phoneNumber :  profileObject.s_phone,
            city : profileObject.s_city,
            selectedCity:profileObject.s_city,
            selectedCityId:profileObject.city_id,
            houseNo : profileObject.s_address,
            name : profileObject.s_firstname,
            lastName : profileObject.s_lastname,
            pinCode:profileObject.s_zipcode,
            address_type:profileObject.address_type,
            editProfileId : profileObject.profile_id
        })
        
        
        $('html, body').animate({
            scrollTop: $($('#address')).offset().top-150
        }, 1000, 'linear');
     


    }
    toggleEditAdress(){
        if(this.state.showEditAdress){
            this.setState({
          
                phoneNumber :  "",
                city : "",
                houseNo : "",
                name : "",
                lastName : "",
                pinCode:"",
                address_type:'home',
                editProfileId : false,
                showAddressForm : false
            })
        }
        this.setState({
            showEditAdress : !this.state.showEditAdress
        })
    }
    getCurrentLocation = ()=>{
    
        if (window.navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(
                (position)=> {
                    this.setState({
                        lat : position.coords.latitude,
                        long : position.coords.longitude,
                        
                    })
          
                },
                function() {notify.show("Geolocation not available.","error")}
            );
       
        }
        
        this.setState({
            showMap : true
        })
        
    }

    updateState(e){
        this.setState({
			  [e.target.name+"Error"] : ""
		  })
		  this.setState({
			  [e.target.name]:e.target.value,
          })
         
    }
    toogleAddressForm(){
        this.setState({selectedCity : "",selectedCityId : "",showAddressForm : ! this.state.showAddressForm});
    }

    cancelAddressForm(){
        this.setState({selectedCity : "",selectedCityId:"",name:"",lastName:"",houseNo:"",pinCode:"",phoneNumber:"",addressType:"",showAddressForm : ! this.state.showAddressForm});
    }

    setAddress=(data)=>{ 
        if(data.country!=='Saudi Arabia'){
            notify.show('Kindly Select Saudi Arabia Location To Proceed Further');
            return false;
        }
        
        let selCity = this.state.cityData.data.response.filter((city)=>    
            city.city==data.city
        );
        this.setState({
          
           state :  data.state,
           city : data.city,
           houseNo : data.houseNo,
           name : this.state.profileData.response[this.state.profileData.response.length-1].s_firstname,
           lastName :  this.state.profileData.response[this.state.profileData.response.length-1].s_lastname,
           selectedCityId:(selCity[0]!=="" && selCity[0]!==undefined)?selCity[0].id:0,
           selectedCity:(selCity[0]!=="" && selCity[0]!==undefined)?selCity[0].city:""
       })
    }

    addProfile(){
        event.preventDefault();
        
        let error = false;
        
        let vcity = checkRequired(this.state.selectedCity, this.state.language)
        // console.log("hello")
        if(!vcity[0]){
            error = true
            this.setState({
                cityError : vcity[1]
            })
        }
        
        let vname = checkRequired(this.state.name, this.state.language)
        if(!vname[0]){
            error =  true;
            this.setState({
                nameError : vname[1]
            })
        }
        let vphone = checkPhonenumber(this.state.phoneNumber, this.state.language)
        if(!vphone[0]){
            error =  true;
            this.setState({
                phoneNumberError : vphone[1]
            })
        }
        
       
        let vaddress = checkRequired(this.state.addressType, this.state.language)
        if(!vaddress[0]){
            error =  true;
            this.setState({
                addressTypeError : vaddress[1]
            })
        }
        let vhouse = checkRequired(this.state.houseNo, this.state.language)
        if(!vhouse[0]){
            error =  true;
            this.setState({
                houseNoError : vhouse[1]
            })
        }
        // let vpincode = checkPincode(this.state.pinCode, this.state.language);
        // if(!vpincode[0]){
        //     error =  true;
        //     this.setState({
        //         pinCodeError : vpincode[1]
        //     })
        // }
       
        
        if(!error){
            this.setState({
                showLoader : true
            },()=>{
                let data = {
                
                    "s_firstname": this.state.name,
                    "s_lastname": this.state.lastName,
                    "s_address": this.state.houseNo,
                    "s_address_2": '',
                    "s_city": this.state.selectedCity,
                    "s_state": '',
                    "s_zipcode": this.state.pinCode,
                    "s_phone": this.state.phoneNumber,
                    "address_type": this.state.addressType,
                    "platform": "D",
                    "latitude": latLongData?latLongData.lat:"",
                    "longitude": latLongData?latLongData.lng:"",
                    "city_id":this.state.selectedCityId
                }
                if(this.state.editProfileId){
                    data['profile_id'] = this.state.editProfileId
                }
                
                this.props.dispatch(editProfileAction(data));

            })
            
        }
    }
    updateLatLongValue = (data)=>{
        latLongData = data;
        return;
   }
    render() {
        const { classes } = this.props;

        return ( 
            <div className="col-md-9 right_sec address_outer profile_outer order_outer">
                <h2 className="subheading">{local.addrMyAccount[this.state.language]}</h2>
                <ul className="listing_outer">
                {this.state.profileData? this.state.profileData.response.map((value,index)=>{
                    if(!value.address_type){
                        return
                    }
                    return <li className="listing">
                            <div className="details">
                                <div className="inner pl-0">
                                    <p className="inr_detail">{value.s_firstname} {value.s_lastname}</p>
                                    <p className="inr_detail">{value.s_address+ " "+value.s_address_2}</p>
                                    <p className="inr_detail">{value.s_phone}</p>
                                    <a className="common_links"  href="JavaScript:void(0);" onClick={this.editAddress.bind(this,value)}>{local.addrEdit[this.state.language]}</a>
                                    <a className="common_links" href="JavaScript:void(0);" onClick={this.removeAddress.bind(this,value.profile_id)}>{local.addrRemoveAddress[this.state.language]}</a>
                                </div>
                            </div>
                        </li>
                    
                }):""}
                </ul>    
                <a className="add_address" id="address" onClick={this.toogleAddressForm.bind(this)} href="JavaScript:void(0);">{local.addrAddNewAddress[this.state.language]}</a>
                {this.state.showAddressForm ?<div>
                <div className="locatn_sec" >
                        <span><svg className="icon icon-gps"><use xlinkHref="/img/icon-sprite.svg#icon-gps"></use></svg> <a onClick={this.getCurrentLocation.bind(this)} href="Javascript:void(0);">{local.addrUseCurrenLocation[this.state.language]}</a></span>{/*<span className="ml-4 mr-4">or</span> /*}
                        {/* <span><svg className="icon icon-qr-code"><use xlinkHref="/img/icon-sprite.svg#icon-qr-code"></use></svg> <a href="Javascript:void(0);">Upload Address Registry QR Code</a></span> */}
                    </div>
                    {this.state.showMap && <Map updateLatLong={this.updateLatLongValue} address = {this.setAddress} lat =  {this.state.lat} lng = {this.state.long} />}
                    <form className="row">
                        <MuiThemeProvider theme={mui}>
                            <TextField
                              id="name"
                              className="input_outer col-sm-6"
                              label={local.addrFirstName[this.state.language]}
                              type="text"
                              name = "name"
                              value = {this.state.name}
                              helperText={this.state.nameError}
                              onChange = {e=>this.updateState(e)}
                            />
                            <TextField
                              id="lname"
                              className="input_outer col-sm-6"
                              label={local.addrLastName[this.state.language]}
                              type="text"
                              name = "lastName"
                              value = {this.state.lastName}
                              
                              onChange = {e=>this.updateState(e)}
                            />
                            <TextField
                              id="address"
                              name = "houseNo"
                              className="input_outer col-sm-6"
                              label={local.addrAddress[this.state.language]}
                              type="text"
                              value = {this.state.houseNo}
                              helperText={this.state.houseNoError}
                              onChange = {e=>this.updateState(e)}
                            />
                            
                            {/*<TextField
                              id="city"
                              className="input_outer col-sm-6"
                              label={local.addrCity[this.state.language]}
                              type="text"
                              name = "city"
                              value = {this.state.city}
                              helperText={this.state.cityError}
                              onChange = {e=>this.updateState(e)}
                            />*/}
                            <FormGroup className="col-sm-6 col-md-6 d-flex">
                            <FormControl className="formControl slect_outer">
                                    <InputLabel shrink htmlFor="city-placeholder">{local.addrCity[this.state.language]}</InputLabel>
                                    <Select
                                        value={this.state.selectedCityId}
                                        input={<Input name="city" id="city-placeholder" />}
                                        name="city"
                                        className="selectEmpty" 
                                        onChange={(e) => this.selectCity(e.target.value)}
                                        >
                                    {
                                        this.state.cityData.data.response && this.state.cityData.data.response.map(city => (
                                            <MenuItem value={city.id}>{city.city}</MenuItem>
                                        ))
                                        
                                    }
                        
                                    </Select>
                                    </FormControl>
                                    </FormGroup>
                            <TextField
                              id="pincode"
                              className="input_outer col-sm-6"
                              label={local.addrPinCode[this.state.language]}
                              type="text"
                              name = "pinCode"
                              value = {this.state.pinCode}
                              helperText={this.state.pinCodeError}
                              onChange = {e=>this.updateState(e)}
                            /> 
                            
                            <TextField
                              id="phone_number"
                              className="input_outer col-sm-6"
                              label={local.addrPhoneNumber[this.state.language]}
                              type="text"
                              name = "phoneNumber"
                              value = {this.state.phoneNumber}
                              helperText={this.state.phoneNumberError}
                              onChange = {e=>this.updateState(e)}
                            /> 
                        </MuiThemeProvider>
                        <div className="input_radio d-sm-flex w-100 align-items-center">
                            <FormLabel component="legend">{local.addrAddressType[this.state.language]}</FormLabel>
                            <RadioGroup
                                aria-label="Address Type"
                                name="addressType"
                                value={this.state.addressType}
                                onChange = {e=>this.updateState(e)}
                                helperText = {this.state.addressTypeError}
                                className="radio_outer"
                            >
                                <FormControlLabel
                                  value="home"
                                  control={
                                    <Radio
                                      classes={{root: classes.root, checked: classes.checked}}
                                    />
                                  }
                                  label={local.addrHome[this.state.language]}
                                  labelPlacement="end"
                                />
                                <FormControlLabel
                                  value="office"
                                  control={
                                    <Radio
                                      classes={{root: classes.root, checked: classes.checked}}
                                    />
                                  }
                                  label={local.addrOffice[this.state.language]}
                                  labelPlacement="end"
                                />
                                <FormControlLabel
                                  value="other"
                                  control={
                                    <Radio
                                      classes={{root: classes.root, checked: classes.checked}}
                                    />
                                  }
                                  label={local.addrOthers[this.state.language]}
                                  labelPlacement="end"
                                />
                            </RadioGroup>
                        </div>
                        <div>
                        {!this.state.showEditAdress && <div><Button
                            variant="contained"
                            color="primary"
                            onClick ={this.addProfile.bind(this)}
                        >
                            {local.addrSaveAddress[this.state.language]}
                        </Button>
                        <Button
                            variant="contained"
                            color="primary"
                            onClick ={this.cancelAddressForm.bind(this)}
                        >
                            {local.addrCancel[this.state.language]}
                        </Button></div>
                        }
                        {this.state.showEditAdress && <Button
                            variant="contained"
                            color="primary"
                            onClick ={this.addProfile.bind(this,this.state.editProfileId)}
                        >
                            {local.addrUpdateAddress[this.state.language]}
                        </Button>
                        
                        }
                        {this.state.showEditAdress && <span className="icon icon-gps cancel-address" onClick={this.toggleEditAdress.bind(this)}>            
                        
                        {local.addrCancel[this.state.language]}
                        </span>
                        
                        }
                        </div>
                    </form>
                   
                </div>
             :""}   
            {this.state.showLoader ? <Loader />:""}
            </div>
        );
    }
}

function mapStateToProps(state) {
    
    return {
        UserProfileReducer : state.UserProfileReducer,
        RemoveProfileReducer : state.RemoveProfileReducer,
        languageReducer : state.languageReducer,
        cityData: state.cityReducer
    }
}

export default connect(mapStateToProps)(withStyles(styles)(AddressBook));
