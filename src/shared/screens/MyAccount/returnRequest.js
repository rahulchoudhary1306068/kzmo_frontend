import React, { Component } from 'react';
import {connect} from 'react-redux';
import {
  MuiThemeProvider,
  createMuiTheme,
  createGenerateClassName,
} from '@material-ui/core/styles';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Sidebar from '../../components/account_sidebar';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import FormLabel from '@material-ui/core/FormLabel';
import TextField from '@material-ui/core/TextField';
import FormGroup from '@material-ui/core/FormGroup';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Map from '../../components/map';
import { env } from "../../constant";


// import "react-responsive-carousel/lib/styles/carousel.min.css";
import local, {deafultLanguage} from '../../localization';

import { getter, getUserInfo,checkRequired, checkPhonenumber, checkPincode } from '../../commonfunction';
import Loader from "../../components/loader";

import { createReturnDetailAction, submitReturnAction } from '../../actions/myAccount';

import { InitailStateAction,InvalidTokenAction } from '../../actions/login';
import {CityAction} from '../../actions/header';
import { userProfileAction,editProfileAction,userBankAction,addBankAction,editBankAction,allBankAction } from '../../actions/userprofile';
import {notify} from 'react-notify-toast';
import FormHelperText from '@material-ui/core/FormHelperText';

const styles = theme => ({
  root: {
    '&$checked': {
      color: '#e4b122'
    }
  },
  checked: {},
  
});

const mui = createMuiTheme({
  overrides: {
    MuiFormLabel: {
      root: {
        color: "#b1b1b1"
      },
      focused: {
        color: "black !important"
      },
    },
    MuiInput: {
            root: {
                    '&:after': {
                      backgroundColor: '#ff6363',
                    }
              },
        },
  }
});

const CreateReturn = (props) => {
    const {classes} = props.classes;

    let orderInfo = props.data.order_info;
    let reasons = props.data.rma_status_bucket;
    let subReasons = props.data.reasons;
    let userProfile = props.userProfileData.data.response;
    let userBanks = props.userBankData.data.data;
 
    
    return(
        <div className="col-md-9 right_sec  return_outer order_outer">
            <h2 className="subheading">{local.returnRequest[props.language]}</h2>
            <p className="subcontent mb-0">{local.HowToHandleReturn[props.language]}</p>
            <ul className="listing_outer">
                <li className="listing">
                    <div className="details">
                        <p className="mb-3 ordr_detail">
                            {local.orderId[props.language]}: <a href="Javascript:void(0);" >{orderInfo.order_id}</a>
                            <span>{local.orderPlacedDateText[props.language]} {props.timeConverter(orderInfo.timestamp)}</span>
                            <span>{local.orderDeliveredDateText[props.language]} {props.timeConverter(orderInfo.products_delivery_date)}</span>
                            
                        </p>
                        {
                            Object.keys(orderInfo.items).map((itemKey) => {
                                let itemData = orderInfo.items[itemKey];
                                return(    
                                    itemKey == props.itemId && <div>
                                        <img src={itemData.product_image_path} />
                                        <div className="inner">
                                            {itemData.amount && props.itemAmount == 0 && props.initializeQty(Number(itemData.amount))}
                                            <p className="name"><a href="JavaScript:void(0);" >{itemData.product}</a></p>
                                            <p className="color_name">{local.orderTotalAmount[props.language]}{itemData.price}</p>
                                            {Number(itemData.amount) == 1 && <p className="size_name">{local.orderOty[props.language]}: {itemData.amount}</p>}
                                            {Number(itemData.amount) > 1 && <div className="control">
                                                <button onClick="" className="cart-qty-minus btn" type="button" value="-" onClick={() => props.itemAmount > 1 &&  props.updateQty(props.itemAmount - 1)}>-</button>
                                                <input type="text" value={props.itemAmount} title="Qty" className="input-text qty" />
                                                <button onClick="" className="cart-qty-plus btn" type="button" value="+" onClick={() => props.itemAmount < Number(itemData.amount) && props.updateQty(props.itemAmount + 1)}>+</button>
                                            </div>}
                                        </div>
                                    </div>
                                )
                            })
                        }
                    </div>
                </li>
            </ul>
            <ul className="form_list">
                <li className="mb-4">
                    <h4><span>1</span>{local.ReasonForReturn[props.language]}</h4>
                     <form>
                        <FormControl className="formControl d-block slect_outer mb-4 mt-4">
                            <InputLabel shrink htmlFor="city-placeholder">
                                {local.orderSelectReason[props.language]} 
                            </InputLabel>
                            <Select
                                    value={props.reaturnReason}
                                    input={<Input name="select_reason" id="cancel-placeholder" />}
                                    displayEmpty
                                    name="select_reason"
                                    className= {"selectEmpty"} 
                                    onChange={(e) => props.selectReturnReason(e)}
                                >

                                    {/*<MenuItem value="">{local.selectreturnReason[props.language]}</MenuItem>*/}

                                    {
                                        Object.keys(reasons).map((reasonKey) => (
                                            <MenuItem value={reasonKey}>{reasons[reasonKey]}</MenuItem>
                                        ))
                                    }
                            </Select>
                            {props.select_reasonError && <FormHelperText style={{color:"red"}}>{local.selectreasonError[props.language]}</FormHelperText>}
                        </FormControl>

                        {
                            props.reaturnReason && <FormControl className="formControl d-block slect_outer mb-4 mt-4">
                                <InputLabel shrink htmlFor="city-placeholder">
                                   {local.orderSelectSubReason[props.language]} 
                                </InputLabel>
                                <Select
                                        value={props.reaturnSubReason}
                                        input={<Input name="select_sub_reason" id="cancel-placeholder" />}
                                        displayEmpty
                                        name="select_sub_reason"
                                        className={"selectEmpty"}
                                        onChange={(e) => props.selectReturnSubReason(e)}
                                    >
                                        {/*<MenuItem value="">{local.selectreturnSubReason[props.language]}</MenuItem>*/}
                                        {
                                            Object.keys(subReasons).map((reasonKey) => (
                                                subReasons[reasonKey].bucket_id == props.reaturnReason && <MenuItem value={reasonKey}>{subReasons[reasonKey].property}</MenuItem>
                                            ))
                                        }
                                </Select>
                                {props.select_sub_reasonError && <FormHelperText style={{color:"red"}}>{local.selectsubreasonError[props.language]}</FormHelperText>}
                            </FormControl>
                        }

                        <MuiThemeProvider theme={mui}>
                            <TextField
                                id="feedback"
                                className="input_outer textField mt-1"
                                label={local.comments[props.language]}
                                multiline
                                required
                                rowsMax="4"
                                fullWidth 
                                value={props.userComment} 
                                onChange={(e) => props.updateUserComment(e.target.value)}
                                
                                error={props.userCommentError!==""?true:false}    
                            />
                            {/*props.userCommentError && (function(){alert("sd");document.getElementById("feedback-helper-text").style.color="red";$("#feedback-helper-text").css('color','#f44336');})()*/}
                        </MuiThemeProvider>
                    </form>
                </li>
                <li className="mb-4">
                    <h4><span>2</span>{local.PickUpAddress[props.language]}</h4>
                    <div className="d-flex mt-4 align-items-baseline">
                        {
                            userProfile[props.selectedProfileIndex] && <div className="address col-sm-7">
                                <p>{userProfile[props.selectedProfileIndex].s_firstname} {userProfile[props.selectedProfileIndex].s_lastname} </p>
                                <p>{userProfile[props.selectedProfileIndex].s_address+ " "+userProfile[props.selectedProfileIndex].s_address_2}</p>
                                <p>{userProfile[props.selectedProfileIndex].s_phone}</p>
                                <a href="Javascript:void(0);" data-toggle="modal" data-target="#check_address">{local.ChangeAddress[props.language]}</a>
                            </div>
                        }
                        <div className="verify">
                            <img src="/img/tick.png" />
                            {local.AddressEligibleForPickUp[props.language]}
                        </div>
                    </div>
                </li>
                <li className="mb-4 d-flex">
                    <div className="col-sm-6">
                        <h4><span>3</span> {local.refundOrReplacement[props.language]}</h4>
                        <div className="input_radio d-sm-flex w-100 align-items-center">
                            <RadioGroup
                                aria-label="Return Type"
                                name="returnType"
                                value={props.returnAction}
                                onChange = {e => props.updateReturnAction(e.target.value)}
                                helperText = ""
                                className="radio_outer"
                            >
                                {/*<FormControlLabel
                                  value="1"
                                  control={
                                    <Radio
                                      classes={{root: classes.root, checked: classes.checked}}
                                    />
                                  }
                                  label="Replacement"
                                  labelPlacement="end"
                                />*/}
                                <FormControlLabel
                                  value="2"
                                  control={
                                    <Radio
                                      classes={{root: classes.root, checked: classes.checked}}
                                    />
                                  }
                                  label={local.refund[props.language]}
                                  labelPlacement="end"
                                />
                            </RadioGroup>
                        </div>
                        {/*
                            props.returnAction == "1" && <div className="radio_contnt mt-3 mb-3">
                                <ul>
                                    <li>Replacement is subject to availability of product.</li>
                                    <li>In case product is not available, you will be refunded.</li>
                                </ul>
                            </div>
                        */}
                        <Button variant="contained" color="primary" onClick={() => props.submitReturn()}>{local.orderSubmit[props.language]}</Button>
                    </div>
                    {
                        props.returnAction == "2" && <div className="mode_outer">
                            <h4>{local.returnModeOfRefund[props.language]}</h4>
                            <div className="input_radio d-sm-flex w-100 align-items-center">
                                <RadioGroup
                                    aria-label="Return Mode"
                                    name="returnMode"
                                    value={props.refundMode}
                                    onChange = {e => props.updateRefundMode(e.target.value)}
                                    helperText = ""
                                    className="radio_outer"
                                >
                                    {/*<FormControlLabel
                                      value="refund_in_cb"
                                      control={
                                        <Radio
                                          classes={{root: classes.root, checked: classes.checked}}
                                        />
                                      }
                                      label="Refund in KZMO points" 
                                      labelPlacement="end"
                                    />
                                    <div className="content w-100">
                                        <ul>
                                            <li>Refund within 24 Hours</li>
                                            <li>Will get credited to KZMO Account</li>
                                        </ul>
                                    </div>*/}
                                    <FormControlLabel
                                      value="refund_in_ac"
                                      control={
                                        <Radio
                                          classes={{root: classes.root, checked: classes.checked}}
                                        />
                                      }
                                      label={local.refundIn[props.language]}
                                      labelPlacement="end"
                                    />
                                </RadioGroup>
                            </div>
                            <div className="d-flex mt-4 align-items-baseline" >
                        {
                            userBanks[props.selectedBankIndex]&& props.refundMode ==="refund_in_ac" && <div className="address col-sm-7" id="userselectedBank">
                                <p>{userBanks[props.selectedBankIndex].account_holder_name} </p>
                                {/*<p>User Id: {userBanks[props.selectedBankIndex].user_id} </p>
                                <p>Neft Id: {userBanks[props.selectedBankIndex].user_neft_id} </p>*/}
                                <p>{local.bankName[props.language]}: {userBanks[props.selectedBankIndex].bank_name}</p>
                                <p>{local.accountNumber[props.language]}: {userBanks[props.selectedBankIndex].account_no}</p>
                                <p>{local.iban[props.language]}: {userBanks[props.selectedBankIndex].IBAN}</p>
                                <a href="Javascript:void(0);" data-toggle="modal" data-target="#check_banks">{local.ChangeBank[props.language]}</a>
                            </div>
                        }
                        
                    </div>
                        </div>

                    }
                </li>
            </ul>
            <AddressPopup 
                userProfileData={props.userProfileData}
                userProfileDataError={props.userProfileDataError}
                userBankData={props.userBankData}
                userBankDataError={props.userBankDataError} 
                selectedProfileTempIndex={props.selectedProfileTempIndex} 
                selectAddress={props.selectAddress} 
                classes={props.classes} 
                updateChangedAddress={props.updateChangedAddress}
                addProfileAddress={props.addProfileAddress}
                updateState={props.updateState}
                language={props.language}
                lat={props.lat}
                long={props.long}
                showMap={props.showMap}
                updateLatLongValue={props.updateLatLongValue}
                setAddress={props.setAddress}
                getCurrentLocation = {props.getCurrentLocation}
                editProfileDetails = {props.editProfileDetails}
                houseNo = {props.houseNo}
                houseNoError={props.houseNoError}
                city = {props.city}
                cityError={props.cityError}
                phoneNo = {props.phoneNo}
                phoneNumberError={props.phoneNumberError}
                name = {props.name}
                nameError={props.nameError}
                addressType = {props.addressType}
                addressTypeError={props.addressTypeError}
                editProfileAddress = {props.editProfileAddress}
                selectIsActiveBank = {props.selectIsActiveBank}
                addBankInit = {props.addBankInit}
                addAddressInit = {props.addAddressInit}
                allBankData={props.allBankData}
                selectedBank={props.selectedBank}
                selectedBankError={props.selectedBankError}
                selectBanktoAdd={props.selectBanktoAdd}
                cityData= {props.cityData}
                selectedCity={props.selectedCity}
                selectedCityId={props.selectedCityId}
                selectCityFunction={props.selectCityFunction}
            />

            <div className="modal canceloder_outer address_outerpopup order_outerpopup" id="check_banks">
                <div className="modal-dialog">
                    <div className="modal-content">
                        <div className="modal-body">
                            <button id="close-bank-popup" type="button" className="close" data-dismiss="modal">&times;</button>
                            <h2 className="text-center">{local.ChooseBank[props.language]}</h2>
                            <a className="add_new" href="Javascript:void(0);" data-toggle="modal" data-target="#add_bank_modal" data-dismiss="modal"  onClick={()=>props.addBankInit()}>{local.AddNewBank[props.language]}</a>
                            <form>
                                <RadioGroup
                                    aria-label="Banks"
                                    name="addressType"
                                    value={props.selectedBankTempIndex} 
                                    onChange = {(e) => props.selectBank(e.target.value)} 
                                    helperText = ""
                                    className="radio_outer"
                                >
                                    {  
                                        userBanks.map((profile, index) => (
                                            <FormControlLabel
                                              value={index}
                                              control={
                                                <Radio
                                                  checked={index == props.selectedBankTempIndex }
                                                  classes={{root: classes.root, checked: classes.checked}}
                                                  
                                                />
                                              }
                                              label={<div className="content"><p>{profile.account_holder_name} </p>
                                              <p>{local.iban[props.language]}: {profile.IBAN}</p>
                                              <p>{local.bankName[props.language]}: {profile.bank_name}</p>
                                              <p>{local.accountNumber[props.language]}: {profile.account_no}</p>
                                                <a href="JavaScript:void(0);" onClick={() => props.editBankDetails({index},props.userBankData)}  data-toggle="modal" data-target="#edit_bank_modal" data-dismiss="modal">{local.addrEdit[props.language]}</a></div>}
                                              labelPlacement="end"
                                            />
                                        ))
                                    }
                                    {/*<div className="verify mt-2 mb-3">
                                        <img src="/img/tick.png" />
                                        This address is eligible for pickup.
                                    </div>*/}
                                </RadioGroup>
                                <button type="button" onClick={() => props.updateChangedBank()}>{local.logContinue[props.language]}</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

            {<div className="modal canceloder_outer right_sec address_outer profile_outer add_addresspopup order_outerpopup" id="add_bank_modal">
                <div className="modal-dialog">
                    <div className="modal-content">
                        <div className="modal-body">
                            <button type="button" id="add_bank_close_modal" className="close" data-dismiss="modal">&times;</button>
                            <h2 className="text-center">{local.AddNewBank[props.language]}</h2>
                            <a className="add_new" href="Javascript:void(0);" data-toggle="modal" data-target="#check_banks" data-dismiss="modal">{local.BackToBanks[props.language]}</a>
                            <form className="row mt-4">
                                <MuiThemeProvider theme={mui}>
                                    <TextField
                                      id="account_holder_name"
                                      name = "accHolderName"
                                      className="input_outer col-sm-6"
                                      label={local.accountHolderName[props.language]}
                                      type="text"
                                      value = {props.accHolderName}
                                      helperText={props.accHolderNameError}
                                      onChange = {e=>props.updateState(e)}
                                    />
                                    <TextField
                                      id="accNo"
                                      className="input_outer col-sm-6"
                                      label={local.accountNumber[props.language]}
                                      type="text"
                                      name = "accNo"
                                      value = {props.accNo}
                                      helperText={props.accNoError}
                                      onChange = {e=>props.updateState(e)}
                                    />
                                    <TextField
                                      id="IBAN"
                                      className="input_outer col-sm-6"
                                      label={local.iban[props.language]}
                                      type="text"
                                      name = "iban"
                                      value = {props.iban}
                                      helperText={props.ibanError}
                                      onChange = {e=>props.updateState(e)}
                                    />
                                    <FormGroup className="col-sm-6 col-md-6">
                                    <FormControl className="formControl slect_outer">
                                    <InputLabel shrink htmlFor="city-placeholder">{local.bankName[props.language]}</InputLabel>
                                    <Select
                                    value={props.bankName}
                                    displayEmpty
                                    name="bankName"
                                    className={props.bankNameError +" selectEmpty"} 
                                    onChange={(e) => props.selectBanktoAdd(e)}
                                    helperText = {props.bankNameError}
                                >
                                
                                {/*<MenuItem value="">{local.bankName[props.language]}</MenuItem>*/}
                                    {
                                        props.allBankData.data.data.map((item) => (
                                            <MenuItem value={item.bank_name}>{item.bank_name}</MenuItem>
                                        ))
                                    }
                                    
                                    
                            </Select>
                            </FormControl>
                            </FormGroup>
                                    {/*<TextField
                                      id="bank_name"
                                      className="input_outer col-sm-6"
                                      label={local.bankName[props.language]}
                                      type="text"
                                      name = "bankName"
                                      value = {props.bankName}
                                      helperText={props.bankNameError}
                                      onChange = {e=>props.updateState(e)}
                                    />
                                    <TextField
                                      id="user_id"
                                      className="input_outer col-sm-6"
                                      label={local.userId[props.language]}
                                      type="text"
                                      name = "userId"
                                      value = {props.userId}
                                      helperText={props.userIdError}
                                      onChange = {e=>props.updateState(e)}
                                    />
                                    <TextField
                                      id="user_neft_id"
                                      className="input_outer col-sm-6"
                                      label={local.userNeftId[props.language]}
                                      type="text"
                                      name = "userNeftId"
                                      value = {props.userNeftId}
                                      helperText={props.userNeftIdError}
                                      onChange = {e=>props.updateState(e)}
                                    />

                                    <TextField
                                      id="bank_type"
                                      className="input_outer col-sm-6"
                                      label={local.bankType[props.language]}
                                      type="text"
                                      name = "bankType"
                                      value = {props.bankType}
                                      helperText={props.bankTypeError}
                                      onChange = {e=>props.updateState(e)}
                                    />
                                    <TextField
                                      id="is_active"
                                      className="input_outer col-sm-6"
                                      label={local.isActive[props.language]}
                                      type="text"
                                      name = "isActive"
                                      value = {props.isActive}
                                      helperText={props.isActiveError}
                                      onChange = {e=>props.updateState(e)}
                                    />

                                    <Select
                                    value={props.isActive}
                                    
                                    displayEmpty
                                    name="bankisActive"
                                    className="col-sm-6" 
                                    onChange={(e) => props.selectIsActiveBank(e)}
                                    helperText = {props.isActiveError}
                                >
                                    <MenuItem value="">{local.isActive[props.language]}</MenuItem>
                                    <MenuItem value="Y">{local.isActiveOption[0][props.language]}</MenuItem>
                                    <MenuItem value="N">{local.isActiveOption[1][props.language]}</MenuItem>
                            </Select>*/}
                            
                            
                                </MuiThemeProvider>
                                <div>
                                    <Button
                                        variant="contained"
                                        color="primary"
                                        onClick={() => props.addNewBank()}
                                        
                                    >
                                    {local.bankSaveAddress[props.language]}
                                    </Button>
                                </div>
                            </form>    
                            
                        </div>
                    </div>
                </div>
            </div>}


            {<div className="modal canceloder_outer right_sec address_outer profile_outer add_addresspopup order_outerpopup" id="edit_bank_modal">
                <div className="modal-dialog">
                    <div className="modal-content">
                        <div className="modal-body">
                            <button type="button" id="edit_bank_close_modal" className="close" data-dismiss="modal">&times;</button>
                            <h2 className="text-center">{local.EditBank[props.language]}</h2>
                            <a className="add_new" href="Javascript:void(0);" data-toggle="modal" data-target="#check_banks" data-dismiss="modal">{local.BackToBanks[props.language]}</a>
                            <form className="row mt-4">
                                <MuiThemeProvider theme={mui}>
                                    <TextField
                                      id="account_holder_name"
                                      name = "accHolderName"
                                      className="input_outer col-sm-6"
                                      label={local.accountHolderName[props.language]}
                                      type="text"
                                      value = {props.accHolderName}
                                      helperText={props.accHolderNameError}
                                      onChange = {e=>props.updateState(e)}
                                    />
                                    <TextField
                                      id="accNo"
                                      className="input_outer col-sm-6"
                                      label={local.accountNumber[props.language]}
                                      type="text"
                                      name = "accNo"
                                      value = {props.accNo}
                                      helperText={props.accNoError}
                                      onChange = {e=>props.updateState(e)}
                                    />
                                    <TextField
                                      id="IBAN"
                                      className="input_outer col-sm-6"
                                      label={local.iban[props.language]}
                                      type="text"
                                      name = "iban"
                                      value = {props.iban}
                                      helperText={props.ibanError}
                                      onChange = {e=>props.updateState(e)}
                                    />
                                    <FormGroup className="col-sm-6 col-md-6">
                                    <FormControl className="formControl slect_outer">
                                    <InputLabel shrink htmlFor="city-placeholder">{local.bankName[props.language]}</InputLabel>
                                    <Select
                                    value={props.bankName}
                                    
                                    displayEmpty
                                    name="bankName"
                                    className={props.bankNameError +" selectEmpty"} 
                                    onChange={(e) => props.selectBanktoAdd(e)}
                                    helperText = {props.bankNameError}
                                >
                                
                                {/*<MenuItem value="">{local.bankName[props.language]}</MenuItem>*/}
                                    {
                                        props.allBankData.data.data.map((item) => (
                                            <MenuItem value={item.bank_name}>{item.bank_name}</MenuItem>
                                        ))
                                    }
                                    
                                    
                                </Select>
                                </FormControl>
                                </FormGroup>
                                    {/*<TextField
                                      id="bank_name"
                                      className="input_outer col-sm-6"
                                      label={local.bankName[props.language]}
                                      type="text"
                                      name = "bankName"
                                      value = {props.bankName}
                                      helperText={props.bankNameError}
                                      onChange = {e=>props.updateState(e)}
                                    />
                                    <TextField
                                      id="user_id"
                                      className="input_outer col-sm-6"
                                      label={local.userId[props.language]}
                                      type="text"
                                      name = "userId"
                                      value = {props.userId}
                                      helperText={props.userIdError}
                                      onChange = {e=>props.updateState(e)}
                                    />
                                    <TextField
                                      id="user_neft_id"
                                      className="input_outer col-sm-6"
                                      label={local.userNeftId[props.language]}
                                      type="text"
                                      name = "userNeftId"
                                      value = {props.userNeftId}
                                      helperText={props.userNeftIdError}
                                      onChange = {e=>props.updateState(e)}
                                    />

                                    {<TextField
                                      id="bank_type"
                                      className="input_outer col-sm-6"
                                      label={local.bankType[props.language]}
                                      type="text"
                                      name = "bankType"
                                      value = {props.bankType}
                                      helperText={props.bankTypeError}
                                      onChange = {e=>props.updateState(e)}
                                    />
                                    <TextField
                                      id="is_active"
                                      className="input_outer col-sm-6"
                                      label={local.isActive[props.language]}
                                      type="text"
                                      name = "isActive"
                                      value = {props.isActive}
                                      helperText={props.isActiveError}
                                      onChange = {e=>props.updateState(e)}
                                    />

                                    <Select
                                    value={props.isActive}
                                    
                                    displayEmpty
                                    name="bankisActive"
                                    className="col-sm-6" 
                                    onChange={(e) => props.selectIsActiveBank(e)}
                                    helperText = {props.isActiveError}
                                >
                                    <MenuItem value="">{local.isActive[props.language]}</MenuItem>
                                    <MenuItem value="Y">{local.isActiveOption[0][props.language]}</MenuItem>
                                    <MenuItem value="N">{local.isActiveOption[1][props.language]}</MenuItem>
                                    </Select>*/} 
                                </MuiThemeProvider>
                                <div>
                                    <Button
                                        variant="contained"
                                        color="primary"
                                        onClick={() => props.editNewBank()}
                                        
                                    >
                                    {local.bankSaveAddress[props.language]}
                                    </Button>
                                </div>
                            </form>    
                            
                        </div>
                    </div>
                </div>
            </div>}
        </div>
    )
}

const AddressPopup = (props) => {
    const {classes} = props.classes;
    return (
        <div>
            <div className="modal canceloder_outer address_outerpopup order_outerpopup" id="check_address">
                <div className="modal-dialog">
                    <div className="modal-content">
                        <div className="modal-body">
                            <button id="close-address-popup" type="button" className="close" data-dismiss="modal">&times;</button>
                            <h2 className="text-center">{local.chooseAnAddress[props.language]}</h2>
                            <a className="add_new" href="Javascript:void(0);" data-toggle="modal" data-target="#add_address" data-dismiss="modal"  onClick={()=>props.addAddressInit()}>{local.addrAddNewAddress[props.language]}</a>
                            <form>
                                <RadioGroup
                                    aria-label="Address Type"
                                    name="addressType"
                                    value={props.selectedProfileTempIndex} 
                                    onChange = {(e) => props.selectAddress(e.target.value)} 
                                    helperText = ""
                                    className="radio_outer"
                                >
                                    {
                                        props.userProfileData.data.response.filter(profile=> profile.profile_type!=="P").map((profile, index) => (
                                            <FormControlLabel
                                              value={index}
                                              control={
                                                <Radio
                                                  checked={index == props.selectedProfileTempIndex }
                                                  classes={{root: classes.root, checked: classes.checked}}
                                                  
                                                />
                                              }
                                              label={
                                                <div className="content"><p>{profile.s_firstname} {profile.s_lastname} </p><p>{profile.s_address+ " "+profile.s_address_2}</p>
                                                <p>{profile.s_phone}</p><a href="JavaScript:void(0);" data-toggle="modal" data-target="#edit_address" data-dismiss="modal" onClick={()=>props.editProfileDetails(index,profile)}>{local.addrEdit[props.language]}</a></div>
                                                                            
                                              }
                                              labelPlacement="end"
                                            />
                                        ))
                                    }
                                    {/*<div className="verify mt-2 mb-3">
                                        <img src="/img/tick.png" />
                                        This address is eligible for pickup.
                                    </div>*/}
                                </RadioGroup>
                                <button type="button" onClick={() => props.updateChangedAddress()}>{local.logContinue[props.language]}</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            {<div className="modal canceloder_outer right_sec address_outer profile_outer add_addresspopup order_outerpopup" id="add_address">
                <div className="modal-dialog">
                    <div className="modal-content">
                        <div className="modal-body">
                            <button type="button" id="add_address_close_modal" className="close" data-dismiss="modal">&times;</button>
                            <h2 className="text-center">{local.addrAddNewAddress[props.language]}</h2>
                            <a className="add_new" href="Javascript:void(0);" data-toggle="modal" data-target="#check_address" data-dismiss="modal">{local.backtoaddress[props.language]}</a>
                            <div className="locatn_sec" >
                                <span><svg className="icon icon-gps"><use xlinkHref="/img/icon-sprite.svg#icon-gps"></use></svg> <a href="Javascript:void(0);" onClick={props.getCurrentLocation.bind(props)}>{local.addrUseCurrenLocation[props.language]}</a></span>
                                {/*<span className="ml-4 mr-4">or</span>
                                <span><svg className="icon icon-qr-code"><use xlinkHref="/img/icon-sprite.svg#icon-qr-code"></use></svg> <a href="Javascript:void(0);">Upload Address Registry QR Code</a></span>*/}
                            </div>
                            {props.showMap && <Map updateLatLong={props.updateLatLongValue} address = {props.setAddress} lat =  {props.lat} lng = {props.long} />}
                            {/*<img className="w-100" src="/img/map.jpg" />*/}
                            <form className="row mt-4">
                                <MuiThemeProvider theme={mui}>
                                    <TextField
                                      id="address"
                                      name = "houseNo"
                                      className="input_outer col-sm-6"
                                      label={local.checkHouseNumber[props.language]}
                                      type="text"
                                      value = {props.houseNo}
                                      helperText={props.houseNoError}
                                      onChange = {e=>props.updateState(e)}
                                    />
                                    {/*<TextField
                                      id="city"
                                      className="input_outer col-sm-6"
                                      label={local.addrCity[props.language]}
                                      type="text"
                                      name = "city"
                                      value = {props.city}
                                      helperText={props.cityError}
                                      onChange = {e=>props.updateState(e)}
                                    />*/}
                                    <FormGroup className="col-sm-6 col-md-6">
                                    <FormControl className="formControl slect_outer">
                                    <InputLabel shrink htmlFor="city-placeholder">{local.addrCity[props.language]}</InputLabel>
                                    <Select
                                        value={props.selectedCityId}
                                        input={<Input name="city" id="city-placeholder" />}
                                        displayEmpty
                                        name="city"
                                        className={props.cityError +" selectEmpty"}
                                        helperText = {props.cityError} 
                                        onChange={(e) => props.selectCityFunction(e.target.value)}
                                        >
                                    {
                                        props.cityData.response && props.cityData.response.map(city => (
                                            <MenuItem value={city.id}>{city.city}</MenuItem>
                                        ))
                                        
                                    }
                        
                                    </Select>
                                    </FormControl>
                                    </FormGroup>
                                    <TextField
                                      id="name"
                                      className="input_outer col-sm-6"
                                      label={local.addrName[props.language]}
                                      type="text"
                                      name = "name"
                                      value = {props.name}
                                      helperText={props.nameError}
                                      onChange = {e=>props.updateState(e)}
                                    />
                                    <TextField
                                      id="phone_number"
                                      className="input_outer col-sm-6"
                                      label={local.addrPhoneNumber[props.language]}
                                      type="text"
                                      name = "phoneNo"
                                      value = {props.phoneNo}
                                      helperText={props.phoneNumberError}
                                      onChange = {e=>props.updateState(e)}
                                    />  
                                </MuiThemeProvider>
                                <div className="input_radio d-sm-flex w-100 align-items-center">
                                    <FormLabel component="legend"></FormLabel>
                                    <RadioGroup
                                        aria-label="Address Type"
                                        name="addressType"
                                        value={props.addressType}
                                        onChange = {e=>props.updateState(e)}
                                        helperText = {props.addressTypeError}
                                        className="radio_outer"
                                    >

                                    <FormControlLabel
                                          value="home"
                                          control={
                                            <Radio
                                              classes={{root: classes.root, checked: classes.checked}}
                                            />
                                          }
                                          label={local.addrHome[props.language]}
                                          labelPlacement="end"
                                        />
                                        <FormControlLabel
                                          value="office"
                                          control={
                                            <Radio
                                              classes={{root: classes.root, checked: classes.checked}}
                                            />
                                          }
                                          label={local.addrOffice[props.language]}
                                          labelPlacement="end"
                                        />
                                        <FormControlLabel
                                          value="other"
                                          control={
                                            <Radio
                                              classes={{root: classes.root, checked: classes.checked}}
                                            />
                                          }
                                          label={local.addrOthers[props.language]}
                                          labelPlacement="end"
                                        />
                                    </RadioGroup>
                                </div>
                               <div>
                                    <Button
                                        variant="contained"
                                        color="primary"
                                        onClick={() => props.addProfileAddress()}
                                        
                                    >
                                    {local.addrSaveAddress[props.language]}
                                    </Button>
                                </div>
  
                            </form>
                        </div>
                    </div>
                </div>
            </div>}

            {<div className="modal canceloder_outer right_sec address_outer profile_outer add_addresspopup order_outerpopup" id="edit_address">
                <div className="modal-dialog">
                    <div className="modal-content">
                        <div className="modal-body">
                            <button type="button" id="edit_address_close_modal" className="close" data-dismiss="modal">&times;</button>
                            <h2 className="text-center">{local.editAddress[props.language]}</h2>
                            <a className="add_new" href="Javascript:void(0);" data-toggle="modal" data-target="#check_address" data-dismiss="modal">{local.backtoaddress[props.language]}</a>
                            <div className="locatn_sec" >
                                {/*<span><svg className="icon icon-gps"><use xlinkHref="/img/icon-sprite.svg#icon-gps"></use></svg> <a href="Javascript:void(0);" onClick={props.getCurrentLocation.bind(props)}>{local.addrUseCurrenLocation[props.language]}</a></span>
                                <span className="ml-4 mr-4">or</span>
                                <span><svg className="icon icon-qr-code"><use xlinkHref="/img/icon-sprite.svg#icon-qr-code"></use></svg> <a href="Javascript:void(0);">Upload Address Registry QR Code</a></span>*/}
                            </div>
                            {/*props.showMap && <Map updateLatLong={props.updateLatLongValue} address = {props.setAddress} lat =  {props.lat} lng = {props.long} />*/}
                            {/*<img className="w-100" src="/img/map.jpg" />*/}
                            <form className="row mt-4">
                                <MuiThemeProvider theme={mui}>
                                    <TextField
                                      id="address"
                                      name = "houseNo"
                                      className="input_outer col-sm-6"
                                      label={local.checkHouseNumber[props.language]}
                                      type="text"
                                      value = {props.houseNo}
                                      helperText={props.houseNoError}
                                      onChange = {e=>props.updateState(e)}
                                    />
                                    {/*<TextField
                                      id="city"
                                      className="input_outer col-sm-6"
                                      label={local.addrCity[props.language]}
                                      type="text"
                                      name = "city"
                                      value = {props.city}
                                      helperText={props.cityError}
                                      onChange = {e=>props.updateState(e)}
                                    />*/}
                                    <FormGroup className="col-sm-6 col-md-6">
                                    <FormControl className="formControl slect_outer">
                                    <InputLabel shrink htmlFor="city-placeholder">{local.addrCity[props.language]}</InputLabel>
                                    <Select
                                        value={props.selectedCityId}
                                        input={<Input name="city" id="city-placeholder" />}
                                        name="city"
                                        className={props.cityError +" selectEmpty"}
                                        helperText = {props.cityError} 
                                        onChange={(e) => props.selectCityFunction(e.target.value)}
                                        >
                                    {
                                        props.cityData.response && props.cityData.response.map(city => (
                                            <MenuItem value={city.id}>{city.city}</MenuItem>
                                        ))
                                        
                                    }
                        
                                    </Select>
                                    </FormControl>
                                    </FormGroup>
                                    <TextField
                                      id="name"
                                      className="input_outer col-sm-6"
                                      label={local.addrName[props.language]}
                                      type="text"
                                      name = "name"
                                      value = {props.name}
                                      helperText={props.nameError}
                                      onChange = {e=>props.updateState(e)}
                                    />
                                    <TextField
                                      id="phone_number"
                                      className="input_outer col-sm-6"
                                      label={local.addrPhoneNumber[props.language]}
                                      type="text"
                                      name = "phoneNo"
                                      value = {props.phoneNo}
                                      helperText={props.phoneNumberError}
                                      onChange = {e=>props.updateState(e)}
                                    />  
                                </MuiThemeProvider>
                                <div className="input_radio d-sm-flex w-100 align-items-center">
                                    <FormLabel component="legend"></FormLabel>
                                    <RadioGroup
                                        aria-label="Address Type"
                                        name="addressType"
                                        value={props.addressType}
                                        onChange = {e=>props.updateState(e)}
                                        helperText = {props.addressTypeError}
                                        className="radio_outer"
                                    >

                                    <FormControlLabel
                                          value="home"
                                          control={
                                            <Radio
                                              classes={{root: classes.root, checked: classes.checked}}
                                            />
                                          }
                                          label={local.addrHome[props.language]}
                                          labelPlacement="end"
                                        />
                                        <FormControlLabel
                                          value="office"
                                          control={
                                            <Radio
                                              classes={{root: classes.root, checked: classes.checked}}
                                            />
                                          }
                                          label={local.addrOffice[props.language]}
                                          labelPlacement="end"
                                        />
                                        <FormControlLabel
                                          value="other"
                                          control={
                                            <Radio
                                              classes={{root: classes.root, checked: classes.checked}}
                                            />
                                          }
                                          label={local.addrOthers[props.language]}
                                          labelPlacement="end"
                                        />
                                    </RadioGroup>
                                </div>
                               <div>
                                    <Button
                                        variant="contained"
                                        color="primary"
                                        onClick={() => props.editProfileAddress()}
                                        
                                    >
                                    {local.addrSaveAddress[props.language]}
                                    </Button>
                                </div>
  
                            </form>
                        </div>
                    </div>
                </div>
            </div>}
        </div>
    );
}

const ReturnThankYou = (props) => {
    return(
        <div className="col-md-9 right_sec return_thankyou return_outer order_outer">
            <h2 className="subheading">{local.thankyou[props.language]}</h2>
            <p className="thankyoucontnt"> <img src="/img/tick.png" />{local.returnRequestSubmitted[props.language]}</p>
            {props.submitReturnData.return_info && <p>{local.returnRequestNumber[props.language]} : {props.submitReturnData.return_info.return_id}</p>}
            <p className="subsubheading mb-0">{local.returnDetails[props.language]}</p>
            
            <ul className="listing_outer">
                <li className="listing">
                    <div className="details mb-4">
                        <p className="mb-3 ordr_detail">
                            {local.orderId[props.language]}: <a href="Javascript:void(0);" >{props.data.order_info.order_id}</a>
                            <span>{local.orderPlacedDateText[props.language]} {props.timeConverter(props.data.order_info.timestamp)}</span>
                            <span>{local.orderDeliveredDateText[props.language]} {props.timeConverter(props.data.order_info.products_delivery_date)}</span>
           
                        </p>
                        {
                            Object.keys(props.data.order_info.items).map((itemKey) => {
                                let itemData = props.data.order_info.items[itemKey];
                                return(
                                    <div>
                                        <img src={itemData.product_image_path} />
                                        <div className="inner">
                                            <p className="name"><a href="JavaScript:void(0);" >{itemData.product}</a></p>
                                            <p className="color_name">{local.orderTotalAmount[props.language]}{itemData.price}</p>
                                            <p className="size_name">{local.orderOty[props.language]} {itemData.amount}</p>
                                        </div>
                                    </div>
                                )
                            })
                        }
                    </div>
                    <p className="subsubheading mb-0">{local.returnInfo[props.language]}</p>
                    <ul className="mt-3 sub_list mb-3">
                        <li><span>{local.returnReason[props.language]}</span> : {props.data.rma_status_bucket[props.reaturnReason]}</li>
                        <li><span>{local.returnYouHaveAsked[props.language]}</span> : {props.returnAction == "1" ? "Replacement" : "Refund"}</li>
                        {props.returnAction == "2" && <li><span>{local.returnModeOfRefund[props.language]}</span> : {props.refundMode == "refund_in_cb" ? local.returnRefundInKZMO[props.language] : local.refundIn[props.language]}</li>}
                        <li><span>{local.comments[props.language]} </span>: {props.userComment}</li>
                    </ul>
                    <Button variant="contained" color="primary" onClick={() => props.goToOrders()}>
                        {local.BackToOrders[props.language]}
                    </Button>
                </li>
            </ul>
        </div>
    )
}

let latLongData;
class ReturnRequest extends Component {
    constructor(props){
        super(props);
        this.state = {
            language: '',
            orderId: "",
            itemId: "",
            returnDetails: {},
            reaturnReason: "",
            reaturnSubReason: "",
            userComment: "",
            userProfileData: "",
            userProfileDataError:"",
            selectedProfileIndex: 0,
            selectedProfileTempIndex: 0,
            returnAction: "1",
            refundMode: "refund_in_cb",
            submitReturnData: "",
            select_reasonError : "",
            select_sub_reasonError : "",
            itemAmount: 0,
            city:"",
            name:"",
            phoneNo:"",
            houseNo:"",
            cityError:"",
            nameError:"",
            phoneNumberError:"",
            houseNoError:"",
            addressType:"",
            addressTypeError:"",
            newProfileAdd : false,
            showMap : false,
            profileData:false,
            lat : 23.7224509,
            long : 46.7893653,
            mapheight : "0px",
            userBankData:"",
            userBankDataError:"",
            accHolderName : "",
            accHolderNameError : "",
            accNo : "",
            accNoError : "",
            iban : "",
            ibanError : "",
            bankName : "",
            bankNameError : "",
            userId : "",
            userIdError : "",
            userNeftId : "",
            userNeftIdError : "",
            bankType:"",
            bankTypeError:"",
            isActive:"",
            isActiveError:"",
            newBankAdd: false,
            selectedBankTempIndex:0,
            selectedBankIndex:0,
            editableBankIndex:0,
            bankIsActiveError:"",
            allBankData:"",
            selectedBank:"",
            selectedBankError:"",
            profileId:"",
            profileIdError:"",
            cityData:{},
            cityDataError:"",    
            selectedCity:"",
            selectedCityId:"",
            countryDefaultValue : "SA",
            actionIdentifierFlag:{profile:"",bank:""},
            userCommentError:""
        };
    }

    static initialAction({url, store}) {
        return true;
    }

    componentDidMount(){
        this.setState({
            language: getter('language') ? getter('language') : deafultLanguage
        });

        this.setState({orderId: this.props.match.params.orderId});
        this.setState({itemId: this.props.match.params.itemId});

        let params = {
            orderId: this.props.match.params.orderId,
            itemId: this.props.match.params.itemId
        }
        this.props.dispatch(createReturnDetailAction(params));
        this.props.dispatch(userProfileAction());
        this.props.dispatch(userBankAction());
        this.props.dispatch(allBankAction());
        this.props.dispatch(CityAction(this.state.countryDefaultValue));
        let user = getUserInfo();
        this.setState({userId:user.data.user_id});
        
        


    }

    componentWillReceiveProps(nextProps){
        if(nextProps.languageReducer.value){
            this.setState({
                language: nextProps.languageReducer.value
            })  
        }

        if(nextProps.returnDetails.result){
            window.scrollTo(0,0);
            this.setState({returnDetails: nextProps.returnDetails});
            this.props.dispatch(InitailStateAction("Initail_CREATE_RETURN_ACTION"));
        }

        if(nextProps.userProfileData != this.props.userProfileData){
            if(nextProps.userProfileData.data.status===200 && this.state.actionIdentifierFlag.profile=="edit")
            {
                notify.show(local.userProfileEditSuccessMessage[this.state.language]);
            }
            else if(nextProps.userProfileData.data.status!==200 && this.state.actionIdentifierFlag.profile=="edit")
            {
                notify.show(local.userProfileEditErrorMessage[this.state.language]);
            }
            else if(nextProps.userProfileData.data.status===200 && this.state.actionIdentifierFlag.profile=="add")
            {
                notify.show(local.userProfileAddSuccessMessage[this.state.language]);
            }
            else if(nextProps.userProfileData.data.status!==200 && this.state.actionIdentifierFlag.profile=="add")
            {
                notify.show(local.userProfileAddErrorMessage[this.state.language]);
            }
            this.setState({userProfileData: nextProps.userProfileData});
        }

        if((nextProps.submitReturnData != this.props.submitReturnData) && (this.state.submitReturnData.result == undefined || this.state.submitReturnData.result === false) ){
            this.setState({submitReturnData: nextProps.submitReturnData}, () => {
                if(this.state.submitReturnData.result){
                    window.scrollTo(0,0);
                    this.props.dispatch(InitailStateAction("Initail_SUBMIT_RETURN_ACTION"));
                }
            });
        }


        if(nextProps.userBankData != this.props.userBankData){
            if(nextProps.userBankData.data.code===200 && this.state.actionIdentifierFlag.bank=="edit")
            {
                notify.show(local.userBankEditSuccessMessage[this.state.language]);
            }
            else if(nextProps.userBankData.data.code!==200 && this.state.actionIdentifierFlag.bank=="edit")
            {
                notify.show(local.userBankEditErrorMessage[this.state.language]);
            }
            else if(nextProps.userBankData.data.code===200 && this.state.actionIdentifierFlag.bank=="add")
            {
                notify.show(local.userBankAddSuccessMessage[this.state.language]);
            }
            else if(nextProps.userBankData.data.code!==200 && this.state.actionIdentifierFlag.bank=="add")
            {
                notify.show(local.userBankAddErrorMessage[this.state.language]);
            }
            this.setState({userBankData: nextProps.userBankData});
        }

        if(nextProps.allBankData != this.props.allBankData){
            this.setState({allBankData: nextProps.allBankData});
        }

        if(nextProps.cityData.data != this.props.cityData.data){
            this.setState({cityData: nextProps.cityData.data});
        }

        if(nextProps.userBankData.data.status==403 && nextProps.userBankData.data.message==="UnAuthorised Access")
        {
           this.props.dispatch(InvalidTokenAction()); 
        }


    }



    selectIsActiveBank(e){
       
        if(!e.target.value){
            this.setState({
                [e.target.name+"Error"] : "error"
            })
        }
        else {
            this.setState({
                [e.target.name+"Error"] : " "
            })
        }
        
        this.setState({isActive: e.target.value});
    }

    selectBanktoAdd(e){
       
        if(!e.target.value){
            this.setState({
                [e.target.name+"Error"] : "error"
            })
        }
        else {
            this.setState({
                [e.target.name+"Error"] : " "
            })
        }
        
        this.setState({
                [e.target.name] : e.target.value
            })
    }

    selectReturnReason(e){
        
        if(!e.target.value){
            this.setState({
                [e.target.name+"Error"] : "error"
            })
        }
        else {
            this.setState({
                [e.target.name+"Error"] : ""
            })
        }
        this.setState({reaturnReason: e.target.value});
    }

    selectReturnSubReason(e){
        if(!e.target.value){
            this.setState({
                [e.target.name+"Error"] : "error"
            })
        }
        else {
            this.setState({
                [e.target.name+"Error"] : ""
            })
        }
        this.setState({reaturnSubReason: e.target.value});
    }

    updateState(e){
        this.setState({
              [e.target.name+"Error"] : ""
          })
          this.setState({
              [e.target.name]:e.target.value,
          })
    }

    updateLatLongValue =(data)=>{
        let selCity = this.state.cityData.response.filter((city)=>    
            city.city==data.city
        );
        latLongData = data;
        this.setState({"city":data.city});
        this.setState({"houseNo":data.houseNo});
        this.setState({selectedCityId:(selCity[0]!=="" && selCity[0]!==undefined)?selCity[0].id:0}
        );
        this.setState({selectedCity:(selCity[0]!=="" && selCity[0]!==undefined)?selCity[0].city:""});
        
        return;
    }

    updateUserComment(val){
        this.setState({userComment: val});
    }

    updateReturnAction(val){
        this.setState({returnAction: val});
    }

    updateRefundMode(val){
        if(val==="refund_in_ac")
        {
            $("#check_banks").modal("show");
        }
        else
        {
            $("#check_banks").modal("hide");   
        }
        let selectedBank = this.state.userBankData.data.data[this.state.selectedBankTempIndex];
        this.setState({bankName:selectedBank.bank_name,iban:selectedBank.IBAN,accHolderName:selectedBank.account_holder_name,accountNumber:selectedBank.account_no,refundMode: val});
        
    }

    submitReturn(){
        let error = false;
        
        if(!this.state.reaturnReason){
            error = true;
            this.setState({
                select_reasonError : local.selectreasonError[this.state.language]
            })
        }
        if(!this.state.reaturnSubReason){
            error = true;
            this.setState({
                select_sub_reasonError : local.selectsubreasonError[this.state.language]
            })
        }
        if(!this.state.userComment){
            error = true;
            this.setState({
                userCommentError : local.userCommentError[this.state.language]
            })
        }
        if(error){
            
            return false;
        }
        
        let orderInfo = this.state.returnDetails.data.order_info;
        let itemKey;
        Object.keys(orderInfo.items).map(key =>{
            if(key == this.state.itemId){
                itemKey = key;
            }
        })
        let item = orderInfo.items[itemKey];
        let returnObj = {};
        returnObj[itemKey] = {
            "chosen":"Y",
            "product_id": item.product_id,
            "available_amount": item.amount,
            "item_id": itemKey,
            "amount": this.state.itemAmount,
            "reason": this.state.reaturnSubReason,
            "browser": navigator.userAgent
        }

        let bazooka_url = "http://developer.kzmo.com/api/";
        if(env == 'dev'){
            bazooka_url = "http://test-developer.kzmo.com/api/"
        }

        let bank_details= {
            "username": getUserInfo().data.firstname + " " + getUserInfo().data.lastname,
            "bank_name": this.state.bankName,
            "other_bank": "test",
            "bank_type": "Savings",
            "account_no": this.state.accountNumber,
            "iban": this.state.iban,
            "account_holder_name": this.state.accHolderName,
            "ifsc_code":this.state.iban,
            "bank_branch": "test"
            }
            

        let data = {
            "quantity": item.amount,
            "returns": returnObj,
            "bank_details":bank_details,
            "items": [itemKey],
            "username": getUserInfo().data.firstname + " " + getUserInfo().data.lastname,
            "order_id": orderInfo.order_id,
            "user_id": getUserInfo().data.user_id,
            "payment_id": orderInfo.payment_id,
            "apv_req":"Y",
            "sc_apv_req":"Y",
            "mc_apv_req":"N",
            "picture_req":"N",
            "meta_reason": this.state.returnDetails.data.rma_status_bucket[this.state.reaturnReason],
            "meta_reason_value": this.state.reaturnReason,
            "cust_msg": this.state.userComment,
            "return":"",
            "refund_mode_cod":"",
            "replacementVariant":"",
            "variantError":"",
            "shipping_mode":"pick_up_frm_my_shipping_add",
            "comment":"",
            "return_address_id": this.state.userProfileData.data.response[this.state.selectedProfileIndex].profile_id,
            "action": this.state.returnAction,
            "refund_mode": this.state.refundMode,
            "ttl": getUserInfo().data.ttl,
            "token": getUserInfo().data.token,
            "bazooka_url": bazooka_url,
        }

        this.props.dispatch(submitReturnAction(data));
    }

    selectAddress(ind){
        this.setState({selectedProfileTempIndex: ind});
    }

    updateChangedAddress(){
        this.setState({selectedProfileIndex: this.state.selectedProfileTempIndex});
        document.getElementById("close-address-popup").click();
    }


    selectBank(ind){
        this.setState({selectedBankTempIndex: ind});
    }

    updateChangedBank(){
        this.setState({selectedBankIndex: this.state.selectedBankTempIndex});
        
        let selectedBank = this.state.userBankData.data.data[this.state.selectedBankTempIndex];
        let userProfile = this.state.userProfileData.data.response[0];
        this.setState({bankName:selectedBank.bank_name});
        this.setState({iban:selectedBank.IBAN});
        this.setState({accHolderName:selectedBank.account_holder_name});
        this.setState({accountNumber:selectedBank.account_no});
        this.setState({name:userProfile.s_firstname+" "+userProfile.s_lastname});
        document.getElementById("close-bank-popup").click();
    }

    setAddress=(data)=>{
        if(data.country!=='Saudi Arabia'){
            notify.show(local.logErrorSelectSaudi[this.state.language]);
            return false;
        }

        this.setState({
            city : data.city,
            houseNo : data.houseNo,
            name : this.state.profileData.response[this.state.profileData.response.length-1].s_firstname
        })
    }

    getCurrentLocation = ()=>{
        if (window.navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(
                (position)=> {
                    this.setState({
                        lat : position.coords.latitude,
                        long : position.coords.longitude,
                        mapheight : "500px"
                    })
          
                },
                function() {notify.show(local.logErrorGeolocationNot[this.state.language],"error")}
            );
        }
        this.setState({
            showMap : true
        })
    }

    addBankInit()
    {
        this.setState({accHolderName:""});
        this.setState({accNo:""});
        this.setState({iban:""});
        this.setState({bankName:""});
        this.setState({actionIdentifierFlag:{bank:"add"}});
    }
    addNewBank()
    {
        event.preventDefault();
        let error = false;
        
        let accHolderName = checkRequired(this.state.accHolderName, this.state.language)
        if(!accHolderName[0]){
            error = true
            this.setState({
                accHolderNameError :accHolderName[1]
            })
        }

        let accNo = checkRequired(this.state.accNo, this.state.language)
        if(!accNo[0]){
            error = true
            this.setState({
                accNoError :accNo[1]
            })
        }

        let iban = checkRequired(this.state.iban, this.state.language)
        if(!iban[0]){
            error = true
            this.setState({
                ibanError :iban[1]
            })
        }

        let bankName = checkRequired(this.state.bankName, this.state.language)
        if(!bankName[0]){
            error = true
            this.setState({
                bankNameError :bankName[1]
            })
        }

        /*let userId = checkRequired(this.state.userId, this.state.language)
        if(!userId[0]){
            error = true
            this.setState({
                userIdError :userId[1]
            })
        }

        let userNeftId = checkRequired(this.state.userNeftId, this.state.language)
        if(!userNeftId[0]){
            error = true
            this.setState({
                userNeftIdError :userNeftId[1]
            })
        }

        let bankType = checkRequired(this.state.bankType, this.state.language)
        if(!bankType[0]){
            error = true
            this.setState({
                bankTypeError :bankType[1]
            })
        }

        let isActive = checkRequired(this.state.isActive, this.state.language)
        if(!isActive[0]){
            error = true
            this.setState({
                isActiveError :isActive[1]
            })
        }*/

        if(!error)
        {
            let data={
                    "user_id":this.state.userId,
                    "account_name":this.state.accHolderName, 
                    "bank_name":this.state.bankName, 
                    "account_no":this.state.accNo,
                    "bank_type":"Savings", 
                    "IBAN":this.state.iban,
                    "is_active": "Y"
            };

            
            let bankData = Object.assign({}, data);
            this.setState({
                newBankAdd : true
            })
            this.props.dispatch(addBankAction(bankData));

            document.getElementById("add_bank_close_modal").click();
            this.setState({selectedBankTempIndex:0});
            this.setState({selectedBankIndex:0});

        }
        
        
        
    }

    editNewBank(){
        event.preventDefault();
        let error = false;
        
        let accHolderName = checkRequired(this.state.accHolderName, this.state.language)
        if(!accHolderName[0]){
            error = true
            this.setState({
                accHolderNameError :accHolderName[1]
            })
        }

        let accNo = checkRequired(this.state.accNo, this.state.language)
        if(!accNo[0]){
            error = true
            this.setState({
                accNoError :accNo[1]
            })
        }

        let iban = checkRequired(this.state.iban, this.state.language)
        if(!iban[0]){
            error = true
            this.setState({
                ibanError :iban[1]
            })
        }

        let bankName = checkRequired(this.state.bankName, this.state.language)
        if(!bankName[0]){
            error = true
            this.setState({
                bankNameError :bankName[1]
            })
        }

        let userNeftId = checkRequired(this.state.userNeftId, this.state.language)
        if(!userNeftId[0]){
            error = true
            this.setState({
                userNeftIdError :userNeftId[1]
            })
        }

        /*let userId = checkRequired(this.state.userId, this.state.language)
        if(!userId[0]){
            error = true
            this.setState({
                userIdError :userId[1]
            })
        }

        

        let bankType = checkRequired(this.state.bankType, this.state.language)
        if(!bankType[0]){
            error = true
            this.setState({
                bankTypeError :bankType[1]
            })
        }

        let isActive = checkRequired(this.state.isActive, this.state.language)
        if(!isActive[0]){
            error = true
            this.setState({
                isActiveError :isActive[1]
            })
        }*/

        if(!error)
        {
            let data={
                    "user_id":this.state.userId,
                    "account_name":this.state.accHolderName, 
                    "bank_name":this.state.bankName, 
                    "account_no":this.state.accNo,
                    "bank_type":"Savings", 
                    "IBAN":this.state.iban,
                    "is_active": "Y",
                    "user_neft_id":this.state.userNeftId
                    
            };

            
            let bankData = Object.assign({}, data);
            this.setState({
                newBankAdd : true
            })
            this.props.dispatch(editBankAction(bankData));

            document.getElementById("edit_bank_close_modal").click();
        }
        
        

    }

    addAddressInit()
    {
        this.setState({city:""});
        this.setState({selectedCity:""});
        this.setState({selectedCityId:""});
        this.setState({houseNo:""});
        this.setState({name:""});
        this.setState({addressType:""});
        this.setState({phoneNo:""});
        this.setState({actionIdentifierFlag:{profile:"add"}});
    }

    addProfileAddress()
    {
        event.preventDefault();
        let error = false;
        let vcity = checkRequired(this.state.selectedCity, this.state.language)
        if(!vcity[0]){
            error = true
            this.setState({
                cityError :vcity[1]
            })
        }

        let vname = checkRequired(this.state.name, this.state.language)
        if(!vname[0]){
            error =  true;
            this.setState({
                nameError : vname[1]
            })
        }
        let vphone = checkPhonenumber(this.state.phoneNo, this.state.language)
        if(!vphone[0]){
            error =  true;
            this.setState({
                phoneNumberError : vphone[1]
            })
        }
        let vhouse = checkRequired(this.state.houseNo, this.state.language)
        if(!vhouse[0]){
            error =  true;
            this.setState({
                houseNoError : vhouse[1]
            })
        }

        let vaddress = checkRequired(this.state.addressType, this.state.language)
        if(!vaddress[0]){
            error =  true;
            this.setState({
                addressTypeError : vaddress[1]
            })
        }

        if(!error){
            
            let fname="",lname="";
            let full_name = this.state.name.split(" ");
            let i=0;
            full_name.forEach((it)=>{
                if(i===0) {fname=it;}
                else{lname+=it;}
                i++;
            });
            let data = {
                
                "s_firstname": fname,
                "s_lastname": lname,
                "s_address": this.state.houseNo,
                "s_address_2": '',
                "s_city": this.state.selectedCity,
                "s_state": '',
                "s_zipcode": "",
                "s_phone": this.state.phoneNo,
                "address_type": this.state.addressType,
                "platform": "D",
                "latitude": latLongData?latLongData.lat:"",
                "longitude": latLongData?latLongData.lng:"",
                "city_id":this.state.selectedCityId
            }

            let addressData = Object.assign({}, data);
            this.setState({
                newProfileAdd : true
            })
            this.props.dispatch(editProfileAction(addressData));

            document.getElementById("add_address_close_modal").click();
            this.setState({selectedProfileIndex:0});
            this.setState({selectedProfileTempIndex:0});
        }
        

    }

    editProfileDetails(selectedIndex,userData)
    {
        
        this.setState({houseNo:userData.b_address});
        this.setState({phoneNo:userData.b_phone});
        this.setState({city:userData.b_city});
        this.setState({selectedCity:userData.b_city});
        this.setState({name:userData.s_firstname+" "+userData.s_lastname});
        this.setState({addressType:userData.address_type});
        this.setState({profileId:userData.profile_id});
        this.setState({selectedCityId:(userData.city_id!=="" && userData.city_id!==null && userData.city_id!==undefined)?userData.city_id:""});
        this.setState({actionIdentifierFlag:{profile:"edit"}});
    }

    editProfileAddress()
    {
        event.preventDefault();
        let error = false;
        let vcity = checkRequired(this.state.selectedCity, this.state.language)
        if(!vcity[0]){
            error = true
            this.setState({
                cityError :vcity[1]
            })
        }

        let vname = checkRequired(this.state.name, this.state.language)
        if(!vname[0]){
            error =  true;
            this.setState({
                nameError : vname[1]
            })
        }
        let vphone = checkPhonenumber(this.state.phoneNo, this.state.language)
        if(!vphone[0]){
            error =  true;
            this.setState({
                phoneNumberError : vphone[1]
            })
        }
        let vhouse = checkRequired(this.state.houseNo, this.state.language)
        if(!vhouse[0]){
            error =  true;
            this.setState({
                houseNoError : vhouse[1]
            })
        }

        let vaddress = checkRequired(this.state.addressType, this.state.language)
        if(!vaddress[0]){
            error =  true;
            this.setState({
                addressTypeError : vaddress[1]
            })
        }

        if(!error){
            
            let fname="",lname="";
            let full_name = this.state.name.split(" ");
            let i=0;
            full_name.forEach((it)=>{
                if(i===0) {fname=it;}
                else{lname+=it;}
                i++;
            });
            let data = {
                
                "s_firstname": fname,
                "s_lastname": lname,
                "s_address": this.state.houseNo,
                "s_address_2": '',
                "s_city": this.state.selectedCity,
                "s_state": '',
                "s_zipcode": "",
                "s_phone": this.state.phoneNo,
                "address_type": this.state.addressType,
                "platform": "D",
                "latitude": latLongData?latLongData.lat:"",
                "longitude": latLongData?latLongData.lng:"",
                "profile_id": this.state.profileId,
                "city_id":this.state.selectedCityId
            }

            let addressData = Object.assign({}, data);
            
            this.props.dispatch(editProfileAction(addressData));

            document.getElementById("edit_address_close_modal").click();
        }
        


    }

    editBankDetails(selectedIndex,userBankData){
        
        this.setState({
                userId : userBankData.data.data[selectedIndex.index].user_id
            });
        this.setState({
                accHolderName : userBankData.data.data[selectedIndex.index].account_holder_name
            });
        this.setState({
                bankName : userBankData.data.data[selectedIndex.index].bank_name
            });
        this.setState({
                accNo : userBankData.data.data[selectedIndex.index].account_no
            });
        // this.setState({
        //         bankType : userBankData.data.data[selectedIndex.index].user_id
        //     });
        
        this.setState({
                iban : userBankData.data.data[selectedIndex.index].IBAN
            });
        
        this.setState({
                isActive : userBankData.data.data[selectedIndex.index].is_active
            });
        this.setState({
                userNeftId : userBankData.data.data[selectedIndex.index].user_neft_id
            });
        this.setState({editableBankIndex:selectedIndex.index});
        this.setState({actionIdentifierFlag:{bank:"edit"}});


    }
    

    goToOrders(){
        this.props.history.push("/myaccount/orders");
    }

    setView(view){
        this.props.history.push("/myaccount/" + view);
    }

    initializeQty(itemAmount){
        console.log("ininitialize");
        this.setState({itemAmount: itemAmount});
    }

    updateQty(qty){
        this.setState({itemAmount: qty});
    }

    timeConverter(UNIX_timestamp){
        let a = new Date(UNIX_timestamp * 1000);
        let months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
        let year = a.getFullYear();
        let month = months[a.getMonth()];
        let date = a.getDate();
        let hour = a.getHours();
        let min = (a.getMinutes()<10?'0':'') + a.getMinutes();
        let sec = (a.getSeconds()<10?'0':'') + a.getSeconds();
        let time = date + ' ' + month + ' ' + year + ' ' + this.tConvert (hour + ':' + min + ':' + sec);
        return time;
    }
    tConvert (time) {
       // Check correct time format and split into components
       time = time.toString ().match (/^([01]\d|2[0-3])(:)([0-5]\d)(:[0-5]\d)?$/) || [time];

       if (time.length > 1) { // If time format correct
        time = time.slice (1);  // Remove full string match value
        time[5] = +time[0] < 12 ? ' AM' : ' PM'; // Set AM/PM
        time[0] = +time[0] % 12 || 12; // Adjust hours
      }
      return time.join (''); // return adjusted time or original string
    }

    selectCity(cityId){
        let selCity = this.state.cityData.response.filter((city)=> cityId===city.id);
        this.setState({selectedCity: selCity[0].city});
        this.setState({selectedCityId: selCity[0].id});
    }

    render() {
        return (
            <div className="dashboard_outer">
                <h1 className="category_name text-left">
                <a href="#"><svg className="icon icon-left-arrow"><use xlinkHref="/img/icon-sprite.svg#icon-left-arrow"></use></svg></a>
                {local.addrMyAccount[this.state.language]}</h1>
                <div className="row">
                    <div className="col-md-3 left_sec">
                        <Sidebar 
                            setView={(view) => this.setView(view)}
                        />
                    </div>
                    {
                        !this.state.submitReturnData.result && this.state.returnDetails.result && this.state.userProfileData.result && <CreateReturn 
                            data = {this.state.returnDetails.data} 
                            reaturnReason = {this.state.reaturnReason} 
                            reaturnSubReason = {this.state.reaturnSubReason} 
                            userComment = {this.state.userComment} 
                            selectReturnReason = {(val) => this.selectReturnReason(val)} 
                            selectReturnSubReason = {(val) => this.selectReturnSubReason(val)} 
                            selectedProfileIndex = {this.state.selectedProfileIndex} 
                            selectedProfileTempIndex = {this.state.selectedProfileTempIndex} 
                            userProfileData = {this.state.userProfileData}
                            userProfileDataError={this.state.userProfileDataError}
                            userBankData = {this.state.userBankData}
                            userBankDataError={this.state.userBankDataError} 
                            updateUserComment = {(val) => this.updateUserComment(val)} 
                            returnAction = {this.state.returnAction} 
                            updateReturnAction = {(val) => this.updateReturnAction(val)} 
                            refundMode = {this.state.refundMode} 
                            updateRefundMode = {(val) => this.updateRefundMode(val)} 
                            submitReturn = {() => this.submitReturn()} 
                            selectAddress = {(ind) => this.selectAddress(ind)} 
                            classes = {this.props} 
                            updateChangedAddress = {() => this.updateChangedAddress()}
                            addProfileAddress = {() => this.addProfileAddress()} 
                            updateState = {(e) => this.updateState(e)}
                            language = {this.state.language}
                            lat = {this.state.lat}
                            long = {this.state.long}
                            updateLatLongValue={(d)=> this.updateLatLongValue(d)}
                            getCurrentLocation={()=> {let self=this;this.getCurrentLocation(self)}}
                            showMap = {this.state.showMap}
                            itemId = {this.state.itemId}
                            select_reasonError = {this.state.select_reasonError}
                            select_sub_reasonError = {this.state.select_sub_reasonError} 
                            initializeQty = {(itemAmount) => this.initializeQty(itemAmount)}
                            itemAmount = {this.state.itemAmount} 
                            updateQty = {(qty) => this.updateQty(qty)}
                            timeConverter = {(timestamp) => this.timeConverter(timestamp)}
                            language = {this.state.language}
                            accHolderName = {this.state.accHolderName}
                            accHolderNameError = {this.state.accHolderNameError}
                            accNo = {this.state.accNo}
                            accNoError = {this.state.accNoError}
                            iban = {this.state.iban}
                            ibanError = {this.state.ibanError}
                            bankName = {this.state.bankName}
                            bankNameError = {this.state.bankNameError}
                            userId = {this.state.userId}
                            userIdError = {this.state.userIdError}
                            userNeftId = {this.state.userNeftId}
                            userNeftIdError = {this.state.userNeftIdError}
                            addNewBank = { () => this.addNewBank() }
                            editNewBank = { () => this.editNewBank() }
                            bankType ={ this.state.bankType }
                            bankTypeError ={ this.state.bankTypeError }
                            isActive ={ this.state.isActive }
                            isActiveError ={ this.state.isActiveError }
                            updateChangedBank={()=> this.updateChangedBank()}
                            selectBank = {(ind) => this.selectBank(ind)}
                            selectedBankIndex = {this.state.selectedBankIndex} 
                            selectedBankTempIndex = {this.state.selectedBankTempIndex}
                            refundMode = {this.state.refundMode}
                            editBankDetails = {(i,data)=>this.editBankDetails(i,data)}
                            editProfileDetails = {(i,data)=>this.editProfileDetails(i,data)}
                            editProfileAddress = {()=>this.editProfileAddress()}

                            editableBankIndex = {this.state.editableBankIndex}
                            houseNo = {this.state.houseNo}
                            houseNoError={this.state.houseNoError}
                            city = {this.state.city}
                            cityError={this.state.cityError}
                            phoneNo = {this.state.phoneNo}
                            phoneNumberError={this.state.phoneNumberError}
                            name = {this.state.name}
                            nameError={this.state.nameError}
                            addressType = {this.state.addressType}
                            addressTypeError={this.state.addressTypeError}
                            selectIsActiveBank = {(val)=>this.selectIsActiveBank(val)}
                            addBankInit = {()=> this.addBankInit()}
                            addAddressInit = {()=>this.addAddressInit()}
                            setAddress={()=> this.setAddress}
                            allBankData= {this.state.allBankData}
                            selectedBank={this.state.selectedBank}
                            selectedBankError={this.state.selectedBankError}
                            selectBanktoAdd={(e)=>this.selectBanktoAdd(e)}
                            cityData= {this.state.cityData}
                            selectedCity={this.state.selectedCity}
                            selectedCityId={this.state.selectedCityId}
                            selectCityFunction={(city_id)=> this.selectCity(city_id)}
                            userCommentError={this.state.userCommentError}


                        />
                    }
                    {
                        this.state.submitReturnData.result && <ReturnThankYou 
                            data = {this.state.returnDetails.data} 
                            submitReturnData = {this.state.submitReturnData.data} 
                            reaturnReason = {this.state.reaturnReason} 
                            returnAction = {this.state.returnAction} 
                            refundMode = {this.state.refundMode} 
                            userComment = {this.state.userComment} 
                            goToOrders = {() => this.goToOrders()}
                            timeConverter = {(timestamp) => this.timeConverter(timestamp)}
                            language = {this.state.language} 

                        />
                    }
                    {
                        ( !this.state.returnDetails.result || this.state.userProfileData.isFetching || this.state.submitReturnData.isFetching ) && <Loader />
                    }
                </div>
            </div>
        );
    }1
}

function mapStateToProps(state) {
    console.log(state);
    return {
        example: state.example,
        languageReducer: state.languageReducer,
        returnDetails: state.createReturnDetailReducer,
        userProfileData : state.UserProfileReducer,
        submitReturnData: state.submitReturnReducer,
        userBankData : state.UserBankReducer,
        allBankData : state.AllBankReducer,
        cityData: state.cityReducer,
    }
}
export default connect(mapStateToProps)(withStyles(styles)(ReturnRequest));

