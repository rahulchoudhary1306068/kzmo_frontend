import React, { Component } from 'react';
import {connect} from 'react-redux';

import { MuiThemeProvider, createMuiTheme, createGenerateClassName } from '@material-ui/core/styles';

import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';

import Sidebar from '../../components/account_sidebar';
import Loader from "../../components/loader";
import { getter,months, days} from '../../commonfunction';
import { notify } from 'react-notify-toast';

import { returnDetailAction, refundDetailAction } from '../../actions/myAccount';

import local,{deafultLanguage} from '../../localization';

const mui = createMuiTheme({
  overrides: {
    MuiFormLabel: {
      root: {
        color: "#b1b1b1"
      },
      focused: {
        color: "black !important"
      },
    }
  }
});

const Details = (props) => {
    let orderInfo = props.details.order_info;

    let date = new Date(parseInt(orderInfo.timestamp)*1000);
    let orderDate = days[date.getDay()-1] + ", " + date.getDate() + " " + months[date.getMonth()] + " " + date.getFullYear();

    /*let deliveryTimestamp = new Date(parseInt(orderInfo.pdd_edd.edd)*1000);
    let deliveryTime = days[deliveryTimestamp.getDay()-1] + ", " + months[deliveryTimestamp.getMonth()] + " " + deliveryTimestamp.getDate() + " &apos;" + deliveryTimestamp.getFullYear().toString().substr(-2);*/

    return (
        <div className="col-md-9 right_sec order_outer mobile_order_detail">
    		<div className="listing_outer">
    			<div className="listing">
    				<div className="d-flex">
    		            <div className="buffer_prop details">
    		                {<p className="mb-0 ordr_detail">{local.orderId[props.language]}: {orderInfo.order_id}</p>}
    		                <p className="mb-3 placing_dte">{local.orderPlacedOn[props.language]} {orderDate}</p>
    		                {
                                Object.keys(orderInfo.items).map(itemKey => {
                                    let prodInfo = orderInfo.items[itemKey];
                                    return(    
                                        <div>
                                            <img src={prodInfo.product_image_path} />
                                            <div className="inner">
                                                <p className="name"><a href="JavaScript:void(0);">{prodInfo.product_name}</a></p>
                                                {
                                                    prodInfo.product_options && typeof(product_options) == "object" && product_options.map(option => (
                                                       <p className="color_name">{option.option_name}: {option.variant_name}</p>
                                                    )) 
                                                }
                                            </div>
                                        </div>
                                    )
                                })
    		                }
    		                <p className="mb-0">{props.details.order_status_text.customer_facing_name}</p>
    		            </div>
    		            <div className="inner_right text-right">
    		                <p>
    		                    {props.details.cancel_return_link == "Y" && <a className="links_detail" data-toggle="modal" data-target="#cancel_order" href="JavaScript:void(0);">{local.orderCancelReturn[props.language]}</a>}
    		                </p>
    		            </div>
    		        </div>
    		    </div>
    		</div>
            <ReturnInfo
                details={props.details}
                language={props.language}
            />
    	</div>
    );
};

const ReturnInfo = (props) => {
    let returnInfo = props.details.return_info;
    let returnItemInfoKey = Object.keys(returnInfo.items["A"]);
    let returnItemInfo = returnInfo.items["A"][returnItemInfoKey[0]];
    let reasons = props.details.reasons;
    let actions = props.details.actions;

    let date = new Date(parseInt(returnInfo.timestamp)*1000);
    let returnDate = date.getDate() + " " + months[date.getMonth()] + " " + date.getFullYear();
    return (
        <div className="innerorder_detail mt-4 pt-4">
            <h5>{local.returnInfo[props.language]}</h5>
            <ul className="return_detail">
                <li className="d-flex justify-content-between"><p>{local.returnReason[props.language]}:</p><p className="buffer-color">{reasons[returnItemInfo.reason].description}</p></li>
                <li className="d-flex justify-content-between"><p>{local.returnYouHaveAsked[props.language]}: </p><p className="buffer-color">{actions[returnInfo.action].property}</p></li>
                <li className="d-flex justify-content-between"><p>{local.returnModeOfRefund[props.language]}: </p><p className="buffer-color">{returnInfo.refund_in_ac == "Y" ? local.returnRefundInAccount[props.language] : (returnInfo.refund_in_cb == "Y" ? local.returnRefundInKZMO[props.language] : "")}</p></li>
                <li className="d-flex justify-content-between"><p>{local.returnHowShipped[props.language]}:  </p><p className="buffer-color">{returnInfo.cust_will_send_product == "Y" ? local.returnCustomerWillSend[props.language] : (returnInfo.pick_up_frm_my_shipping_add == "Y" ? local.returnKZMOWillPick[props.language] : "")}</p></li>
                <li className="d-flex justify-content-between"><p>{local.returnId[props.language]}: </p><p className="buffer-color">{returnInfo.return_id}</p></li>
                <li className="d-flex justify-content-between"><p>{local.returnCreatedOn[props.language]}: </p><p className="buffer-color">{returnDate}</p></li>
                <li className="d-flex justify-content-between"><p>{local.returnStatus[props.language]}: </p><p className="buffer-color">{props.details.returnstatus}</p></li>
            </ul>
        </div>
    );
}

const CommentSec = (props) => (
    {/*<div className="needhelp_outer mt-4 pt-4">
                <form>
                    <MuiThemeProvider theme={mui}>
                       <TextField
                          id="comment"
                          className="input_outer textField mt-1"
                          label="Write new comment"
                          multiline
                          rowsMax="10"
                          rows="5"
                          fullWidth
                          variant="outlined"
                        />
                    </MuiThemeProvider>
                    <Button variant="contained" color="primary" type="button" >
                        Add comment
                    </Button>
                </form>
                <h2 className="mb-4 mt-3">Recent Comments</h2>
                <ul className="comment_list">
                    <li>
                        <p>The size is incorrect for me , hense I am returning the product.</p>
                        <div>Rakesh gaur <span>05:13 pm, 3 Oct, 2018</span></div>
                    </li>
                    <li>
                        <p>The size is incorrect for me , hense I am returning the product.</p>
                        <div>Rakesh gaur <span>05:13 pm, 3 Oct, 2018</span></div>
                    </li>
                    <li>
                        <p>The size is incorrect for me , hense I am returning the product.</p>
                        <div>Rakesh gaur <span>05:13 pm, 3 Oct, 2018</span></div>
                    </li>
                </ul>
            </div>*/}
);

class ReturnDetail extends Component {
	constructor(props) {
        super(props);
        this.state = {
            orderId: "",
            returnId: "",
            returnDetail: {},
            language: ''
        }
    }

    static initialAction({url, store}) {
       	return true;
    }

    componentDidMount(){
        this.setState({
            language: getter('language')?getter('language'):deafultLanguage
        })
        let orderId = this.props.match.params.orderId;
        this.setState({orderId: orderId});
        let returnId = this.props.match.params.returnId;
        this.setState({returnId: returnId});
        this.props.dispatch(returnDetailAction(returnId));
        this.props.dispatch(refundDetailAction({returnId: returnId,orderId: orderId}));
    }

    componentWillReceiveProps(nextProps){
        if(nextProps.languageReducer.value){
            this.setState({
                    language: nextProps.languageReducer.value
            })  
        }
        if(nextProps.returnDetail != this.props.returnDetail){
            this.setState({returnDetail: nextProps.returnDetail}, () => {console.log("this.state.returnDetail.data", this.state.returnDetail.data);});
        }
    }
    setView(view){
        this.props.history.push("/myaccount/" + view);
    }
    render() {
        if(!this.state.returnDetail.isFetching){
        	return (
        		<div className="dashboard_outer">
                    <h1 className="category_name text-left">
                    <a href="#"><svg className="icon icon-left-arrow"><use xlinkHref="/img/icon-sprite.svg#icon-left-arrow"></use></svg></a>
                    {local.addrMyAccount[this.state.language]}</h1>
                    <div className="row">
                        <div className="col-md-3 left_sec">
                            <Sidebar 
                                viewType="orders" 
                                setView={(view) => this.setView(view)}
                                language={this.state.language}
                            />
                      	</div>
                        { 
                           this.state.returnDetail.data && this.state.returnDetail.data && <Details 
                                details={this.state.returnDetail.data} language={this.state.language}
                            />
                        }
                    </div>
                    {/*<Loader />*/}
                </div>
    		);
        }
        else{
            return (
                <Loader />
            );
        }
    }
}

function mapStateToProps(state) {
    return {
        returnDetail: state.returnDetailReducer,
        refundDetail: state.refundDetailReducer,
        languageReducer: state.languageReducer
    }
}

export default connect(mapStateToProps)(ReturnDetail);