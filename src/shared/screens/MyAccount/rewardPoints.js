import React, { Component } from 'react';
import {connect} from 'react-redux';
import Sidebar from '../../components/account_sidebar';

import Loader from "../../components/loader";

import { rewardPointsAction } from '../../actions/myAccount';

import { getter, months } from '../../commonfunction';
import local, {deafultLanguage} from '../../localization';

const RewarsPointSection = (props) => (
    <div className="col-md-9 right_sec points_outer">
        <div className="value_container position-relative">
            <a className="term_link position-absolute" href="Javascript:void(0);">{local.rewardTandC[props.language]}</a>
            <svg className="icon icon-value"><use xlinkHref="/img/icon-sprite.svg#icon-value"></use></svg>
            <div className="d-inline-block inner">
                <div className="value">{props.data.total_cb}</div>
                <p className="m-0">{local.rewardPoints[props.language]}</p>
            </div>
            <p className="text-right redem_outer"><a href="Javascript:void(0);">{local.rewardHowToRedeem[props.language]}</a></p>
        </div>
        <h2 className="subheading">{local.rewardPointHistory[props.language]}</h2>
        <ul className="list_outer">
            {
                props.data.cb_history.user_cb_logs.length ? (props.data.cb_history.user_cb_logs.map(history => {
                    let date = new Date(parseInt(history.timestamp)*1000);
                    let day = date.getDate();
                    let month = date.getMonth();
                    let year = date.getFullYear();
                    return(
                        <li>
                            <div>
                                <p className="m-0">{history.reason}</p>
                                <p className="m-0 value">{day + " " + months[month] + " " + year}</p>
                            </div>
                            <p className="m-0 point loss profit">{history.amount} KP</p>
                        </li>
                    )}
                )) : (<div>{local.rewardNoHistoryFound[props.language]}</div>)
            }
        </ul>
        {props.totalPageCount > props.currentPage && <a href="JavaScript:void(0);" className="show_more" onClick={() => props.loadMore()}>{local.rewardShowMore[props.language]}</a>}
    </div>
)

class MyKzmoPoint extends Component {
    constructor(props) {
        super(props);
        this.state = {
            rewardPointData: {},
            currentPage: 1,
            totalPageCount: 1,
            language: ''
        }
    }

    componentDidMount(){

        this.setState({
            language: getter('language') ? getter('language') : deafultLanguage
        })

        let userId = localStorage.getItem('userId');
        this.setState({userId: userId}, () => {
            this.getRewardPoints();
        });
        
    }

    getRewardPoints(){
        this.props.dispatch(rewardPointsAction({userId: this.state.userId, currentPage: this.state.currentPage}));
    }

    componentWillReceiveProps(nextProps){

        if(nextProps.languageReducer.value){
            this.setState({
                    language: nextProps.languageReducer.value
            })  
            
        }

        if(nextProps.rewardPointData != this.props.rewardPointData){
            if(this.state.currentPage == 1){
                this.setState({rewardPointData: nextProps.rewardPointData});

                if(nextProps.rewardPointData.data.response){
                    let perPageRecord = nextProps.rewardPointData.data.response.cb_history.per_page_record;
                    let totalRecords = nextProps.rewardPointData.data.response.total_records;
                    this.setState({totalPageCount: Math.ceil(parseInt(totalRecords)/parseInt(perPageRecord))});
                }
            }
            else{
                if(nextProps.rewardPointData.data.response){
                    let rewardPointData = {...this.state.rewardPointData};
                    rewardPointData.data.response.cb_history.user_cb_logs = rewardPointData.data.response.cb_history.user_cb_logs.concat(nextProps.rewardPointData.data.response.cb_history.user_cb_logs);
                    this.setState({rewardPointData: rewardPointData});
                }
            }
            
        }
    }

    loadMore(){
        this.setState({currentPage: this.state.currentPage + 1}, () => {
            this.getRewardPoints();
        });
    }
   
    render() {
        if(this.state.rewardPointData.data && this.state.rewardPointData.data.response){
            return (
                <RewarsPointSection 
                    data={this.state.rewardPointData.data.response} 
                    loadMore={() => this.loadMore()} 
                    currentPage={this.state.currentPage} 
                    totalPageCount={this.state.totalPageCount}
                    language={this.state.language} 
                />
            );
        }
        else{
            return (
                <Loader />
            );
        }
    }
}

function mapStateToProps(state) {
    return {
        rewardPointData: state.rewardPointReducer,
        languageReducer: state.languageReducer
    }
}

export default connect(mapStateToProps)(MyKzmoPoint);
