import React, { Component } from 'react';
import {connect} from 'react-redux';
import Sidebar from '../../components/account_sidebar';
// import "react-responsive-carousel/lib/styles/carousel.min.css";
import PropTypes from 'prop-types';
import {
  MuiThemeProvider,
  createMuiTheme,
  createGenerateClassName,
} from '@material-ui/core/styles';
import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import FormLabel from '@material-ui/core/FormLabel';
import Button from '@material-ui/core/Button';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import local, {deafultLanguage} from '../../localization';
import {getter} from '../../commonfunction';

const styles = theme => ({
  root: {
    '&$checked': {
      color: '#e4b122'
    }
  },
  checked: {},
  
});
const mui = createMuiTheme({
  overrides: {
    MuiFormLabel: {
      root: {
        color: "#b1b1b1"
      },
      focused: {
        color: "black !important"
      },
    }
  }
});

class AddressBook extends Component {
  
    static initialAction({url, store}) {
       // store.dispatch(productListAction());
       return true;
    }
    state = {
        value: 'home'
    };

    handleChange = event => {
        this.setState({ value: event.target.value });
    };
    componentDidMount(){

        this.setState({
            language: getter('language') ? getter('language') : deafultLanguage
        })
    }
    componentWillReceiveProps(nextProps){
        
        if(nextProps.languageReducer.value){
            this.setState({
                language: nextProps.languageReducer.value
            })  
            
        }
    }
    render() {
        const { classes } = this.props;

        return (
            <div className="dashboard_outer">
                <h1 className="category_name text-left">{local.addrMyAccount[this.state.language]}</h1>
                <div className="row">
                    <div className="col-md-3 left_sec">
                      <Sidebar />
                    </div>
                    <div className="col-md-9 right_sec address_outer profile_outer order_outer">
                        <h2 className="subheading">{local.addrMyAddressBook[this.state.language]}</h2>
                        <a className="add_address" href="JavaScript:void(0);">{local.addrAddNewAddress[this.state.language]}</a>
                        <div>
                            <div className="locatn_sec">
                                <span><svg className="icon icon-gps"><use xlinkHref="/img/icon-sprite.svg#icon-gps"></use></svg> <a href="Javascript:void(0);">{local.addrUseCurrenLocation[this.state.language]}</a></span><span className="ml-4 mr-4">or</span>
                                <span><svg className="icon icon-qr-code"><use xlinkHref="/img/icon-sprite.svg#icon-qr-code"></use></svg> <a href="Javascript:void(0);">Upload Address Registry QR Code</a></span>
                            </div>
                             <div className="map_section position-relative">
                                <form className="position-absolute">
                                    <input name="search" type="text" id="search_pincode" placeholder="Enter pincode" />
                                    <button className="search btn" title="search" type="submit">Search
                                    </button>
                                </form>
                                <img src="/img/map.jpg" />
                            </div>
                            <form className="row">
                                <MuiThemeProvider theme={mui}>
                                    <TextField
                                      id="address"
                                      name = "address"
                                      className="input_outer col-sm-6"
                                      label="Address"
                                      type="text"
                                    />
                                    
                                    <TextField
                                      id="city"
                                      className="input_outer col-sm-6"
                                      label="City"
                                      type="text"
                                      name = "city"
                                    />
                                    <TextField
                                      id="name"
                                      className="input_outer col-sm-6"
                                      label="Name"
                                      type="email"
                                      name = "name"
                                    />
                                    <TextField
                                      id="phone_number"
                                      className="input_outer col-sm-6"
                                      label="phone number"
                                      type="text"
                                      name = "phoneNumber"
                                    /> 
                                </MuiThemeProvider>
                                <div className="input_radio d-flex w-100 align-items-center">
                                    <FormLabel component="legend">Address Type </FormLabel>
                                    <RadioGroup
                                        aria-label="Address Type"
                                        name="addressType"
                                        value={this.state.value}
                                        onChange={this.handleChange}
                                        className="radio_outer"
                                    >
                                        <FormControlLabel
                                          value="home"
                                          control={
                                            <Radio
                                              classes={{root: classes.root, checked: classes.checked}}
                                            />
                                          }
                                          label="Home"
                                          labelPlacement="end"
                                        />
                                        <FormControlLabel
                                          value="office"
                                          control={
                                            <Radio
                                              classes={{root: classes.root, checked: classes.checked}}
                                            />
                                          }
                                          label="Office"
                                          labelPlacement="end"
                                        />
                                        <FormControlLabel
                                          value="other"
                                          control={
                                            <Radio
                                              classes={{root: classes.root, checked: classes.checked}}
                                            />
                                          }
                                          label="Others"
                                          labelPlacement="end"
                                        />
                                    </RadioGroup>
                                </div>
                                <Button
                                    variant="contained"
                                    color="primary"
                                >
                                    Save Address
                                </Button>
                            </form>
                        </div>
                        <ul className="listing_outer">
                            <li className="listing">
                                <div className="details">
                                    <div className="inner pl-0">
                                        <p className="inr_detail">Akansha Goyal</p>
                                        <p className="inr_detail">3rd floor UDB landmark complex Plot No A-6 Basant Bahar Tonk Road</p>
                                        <p className="inr_detail">7300083928</p>
                                        <a className="common_links" href="JavaScript:void(0);">Edit</a>
                                        <a className="common_links" href="JavaScript:void(0);">Remove Address</a>
                                    </div>
                                </div>
                            </li>
                            <li className="listing">
                                <div className="details">
                                    <div className="inner pl-0">
                                        <p className="inr_detail">Akansha Goyal</p>
                                        <p className="inr_detail">3rd floor UDB landmark complex Plot No A-6 Basant Bahar Tonk Road</p>
                                        <p className="inr_detail">7300083928</p>
                                        <a className="common_links" href="JavaScript:void(0);">Edit</a>
                                        <a className="common_links" href="JavaScript:void(0);">Remove Address</a>
                                    </div>
                                </div>
                            </li>
                            <li className="listing">
                                <div className="details">
                                    <div className="inner pl-0">
                                        <p className="inr_detail">Akansha Goyal</p>
                                        <p className="inr_detail">3rd floor UDB landmark complex Plot No A-6 Basant Bahar Tonk Road</p>
                                        <p className="inr_detail">7300083928</p>
                                        <a className="common_links" href="JavaScript:void(0);">Edit</a>
                                        <a className="common_links" href="JavaScript:void(0);">Remove Address</a>
                                    </div>
                                </div>
                            </li>
                            <li className="listing">
                                <div className="details">
                                    <div className="inner pl-0">
                                        <p className="inr_detail">Akansha Goyal</p>
                                        <p className="inr_detail">3rd floor UDB landmark complex Plot No A-6 Basant Bahar Tonk Road</p>
                                        <p className="inr_detail">7300083928</p>
                                        <a className="common_links" href="JavaScript:void(0);">Edit</a>
                                        <a className="common_links" href="JavaScript:void(0);">Remove Address</a>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    console.log(state);
    return {
        example: state.example,
        languageReducer : state.languageReducer
    }
}

export default connect(mapStateToProps)(withStyles(styles)(AddressBook));
