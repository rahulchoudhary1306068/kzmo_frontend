import React, { Component } from 'react';
import {connect} from 'react-redux';

// import "react-responsive-carousel/lib/styles/carousel.min.css";
import local, {deafultLanguage} from '../../localization';
import {getter} from '../../commonfunction';

class LandingPage extends Component {
    constructor(props){
        super(props);
        this.state = {
            language: ''
        };
    }
  
    static initialAction({url, store}) {
       // store.dispatch(productListAction());
       return true;
    }
    componentDidMount(){
        this.setState({
            language: getter('language') ? getter('language') : deafultLanguage
        })
    }
    componentWillReceiveProps(nextProps){
        if(nextProps.languageReducer.value){
            this.setState({
                    language: nextProps.languageReducer.value
            })  
        }
    }
   
    render() {
        return (
            <div className="landing_outer">
                <div className="text-center launching_tab align-baseline">
                  <button className="launch_btn btn">{local.landLaunchingOffer[this.state.language]}</button>
                    {local.landOffColdWeather[this.state.language]}
                </div>
                <div className="d-flex content position-relative text-center row m-0">
                  <h1 className="position-absolute d-none d-md-block">{local.landOneDestination[this.state.language]}</h1>
                  <div className="female_sec position-relative col-md-6 p-0">
                    <img src="/img/temp/landing_women.jpg" />
                    <div className="position-absolute inner">
                        <h2 className="section_heading">{local.landShopForWomen[this.state.language]}</h2>
                        <button className="shop_now btn">{local.landShopForWomen[this.state.language]}</button>
                    </div>
                  </div>
                  <div className="male_sec position-relative col-md-6 p-0">
                    <img src="/img/temp/landing_men.jpg" />
                    <div className="position-absolute inner">
                        <h2 className="section_heading">{local.landShopForMen[this.state.language]}</h2>
                        <button className="shop_now btn">{local.landShopForMen[this.state.language]}</button>
                    </div>
                  </div>
                </div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    console.log(state);
    return {
        example: state.example,
        languageReducer: state.languageReducer
    }
}

export default connect(mapStateToProps)(LandingPage);
