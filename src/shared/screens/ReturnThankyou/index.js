import React, { Component } from 'react';
import {connect} from 'react-redux';
import {
  MuiThemeProvider,
  createMuiTheme,
  createGenerateClassName,
} from '@material-ui/core/styles';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Sidebar from '../../components/account_sidebar';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import FormLabel from '@material-ui/core/FormLabel';
import TextField from '@material-ui/core/TextField';
import FormGroup from '@material-ui/core/FormGroup';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControlLabel from '@material-ui/core/FormControlLabel';

// import "react-responsive-carousel/lib/styles/carousel.min.css";
import local, {deafultLanguage} from '../../localization';
import {getter} from '../../commonfunction';


const styles = theme => ({
  root: {
    '&$checked': {
      color: '#e4b122'
    }
  },
  checked: {},
  
});



const mui = createMuiTheme({
  overrides: {
    MuiFormLabel: {
      root: {
        color: "#b1b1b1"
      },
      focused: {
        color: "black !important"
      },
    }
  }
});

class ReturnThankyou extends Component {
    constructor(props){
        super(props);
        this.state = {
            language: ''
        };
    }
    static initialAction({url, store}) {
       // store.dispatch(productListAction());
       return true;
    }

   componentDidMount(){
        this.setState({
            language: getter('language') ? getter('language') : deafultLanguage
        })
   }
   componentWillReceiveProps(nextProps){
        if(nextProps.languageReducer.value){
            this.setState({
                    language: nextProps.languageReducer.value
            })  
        }
   }
    render() {
        const { classes } = this.props;
        
        return (
            <div className="dashboard_outer">
                <h1 className="category_name text-left">
                <a href="#"><svg className="icon icon-left-arrow"><use xlinkHref="/img/icon-sprite.svg#icon-left-arrow"></use></svg></a>
                {local.addrMyAccount[this.state.language]}</h1>
                <div className="row">
                    <div className="col-md-3 left_sec">
                      <Sidebar />
                    </div>
                    <div className="col-md-9 right_sec return_thankyou return_outer order_outer">
                        <h2 className="subheading">Thank you </h2>
                        <p className="thankyoucontnt"> <img src="/img/tick.png" /> The return request has been submitted and is under review. We shall respond back in next 48 hours.</p>
                        <p>Your return request number : 8575424</p>
                        <p className="subsubheading mb-0">Return details</p>
                        
                        <ul className="listing_outer">
                            <li className="listing">
                                <div className="details mb-4">
                                    <p className="mb-3 ordr_detail">
                                        Order ID: <a href="Javascript:void(0);" >154110268</a>
                                        <span>Order Placed on Fri, Sep 28 2018, 2:59 PM</span>
                                        <span>Delivered on Fri, Oct 11 2018, 5:40 PM</span>
                                    </p>
                                    <img src="/img/perfume.jpg" />
                                    <div className="inner">
                                        <p className="name"><a href="JavaScript:void(0);" >perfume</a></p>
                                        <p className="color_name">Total amount: $45</p>
                                        <p className="size_name">Qty: 1</p>
                                        <div className="control">
                                             <button onClick="" className="cart-qty-minus btn" type="button" value="-">-</button>
                                             <input type="text" name="1" id="" value="1" title="Qty" className="input-text qty" />
                                             <button onClick="" className="cart-qty-plus btn" type="button" value="+">+</button>
                                         </div>
                                    </div>
                                </div>
                                <p className="subsubheading mb-0">Return information</p>
                                <ul className="mt-3 sub_list mb-3">
                                    <li><span>Return reason</span> : Product appears used</li>
                                    <li><span>You have asked for</span> : Refund</li>
                                    <li><span>Mode of refund</span> : Refund in bank account/ Debit card / Wallets</li>
                                    <li><span>Comment </span>: I want my money back</li>
                                </ul>
                                <Button
                                        variant="contained"
                                        color="primary"
                                    >
                                    Back to My Orders
                                </Button>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        );
    }1
}

function mapStateToProps(state) {
    console.log(state);
    return {
        example: state.example,
        languageReducer: state.languageReducer
    }
}
export default connect(mapStateToProps)(withStyles(styles)(ReturnThankyou));

