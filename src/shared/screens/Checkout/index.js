import React, { Component } from 'react';
import { connect } from 'react-redux';
import { InitailStateAction, OTPVerifyAction, InvalidTokenAction } from '../../actions/login'

import {
  MuiThemeProvider,
  createMuiTheme,
  createGenerateClassName,
} from '@material-ui/core/styles';
import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import FormLabel from '@material-ui/core/FormLabel';
import FormControl from '@material-ui/core/FormControl';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormHelperText from '@material-ui/core/FormHelperText';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import Checkbox from '@material-ui/core/Checkbox';
import { getter, createSessionId, createToken, createTtl,getUserAgent, getObject } from '../../commonfunction';
import Login from '../Login';
import { showLogin } from '../../actions/login';
import  {StoreAction} from '../../actions/header';
import { userProfileAction, editProfileAction, removeProfileAction } from '../../actions/userprofile'
import { PaymentOptionAction, placeOrderAction, codOTPAction } from '../../actions/checkout'
import { checkRequired, checkPhonenumber, checkPincode } from '../../commonfunction'
import  {SADA_Payment_Option_Id,platform,omniture_mcid,paymentGateway_URL,api_KEY,COD_Payment_Option_Id,returnUrlFail,returnUrlSuccess} from '../../constant';
import {notify} from 'react-notify-toast';
import {PinMap} from '../../components/pinMap';
import Map from '../../components/map';
import Loader from '../../components/loader'

import local, {deafultLanguage} from '../../localization';
import ReactGA from 'react-ga'
import InputLabel from '@material-ui/core/InputLabel';
import Select from '@material-ui/core/Select';
import Input from '@material-ui/core/Input';
import MenuItem from '@material-ui/core/MenuItem';
import {CityAction} from '../../actions/header'


const styles = theme => ({
  root: {
    '&$checked': {
      color: '#e4b122'
    }
  },
  checked: {},
  
});
const mui = createMuiTheme({
  overrides: {
    MuiFormLabel: {
      root: {
        color: "#b1b1b1"
      },
      focused: {
        color: "black !important"
      },
    }
  }
});
var latLongData;
class Checkout extends Component {
    static initialAction({url, store}) {
       return true;
    }
    constructor(props){
        super(props);
        this.state = {
            cartData : false,
            paymentoption : false,
            profileData : false,
            pinCode : "",
            pinCodeError : "",
            city : "",
            cityError: "",
            state : "",
            stateError : "",
            name : "",
            nameError : "",
            lastname : "",
            phoneNumber: "",
            phoneNumberError : "",
            houseNo : "",
            houseNoError : "",
            street : "",
            streetError : "",
            addressType : "home",
            addressTypeError : "",
            useBilling: true,
            useBillingError : "",
            bPinCode : "",
            bPinCodeError : "",
            bCity : "",
            bCityError: "",
            bState : "",
            bStateError : "",
            bName : "",
            blastName:"",
            
            bNameError : "",
            bPhoneNumber: "",
            bPhoneNumberError : "",
            bHouseNo : "",
            bHouseNoError : "",
            bStreet : "",
            bStreetError : "",
            bAddressType : "home",
            bAddressTypeError : "",
            paymentOptionValue : "",
            paymentOptionError : "",
            profileAddress: false,
            showAddress : true,
            useBillinglist : true,
            billingDataName: "" ,
            billingDataLastname:"",
            billingDataAddress: "" ,
            billingDataPhone: "",
            profilePinCode:"",
            user_Id : "",
            order_id : "",
            deliveryType : "1",
            markers:[],
            mymap :"",
            lat : 23.7224509,
            long : 46.7893653,
            mapheight : "0px",
            storeData : false,
            checkIs_code : false,
            chooseStore : false,
            storeDataMap : [],
            otpintervalClear : "",
            otpTimer : 30,
            resendOTPshow : false,
            showMap : false,
            ResendOTPData : false,
            resendOTPStatus :false,
            showLoader : false,
            newProfileAdd : false,
            showPickStoreComing: false,
            ttl : "",
            token : "",
            userAgent : "",
            paymentButtonText : "Make Secure Payment",
            placeOrderButtonDisable : "",
            language: '',
            onlinePaymentMode: true,
            isRender : true,
            cityData:{data:{response:[]}},
            selectedCity:"",
            selectedCityId:"",
            selectedbCity:"",
            selectedbCityId:"",
            countryDefaultValue : "SA", 
                
        };
        this.pG = React.createRef();
    }

    componentDidMount(){
        ReactGA.pageview('checkout');
        this.setState({
            language: getter('language') ? getter('language') : deafultLanguage
        },()=>{
            this.setState({paymentButtonText: local.checkMakePaymentSecure[this.state.language]});
        })

       
        if(!getter('userId')){
            this.props.LoginReducer.showLogin = false;
            this.props.dispatch(showLogin());
        }
        else{
            
            this.props.dispatch(userProfileAction()); 
             
        }    
        
        this.props.dispatch(PaymentOptionAction());
        this.props.dispatch(CityAction(this.state.countryDefaultValue));
        
        //this.initMap();
    }

    componentWillReceiveProps(nextProps){
        if(nextProps.languageReducer.value){
            this.setState({
                    language: nextProps.languageReducer.value
            })  
        }
        this.checkPaymentMode();
        if(Object.keys(nextProps.OTPReducer.data).length){
            if(nextProps.OTPReducer.data.message){
                this.setState({
                    otpValueError : nextProps.OTPReducer.data.message
                })
                
                
            }
            else{
                this.toggleOTPPopUp('hide');
                this.redirectToPaymentGateway()
                //this.props.history.push('/thankyou/order_id/'+this.state.order_id);
            }
            this.props.dispatch(InitailStateAction('InitailStateOTPVerify_ACTION'))
        }
        
        if(nextProps.cartReducer.cart.response){
            
            this.setState({
                cartData : nextProps.cartReducer.cart,
                checkIs_code : nextProps.cartReducer.cart.response.is_cod?false:true
            })
            // if(nextProps.cartReducer.cart.response.total >=10000){
            //     this.setState({
            //         placeOrderButtonDisable : "btn_disable"
            //     })
            // }
            // else{
            //     this.setState({
            //         placeOrderButtonDisable : "false"
            //     })
            // }
            
            
        }
        if(Object.keys(nextProps.codOTPReducer.data).length){
            this.setState({
                showLoader:false
            }) 

            if(!this.state.resendOTPStatus)
                this.toggleOTPPopUp('show');
            else
                this.toggleOTPPopUp()
            this.setState({
                otpMobileNumber : nextProps.codOTPReducer.data.response.phone
            })  
            notify.show(nextProps.codOTPReducer.data.response.sms_response)  
            this.props.dispatch(InitailStateAction('InitailStatecodOTP_ACTION'));
                     
            
        }
        if(nextProps.storeReducer && nextProps.storeReducer.data.data){
           
            this.setState({
                storeData : nextProps.storeReducer.data.data
            })
            typeof(nextProps.storeReducer.data.data) == "object" && nextProps.storeReducer.data.data.map((value)=>{
                this.setState({
                    storeDataMap :[...this.state.storeDataMap,...nextProps.storeReducer.data.data]
                })
            })
        }
        
        if(nextProps.UserProfileReducer.data.response){
            //console.log("profile data", nextProps.UserProfileReducer.data);
            this.setState({
                profileData : nextProps.UserProfileReducer.data,
                
                
            })
            if(nextProps.UserProfileReducer.data.response.length>1){
                
                this.setState({
                    showAddress : false
                })
            }
            else{
                this.setState({
                    showAddress : true
                })
            }
            if(nextProps.UserProfileReducer.data.response.length==2 && !this.state.profileAddress){
                
                this.setState({
                    profileAddress : 'b_0',
                    
                })
            }
            if(this.state.newProfileAdd){
                window.scrollTo(0,0)
                this.setState({
                    profileAddress : 'b_0',
                    newProfileAdd : false
                })
            }
        }
       
        if(nextProps.PaymentOptionReducer.data != this.props.PaymentOptionReducer.data){
            this.setState({
                paymentoption : nextProps.PaymentOptionReducer.data.response
            })
        }
        if(nextProps.placeOrderReducer.data.response){
            if(nextProps.placeOrderReducer.data.response.order_id){
                this.setState({
                    order_id : nextProps.placeOrderReducer.data.response.order_id.order_id
                })
                if(this.state.subPaymenttype == COD_Payment_Option_Id)
                {
                    this.setState({
                        order_id:nextProps.placeOrderReducer.data.response.order_id.order_id,
                      ResendOTPData :  {"platform":"D","order_id":nextProps.placeOrderReducer.data.response.order_id.order_id,
                      "device_id":"346378290","user_profile_id":this.state.profileData.response[this.state.profileAddress.split("_")[1]].profile_id,
                      "pincode":this.state.profilePinCode}
                    },()=>{
                        this.props.dispatch(codOTPAction(this.state.ResendOTPData));
                    }) 
                  
                    
                }
                else{
                    this.setState({
                        showLoader:false
                    },()=>{
                        this.redirectToPaymentGateway();
                    })
                    
                    
                }
                
                this.props.dispatch(InitailStateAction('InitailStatePlaceOrder_ACTION'));
            }
            else{
                this.setState({
                    showLoader:false
                })  
                // show error message when issue with login
                // if(nextProps.placeOrderReducer.data.extra==='_api_param_invalid_token'){
							
                //     this.props.dispatch(InvalidTokenAction());
                //     return;
                // }
                // show error message receive while placing order
                if(nextProps.placeOrderReducer.data.message)
                    notify.show(nextProps.placeOrderReducer.data.message,"error");
                    this.props.dispatch(InitailStateAction('InitailStatePlaceOrder_ACTION'));
               // alert(nextProps.placeOrderReducer.data.message)
            }
            
            
        }
        if(Object.keys(nextProps.RemoveProfileReducer.data).length){
            if(nextProps.RemoveProfileReducer.data.status=='Successful'){
                notify.show(nextProps.RemoveProfileReducer.data.msg,"success");
                this.setState({
                    profileAddress : false
                })
            
            }
            this.props.dispatch(InitailStateAction('InitailStateRemoveProfile_ACTION'));
        }
        if(nextProps.cityData!==this.props.cityData)
        {
            this.setState({cityData:nextProps.cityData});
        }
    }

    checkPaymentMode(){
        if(this.state.onlinePaymentMode)
            this.setState({paymentButtonText: local.checkMakePaymentSecure[this.state.language]})
        else
            this.setState({paymentButtonText: local.checkConfirmOrder[this.state.language]})
    }

    redirectToPaymentGateway(){
        const node = this.pG.current;
        node.submit();
    }
    selectCity(cityId,type){
        
        if(type!=="bCity")
        {
            let selCity = this.state.cityData.data.response.filter((city)=> cityId===city.id);
            this.setState({selectedCity: selCity[0].city});
            this.setState({selectedCityId: selCity[0].id});  
        }
        else
        {
            this.setState({selectedbCity: cityId})
            //this.setState({selectedbCity: selCity[0].city});
            //this.setState({selectedbCityId: selCity[0].id});
        }
    }

    addProfile(){
        event.preventDefault();
        console.log("amkt",latLongData);
        let error = false;
        let vpincode = checkPincode(this.state.pinCode);
   //      if(!vpincode[0]){
			// error = true;
			// this.setState({
			// 	pinCodeError : vpincode[1]
			// })
   //      }
        let vcity = checkRequired(this.state.selectedCity, this.state.language)
        if(!vcity[0]){
            error = true
            this.setState({
                cityError :vcity[1]
            })
        }

       
        let vname = checkRequired(this.state.name, this.state.language)
        if(!vname[0]){
            error =  true;
            this.setState({
                nameError : vname[1]
            })
        }
        let vphone = checkPhonenumber(this.state.phoneNumber, this.state.language)
        if(!vphone[0]){
            error =  true;
            this.setState({
                phoneNumberError : vphone[1]
            })
        }
        let vhouse = checkRequired(this.state.houseNo, this.state.language)
        if(!vhouse[0]){
            error =  true;
            this.setState({
                houseNoError : vhouse[1]
            })
        }
       
        let vaddress = checkRequired(this.state.addressType, this.state.language)
        if(!vaddress[0]){
            error =  true;
            this.setState({
                addressTypeError : vaddress[1]
            })
        }
        if(!this.state.useBilling){
            let bvpincode = checkPincode(this.state.bPinCode);
   //      if(!bvpincode[0]){
			// error = true;
			// this.setState({
			// 	bPinCodeError : bvpincode[1]
			// })
   //      }
        let bvcity = checkRequired(this.state.selectedbCity, this.state.language)
        if(!bvcity[0]){
            error = true
            this.setState({
                bCityError :bvcity[1]
            })
        }

       
        let bvname = checkRequired(this.state.bName, this.state.language)
        if(!bvname[0]){
            error =  true;
            this.setState({
                bNameError : bvname[1]
            })
        }
        let bvphone = checkPhonenumber(this.state.bPhoneNumber, this.state.language)
        if(!bvphone[0]){
            error =  true;
            this.setState({
                bPhoneNumberError : bvphone[1]
            })
        }
        let bvhouse = checkRequired(this.state.bHouseNo, this.state.language)
        if(!bvhouse[0]){
            error =  true;
            this.setState({
                bHouseNoError : bvhouse[1]
            })
        }
       
        let bvaddress = checkRequired(this.state.bAddressType, this.state.language)
        if(!bvaddress[0]){
            error =  true;
            this.setState({
                bAddressTypeError : bvaddress[1]
            })
        }
        }
        //alert("error = " + error);
        
        if(!error){
            //console.log(submit request to for add profile)
            console.log(this.state);
            let data = {
                
                "s_firstname": this.state.name,
                "s_lastname": this.state.lastname,
                "s_address": this.state.houseNo,
                "s_address_2": '',
                "s_city": this.state.selectedCity,
                "s_state": '',
                "s_zipcode": this.state.pinCode,
                "s_phone": this.state.phoneNumber,
                "address_type": this.state.addressType,
                "platform": "D",
                "latitude": latLongData?latLongData.lat:"",
                "longitude": latLongData?latLongData.lng:"",
                "city_id":this.state.selectedCityId
            }

            let temp = {};

            if(!this.state.useBilling){
              temp = {
                "b_firstname": this.state.bName,
                "b_lastname": this.state.blastName,
                "b_address": this.state.bHouseNo,
                "b_address_2": '',
                "b_city": this.state.selectedbCity,
                "b_state": '',
                "b_zipcode": this.state.bPinCode,
                "b_phone": this.state.bPhoneNumber,
                "latitude": latLongData?latLongData.lat:"",
                "longitude": latLongData?latLongData.lng:""
              }
            }

            let addressData = Object.assign({}, data, temp);
            this.setState({
                newProfileAdd : true
            })
            this.props.dispatch(editProfileAction(addressData));
        }
    

    }
    updateState(e){
        this.setState({
			  [e.target.name+"Error"] : ""
		  })
		  this.setState({
			  [e.target.name]:e.target.value,
          })
         
    }
    updateStatePayment(e,index){
        
        this.setState({
            [e.target.name]:e.target.value,
        })
        if(e.target.value==4){
            console.log(local.checkConfirmOrder[this.state.language]);
            this.setState({
                paymentButtonText : local.checkConfirmOrder[this.state.language],
                paymentMode: false
            }) 
            if(Object.keys(this.state.cartData.response).length && this.state.cartData.response.total >=3750 ){
                     notify.show(local.cartCashOnDelivery[this.state.language],'error');
                     this.setState({
                             placeOrderButtonDisable : "btn_disable"
                     })
                     return;
                } 
        }
        else{
            this.setState({
                paymentButtonText : local.checkMakePaymentSecure[this.state.language],
                paymentMode: true,
                placeOrderButtonDisable : "false"
            }) 
        }
        $('.card_dumy').addClass('collapse');
        $('#credit_detail'+index).removeClass('collapse')
            
      }
    updateStatedeliveryType=(e)=>{
      
        this.setState({
            [e.target.name]:e.target.value,
        })
        if(e.target.value==2){//2 pick from store
            this.getCurrentLocationforstore();
            $('#delivery_here').removeClass('show')
            $('#delivery_here').removeClass('active')
            $('#pick_here').addClass('show active')
            this.setState({
                chooseStore : false
            })
            this.togglePickStoreComing('show'); // temporary
        }
        else{
            $('#pick_here').removeClass('show')
            $('#pick_here').removeClass('active')
            $('#delivery_here').addClass('show')
            $('#delivery_here').addClass('active')
        }
        
    }
    togglePickStoreComing(value){ // tempoary function
        
        $('#pickStoreComing').modal(value);
        if(value =='hide'){
            $('#pick_here').removeClass('show')
            $('#pick_here').removeClass('active')
            $('#delivery_here').addClass('show')
            $('#delivery_here').addClass('active')
            this.setState({
                deliveryType : "1"
            })
        }
        
    }
       
        
    updateStateCheckbox(e,isChecked){
        
      this.setState({
			  [e.target.name]: e.target.checked,
          })
        if(e.target.name=='useBillinglist'){
            this.showAddNewAddress();
        }
    }
    showAddNewAddress(){
        
        this.setState({selectedCity:""});
        this.setState({selectedCityId:""});
        this.setState({selectedbCity:""});
        this.setState({selectedbCityId:""});
        this.setState({
            showAddress : !this.state.showAddress,
            mapheight : "0px"
        },()=>{
            if(this.state.showAddress){
                $('html, body').animate({
                    scrollTop: $($('#addressCheckout')).offset().top-120
                }, 500, 'linear');  

            }
            
        })
        
    }
    continueShippingList(){
        if(!this.state.profileAddress){
            notify.show(local.logErrorChooseAddr[this.state.language],"error");
            return false;
        }
       
        if(this.state.useBillinglist){
            // open payment accrodring
            $("#checkout_accordion .collapse").removeClass('show');
          
           $("#payment_content").addClass('show');
           $('html, body').animate({
            scrollTop: $($('#payment_content')).offset().top-150
        }, 500, 'linear');
        }
        else{
            $("#checkout_accordion .collapse").removeClass('show');
          
           $("#billing_content").addClass('show');
         
                // else open billing address accrdine       
        }
        this.setState({
               billingDataName: this.state.profileData.response[this.state.profileAddress.split("_")[1]].b_firstname,
               billingDataLastName: this.state.profileData.response[this.state.profileAddress.split("_")[1]].b_lastname,
               billingDataAddress:this.state.profileData.response[this.state.profileAddress.split("_")[1]].b_address + " " +this.state.profileData.response[this.state.profileAddress.split("_")[1]].b_address_2,
               billingDataPhone: this.state.profileData.response[this.state.profileAddress.split("_")[1]].b_phone
           })
    }
    continueBillingList(){
        $("#checkout_accordion .collapse").removeClass('show');
          
        $("#payment_content").addClass('show');
    }

    placeOrder(){
        event.preventDefault();
        if(Object.keys(this.state.cartData.response).length && this.state.cartData.response.total >=3750 && this.state.paymentOptionValue ==4){
            //notify.show(local.cartCashOnDelivery[this.state.language],'error');
            return;
        } 
        let subpaymentOption = "";    
        if(this.state.paymentOptionValue ==4){ // COD case
            subpaymentOption = COD_Payment_Option_Id;
            this.setState({
                subPaymenttype : subpaymentOption
            })
        }
        else if(this.state.paymentOptionValue ==10000){
            subpaymentOption = SADA_Payment_Option_Id;
            this.setState({
                subPaymenttype : subpaymentOption
            })
        }
        else{
            subpaymentOption = this.state.subPaymenttype;
        }
        
        if(!this.state.profileAddress)
        {
            notify.show(local.logErrorChoodeAddrFurther[this.state.language],"error");
            return false;
        }
        
        if(!(this.state.cartData && Object.keys(this.state.cartData.response).length)){
            notify.show(local.logErrorSomethingWentWrong[this.state.language],"error");
            return false;

        }
        
        if(!subpaymentOption ){
            notify.show(local.logErrorChoosePayMode[this.state.language],'error');
            return false;

        }
        this.setState({
            profilePinCode : this.state.profileData.response[this.state.profileAddress.split("_")[1]].s_zipcode,
            user_Id : getter('userId')
        })
        
        let profileId = this.state.profileData.response[this.state.profileAddress.split("_")[1]].profile_id;
        let data = {profileId : profileId,payment_option_id:subpaymentOption}
        if(this.state.cartData.response.applied_coupon){
            data['coupon_codes'] = this.state.cartData.response.applied_coupon.cart[0]
        }
        
        this.setState({
            showLoader:true
        })
        this.props.dispatch(placeOrderAction(data))
    }

    setAddress=(data)=>{
        if(data.country!=='Saudi Arabia'){
            notify.show(local.logErrorSelectSaudi[this.state.language]);
            return false;
        }
        let selCity = this.state.cityData.data.response.filter((city)=>    
            city.city==data.city
        );
        this.setState({
            city : data.city,
            houseNo : data.houseNo,
            name : this.state.profileData.response[this.state.profileData.response.length-1].s_firstname,
            lastname: this.state.profileData.response[this.state.profileData.response.length-1].s_lastname,
            selectedCityId:(selCity[0]!=="" && selCity[0]!==undefined)?selCity[0].id:0,
            selectedCity:(selCity[0]!=="" && selCity[0]!==undefined)?selCity[0].city:""
        })
    }

    getCurrentLocationforstore = () =>{
        if (window.navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(
                (position)=> {
                    this.setState({
                        lat : position.coords.latitude,
                        long : position.coords.longitude,
                        mapheight : "500px"
                    },()=>{
                        this.props.dispatch(StoreAction({lat:this.state.lat,long:this.state.long}))
                    })
                } 
            )
        }
        else{
            this.props.dispatch(StoreAction({lat:this.state.lat,long:this.state.long}))
        }          
    }
    
    getCurrentLocation = ()=>{
        if (window.navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(
                (position)=> {
                    this.setState({
                        lat : position.coords.latitude,
                        long : position.coords.longitude,
                        mapheight : "500px"
                    })
          
                },
                function() {notify.show(local.logErrorGeolocationNot[this.state.language],"error")}
            );
        }
        this.setState({
            showMap : true
        })
    }

    selectStore(type,index){
        this.setState({
            chooseStore : !this.state.chooseStore
        })
        
        if(type){
            this.setState({
                storeData:[this.props.storeReducer.data.data[index]]
            })
        }
        else{
            this.setState({
                storeData : this.props.storeReducer.data.data
            })
        }
    }

    toggleOTPPopUp(value,close){
        if(value)
            $('#cod_confirmation').modal({backdrop: 'static',value});
        if(value == 'show' || this.state.resendOTPStatus){
            this.state.otpintervalClear = setInterval(()=>{ 
                
                if(this.state.otpTimer > 0){
                    this.setState({
                        otpTimer : this.state.otpTimer-1
                    }) 
                }
                else{
                    this.setState({
                        resendOTPshow:true
                })
                    clearInterval(this.state.otpintervalClear);
                }
            },1000)
        }
        else{
            this.setState({
                resendOTPshow:false,
                otpTimer : 30
            })
        }

        this.setState({
            resendOTPStatus: false
        })
        if(close){
            this.props.history.push('/orderfail');
        }
    }

    resendOTP(){
        this.setState({
            resendOTPStatus: true,
            resendOTPshow : false,
            otpTimer :30
        })
        this.props.dispatch(codOTPAction(this.state.ResendOTPData));
    }
   
    removeAddress(profileID){
        this.props.dispatch(removeProfileAction(profileID))
    }

    otpVerify(){
        let error = false
        let otpValue = checkRequired(this.state.otpValue, this.state.language)
        if(!otpValue[0]){
            error =  true;
            this.setState({
                otpValueError : otpValue[1]
            })
        }
        if(!error){
            // this setState require for payment Gateway form
            let data = {...this.state.ResendOTPData};
            let userObject =  getObject('userObject')
            data['otp'] =  this.state.otpValue;
            
            data['cod'] = true
            this.setState({
                ttl :userObject.data.ttl,
                token : userObject.data.token,
                userAgent : navigator.userAgent
            },()=>{
                data ['ttl'] = this.state.ttl;
                data ['token'] = this.state.token;
            })
            console.log(data);
            this.props.dispatch(OTPVerifyAction(data));
            
        }
    }
    
    updateLatLongValue = (data)=>{
        latLongData = data;
        return;
    }
   
    render() {
        const { classes } = this.props;

        return ( 
            <div className="checkout_outer">
                <h1 className="category_name text-left">{local.checkOut[this.state.language]}</h1>
                <div className="row">
                    <div className="col-lg-9 col-md-8 left_sec">
                        <div className="details_section">
                            <ul className="nav nav-tabs" role="tablist">
                              <li className="nav-item">
                                <a className="nav-link active" href="#delivery_here" role="tab" >
                                    <RadioGroup
                                        aria-label="Delivery Type"
                                        
                                        name="deliveryType"
                                        value={this.state.deliveryType}
                                        onChange={e=>this.updateStatedeliveryType(e)}
                                    >
                                        <FormControlLabel
                                          value="1"
                                          control={
                                            <Radio
                                              classes={{root: classes.root, checked: classes.checked}}
                                            />
                                          }
                                          label={local.checkDeliverHere[this.state.language]}
                                          labelPlacement="end"
                                        />
                                    </RadioGroup>
                                </a>
                              </li>
                               {/* <li className="nav-item">
                                <a className="nav-link" href="#pick_here" role="tab" >
                                    <RadioGroup
                                        aria-label="Delivery Type"
                                        name="deliveryType"
                                        value={this.state.deliveryType}
                                        onChange={e=>this.updateStatedeliveryType(e)}
                                    >
                                       
                                        <FormControlLabel
                                          value="2"
                                          control={
                                            <Radio
                                              classes={{root: classes.root, checked: classes.checked}}
                                            />
                                          }
                                          label={local.checkPickFromStore[this.state.language]}
                                          labelPlacement="end"
                                         
                                        />
                                    </RadioGroup>
                               </a>
                              </li> */}
                            </ul>
                            <div className="tab-content">
                           
                                <div role="tabpanel" className="tab-pane fade in active show" id="delivery_here">
                                    <div id="checkout_accordion" className="checkout_acc">
                                        <div className="card">
                                            <div className="card-header" id="shiping_heading">
                                                <h5 className="m-0">
                                                    <button className="btn btn-link text-left sub_heading" data-toggle="collapse" data-target="#shipping_content" aria-expanded="true" aria-controls="shipping_content">
                                                        {local.checkShippingAddress[this.state.language]}
                                                    </button>
                                                </h5>
                                            </div>
                                            <div id="shipping_content" className="collapse show" aria-labelledby="shiping_heading" data-parent="#checkout_accordion">
                                                <div className="card-body">
                                               
                                               {this.state.profileData && this.state.profileData.response.length>1 && <form className="row choose_address">
                                                        <div className="input_radio w-100">
                                                            <RadioGroup
                                                                aria-label="profileAddress"
                                                                name="profileAddress"
                                                                value={this.state.profileAddress}
                                                                onChange={e=>this.updateState(e)}
                                                            >
                                                                { this.state.profileData && this.state.profileData.response.map((value,index)=>{
                                                                    if(!value.address_type){
                                                                        return
                                                                    }
                                                                    return  <FormControlLabel
                                                                    value={"b_"+index}
                                                                    control={
                                                                      <Radio
                                                                        classes={{root: classes.root, checked: classes.checked}}
                                                                        />
                                                                    }
                                                                    label={
                                                                            <div className="content"><p>{value.s_firstname} {value.s_lastname}</p><p>{value.s_address+ " "+value.s_address_2}</p>
                                                                            <p>{value.s_phone}</p>{<a onClick={this.removeAddress.bind(this,value.profile_id)} href="JavaScript:void(0);">{local.cartRemove[this.state.language]}</a>}</div>
                                                                        } 
                                                                    labelPlacement="end"
                                                                />
                                                                })
                                                               
                                                                }
                                                          
                                                            </RadioGroup>
                                                        </div>
                                                        <FormGroup className="w-100 input_checkbox flex-row align-items-center">
                                                            <a href="JavaScript:void(0);" onClick={this.showAddNewAddress.bind(this)}className="add_address">{local.addrAddNewAddress[this.state.language]}</a>
                                                            <FormControlLabel
                                                              control={
                                                                <Checkbox classes={{root: classes.root, checked: classes.checked}} value="Use as billing address" />
                                                              }
                                                              label={local.checkUseBillingAddress[this.state.language]}
                                                              checked = {this.state.useBillinglist}
                                                             
                                                              name = "useBillinglist"
                                                              onChange = {e=>this.updateStateCheckbox(e)}
                                                            
                                                            />
                                                        </FormGroup>
                                                        <Button
                                                            variant="contained"
                                                            color="primary"
                                                            className="continue"
                                                            onClick={this.continueShippingList.bind(this)}
                                                        >
                                                            {local.logContinue[this.state.language]}
                                                        </Button>
                                                    </form> }
                                                    {this.state.showAddress && 
                                                    <div id='addressCheckout'> 
                                                     <div className="locatn_sec">
                                                    <span><svg className="icon icon-gps"><use xlinkHref="/img/icon-sprite.svg#icon-gps"></use></svg> <a onClick={this.getCurrentLocation.bind(this)} href="Javascript:void(0);">{local.addrUseCurrenLocation[this.state.language]}</a></span>{/*<span className="ml-4 mr-4">or</span>*/} 
                                                   {/* <span><svg className="icon icon-qr-code"><use xlinkHref="/img/icon-sprite.svg#icon-qr-code"></use></svg> <a href="Javascript:void(0);">Upload Address Registry QR Code</a></span>*/}
                                                </div>
                                                {/* <div style={{height :this.state.mapheight}} className="map_section position-relative" id="map">
                                                   
                                                </div> */}
                                                {this.state.showMap && <Map updateLatLong={this.updateLatLongValue} address = {this.setAddress} lat =  {this.state.lat} lng = {this.state.long} />}
                                                    
                                                    <form className="row" >
                                                   
                                                    <MuiThemeProvider theme={mui}>
                                                    
                                                        <TextField
                                                          id="chname"
                                                          className="input_outer col-sm-6"
                                                          label={local.addrFirstName[this.state.language]}
                                                          type="text"
                                                          helperText={this.state.nameError}
                                                          onChange = {e=>this.updateState(e)}
                                                          name = "name"
                                                          value = {this.state.name}
                                                        />
                                                        <TextField
                                                          id="lastname"
                                                          className="input_outer col-sm-6"
                                                          label={local.addrLastName[this.state.language]}
                                                          type="text"
                                                          helperText={this.state.lastnameError}
                                                          onChange = {e=>this.updateState(e)}
                                                          name = "lastname"
                                                          value = {this.state.lastname}
                                                        />
                                                        {/*<TextField
                                                          id="city"
                                                          value = {this.state.city}
                                                          className="input_outer col-sm-6"
                                                          label={local.addrCity[this.state.language]}
                                                          type="text"
                                                          helperText={this.state.cityError}
                                                          onChange = {e=>this.updateState(e)}
                                                          name = "city"
                                                        />*/}
                                                        <FormGroup className="col-sm-6 col-md-6 slect_outer">
                                                        <FormControl className="formControl slect_outer">
                                                        <InputLabel shrink htmlFor="city-placeholder">{local.addrCity[this.state.language]}</InputLabel>
                                                        <Select
                                                            value={this.state.selectedCityId}
                                                            input={<Input name="city" id="city-placeholder" />}
                                                            name="city"
                                                            className="" 
                                                            onChange={(e) => this.selectCity(e.target.value,"city")}
                                                            >
                                                        {
                                                            this.state.cityData.data.response && this.state.cityData.data.response.map(city => (
                                                                <MenuItem value={city.id}>{city.city}</MenuItem>
                                                            ))
                                                            
                                                        }
                                            
                                                        </Select>
                                                        </FormControl>
                                                        </FormGroup>
                                                        <TextField
                                                          id="chphone_number"
                                                          className="input_outer col-sm-6"
                                                          label={local.addrPhoneNumber[this.state.language]}
                                                          type="text"
                                                          onChange = {e=>this.updateState(e)}
                                                          name = "phoneNumber"
                                                          helperText = {this.state.phoneNumberError}
                                                        /> 
                                                        <TextField
                                                          id="pincode"
                                                          value = {this.state.pinCode}
                                                          className="input_outer col-sm-6"
                                                          label={local.addrPinCode[this.state.language]}
                                                          type="text"
                                                          helperText = {this.state.pinCodeError}
                                                          name = "pinCode"
                                                          onChange = {e=>this.updateState(e)}
                                                        />
                                                        <TextField
                                                          id="house_no"
                                                          value = {this.state.houseNo}
                                                          className="input_outer col-sm-6"
                                                          label={local.checkHouseNumber[this.state.language]}
                                                          type="text"
                                                          helperText = {this.state.houseNoError}
                                                          name = "houseNo"
                                                          onChange = {e=>this.updateState(e)}
                                                        />
                                                       
                                                       
                                                         </MuiThemeProvider>
                                                        <div className="input_radio d-sm-flex w-100 align-items-center">
                                                            <FormLabel component="legend">{local.addrAddressType[this.state.language]} </FormLabel>
                                                            <RadioGroup
                                                                aria-label="Address Type"
                                                                name="addressType"
                                                                value={this.state.addressType}
                                                                onChange = {e=>this.updateState(e)}
                                                                helperText = {this.state.addressTypeError}
                                                            >
                                                                <FormControlLabel
                                                                  value="home"
                                                                  control={
                                                                    <Radio
                                                                      classes={{root: classes.root, checked: classes.checked}}
                                                                    />
                                                                  }
                                                                  label={local.addrHome[this.state.language]}
                                                                  labelPlacement="end"
                                                                />
                                                                <FormControlLabel
                                                                  value="office"
                                                                  control={
                                                                    <Radio
                                                                      classes={{root: classes.root, checked: classes.checked}}
                                                                    />
                                                                  }
                                                                  label={local.addrOffice[this.state.language]}
                                                                  labelPlacement="end"
                                                                />
                                                                <FormControlLabel
                                                                  value="other"
                                                                  control={
                                                                    <Radio
                                                                      classes={{root: classes.root, checked: classes.checked}}
                                                                    />
                                                                  }
                                                                  label={local.addrOthers[this.state.language]}
                                                                  labelPlacement="end"
                                                                />

                                                            </RadioGroup>
                                                        </div>
                                                        <FormGroup className="w-100 input_checkbox">
                                                        <FormControlLabel
                                                          control={
                                                            <Checkbox classes={{root: classes.root, checked: classes.checked}} value={this.state.useBilling} />
                                                          }
                                                          label={local.checkUseBillingAddress[this.state.language]}
                                                          checked = {this.state.useBilling}
                                                          helperText={this.state.useBillingError}
                                                          name = "useBilling"
                                                          onChange = {e=>this.updateStateCheckbox(e)}
                                                        />
                                                        </FormGroup>

                                                        {/*Billing address start*/}
                                                        {!this.state.useBilling && <div className="row"><div className="w-100">
                                                        
                                                        </div>


                                                        <MuiThemeProvider theme={mui}>
                                                        <TextField
                                                          id="bChname"
                                                          className="input_outer col-sm-6"
                                                          label={local.addrFirstName[this.state.language]}
                                                          type="text"
                                                          helperText={this.state.bNameError}
                                                          onChange = {e=>this.updateState(e)}
                                                          name = "bName"
                                                        />
                                                        <TextField
                                                          id="blastname"
                                                          className="input_outer col-sm-6"
                                                          label={local.addrLastName[this.state.language]}
                                                          type="text"
                                                          
                                                          onChange = {e=>this.updateState(e)}
                                                          name = "blastName"
                                                        />
                                                        {/*<TextField
                                                          id="bCity"
                                                          className="input_outer col-sm-6"
                                                          label={local.addrCity[this.state.language]}
                                                          type="text"
                                                          helperText={this.state.bCityError}
                                                          onChange = {e=>this.updateState(e)}
                                                          name = "bCity"
                                                        />*/}
                                                        <FormGroup className="col-sm-6 col-md-6">
                                                        <FormControl className="formControl slect_outer" >
                                                        <InputLabel shrink htmlFor="city-placeholder">{local.addrCity[this.state.language]}</InputLabel>
                                                        <Select
                                                            value={this.state.selectedbCity}
                                                            input={<Input name="bCity" id="city-placeholder" />}
                                                            name="bCity"
                                                            className="" 
                                                            onChange={(e) => this.selectCity(e.target.value,"bCity")}
                                                            >
                                                        {
                                                            this.state.cityData.data.response && this.state.cityData.data.response.map(city => (
                                                                <MenuItem value={city.city}>{city.city}</MenuItem>
                                                            ))
                                                            
             



                                                            }      
                                                        </Select>
                                                        </FormControl>
                                                        </FormGroup>
                                                        <TextField
                                                          id="bChphone_number"
                                                          className="input_outer col-sm-6"
                                                          label={local.addrPhoneNumber[this.state.language]}
                                                          type="text"
                                                          onChange = {e=>this.updateState(e)}
                                                          name = "bPhoneNumber"
                                                          helperText = {this.state.bPhoneNumberError}
                                                        />
                                                        <TextField
                                                          id="bpincode"
                                                          className="input_outer col-sm-6"
                                                          label={local.addrPinCode[this.state.language]}
                                                          type="text"
                                                          helperText = {this.state.bPinCodeError}
                                                          name = "bPinCode"
                                                          onChange = {e=>this.updateState(e)}
                                                        /> 
                                                        <TextField
                                                          id="bHouse_no"
                                                          className="input_outer col-sm-6"
                                                          label={local.checkHouseNumber[this.state.language]}
                                                          type="text"
                                                          helperText = {this.state.bHouseNoError}
                                                          name = "bHouseNo"
                                                          onChange = {e=>this.updateState(e)}
                                                        />
                                                        
                                                        {/*<div className="input_radio d-sm-flex w-100 align-items-center">
                                                            <FormLabel component="legend">Address Type </FormLabel>
                                                            <RadioGroup
                                                                aria-label="Address Type"
                                                                name="bAddressType"
                                                                value={this.state.bAddressType}
                                                                onChange = {e=>this.updateState(e)}
                                                                helperText = {this.state.bAddressTypeError}
                                                            >
                                                                <FormControlLabel
                                                                  value="home"
                                                                  control={
                                                                    <Radio
                                                                      classes={{root: classes.root, checked: classes.checked}}
                                                                    />
                                                                  }
                                                                  label="Home"
                                                                  labelPlacement="end"
                                                                />
                                                                <FormControlLabel
                                                                  value="office"
                                                                  control={
                                                                    <Radio
                                                                      classes={{root: classes.root, checked: classes.checked}}
                                                                    />
                                                                  }
                                                                  label="Office"
                                                                  labelPlacement="end"
                                                                />
                                                                <FormControlLabel
                                                                  value="other"
                                                                  control={
                                                                    <Radio
                                                                      classes={{root: classes.root, checked: classes.checked}}
                                                                    />
                                                                  }
                                                                  label="Other"
                                                                  labelPlacement="end"
                                                                />

                                                            </RadioGroup>
                                                        </div>*/}
                                                        
                                                        </MuiThemeProvider>
                                                        </div>}
                                                        {/*Billing address end*/}


                                                     <Button
                                                            variant="contained"
                                                            color="primary"
                                                            className="continue"
                                                            onClick = {this.addProfile.bind(this)}
                                                        >
                                                            {local.logContinue[this.state.language]}
                                                        </Button>   
                                                    </form>
                                                </div>
                                                }
                                               </div>
                                            </div>
                                        </div>
                                        <div className="card">
                                            <div className="card-header" id="billing_heading">
                                                <h5 className="m-0">
                                                    <button className="btn btn-link text-left sub_heading" data-toggle="collapse" data-target="#billing_content" aria-expanded="true" aria-controls="billing_content">
                                                        {local.checkBillingAddress[this.state.language]}
                                                    </button>
                                                </h5>
                                            </div>
                                            <div id="billing_content" className="collapse" aria-labelledby="billing_heading" data-parent="#checkout_accordion">
                                                <div className="card-body">
                                               

                                                    <form className="row choose_address billing_custom">
                                                        <div className="content">
                                                            <p>{this.state.billingDataName} {this.state.billingDataLastName}</p>
                                                            <p>{this.state.billingDataAddress}</p>
                                                            <p>{this.state.billingDataPhone}</p>
                                                        </div>
                                                        <Button
                                                            variant="contained"
                                                            color="primary"
                                                            className="continue"
                                                            onClick = {this.continueBillingList.bind(this)}
                                                        >
                                                            {local.logContinue[this.state.language]}
                                                        </Button>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="card">
                                            <div className="card-header" id="payment_heading">
                                                <h5 className="m-0">
                                                    <button className="btn btn-link text-left sub_heading" data-toggle="collapse" data-target="#payment_content" aria-expanded="true" aria-controls="payment_content">
                                                        {local.checkPayment[this.state.language]}
                                                    </button>
                                                </h5>
                                            </div>
                                            <div id="payment_content" className="collapse" aria-labelledby="payment_heading" data-parent="#checkout_accordion">
                                                <div className="card-body">
                                                    <form className="row choose_payment choose_address">
                                                        <div className="input_radio w-100">
                                                          <div id="payment_option" className="payment-option">
                                                            
                                                            { this.state.paymentoption && Object.keys(this.state.paymentoption).length && Object.keys(this.state.paymentoption).map((value,index)=>{
                                                                if(!this.state.paymentoption[value][0].payment_type_id){
                                                                    return
                                                                }
                                                                return <div className="card">
                                                                  <div className="card-header" id={"credit_card"+index}>
                                                                    <h5 className="mb-0">
                                                                      <span className="btn btn-link"  aria-expanded="true" aria-controls={"credit_detail"+index}>
                                                                         <RadioGroup
                                                                              aria-label="PaymentOption"
                                                                              name="paymentOptionValue"
                                                                              value={this.state.paymentOptionValue}
                                                                              onChange={e=>this.updateStatePayment(e,index)}
                                                                              helperText = {this.state.paymentOptionError} 
                                                                          >
                                                                        <FormControlLabel
                                                                            value={this.state.paymentoption[value][0].payment_type_id}
                                                                           control={
                                                                                <Radio
                                                                                  classes={{root: classes.root, checked: classes.checked}} 
                                                                                  />
                                                                              }
                                                                            label={<div className="content"><span>{value}</span>{<p className="codmsg">{this.state.paymentoption[value][0].payment_type_id==4? local.CODExtraChargeMessage[this.state.language]:""}</p>}</div>}
                                                                            labelPlacement="end"
                                                                            
                                                                            disabled = { (this.state.paymentoption[value][0].payment_type_id==4 ?this.state.checkIs_code:false) }
                                                                        />
                                                                        </RadioGroup>
                                                                      </span>
                                                                    </h5>
                                                                  </div>
                                                                  <div id= {"credit_detail"+index} className="collapse card_details card_dumy" aria-labelledby={"credit_card"+index} data-parent="#payment_option">
                                                                    <div className="card-body">
                                                                    {this.state.paymentoption[value].length>1 && this.state.paymentoption[value].map((val,subindex)=>{
                                                                                return <RadioGroup
                                                                                aria-label="Payment Type"
                                                                                name={"subPaymenttype"}
                                                                                value={this.state["subPaymenttype"]}
                                                                                onChange={e=>this.updateState(e)}
                                                                                className= "d-inline-block"
                                                                            >
                                                                                    <FormControlLabel
                                                                                        value={val.payment_option_id}
                                                                                        control={<Radio
                                                                                              classes={{root: classes.root, checked: classes.checked}}
                                                                                              />}

                                                                                        label={<img src={val.image_url} />}
                                                                                        labelPlacement="end"
                                                                                        
                                                                                    />
                                                                                    
                                                                                
                                                                            
                                                                            </RadioGroup>
                                                                            })}
                                                                    </div>
                                                                  </div>
                                                                </div>
                                                            })}

                                                          </div>
                                                          </div>
                                                        <div className="card_details col-sm-6">
                                                            <Button
                                                                variant="contained"
                                                                color="primary"
                                                                className={"continue"+" "+this.state.placeOrderButtonDisable}
                                                                onClick = {this.placeOrder.bind(this)}
                                                            >
                                                                {this.state.paymentButtonText} 
                                                            </Button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div role="tabpanel" className="tab-pane fade review_outer" id="pick_here">
                                        <div id="store_accordion" className="checkout_acc">
                                        <div className="card">
                                            <div className="card-header" id="store_heading">
                                                <h5 className="m-0">
                                                    <button className="btn btn-link text-left sub_heading" data-toggle="collapse" data-target="#store_content" aria-expanded="true" aria-controls="store_content">
                                                        {local.checkChooseStore[this.state.language]}
                                                    </button>
                                                </h5>
                                            </div>
                                            <div id="store_content" className="collapse show" aria-labelledby="store_heading" data-parent="#store_accordion">
                                                <div className="card-body"> 
                                                 {this.state.storeDataMap.length?<PinMap storeValue = {this.state.storeDataMap} ref={instance => { this.child = instance;}} />:"" }  
                                                    <ul className="store_list">
                                                    {this.state.storeData && typeof(this.state.storeData) == "object" && this.state.storeData.map((value,index)=>{
                                                       return <li>
                                                        <span><svg className="icon icon-location-pointer"><use xlinkHref="/img/icon-sprite.svg#icon-location-pointer"></use></svg></span>
                                                        <div className="d-inline-block align-top">
                                                            <h6 className="brand_name">{value.company_name}</h6>
                                                            <p className="address">{value.address_line1 +" "+ value.address_line2}</p>
                                                            <div className="contact">
                                                                <svg className="icon icon-phone-receiver"><use xlinkHref="/img/icon-sprite.svg#icon-phone-receiver"></use></svg>
                                                                <span>{value.contact_phone}</span> | <span>{local.checkOpensAt[this.state.language]}{value.open_time}</span>
                                                            </div>
                                                            <div>
                                                                {!this.state.chooseStore && <a onClick = {this.selectStore.bind(this,1,index)} href="Javascript:void(0);">{local.checkChooseStore[this.state.language]}</a>}
                                                                 <a onClick={() => { this.child.setDirection(index); }} href="Javascript:void(0);">{local.checkDirection[this.state.language]}</a>
                                                                 {this.state.chooseStore && <a className="d-block mt-3" onClick ={this.selectStore.bind(this,0,index)} href="Javascript:void(0);">{local.checkAnotherStore[this.state.language]}</a>}
                                                            </div>
                                                        </div>
                                                    </li>

                                                    })}
                                                        
                                                         
                                                        
                                                        
                                                    </ul>
                                               </div>
                                            </div>
                                        </div>
                                        <div className="card">
                                            <div className="card-header" id="paymentstore_heading">
                                                <h5 className="m-0">
                                                    <button className="btn btn-link text-left sub_heading" data-toggle="collapse" data-target="#paymentstore_content" aria-expanded="true" aria-controls="paymentstore_content">
                                                        {local.checkPayment[this.state.language]}
                                                    </button>
                                                </h5>
                                            </div>
                                            <div id="paymentstore_content" className="collapse" aria-labelledby="paymentstore_heading" data-parent="#store_accordion">
                                                <div className="card-body">
                                                    <form className="row choose_payment choose_address">
                                                        <div className="input_radio w-100">
                                                          <div id="payment_option" className="payment-option">
                                                            
                                                            {this.state.paymentoption && Object.keys(this.state.paymentoption).length && Object.keys(this.state.paymentoption).map((value,index)=>{
                                                                if(!this.state.paymentoption[value][0].payment_type_id){
                                                                    return
                                                                }
                                                                
                                                                return <div className="card">
                                                                  <div className="card-header" id={"credit_card"+index}>
                                                                    <h5 className="mb-0">
                                                                      <button className="btn btn-link" data-toggle="collapse" data-target={"#credit_detail"+index} aria-expanded="true" aria-controls={"credit_detail"+index}>
                                                                         <RadioGroup
                                                                              aria-label="PaymentOption"
                                                                              name="paymentOptionValue"
                                                                              value={this.state.paymentOptionValue}
                                                                              onChange={e=>this.updateState(e)}
                                                                              helperText = {this.state.paymentOptionError}
                                                                          >
                                                                        <FormControlLabel
                                                                            value={this.state.paymentoption[value][0].payment_type_id}
                                                                           control={
                                                                                <Radio
                                                                                  classes={{root: classes.root, checked: classes.checked}}
                                                                                  />
                                                                              }
                                                                            
                                                                            label={<div className="content"><span>{value}</span></div>}
                                                                            labelPlacement="end"
                                                                        />
                                                                        </RadioGroup> 
                                                                      </button>
                                                                    </h5>
                                                                  </div>
                                                                  <div id= {"credit_detail"+index} className="collapse card_details" aria-labelledby={"credit_card"+index} data-parent="#payment_option">
                                                                    <div className="card-body">
                                                                    <RadioGroup
                                                                               aria-label="Payment Type"
                                                                               name="payment_type"
                                                                               value={this.state.value}
                                                                               onChange={this.handleChange}
                                                                               className= "d-flex flex-row align-items-center"
                                                                           >
                                                                    {this.state.paymentoption[value].length>1 && this.state.paymentoption[value].map((val,index)=>{
                                                                           return <form>
                                                                          
                                                                               <FormControlLabel
                                                                                 value={val.name}
                                                                                 control={
                                                                                      <Radio
                                                                                        classes={{root: classes.root, checked: classes.checked}}
                                                                                        />
                                                                                 }
                                                                                 label={<img src={val.image_url} />}
                                                                                 labelPlacement="end"
                                                                               />
                                                                            
                                                                          
                                                                       </form> 
                                                                    })}
                                                                       </RadioGroup>
                                                                    </div>
                                                                  </div>
                                                                </div>
                                                            })}

                                                          </div>
                                                          </div>
                                                        <div className="card_details col-sm-6">
                                                            <Button
                                                                variant="contained"
                                                                color="primary"
                                                                className="continue"
                                                                onClick = {this.placeOrder.bind(this)}
                                                            >
                                                                {local.checkSecurePayment[this.state.language]}
                                                            </Button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="col-lg-3 col-md-4 right_outer">
                        <div className="right_sec">
                            <h3 className="sub_heading">{local.checkReviewOrder[this.state.language]}</h3>
                            <ul>
                            {this.state.cartData && Object.keys(this.state.cartData.response).length && this.state.cartData.response.products.map((value,index)=>{
                             return   <li className="details">
                                    <img src={value.icon_images.length>0 ?value.icon_images[0].icon_image_path:""} />
                                    <div className="inner">
                                        <p className="name"><a href="JavaScript:void(0);">{value.name}</a></p>
                                        {value.variant && Object.keys(value.variant[0]).length && Object.keys(value.variant[0].details.selected_options).map((data,index)=>{
                                                    return <p>{data} : {value.variant[0].details.selected_options[data]}</p>
                                        })}
                                        <p>{local.cartQuantity[this.state.language]}: {value.quantity}</p>
                                        <p>{local.checkPrice[this.state.language]}: {value.price[0]['price']}</p>
                                    </div>
                                </li>
                            })}
                            </ul>
                            <div className="row sub_totl">
                                <p className="col-6 col-md-8">{local.cartSubTotal[this.state.language]}: </p>
                                <p className="col-6 col-md-4 text-right pl-0">{this.state.cartData?this.state.cartData.response.subtotal:""}</p>
                            </div>
                            {this.state.cartData && this.state.cartData.response.total_discount>0 && 
                                    <div className="row sub_totl">
                                    <p className="col-6 col-md-8">{local.cartDiscount[this.state.language]}: </p>
                                    <p className="col-6 col-md-4 text-right pl-0">{this.state.cartData.response.total_discount}</p>
                                </div>
                            }
                            {this.state.cartData && this.state.paymentOptionValue == 4 && 
                                    <div className="row sub_totl">
                                    <p className="col-6 col-md-8">{local.cartCodCharge[this.state.language]}: </p>
                                    <p className="col-6 col-md-4 text-right pl-0">{this.state.cartData.response.cod_applicable_fee}</p>
                                </div>
                            }
                            <div className="row grand_tot">
                                <p className="col-6 col-md-8">{local.cartGrandTotal[this.state.language]}:</p>
                                <p className="col-6 col-md-4 text-right pl-0">{this.state.cartData?this.state.paymentOptionValue == 4 ? parseInt(this.state.cartData.response.total)+ parseInt(this.state.cartData.response.cod_applicable_fee):this.state.cartData.response.total:""}</p>
                            </div>
                            <div className="row sub_totl">
                                    <p className="vatmsg col-6 col-md-8">{local.VatMessage[this.state.language]} </p>
                                </div>
                            <p className="accpt">{local.cartWeAccept[this.state.language]}</p>
                            <div className="paymnt_list">
                                <img src="/img/temp/barbour.png" />
                                <img src="/img/temp/bitmap1.png" />
                                <img src="/img/temp/bitmap.png" />
                            </div>
                        </div>
                    </div>
                </div>
                <div>
                    {/* forms for payment gateway redirect*/}
                    <form method="post"  action = {paymentGateway_URL} ref ={this.pG}>
                                            
                        <input type="hidden" name = "platform" value={platform} />
                        <input type="hidden" name = "session_id" value={createSessionId()} />
                        <input type="hidden" name = "user_id" value={this.state.user_Id} />
                        <input type="hidden" name = "ttl" value={this.state.ttl} />
                        <input type="hidden" name = "token" value={this.state.token} />
                        <input type="hidden" name = "pincode" value={this.state.profilePinCode} />
                        <input type="hidden" name = "user_agent" value={this.state.userAgent} />
                        <input type="hidden" name = "omniture_mcid" value={omniture_mcid} />
                        <input type="hidden" name = "key" value={api_KEY} />
                        <input type="hidden" name = "order_id" value={this.state.order_id} />
                        <input type="hidden" name = "payment_option_id" value={this.state.subPaymenttype} />
                        <input type="hidden" name = "emi_id" value="0" />
                        <input type="hidden" name = "guestUser" value="0" />
                        <input type="hidden" name = "returnurl_success" value={returnUrlSuccess+this.state.order_id} />
                        <input type="hidden" name = "returnurl_fail" value={returnUrlFail+this.state.order_id} />
                        <input type="hidden" name = "soaUrl" value="1" />
                        {this.state.subPaymenttype == 61 && <input type="hidden" name = "otp_verified" value="1" />}
                        
                                         
                        
                    </form>
                </div>
                <div className="modal store_outer address_outer" id="check_stores">
                    <div className="modal-dialog">
                        <div className="modal-content">
                            <div className="modal-body">
                                <button type="button" id="store-close" className="close" data-dismiss="modal">&times;</button>
                                <h2>{local.checkMall[this.state.language]}</h2>
                                <p>{local.checkBranch[this.state.language]}</p>
                                <p>+91 7073599988</p>
                                <form>
                                    <Button
                                        variant="contained"
                                        color="primary"
                                        className="continue"
                                    >
                                        {local.checkConfirmAddress[this.state.language]}
                                    </Button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="modal store_outer address_outer " id="cod_confirmation">
                    <div className="modal-dialog">
                        <div className="modal-content">
                            <div className="modal-body">
                                <button type="button" id="store-close" className="close" onClick={this.toggleOTPPopUp.bind(this,'hide',true)}>&times;</button>
                                <h2 className="text-center">{local.checkConfirmOrder[this.state.language]}</h2>
                                <p className="mt-5">{local.checkEnterTheOtp[this.state.language]} {this.state.otpMobileNumber} {local.checkToVerifyNumber[this.state.language]}</p>
                                <form autoComplete="off">
                                    <MuiThemeProvider theme={mui}>
                                        <TextField
                                          id="otp"
                                          name = "otpValue"
                                          className="input_outer"
                                          label={local.checkEnterOtp[this.state.language]}
                                          value = {this.state.otpValue}
                                          onChange = {e=>this.updateState(e)}
                                          
                                          helperText = {this.state.otpValueError}
                                          autoFocus
                                          fullWidth
                                        />
                                    </MuiThemeProvider>
                                    <div>
                                        <Button
                                            variant="contained"
                                            color="primary"
                                            className="continue"
                                            onClick= {this.otpVerify.bind(this)}
                                        >
                                            {local.logContinue[this.state.language]}
                                        </Button>
                                        {this.state.resendOTPshow && (
                                            <span className="time_span float-right" >  <a href="JavaScript:void(0);" onClick = {this.resendOTP.bind(this)}>{local.logResendOTP[this.state.language]}</a></span>
                                        )}
                                        {!this.state.resendOTPshow && (
                                            <span className="time_span float-right">{this.state.otpTimer} {local.logSecondsLeft[this.state.language]}</span>
                                        )}
                                        
                                    </div>
                                    
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="modal store_outer address_outer " id="pickStoreComing">
                    <div className="modal-dialog">
                        <div className="modal-content ">
                            <div className="modal-body text-center">
                                <button type="button" id="pickstore-close" className="close" onClick={this.togglePickStoreComing.bind(this,'hide')}>&times;</button>
                                <h2 className="text-center">{local.orderComingSoon[this.state.language]}</h2>
                                <p>{local.checkFeatureWillBeLive[this.state.language]}</p>
                                <div>
                                        <Button
                                            variant="contained"
                                            color="primary"
                                            className="continue"
                                            onClick= {this.togglePickStoreComing.bind(this,'hide')}
                                        >
                                            {local.checkBackToCheckout[this.state.language]}
                                        </Button>
                                </div>        
                            </div>
                        </div>
                    </div>
                </div>
                {this.state.showLoader && <Loader />}
            </div>


        );
    }
}
function mapStateToProps(state) {
    return {
        LoginReducer: state.LoginReducer,
        cartReducer : state.cartReducer,
        PaymentOptionReducer : state.PaymentOptionReducer,
        UserProfileReducer : state.UserProfileReducer,
        placeOrderReducer : state.placeOrderReducer,
        storeReducer : state.storeReducer,
        codOTPReducer : state.codOTPReducer,
        RemoveProfileReducer : state.RemoveProfileReducer,
        OTPReducer : state.OTPReducer,
        languageReducer: state.languageReducer,
        cityData: state.cityReducer
        

    }
}

export default connect(mapStateToProps)(withStyles(styles)(Checkout));

