import React, { Component } from 'react';
import {connect} from 'react-redux';

// import "react-responsive-carousel/lib/styles/carousel.min.css";
import { Carousel } from 'react-responsive-carousel';
import {
  MuiThemeProvider,
  createMuiTheme,
  createGenerateClassName,
} from '@material-ui/core/styles';
import { withStyles } from '@material-ui/core/styles';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';

import { productdetailAction, deliveryDetailAction, addProductReviewAction, getProductImageAction, getNearStoreLocation, getSiblingIdAction, getEddAction } from '../../actions/ProductDetail';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import {ToastContainer, ToastStore} from 'react-toasts';
import {notify} from 'react-notify-toast';

import { StoreAction } from '../../actions/header';
import { showLogin, InitailStateAction } from '../../actions/login';

import Loader from "../../components/loader";
import Subloader from "../../components/subloader"; 

import { addtocartAction, addToWishlistAction } from '../../actions/cart';
import { getter, getUserInfo, checkRequired, checkCharacterCount, localClear, languageForAPI, calculateDiscount ,redirectToMicrosite } from '../../commonfunction';

import local, {deafultLanguage} from '../../localization';
import ReactGA from 'react-ga'
import {productidbyseonameAction} from "../../actions/productList";
const mui = createMuiTheme({
  overrides: {
    MuiFormLabel: {
      root: {
        color: "#b1b1b1"
      },
      focused: {
        color: "black !important"
      },
    }
  }
});

const styles = theme => ({
 
});

const Section1 = (props) => ( 
    <div className="section_outer row"> 
        <div className="col-md-6 slider_outer">
            <div className="product_carosel"> 
                {
                    ((props.data.product_info.product_gallery && props.data.product_info.product_gallery.length) || (props.data.product_info.video_gallery && props.data.product_info.video_gallery.length)) ? (    
                        <Carousel  showThumbs={false} showStatus={false} showIndicators={true} showArrows={false} selectedItem={props.selectedSliderIndex} onChange={(index) => props.onSlideChange(index)} style={{direction: "ltr"}}>
                            {
                                props.data.product_info.product_gallery && props.data.product_info.product_gallery.map((gallery, gallaryInd) => (
                                    
                                    <div id={"carouseldata-cont" + gallaryInd} className="carouseldata-cont" onMouseEnter={() => props.showZoom(gallery["detailed_image"])} onMouseLeave={() => props.hideZoom()} onMouseMove={(e) => props.mouseMove(e)}>
                                        <img src={gallery["640X1"]} />
                                    </div>
                                ))
                            }
                            {props.data.product_info.video_gallery && props.data.product_info.video_gallery.map((video, videoIndex)=>(
                                <div id={"carouseldata-cont" + 3 + videoIndex} className="carouseldata-cont video-slide">
                                    <img src="/img/video_view.svg" />
                                    <video width="100%" controls>
                                        <source src={video.video_url} type="video/mp4" />
                                    </video>
                                </div>
                            ))}
                            
                        </Carousel>
                    ) : ""
                }
                
                <div className="thumb_outer">
                    <button type="button" class="control-arrow control-prev"><svg className="icon icon-down-arrow"><use xlinkHref="/img/icon-sprite.svg#icon-down-arrow"></use></svg></button>
                    <div className="carosel_thumbnail"> 
                    {

                        props.data.product_info && props.data.product_info.product_gallery && props.data.product_info.product_gallery.map((gallery, gallaryInd) => (
                            <img className={props.selectedSliderIndex == gallaryInd ? "selected" : null} src ={gallery["100X128"]}  onClick={()=>props.slideChangeByClick(gallaryInd)} />
                        ))
                    }
                    {

                        props.data.product_info && props.data.product_info.video_gallery && props.data.product_info.video_gallery.map((video, videoIndex) => (
                            <img className={props.selectedSliderIndex == videoIndex + props.data.product_info.product_gallery.length ? "selected" : null} src ="/img/video_view.svg"  onClick={()=>props.slideChangeByClick(videoIndex + props.data.product_info.product_gallery.length)} />
                        ))
                    }
                    </div> 
                    <button type="button" class="control-arrow control-next"><svg className="icon icon-down-arrow"><use xlinkHref="/img/icon-sprite.svg#icon-down-arrow"></use></svg></button>
                </div>

                <div id="zoom_wrapper" className="zoom_wrapper position-absolute" style={{direction: "ltr"}}><img id="zoom_image" src={props.zoomImage} /></div>
            </div>
        </div>
        <div className="col-md-6">
            <ul className="breadcrumbs">
                {
                    props.data.breadcrumb_links.map(breadcrumb => (
                        <li><a href="Javascript:void(0)" onClick={() => props.goToCategory(breadcrumb.cat_id)}>{breadcrumb.seo_name}</a></li>
                    ))
                }
            </ul>
            <h1 className="text-left">{props.data.product_info.product}</h1>
            <div className="review_rating">
                <svg className={props.data.merchant_info.rating >= 1 ? "icon icon-star active" : "icon icon-star"}><use xlinkHref="/img/icon-sprite.svg#icon-star"></use></svg>
                <svg className={props.data.merchant_info.rating >= 2 ? "icon icon-star active" : "icon icon-star"}><use xlinkHref="/img/icon-sprite.svg#icon-star"></use></svg>
                <svg className={props.data.merchant_info.rating >= 3 ? "icon icon-star active" : "icon icon-star"}><use xlinkHref="/img/icon-sprite.svg#icon-star"></use></svg>
                <svg className={props.data.merchant_info.rating >= 4 ? "icon icon-star active" : "icon icon-star"}><use xlinkHref="/img/icon-sprite.svg#icon-star"></use></svg>
                <svg className={props.data.merchant_info.rating >= 5 ? "icon icon-star active" : "icon icon-star"}><use xlinkHref="/img/icon-sprite.svg#icon-star"></use></svg>
                <a href="Javascript:void(0)" className="review_write" onClick={() => props.openReviewPopup()}>{local.productWriteReview[props.language]}</a>
                <a href="Javascript:void(0)" id="open-review-popup-link" style={{display: "none"}} className="review_write" data-toggle="modal" data-target="#write_review_popup"></a>
            </div>
            <p className="brandname">{props.data.product_info.brand}</p>
	         <div className="price_block d-flex ">
                 <p className="price">{props.data.product_info.price} {local.homeSAR[props.language]}</p>
                 {parseInt(props.data.product_info.list_price) > parseInt(props.data.product_info.price)&&<p className="strike">{props.data.product_info.list_price} {local.homeSAR[props.language]}</p>}
                       
                        {parseInt(props.data.product_info.list_price) > parseInt(props.data.product_info.price) && <p className="offer">{calculateDiscount(props.data.product_info.price,props.data.product_info.list_price)}% {local.off[props.language]}</p>}
             </div>
             <div className="sold_by">
                <span>{local.soldByText[props.language]}</span>
                <img id="company_logo" src={props.data.merchant_info.vendor_info.company_logo}  onClick={()=>redirectToMicrosite(props.data.product_info.brand_info.length===0?props.data.merchant_info.vendor_info.seo_name.toLowerCase():props.data.product_info.brand_info[0].seo_name)} style={{cursor:"pointer"}} />
            </div>
            <div className="color_outer">
                {
                    props.data.product_info.tracking == "O" && props.data.product_info.sibling_features_info && props.data.product_info.sibling_features_info.map((siblingInfo, index) => (
                        siblingInfo.type == 'F' && (
                            siblingInfo.variants.map((varient) => (
                                //for active use class active
                                <span className={varient.is_selected == 1 ? "color active" : (varient.disable == 1 ? "color disable" : "color")} onClick={() => varient.disable != 1 && varient.is_selected != 1 && props.setColor(siblingInfo.feature_id, varient.id)}><span style={{backgroundColor: varient.color_family}}></span></span>
                            ))
                        )
                    ))
                }
            </div>
            <div className="sizes">
                {
                    props.data.product_info.tracking == "O" && props.data.product_info.sibling_features_info && props.data.product_info.sibling_features_info.map((siblingInfo, index) => (
                        siblingInfo.type != 'F' && (
                            siblingInfo.variants.map((varient) => (
                                    //use active and disable classes
                                    <a className={ Object.keys(props.selectedSizeOption).length && props.selectedSizeOption[Object.keys(props.selectedSizeOption)[0]] == varient.id ? "active" : (varient.disable == 1 ? "disable" : "")} href="Javascript:void(0)" onClick={() => varient.disable != 1 && props.setSize(siblingInfo.option_id, varient.id)}>{varient.label}</a>
                                ))
                        )
                    ))
                }
            </div>

            { props.data!="" && props.data.product_info.size_chart_image_path!="" &&
                <a className="size_guide" href="#" data-toggle="modal" data-target="#show_size_guide">{local.productSizeGuide[props.language]}</a>
            }
            {/* <a className="size_guide common_tooltip_outer" href="Javascript:void(0)">{local.productSizeGuide[props.language]}  <span className="common_tooltip down">Coming soon</span></a> */}
            <div className="cart_sec"> 
                <div className="control">
                    <button className="cart-qty-minus btn" type="button" value="-" onClick={() => props.productCount > 1 && props.changeProductCount(props.productCount - 1)}>-</button>
                    <input type="number" name="qty" id="qty" value={props.productCount} onChange={e => props.changeProductCount(e.target.value)} title="Qty" className="input-text qty" />
                    <button className="cart-qty-plus btn" type="button" value="+" onClick={() => props.changeProductCount(Number(props.productCount) + 1)}>+</button>
                </div>
                <button className=" add_cart btn" onClick={/*() => ToastStore.success("Product added to cart", 3000, "custom_toast")*/ () => props.addToCart()}>{local.wishliAddToCart[props.language]}</button>
                
                {/*<button className=" add_cart btn" onClick={() => ToastStore.success("Product added to cart", 3000, "custom_toast")}>Click me !</button>
                <ToastContainer position={ToastContainer.POSITION.BOTTOM_CENTER} store={ToastStore}/>*/}
            </div>
            <div className="share_wishlist_sec">
                <button className="btn" onClick={() => props.addToWishlist()}>{local.commonAddWishList[props.language]}</button>
                {/* <button className="btn common_tooltip_outer">{local.productShare[props.language]} <span className="common_tooltip down">Coming soon</span></button> */}
            </div>
            <div className="pincode_sec">
               {/* <p className="heading">Check Availability</p>*/}
                {!props.availabilityTime && <form className="d-flex" style={{marginBottom: "10px"}}>
                    <FormControl className="formControl slect_outer">
                      <InputLabel shrink htmlFor="city-placeholder">
                        {local.commonSelectCity[props.language]}
                      </InputLabel>
                      <Select
                        value={props.selectedCity}
                        input={<Input name="select_city" id="city-placeholder" />}
                        displayEmpty
                        name="select_city"
                        className="selectEmpty" 
                        onChange={(e) => props.selectCity(e.target.value)}
                      >
                                
                        {/*<MenuItem value="0">
                            <input type="search" name="search" className="city_srch" placeholder="Search" />
                        </MenuItem>*/}
                        {
                            props.cityData.response && props.cityData.response.map(city => (
                                <MenuItem value={city.id}>{city.city}</MenuItem>
                            ))
                            
                        }
                        
                      </Select>
                    </FormControl>
                    <Button
                        variant="contained"
                        color="primary"
                        type="submit"
                        fullWidth 
                        type="button"
                        className="chk_avlbty" 
                        onClick={() => props.setAvailability()} 
                    >
                        {local.productAvailability[props.language]}
                    </Button>
                </form>}
                {props.availabilityTime && <p className="delivry_contnt mt-4">{local.productDeliversTo[props.language]}
                    <form className="d-inline-block slctd_city">
                        <FormControl className="formControl slect_outer">
                          <Select
                            value={props.selectedCity}
                            input={<Input name="select_city" id="city-placeholder" />}
                            displayEmpty
                            style={{cursor:"pointer"}}
                            name="select_city"
                            className="selectEmpty" 
                            onChange={(e) => props.selectCity(e.target.value,true)}
                          >
                                    
                            {/*<MenuItem value="0"><input type="search" name="search" className="city_srch" placeholder="Search" /></MenuItem>*/}
                            {
                                props.cityData.response && props.cityData.response.map(city => (
                                    <MenuItem value={city.id}>{city.city}</MenuItem>
                                ))
                            }
                          </Select>
                        </FormControl>
                    </form>

                {/*local.productSameDayDelivery[props.language]*/} {props.availabilityTime}</p>}
                <p className="available mt-4">
                    {
                        !props.nearStoreList.isFetching && props.data.product_info.tracking == "O" && (!Object.keys(props.selectedSizeOption).length || !props.nearStoreList.data.response.data.length) &&  local.selectAvailablePickup[props.language]
                    }

                    {
                        !props.nearStoreList.isFetching && props.nearStoreList.data && (props.data.product_info.tracking == "B" || Object.keys(props.selectedSizeOption).length != 0 ) && props.nearStoreList.data.response.data.length !== 0 &&  (<div>{local.productAvailablePickup[props.language]}  <a href="Javascript:void(0)" onClick={() => props.openPickupStorePopup(props.nearStoreList.data.response.data)}> {typeof(props.nearStoreList.data.response.data) == "object" ? props.nearStoreList.data.response.data.length : 0} {local.productStores[props.language]}</a> {" " + local.productNearYou[props.language]}</div>)
                    }
                    {props.nearStoreList.isFetching && <Subloader align="left" />}
                    <a href="Javascript:void(0)" id="pickup_store_link" style={{display: "none"}} data-toggle="modal" data-target="#pickup_store_popup"></a>
                </p>
                {props.deliveryDetail.fdate && props.deliveryDetail.sdate && <p className="note">{local.productDelivery[props.language]} {props.deliveryDetail.fdate + " - " + props.deliveryDetail.sdate}</p>}
            </div>
            {/*<p className="shipping_detail">Free Shipping & Returns</p>
            <p className="shipping_detail">Orders Over $100</p>*/}
        </div>
    </div>
);

const Section2 = (props) => (
     
    <div className="details_section mt-5">
        <ul id="detail_tab" className="nav nav-tabs d-none d-md-flex" role="tablist">
          <li className="nav-item">
            <a className="nav-link active" href="#product_detail" role="tab" data-toggle="tab">{local.productDetails[props.language]}</a>
          </li>
          <li className="nav-item">
            <a className="nav-link" href="#specification" role="tab" data-toggle="tab">{local.productSpecifications[props.language]}</a>
          </li>
          {<li className="nav-item">
            <a className="nav-link" href="#more_detail" role="tab" data-toggle="tab">{local.productMoreDetails[props.language]}</a>
          </li>}
           <li className="nav-item">
            <a className="nav-link" href="#review" role="tab" data-toggle="tab">{local.productReview[props.language]}</a>
          </li>
        </ul>
        <div className="tab-content common_descp d-none d-md-block">
            <div role="tabpanel" className="tab-pane fade in active show" id="product_detail" dangerouslySetInnerHTML={{__html: props.data.product_info.full_description}}>
            </div>
            <div role="tabpanel" className="tab-pane fade" id="specification">
                <SpecificationSection data={props.data.specification} type="D"/>
            </div>
            <div role="tabpanel" className="tab-pane fade" id="more_detail">
                <table className="w-100">
                    {
                        props.data.product_info.legal_info.length ? (
                            props.data.product_info.legal_info.map(info => (
                                    info.value && (<tr className="row">
                                        <td className="col-lg-4 col-md-5">{info.label}</td>
                                        <td className="col-lg-8 col-md-7"> {info.value} {local.homeSAR[props.language]}</td>
                                    </tr>)
                                ))
                           ) : (
                                   <div>{local.productNoDetail[props.language]}</div>
                               )
                    }
                </table>
            </div>
            <div role="tabpanel" className="tab-pane fade review_outer" id="review">
                <div className="overall_review_rating">
                    <svg className={props.data.reviews.average_rating >= 1? "icon icon-star active" : "icon icon-star" }><use xlinkHref="/img/icon-sprite.svg#icon-star"></use></svg>
                    <svg className={props.data.reviews.average_rating >= 2? "icon icon-star active" : "icon icon-star" }><use xlinkHref="/img/icon-sprite.svg#icon-star"></use></svg>
                    <svg className={props.data.reviews.average_rating >= 3? "icon icon-star active" : "icon icon-star" }><use xlinkHref="/img/icon-sprite.svg#icon-star"></use></svg>
                    <svg className={props.data.reviews.average_rating >= 4? "icon icon-star active" : "icon icon-star" }><use xlinkHref="/img/icon-sprite.svg#icon-star"></use></svg>
                    <svg className={props.data.reviews.average_rating >= 5? "icon icon-star active" : "icon icon-star" }><use xlinkHref="/img/icon-sprite.svg#icon-star"></use></svg>
                    <a href="#" className="review_write" data-toggle="modal" data-target="#write_review_popup">{local.productWriteReview[props.language]}</a>
                </div>
                <p className="overall_review_count">{props.data.reviews.total_reviews} {local.productReview[props.language]}</p>
                <ul>
                    {
                        props.data.reviews.all_reviews ? (
                                props.data.reviews.all_reviews.slice(0, props.shownReviewCount).map(review => (
                                    <li>
                                        <div className="review_rating">
                                            <svg className={review.rating_value >= 1 ? "icon icon-star active" : "icon icon-star"}><use xlinkHref="/img/icon-sprite.svg#icon-star"></use></svg>
                                            <svg className={review.rating_value >= 2 ? "icon icon-star active" : "icon icon-star"}><use xlinkHref="/img/icon-sprite.svg#icon-star"></use></svg>
                                            <svg className={review.rating_value >= 3 ? "icon icon-star active" : "icon icon-star"}><use xlinkHref="/img/icon-sprite.svg#icon-star"></use></svg>
                                            <svg className={review.rating_value >= 4 ? "icon icon-star active" : "icon icon-star"}><use xlinkHref="/img/icon-sprite.svg#icon-star"></use></svg>
                                            <svg className={review.rating_value >= 5 ? "icon icon-star active" : "icon icon-star"}><use xlinkHref="/img/icon-sprite.svg#icon-star"></use></svg>
                                            <span className="review_count">{review.rating_value}</span>
                                            <span style={{fontWeight: "bold"}}> {review.title}</span>
                                        </div>
                                        {/*<p className="sub_desc">Lovely</p>*/}
                                        <p className="desc">{review.message}</p>
                                        <p className="name">{review.name}</p>
                                    </li>
                                ))                                
                            ) : (<div>{props.data.reviews.all_review}</div>)
                    }
                </ul>
                { props.shownReviewCount < props.data.reviews.total_reviews && <a href="Javascript:void(0)" onClick={() => props.showMoreReviews()} className="show_more">{local.productShowMore[props.language]}</a>}
            </div>
        </div>
        <div id="detail_accordion" className="common_descp detail_list d-sm-block d-md-none">
            <div className="card">
                <div className="card-header" id="product_heading" onClick={() => props.manageAccordian(1)}>
                    <h5 className="mb-0">
                        <button className="btn btn-link text-left" data-toggle="collapse" data-target="#product_collapse" aria-expanded="true" aria-controls="product_collapse">
                           {local.productDetails[props.language]} 
                           
                        </button>
                    </h5>
                </div>
                <div id="product_collapse" className="collapse show" aria-labelledby="product_heading" data-parent="#detail_accordion">
                    <div className="card-body" dangerouslySetInnerHTML={{__html: props.data.product_info.full_description}}>
                        
                    </div>
                </div>
            </div>
            <div className="card">
                <div className="card-header" id="specification_heading" onClick={() => props.manageAccordian(2)}>
                    <h5 className="mb-0">
                        <button className="btn btn-link text-left collapsed" data-toggle="collapse" data-target="#specification_collapse" aria-expanded="true" aria-controls="specification_collapse">
                           {local.productSpecifications[props.language]} 
                        </button>
                    </h5>
                </div>
                <div id="specification_collapse" className="collapse" aria-labelledby="specification_heading" data-parent="#detail_accordion">
                    <div className="card-body" >
                        <SpecificationSection data={props.data.specification} language={props.language} type="M"/>
                    </div>
                </div>
            </div>
            <div className="card">
                <div className="card-header" id="moredetail_heading" onClick={() => props.manageAccordian(3)}>
                    <h5 className="mb-0">
                        <button className="btn btn-link text-left collapsed" data-toggle="collapse" data-target="#moredetail_collapse" aria-expanded="true" aria-controls="moredetail_collapse">
                           {local.productMoreDetails[props.language]}
                        </button>
                    </h5>
                </div>
                <div id="moredetail_collapse" className="collapse" aria-labelledby="moredetail_heading" data-parent="#detail_accordion">
                    <div className="card-body" >
                        <table className="w-100">
                            {
                                props.data.product_info.legal_info.length ? (
                                    props.data.product_info.legal_info.map(info => (
                                            info.value && (<tr className="row">
                                                <td className="col-lg-4 col-md-5">{info.label}</td>
                                                <td className="col-lg-8 col-md-7"> {info.value} {local.homeSAR[props.language]}</td>
                                            </tr>)
                                        ))
                                   ) : (
                                           <div>{local.productNoDetail[props.language]}</div>
                                       )
                            }
                        </table>
                    </div>
                </div>
            </div>
            <div className="card">
                <div className="card-header" id="review_heading"  onClick={() => props.manageAccordian(4)}>
                    <h5 className="mb-0">
                        <button className="btn btn-link text-left collapsed" data-toggle="collapse" data-target="#review_collapse" aria-expanded="true" aria-controls="review_collapse">
                           {local.productReview[props.language]} 
                        </button>
                    </h5>
                </div>
                <div id="review_collapse" className="collapse" aria-labelledby="review_heading" data-parent="#detail_accordion">
                    <div className="card-body review_outer" >
                        <div className="overall_review_rating">
                            <svg className={props.data.reviews.average_rating >= 1? "icon icon-star active" : "icon icon-star" }><use xlinkHref="/img/icon-sprite.svg#icon-star"></use></svg>
                            <svg className={props.data.reviews.average_rating >= 2? "icon icon-star active" : "icon icon-star" }><use xlinkHref="/img/icon-sprite.svg#icon-star"></use></svg>
                            <svg className={props.data.reviews.average_rating >= 3? "icon icon-star active" : "icon icon-star" }><use xlinkHref="/img/icon-sprite.svg#icon-star"></use></svg>
                            <svg className={props.data.reviews.average_rating >= 4? "icon icon-star active" : "icon icon-star" }><use xlinkHref="/img/icon-sprite.svg#icon-star"></use></svg>
                            <svg className={props.data.reviews.average_rating >= 5? "icon icon-star active" : "icon icon-star" }><use xlinkHref="/img/icon-sprite.svg#icon-star"></use></svg>
                            <a href="#" className="review_write" data-toggle="modal" data-target="#write_review_popup">{local.productWriteReview[props.language]}</a>
                        </div>
                        <p className="overall_review_count">{props.data.reviews.total_reviews} {local.productReview[props.language]}</p>
                        <ul>
                            {
                                typeof(props.data.reviews.all_reviews) == "object" && props.data.reviews.all_reviews.length ? (
                                        props.data.reviews.all_reviews.slice(0, props.shownReviewCount).map(review => (
                                            <li>
                                                <div className="review_rating">
                                                    <svg className={review.rating_value >= 1 ? "icon icon-star active" : "icon icon-star"}><use xlinkHref="/img/icon-sprite.svg#icon-star"></use></svg>
                                                    <svg className={review.rating_value >= 2 ? "icon icon-star active" : "icon icon-star"}><use xlinkHref="/img/icon-sprite.svg#icon-star"></use></svg>
                                                    <svg className={review.rating_value >= 3 ? "icon icon-star active" : "icon icon-star"}><use xlinkHref="/img/icon-sprite.svg#icon-star"></use></svg>
                                                    <svg className={review.rating_value >= 4 ? "icon icon-star active" : "icon icon-star"}><use xlinkHref="/img/icon-sprite.svg#icon-star"></use></svg>
                                                    <svg className={review.rating_value >= 5 ? "icon icon-star active" : "icon icon-star"}><use xlinkHref="/img/icon-sprite.svg#icon-star"></use></svg>
                                                    <span className="review_count">{review.rating_value}</span>
                                                </div>
                                                {/*<p className="sub_desc">Lovely</p>*/}
                                                <p className="desc">{review.message}</p>
                                                <p className="name">{review.name}</p>
                                            </li>
                                        ))                                
                                    ) : (<div>{local.productNoReview[props.language]}</div>)
                            }
                        </ul>
                        { props.shownReviewCount < props.data.reviews.total_reviews && <a href="Javascript:void(0)" onClick={() => props.showMoreReviews()} className="show_more">{local.productShowMore[props.language]}</a>}
                    </div>
                </div>
            </div>
        </div>
    </div>
);

const Section3 = (props) => (
    <div className="section like_sec">
        <div className="data-cont">
            <div className="title text-left">{local.wishliYouMayAlsoLike[props.language]}</div>
            <div className="img-cont">
                <div className="may_like_loop owl-carousel owl-theme">
                    <div className="item">
                        <div className="img_outer">
                            <a href="#"><img src="/img/temp/section8.png" /></a>
                            <div className="size-cont">
                                <div className="sizes">
                                    <a href="#">S</a>
                                    <a href="#">M</a>
                                    <a href="#">L</a>
                                    <a href="#">XL</a>
                                </div>
                                <div className="icons">
                                    <a href="#"><svg className="icon icon-favorite-heart-button"><use xlinkHref="/img/icon-sprite.svg#icon-favorite-heart-button"></use></svg></a>
                                    <a href="#"><svg className="icon icon-Combined-Shape"><use xlinkHref="/img/icon-sprite.svg#icon-Combined-Shape"></use></svg></a>
                                    <a href="#"><svg className="icon icon-overlay-view"><use xlinkHref="/img/icon-sprite.svg#icon-overlay-view"></use></svg></a>
                                </div>
                            </div>
                        </div>
                        <div className="img-info">
                            <div className="prod-name"><a href="#">{local.wishliPaisleyPatchDress[props.language]}</a></div>
                            <div className="price-cont">
                                <span className="brand">{local.wishliAmomento[props.language]}</span>
                                <span className="saperator">|</span>
                                <span className="price">SAR 456</span>
                            </div>
                        </div>
                    </div>
                    <div className="item">
                        <div className="img_outer">
                            <a href="#"><img src="/img/temp/section8.png" /></a>
                            <div className="size-cont">
                                <div className="sizes">
                                    <a href="#">S</a>
                                    <a href="#">M</a>
                                    <a href="#">L</a>
                                    <a href="#">XL</a>
                                </div>
                                <div className="icons">
                                    <a href="#"><svg className="icon icon-favorite-heart-button"><use xlinkHref="/img/icon-sprite.svg#icon-favorite-heart-button"></use></svg></a>
                                    <a href="#"><svg className="icon icon-Combined-Shape"><use xlinkHref="/img/icon-sprite.svg#icon-Combined-Shape"></use></svg></a>
                                    <a href="#"><svg className="icon icon-overlay-view"><use xlinkHref="/img/icon-sprite.svg#icon-overlay-view"></use></svg></a>
                                </div>
                            </div>
                        </div>
                        <div className="img-info">
                            <div className="prod-name"><a href="#">{local.wishliPaisleyPatchDress[props.language]}</a></div>
                            <div className="price-cont">
                                <span className="brand">{local.wishliAmomento[props.language]}</span>
                                <span className="saperator">|</span>
                                <span className="price">$456</span>
                            </div>
                        </div>
                    </div>
                    <div className="item">
                        <div className="img_outer">
                            <a href="#"><img src="/img/temp/section8.png" /></a>
                            <div className="size-cont">
                                 <div className="sizes">
                                    <a href="#">S</a>
                                    <a href="#">M</a>
                                    <a href="#">L</a>
                                    <a href="#">XL</a>
                                </div>
                                <div className="icons">
                                    <a href="#"><svg className="icon icon-favorite-heart-button"><use xlinkHref="/img/icon-sprite.svg#icon-favorite-heart-button"></use></svg></a>
                                    <a href="#"><svg className="icon icon-Combined-Shape"><use xlinkHref="/img/icon-sprite.svg#icon-Combined-Shape"></use></svg></a>
                                    <a href="#"><svg className="icon icon-overlay-view"><use xlinkHref="/img/icon-sprite.svg#icon-overlay-view"></use></svg></a>
                                </div>
                            </div>
                        </div>
                        <div className="img-info">
                            <div className="prod-name"><a href="#">{local.wishliPaisleyPatchDress[props.language]}</a></div>
                            <div className="price-cont">
                                <span className="brand">{local.wishliAmomento[props.language]}</span>
                                <span className="saperator">|</span>
                                <span className="price">$456</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
);

const Section4 = (props) => (
    <div className="modal show_chart" id="show_size_guide">
        <div className="modal-dialog">
          <div className="modal-content">
            <div className="modal-body">
                <button type="button" className="close" data-dismiss="modal">&times;</button>
                <img src={props.productData.product_info.size_chart_image_path} />
            </div>
          </div>
        </div>
    </div>
);

const Section5 = (props) => (
    <div className="modal reviewwrite" id="write_review_popup">
        <div className="modal-dialog">
            <div className="modal-content">
                <div className="modal-body">
                    <button type="button" id="review-close-btn" className="close" data-dismiss="modal">&times;</button>
                    <div className="row m-0">
                        <div className="col-md-8 left_sec">
                            <h2 className="login_title text-center">{local.reviewWriteReview[props.language]}</h2>
                            <div className="rating_sec text-center">
                                <svg className={props.writeReview.rating >= 1 ? "icon icon-shape active" : "icon icon-shape"} onClick={() => props.updateWriteReview('rating', 1)}><use xlinkHref="/img/icon-sprite.svg#icon-shape"></use></svg>
                                <svg className={props.writeReview.rating >= 2 ? "icon icon-shape active" : "icon icon-shape"} onClick={() => props.updateWriteReview('rating', 2)}><use xlinkHref="/img/icon-sprite.svg#icon-shape"></use></svg>
                                <svg className={props.writeReview.rating >= 3 ? "icon icon-shape active" : "icon icon-shape"} onClick={() => props.updateWriteReview('rating', 3)}><use xlinkHref="/img/icon-sprite.svg#icon-shape"></use></svg>
                                <svg className={props.writeReview.rating >= 4 ? "icon icon-shape active" : "icon icon-shape"} onClick={() => props.updateWriteReview('rating', 4)}><use xlinkHref="/img/icon-sprite.svg#icon-shape"></use></svg>
                                <svg className={props.writeReview.rating >= 5 ? "icon icon-shape active" : "icon icon-shape"} onClick={() => props.updateWriteReview('rating', 5)}><use xlinkHref="/img/icon-sprite.svg#icon-shape"></use></svg>
                            </div>
                            <div>
                                <form autoComplete="off">
                                    <MuiThemeProvider theme={mui}>
                                        <TextField
                                          id="review_heading"
                                          className="input_outer"
                                          label={local.wishliReviewHeading[props.language]}
                                          autoFocus
                                          fullWidth 
                                          value={props.writeReview.title} 
                                          name ='title'
                                          helperText ={props.titleError}
                                          onChange={(e) => props.updateWriteReview('title', e.target.value)}
                                        />
                                        <TextField
                                          id="feedback"
                                          className="input_outer textField"
                                          label={local.wishliFeedback[props.language]}
                                          multiline
                                          rowsMax="10"
                                          rows="5"
                                          fullWidth
                                          variant="outlined"
                                          helperText={props.messageError + " " + ((50-props.writeReview.message.length) > 0 ? ((50-props.writeReview.message.length) + " remaining") : "")} 
                                          value={props.writeReview.message} 
                                          name = 'message'
                                          onChange={(e) => props.updateWriteReview('message', e.target.value)}
                                        />
                                    </MuiThemeProvider>
                                    <Button
                                        variant="contained"
                                        color="primary"
                                        type="submit"
                                        fullWidth 
                                        type="button" 
                                        onClick={() => props.submitReview()}
                                    >
                                        {local.orderSubmit[props.language]}
                                    </Button>
                                </form>
                            </div>
                        </div>
                        <div className="col-md-4 right_sec">
                            <h3>{local.reviewHowToWrite[props.language]} </h3>
                            <ul>
                                <li>{local.reviewFocusOnProduct[props.language]}</li>
                                <li>{local.reviewProAndCons[props.language]}</li>
                                <li>{local.reviewSimpleCrisp[props.language]}</li>
                                <li>{local.reviewBestUsage[props.language]}</li>
                                <li>{local.reviewComparison[props.language]}</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
);

const Section6 = (props) =>(
    <div className="modal reviewwrite image-popup" id="image_popup">
        <div className="modal-dialog">
            <div className="modal-content">
                <div className="modal-body">
                    <button type="button" className="close" data-dismiss="modal">&times;</button>
                        <div>
                        {((props.data.product_info.product_gallery && props.data.product_info.product_gallery.length) || (props.data.product_info.video_gallery && props.data.product_info.video_gallery.length)) ?
                            (<Carousel showStatus={false} showIndicators={false} showArrows={true}>
                                {
                                    props.data.product_info.product_gallery.map(gallery => (
                                        <div className="carouseldata-cont">
                                            <img src={gallery.main_image} />
                                        </div>
                                    ))
                                }
                                {props.data.product_info.video_gallery && props.data.product_info.video_gallery.map((video)=>{
                                 return <video width="100%" controls>
                                            <source src={video.video_url} type="video/mp4" />
                                        </video>
                                    
                                })}
                                 
                            </Carousel>) : ""
                        }
                    </div>
                </div>
            </div>
        </div>
    </div>
);

const SpecificationSection = (props) => {
    let specification = {};
    props.data.map(data => {
        if(!specification[data.group_description]){
            specification[data.group_description] = [];
        }
        specification[data.group_description].push(data);
    });
    return (
        <ul className="spec_outer">
            {
            Object.keys(specification).length ? (Object.keys(specification).map((key, index) => (
                <li>
                    <button className="btn btn-link collapsed" type="button" data-toggle="collapse" data-target={"#key_feature" + index + props.type} aria-expanded="false" aria-controls="key_feature">
                        {key}
                    </button>
                    <div className="collapse" id={"key_feature" + index + props.type}>
                      <div className="card card-body">
                        <table>
                            {
                                specification[key].map(item => (
                                    <tr className="row">
                                        <td className="col-lg-4 col-md-5">{item.feature_description}</td>
                                        <td className="col-lg-8 col-md-7">: {item.final_value}</td>
                                    </tr>
                                ))
                            }
                        </table>
                      </div>
                    </div>
                </li>
            ))) : (<div>{local.productNoSpecifications[props.language]}</div>)
            }
        </ul>
    )
};

const StorePopup = (props) => (
    <div className="modal reviewwrite selct_locn" id="pickup_store_popup">
        <div className="modal-dialog">
            <div className="modal-content">
                <div className="modal-body">
                    <ul className="store_list">
                        {
                            props.nearStoreList && props.nearStoreList.data.response && typeof(props.nearStoreList.data.response.data) == "object" && props.nearStoreList.data.response.data.map(store => (
                                <li>
                                    <span><svg className="icon icon-location-pointer"><use xlinkHref="/img/icon-sprite.svg#icon-location-pointer"></use></svg></span>
                                    <div className="d-inline-block align-top">
                                        <h6 className="brand_name">{store.STORE_NAME}</h6>
                                        <p className="address">{store.MALL_NAME}</p>
                                        <div className="contct">
                                            {store.STORE_CITY}
                                        </div>
                                    </div>
                                    
                                </li>
                            ))
                        }
                    </ul>
                    <button type="button" id="store-close-btn" data-dismiss="modal">Okay, Got it!</button>
                </div>
            </div>
        </div>
    </div>
);

class ProductDetail extends Component {

    constructor(props) {
        super(props);
        this.state = {
            productId: '',
            productData: "",
            deliveryDetail: {
                data: {}
            },
            pincode: '',
            shownReviewCount: 5,
            selectedColorOption: {},
            selectedSizeOption: {},
            productCount: 1,
            cityData: {},
            selectedCity: "",
            storeList: {},
            selectedStore: {
                id: "",
                name: ""
            },
            selectedSliderIndex: 0,
            writeReview: {
                rating: 3,
                title: "",
                message: ""
            },
            messageError : "(Please make sure your reviews contains at least 50 characters.)",
            titleError : "",
            zoomImage: "",
            availabilityTime: "",
            shownAccordian: 1,
            language:'',
            nearStoreData : '',
            siblingData: "",
            eddData : '',
            combination : '',
            selectedCityId: "",
            productIdSeoName:{data:{}}
        }
    }

    static initialAction({url, store, lang}) {
        let prodId = url.split("/")[2];
        store.dispatch(productdetailAction({"prodId":prodId,"lang":lang}));
        // return true;
    }
    

    componentDidMount(){
        ReactGA.pageview('ProductDetail');
        this.setState({
            language: getter('language') ? getter('language') : deafultLanguage
        });
        let pdId=window.location.href.split("/pd/")[1];
        if(!(!isNaN(parseFloat(pdId)) && !isNaN(pdId - 0)))
            this.props.dispatch(productidbyseonameAction({"seo_name":window.location.href.split("/pd/")[1].replace(".html",""),"lang":languageForAPI()}));
        
        else{
            this.setState({productId:window.location.href.split("/pd/")[1]});
        }

        if(this.props.cityData && this.props.cityData.data){
            this.setState({cityData: this.props.cityData.data});
        }
        var intervalId = setInterval(()=>{
           
        if(this.state.productData){
            this.ThumbnailSliderScroll();
            clearInterval(intervalId);
        }
        }, 4000);
        this.getCityname();
    }

    getCityname(){
        if (window.navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(
                (position)=> {
                    let geocoder = new google.maps.Geocoder();
        
                    geocoder.geocode({'latLng': {lat:position.coords.latitude,lng:position.coords.longitude}},(results, status)=> {
                        if (status == google.maps.GeocoderStatus.OK) {
                           
                            if (results[0]) {
                                var addressComponents = results[0].address_components;
                                
                                for(let i=0;i<addressComponents.length;i++) {
                                    if(addressComponents[i].types[0] == "locality") {
                                        this.setState({
                                            selectedCity : addressComponents[i].long_name
                                        })
                                        break;
                                    } 
                                }
                            }
                        }
             
                    }) 
                    
            })    
        }
    }

    ThumbnailSliderScroll(){
        var bigImageHeight =$(".product_carosel .carousel-slider .slide img").height() ;
        var imageHeight = $(".thumb_outer .carosel_thumbnail img").outerHeight() + 10;
        var showItem = Math.round(bigImageHeight / imageHeight);
        $(".thumb_outer .carosel_thumbnail").height(bigImageHeight);
        var TotalHeight = this.state.productData.product_info.product_gallery.length * imageHeight;
        if($(".thumb_outer .carosel_thumbnail img").length > showItem){
            $(".thumb_outer .control-next").show();   
        }
        var sliderscrollTop = 0;
        $(".thumb_outer .control-next").on("click",function(e){
            $(".thumb_outer .carosel_thumbnail").animate({ scrollTop: sliderscrollTop +=  imageHeight }, 700);   
            if(sliderscrollTop >= imageHeight)
            {
                $(".thumb_outer .control-prev").show();   
            }
            var diff = parseInt((TotalHeight - (bigImageHeight+sliderscrollTop)))
            if(parseInt(diff)< imageHeight){
                $(".thumb_outer .control-next").hide();   
            }
        });
        $(".thumb_outer .control-prev").on("click",function(e){
            $(".thumb_outer .carosel_thumbnail").animate({ scrollTop: sliderscrollTop -=  imageHeight }, 700);   
            $(".thumb_outer .control-next").show();   
            if(sliderscrollTop == 0){
                $(".thumb_outer .control-prev").hide();   
            }
        });
    }

    componentWillReceiveProps(nextProps){

        if(nextProps.match.params.product_id != this.props.match.params.product_id){
            this.getProductInfo(nextProps.match.params.product_id);
        }
        if(nextProps.productData.result){
            
                this.setState({productData: nextProps.productData.data});
                if(nextProps.productData.data.product_info.tracking == "B"){
                    this.props.dispatch(getNearStoreLocation({prodId: this.state.productId, lang_code:languageForAPI(), combination: ""}));
                }
                this.props.dispatch(InitailStateAction("Initail_Product_Detail_ACTION"));
            
        }
        
        setTimeout(() => {
            this.setState({deliveryDetail: (nextProps.deliveryDetail!=="" && nextProps.deliveryDetail!==undefined)?nextProps.deliveryDetail.data:{data:{}}});
        },1000);

        if(nextProps.languageReducer.value){
            this.setState({
                language: nextProps.languageReducer.value
            })  
        }

        if(nextProps.cityData.data != this.props.cityData.data){
            this.setState({cityData: nextProps.cityData.data});
        }

        /*if(nextProps.storeList.data != this.props.storeList.data){
            console.log("store list", nextProps.storeList.data);
            this.setState({storeList: nextProps.storeList.data},
                () => {
                    let temp = {
                        id: this.state.storeList.data[0].id,
                        name: this.state.storeList.data[0].address_line1
                    };

                    this.setState({selectedStore: temp});
            });
        }*/

        if(nextProps.cartData != this.props.cartData && nextProps.cartData.addResult){
            notify.show(local.wishliAddedInCart[this.state.language],"success");
        }
        
        if(nextProps.nearStoreReducer != this.props.nearStoreReducer){
            // this.setState({
            //     nearStoreData : nextProps.nearStoreReducer.data.response.data
            // });
            if(!nextProps.nearStoreReducer.initial){
                this.setState({nearStoreData : nextProps.nearStoreReducer});
            }
            if(nextProps.nearStoreReducer.result){
                this.props.dispatch(InitailStateAction("Initail_Near_Store_ACTION"));
            }
        }

        if(nextProps.siblingData != this.props.siblingData){
            this.setState({siblingData: nextProps.siblingData});
            if(nextProps.siblingData.result){
                this.props.history.push("/pd/" + nextProps.siblingData.data.product_id);
                this.props.dispatch(InitailStateAction("Initail_SIBLING_ID_ACTION"));
            }
        }

        if(nextProps.eDDReducer != this.props.eDDReducer){
            if(nextProps.eDDReducer.data.response){

            this.setState({
                eddData : nextProps.eDDReducer.data.response.data
                });

            }
        }
        if(nextProps.productIdSeoName!=this.props.productIdSeoName){
            this.setState({productIdSeoName:nextProps.productIdSeoName.data},()=>{
                if(Object.keys(nextProps.productIdSeoName.data).length>0){
                    this.setState({productId: nextProps.productIdSeoName.data.data[0].object_id});
                    this.props.dispatch(productdetailAction({"prodId":nextProps.productIdSeoName.data.data[0].object_id,"lang":languageForAPI()}));
                }
            });
        }
        
    }

    getProductInfo(productId){
        this.setState({
            productId: productId, 
            productData: "",
            deliveryDetail: {
                data: {}
            },
            pincode: '',
            shownReviewCount: 5,
            selectedColorOption: {},
            selectedSizeOption: {},
            productCount: 1,
            selectedSliderIndex: 0,
            writeReview: {
                rating: 3,
                title: "",
                message: ""
            },
            messageError : "(Please make sure your reviews contains at least 50 characters.)",
            titleError : "",
            zoomImage: "",
            availabilityTime: "",
            shownAccordian: 1,
        });

        this.props.dispatch(productdetailAction({"prodId": productId,"lang": languageForAPI()}))
    }

    checkDeliveryDate(){
        if(this.state.pincode){
            this.props.dispatch(deliveryDetailAction({pincode: this.state.pincode, prodId: this.state.productId}));
        }
    }

	add(){
        if(this.state.productCount < 1){
            notify.show(local.minCartQty[this.state.language], 'error');
            return;
        }

        let error = false;
        let message = "";
        if(this.state.productData.product_info.tracking == "O"){
            this.state.productData.product_info.sibling_features_info.map(siblingFeature => {
                if(siblingFeature.label == "Size" && !Object.keys(this.state.selectedSizeOption).length){
                    error = true;
                    message = local.wishliSelectSize[this.state.language];
                }
            });
        }

        if(!error){
            let obj = Object.assign({}, this.state.selectedSizeOption);
            let data = {};
            let temp = {
                "product_id": this.state.productId,
                "amount": this.state.productCount,
                "product_options": obj
            }
            data[this.state.productId] = temp;
            console.log(data);
            this.props.dispatch(addtocartAction(data));
        }
        else{
            notify.show(message,"error");
        }
    }

    addToWishlist(){
        if(getter('userId')){
            let data = {};
            data[this.state.productId] = {
                product_id: this.state.productId,
                amount: this.state.productCount,
                cart_converted_id: 13,
                language: this.state.language
            };
            this.props.dispatch(addToWishlistAction(data));

        }
        else{
            this.props.dispatch(showLogin());
            // show login popup
        }
    }

    changePincode(e){
        this.setState({pincode: e.target.value});
    }

    changeProductCount(val){
        // if(val > 0){
            this.setState({productCount: val});
        // }
    }

    goToCategory(catId){
        this.props.history.push("/ct/"+catId);
    }

    showMoreReviews(){
        this.setState({shownReviewCount: this.state.shownReviewCount + 5});
    }

    setColor(key, value){
        /*let temp = {};
        temp[key] = value;
        this.setState({selectedColorOption: temp});*/
        //this.props.dispatch(getProductImageAction({"product_id": this.state.productId, "option_id": key, "variant_id": value}));
        let data = {
            productId: this.state.productId,
            str: key + ":" + value
        }
        this.props.dispatch(getSiblingIdAction(data));
    }

    setSize(key, value){console.log(key, value);
        let temp = {};
        temp[key] = value;
        this.setState({selectedSizeOption: temp});
        this.setState({combination : key + "_" + value});
        this.props.dispatch(getNearStoreLocation({prodId:this.state.productId, lang_code:languageForAPI(), combination: key + "_" + value}));
    }

    selectCity(cityId,flag=false){  
        if(!flag)
            this.setState({selectedCityId: cityId,selectedCity: cityId});
        else
          this.setState({selectedCityId: cityId,selectedCity: cityId},()=>{
              this.setAvailability();
          });  
    }

    selectStore(e){console.log(e.target.value, e.target.label);
        let temp = {
            id: e.target.value,
            name: ""
        };

        this.state.storeList.data.map(store => {
            if(store.id == e.target.value){
                temp.name = store.address_line1
            }
        });

        this.setState({selectedStore: temp});
    }

    onSlideChange(index){
        this.setState({selectedSliderIndex: index});
    }

    slideChangeByClick(index){
        this.setState({selectedSliderIndex: index});
    }
    
    updateWriteReview(key, val){
        let writeReview = {...this.state.writeReview};
        writeReview[key] = val;
        this.setState({writeReview: writeReview});
        if(key=='title'){
            this.setState({
                titleError:''
            })
        }
        else if(key=='message'){
            this.setState({
                messageError : local.wishliMessageError[this.state.language],
            })
        }
    }

    openReviewPopup(){
        if(getter('userId')){
            document.getElementById("open-review-popup-link").click();
        }
        else{
            this.props.dispatch(showLogin());
        }
    }

    submitReview(){
        let error = false;
		let rating = checkRequired(this.state.writeReview.rating)
        if(!rating[0]){
            error =  true;
            
        }
        let message = checkCharacterCount(this.state.writeReview.message, 50, local.wishliMessageError[this.state.language])
        if(!message[0]){
            error =  true;
            this.setState({
             messageError : message[1]
            })
        }
        let title = checkRequired(this.state.writeReview.title)
        if(!title[0]){
            error =  true;
            this.setState({
              titleError : title[1]
            })
        }
        if(!error){
            document.getElementById("review-close-btn").click();
            let userInfo = getUserInfo();
            let data = {
                "message": this.state.writeReview.message,
                "rating_value": this.state.writeReview.rating,
                "title": this.state.writeReview.title,
                "object_id": this.state.productId,
                "object_type": "P",
                "user_id": getter('userId'),
                "name": userInfo.data.firstname + " " + userInfo.data.lastname
            }
            this.props.dispatch(addProductReviewAction(data));

        }
       
    }

    showZoom(img){
        this.setState({zoomImage: img});
        document.getElementById("zoom_wrapper").style.display = "block";
    }

    hideZoom(){
        this.setState({zoomImage: ""});
        document.getElementById("zoom_wrapper").style.display = "none";
    }

    mouseMove(e){
        let cont = document.getElementById("carouseldata-cont"+this.state.selectedSliderIndex);
        let contX = cont.getBoundingClientRect().left;
        let contXR = cont.getBoundingClientRect().right;
        let contY = cont.getBoundingClientRect().top;
        let contYB = cont.getBoundingClientRect().bottom;
        let docScroll = document.documentElement.scrollTop;
        contYB = docScroll ? contYB + docScroll + 77 : contYB;
        let relX = e.pageX - contX;
        let relY = e.pageY - contY;
        let contWidth = contXR - contX;
        let contHeight = contYB - contY;

        let zoomWrapper = document.getElementById("zoom_wrapper");
        let zoomWrapperWidth = zoomWrapper.getBoundingClientRect().right - zoomWrapper.getBoundingClientRect().left;
        let zoomWrapperHeight = zoomWrapper.getBoundingClientRect().bottom - zoomWrapper.getBoundingClientRect().top;

        let zoomImage = document.getElementById("zoom_image");
        let zoomImageWidth = zoomImage.getBoundingClientRect().right - zoomImage.getBoundingClientRect().left;
        let zoomImageHeight = zoomImage.getBoundingClientRect().bottom - zoomImage.getBoundingClientRect().top;

        let widthGap = zoomImageWidth - zoomWrapperWidth;
        let heightGap = zoomImageHeight - zoomWrapperHeight;

        let leftPer = (relX*100)/contWidth;
        let topPer = (relY*100)/contHeight;

        let tarPerX = widthGap > 0 ? "-" + (widthGap*leftPer)/100 + "px" : 0;
        let tarPerY = heightGap > 0 ? "-" + (heightGap*topPer)/100 + "px" : 0;

        zoomImage.style.transform = "translate(" + tarPerX + ", " + tarPerY + ")";
    }

    setAvailability(){
        let error = false;
        let message = "";
        if(this.state.productData.product_info.tracking == "O"){

            this.state.productData.product_info.sibling_features_info.map(siblingFeature => {
                if(siblingFeature.label == "Size" && !Object.keys(this.state.selectedSizeOption).length){

                    error = true;
                    message = local.wishliSelectSize[this.state.language];
                }
            });
        }
        if(this.state.selectedCityId==="" || this.state.selectedCityId===undefined)
        {
            error = true;
            message = local.wishliSelectCity[this.state.language];
        }
        if(!error)
        {   
            // below line comment by dinesh
            //this.props.dispatch(StoreAction(this.state.selectedCity));
            this.props.dispatch(getEddAction({prodId:this.state.productIdSeoName.data[0].object_id, 
                lang_code:languageForAPI(), 
                combination: this.state.combination,
                to_city:this.state.selectedCity
            }));   
        }
        else{
            
            notify.show(message,"error");
        }
        this.setState({availabilityTime: this.state.eddData});
    }

    manageAccordian(index){
        if(this.state.shownAccordian == index){
            this.setState({shownAccordian: null});
        }
        else{
            this.setState({shownAccordian: index});
        }
    }

    openPickupStorePopup(storeList){
        if(typeof(storeList) == "object"){
            document.getElementById("pickup_store_link").click();
        }
    }

    render() {
        if(this.state.productData){ 
            return (
                <div className="product_detail">
					{/*<button onClick={this.add.bind(this)}>add to cart</button>*/}
                    <Section1 
                        data={this.state.productData} 
                        deliveryDetail={this.state.deliveryDetail} 
                        checkDeliveryDate={() => this.checkDeliveryDate()} 
                        pincode={this.state.pincode} 
                        changePincode={(e) => this.changePincode(e)} 
                        goToCategory={(catId) => this.goToCategory(catId)} 
                        setColor={(key, value) => this.setColor(key, value)} 
                        setSize={(key, value) => this.setSize(key, value)} 
                        addToCart={() => this.add()} 
                        addToWishlist={() => this.addToWishlist()} 
                        productCount={this.state.productCount}
                        changeProductCount={(val) => this.changeProductCount(val)} 
                        selectedSizeOption={this.state.selectedSizeOption} 
                        selectedColorOption={this.state.selectedColorOption} 
                        cityData={this.state.cityData} 
                        selectedCity={this.state.selectedCity}
                        selectCity={(cityName,flag) => this.selectCity(cityName,flag)} 
                        storeList={this.state.storeList} 
                        selectedStore={this.state.selectedStore} 
                        selectStore={(e) => this.selectStore(e)} 
                        onSlideChange={(index) => this.onSlideChange(index)} 
                        selectedSliderIndex={this.state.selectedSliderIndex} 
                        hideZoom={() => this.hideZoom()} 
                        showZoom={(img) => this.showZoom(img)} 
                        zoomImage={this.state.zoomImage} 
                        mouseMove={(e) => this.mouseMove(e)} 
                        availabilityTime={this.state.eddData} 
                        setAvailability={() => this.setAvailability()}
                        slideChangeByClick = {(index)=>this.slideChangeByClick(index)}
                        language={this.state.language}
                        nearStoreList = {this.state.nearStoreData} 
                        openPickupStorePopup = {(storeList) => this.openPickupStorePopup(storeList)} 
                        openReviewPopup = {() => this.openReviewPopup()}
                        getCityById= {(cityId)=> this.getCityById(cityId)}
                    />
                    <Section2 
                        data={this.state.productData} 
                        shownReviewCount={this.state.shownReviewCount} 
                        showMoreReviews={() => this.showMoreReviews()} 
                        manageAccordian={(index) => this.manageAccordian(index)} 
                        shownAccordian={this.state.shownAccordian}
                        language={this.state.language}
                    />
                    {/*<Section3 language={this.state.language}/>*/}
                    <Section4 
                        productData={this.state.productData}
                    />
                    <Section5 
                        writeReview={this.state.writeReview}
                        titleError = {this.state.titleError}
                        messageError = {this.state.messageError} 
                        updateWriteReview={(key, val) => this.updateWriteReview(key, val)} 
                        submitReview={() => this.submitReview()}
                        language={this.state.language}
                    />
                    <Section6 
                        data={this.state.productData} 
                        deliveryDetail={this.state.deliveryDetail} 
                        checkDeliveryDate={() => this.checkDeliveryDate()} 
                        pincode={this.state.pincode} 
                        changePincode={(e) => this.changePincode(e)} 
                        goToCategory={(catId) => this.goToCategory(catId)} 
                        setColor={(key, value) => this.setColor(key, value)} 
                        setSize={(key, value) => this.setSize(key, value)} 
                        addToCart={() => this.add()} 
                        productCount={this.state.productCount}
                        changeProductCount={(val) => this.changeProductCount(val)} 
                    />
                    <StorePopup nearStoreList={this.state.nearStoreData} />
                    {this.state.siblingData.isFetching && <Loader />}
                </div>
            );
        }
        else{
            return (
                <Loader />
            );
        }
    }
}

function mapStateToProps(state) {
    return {
        productData: state.productDetailReducer,
        deliveryDetail: state.deliveryDetailReducer,
        cityData: state.cityReducer,
        cartData: state.cartReducer,
        languageReducer: state.languageReducer,
        nearStoreReducer : state.nearStoreReducer,
        siblingData: state.siblingIdReducer,
        eDDReducer : state.eDDReducer,
        productIdSeoName: state.productIdReducer
    }
}

export default connect(mapStateToProps)(withStyles (styles)(ProductDetail));
