import React from 'react';

export default class Subloader extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div class="cs-loader inner_loader" style={{margin: this.props.align == "left" ? "0" : "0 auto"}}>
                <div class="cs-loader-inner">
                    <label> ●</label>
                    <label> ●</label>
                    <label> ●</label>
                    <label> ●</label>
                    <label> ●</label>
                    <label> ●</label>
                </div>
            </div>
        );
    }
}