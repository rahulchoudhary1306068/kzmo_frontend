import React from 'react';
import {connect} from 'react-redux';
import Notifications from 'react-notify-toast';
import local, {deafultLanguage} from '../localization';
import {getter} from '../commonfunction';

class Footer extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            language: ''
        };
    }
    componentDidMount(){
        this.setState({
            language: getter('language') ? getter('language') : deafultLanguage
        })
    }
    componentWillReceiveProps(nextProps){
        if(nextProps.languageReducer.value){
            this.setState({
                    language: nextProps.languageReducer.value
            })  
        }
    }
	render() {
    	return (
    		<div className="footer_container">
                <div className="row">
                    <div className="col col-md-6 d-none d-md-block">
                        <div className="row">
                            <div className="col">
                                <h6>{local.footerUsefulLinks[this.state.language]}</h6>
                                <ul>
                                    <li><a href="#">{local.footerContactUs[this.state.language]}</a></li>
                                    <li><a href="#">{local.footerTrackOrder[this.state.language]}</a></li>
                                    <li><a href="#">{local.footerShipping[this.state.language]}</a></li>
                                    <li><a href="#">{local.footerCancellation[this.state.language]}</a></li>
                                    <li><a href="#">{local.footerReturns[this.state.language]}</a></li>
                                    <li><a href="#">{local.footerFAQ[this.state.language]}</a></li>
                                    
                                </ul>
                            </div>
                            <div className="col">
                                <h6>{local.footerShopWithUs[this.state.language]}</h6>
                                <ul>
                                    <li><a href="#">{local.footerMen[this.state.language]}</a></li>
                                    <li><a href="#">{local.footerWomen[this.state.language]}</a></li>
                                    <li><a href="#">{local.footerBrands[this.state.language]}</a></li>
                                    <li><a href="#">{local.headerStoreLocator[this.state.language]}</a></li>
                                </ul>
                            </div>
                            <div className="col">
                                <h6>{local.footerWhoWeAre[this.state.language]}</h6>
                                <ul>

                                    <li><a href="#">{local.headerAboutUs[this.state.language]}</a></li>
                                    <li><a href="#">{local.headerBlogs[this.state.language]}</a></li>
                                    <li><a href="#">{local.headerTermsOfUse[this.state.language]}</a></li>
                                    <li><a href="#">{local.headerPrivacyPolicy[this.state.language]}</a></li>
                                </ul>
                            </div>
                        </div>
                        
                    </div>
                    <div className="col newsletter_sub col-lg-4 col-md-5 offset-lg-2 offset-md-1">
                        <p>{local.footerSignUpForUpdates[this.state.language]}</p>
                        <form>
                            <input name="email" type="email" id="newsletter" placeholder={local.footerEmailAddress[this.state.language]} />
                            <button className="subscribe btn" title="Subscribe" type="submit">
                                <svg className="icon icon-submit"><use xlinkHref="/img/icon-sprite.svg#icon-submit"></use></svg>
                            </button>
                        </form>
                    </div>
                </div>
                {/* <div className="social_icons text-sm-right">
                        <a href="#"><svg className="icon icon-facebook"><use xlinkHref="/img/icon-sprite.svg#icon-facebook"></use></svg></a>
                        <a href="#"><svg className="icon icon-pinterest"><use xlinkHref="/img/icon-sprite.svg#icon-pinterest"></use></svg></a>
                        <a href="#"><svg className="icon icon-twitter"><use xlinkHref="/img/icon-sprite.svg#icon-twitter"></use></svg></a>
                        <a href="#"><svg className="icon icon-youtube"><use xlinkHref="/img/icon-sprite.svg#icon-youtube"></use></svg></a>
                        <a href="#"><svg className="icon icon-instagram"><use xlinkHref="/img/icon-sprite.svg#icon-instagram"></use></svg></a>
                </div> */}
                <div className="row google_icons">
                    <div className="col-sm-6">
                        <a href="#"><svg className="icon icon-google-play-dark"><use xlinkHref="/img/icon-sprite.svg#icon-google-play-dark"></use></svg></a>
                        <a href="#"><svg className="icon icon-apple-store-dark-1"><use xlinkHref="/img/icon-sprite.svg#icon-apple-store-dark-1"></use></svg></a>
                    </div>
                   <div className="col-sm-6">
                        <p className="text-sm-right">{local.footerCopyright[this.state.language]}</p>
                   </div>
                </div>
                <Notifications options={{top: '90%'}} /> {/* toast componet*/}
    		</div>
    	);
    }
}

function mapStateToProps(state) {
    return {
				languageReducer : state.languageReducer
    }
}

export default connect(mapStateToProps)(Footer);