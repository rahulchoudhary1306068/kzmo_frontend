import React, { Component } from 'react';
import {connect} from 'react-redux';
import {
    MuiThemeProvider,
    createMuiTheme,
    createGenerateClassName,
  } from '@material-ui/core/styles';
  
const styles = theme => ({
 
});

export class PinMap extends Component {
    
   constructor(props){
       super(props)
       this.state = {
           lat : 24.9090,
           long : 45.75656,
           markers : [],
           mymap : "",
           mapObject :[],
           directionsService :"",
           directionsDisplay : "",
           lat : 23.7224509,
           lng : 46.7893653,
           myLocMapObject : {}
       }
   }         
   getCurrentLocation = ()=>{
    this.putMarker({lat:this.state.lat,lng:this.state.lng}, true,'Your Location','','',1);
    
   
}
    
        
    
    initMap=()=> {

        let myLatLng = {lat: this.state.lat, lng: this.state.long}
        let value =new google.maps.Map(document.getElementById('pinMap'), {
            zoom: 5,
            center: myLatLng
        })
        
        this.setState({
            mymap :  value
        })
      
       
        setTimeout(()=>{
            
            this.state.markers.map((value)=>{
                this.putMarker(value.pos, true,'Your Location',value.address,'');
            });
            this.getCurrentLocation();
            this.mapCenterCoverAllMarkers()
        },100)
             
    }

    putMarker=(myLatLng,draggable,title,info,markerIcon,type)=> {
        
        let marker = new google.maps.Marker(
                {
                    map:this.state.mymap,
                    
                    animation: google.maps.Animation.DROP,
                    position: myLatLng,
                    title: title,
                    
                });
                if(info) {
                    let infowindow = new google.maps.InfoWindow({
                        content: info
                    });
                    marker.addListener('click', ()=> {
                        infowindow.open(this.state.mymap, marker);
                    });
                    //infoWindows.push(infowindow);
                }
       
        if(type==1){
            this.state.myLocMapObject = marker;
        }else{ 
            this.state.mapObject.push(marker);
        }
        
    }
     mapCenterCoverAllMarkers = ()=> {
        let bounds = new google.maps.LatLngBounds();
        this.state.mapObject.map((value)=>{
            bounds.extend(value.getPosition())
        })
        this.state.mymap.fitBounds(bounds);
    }

    componentDidMount(){
        //console.log(4545454);
        //console.log(this.props.storeValue)
        this.props.storeValue.map((value)=>{
            var locationData = {pos:{lat:parseFloat(value.latitude),lng:parseFloat(value.longitude)},address:value.company_name+""+value.address_line1}
            this.state.markers.push(locationData)
        })
        
        this.initMap();
        this.setState({
             directionsService : new google.maps.DirectionsService(),
             directionsDisplay : new google.maps.DirectionsRenderer()
        })
        
    }

    setDirection = (index)=> {
        console.log(this.state.mapObject[index]);
        console.log(this.state.myLocMapObject)
        let request = {
            origin: this.state.myLocMapObject.getPosition(),
            destination: this.state.mapObject[index].getPosition(),
            travelMode: google.maps.TravelMode.DRIVING
        };
        this.state.directionsService.route(request, (response, status)=> {
            if (status == google.maps.DirectionsStatus.OK) {
                this.state.directionsDisplay.setDirections(response);
                this.state.directionsDisplay.setMap(this.state.mymap);
            } else {
                //console.log(response)
                alert(status)
                //alert("Directions Request from " + start.toUrlValue(6) + " to " + end.toUrlValue(6) + " failed: " + status);
            }
        });
    }
    render() { //console.log(this.props)
        return (
            <div>
                <div style={{height :"500px"}} className="map_section position-relative" id="pinMap"></div>
               
            </div>
        )
    }
}