import React from 'react';

export default class Loader extends React.Component {
    render() {
        return (
            <div class="cs-loader">
                <div class="cs-loader-inner">
                    <label> ●</label>
                    <label> ●</label>
                    <label> ●</label>
                    <label> ●</label>
                    <label> ●</label>
                    <label> ●</label>
                </div>
            </div>
        );
    }
}