import React from 'react';
import {connect} from 'react-redux';
import {DebounceInput} from 'react-debounce-input';


import PropTypes from 'prop-types';
import {
  MuiThemeProvider,
  createMuiTheme,
  createGenerateClassName,
} from '@material-ui/core/styles';
import { withStyles } from '@material-ui/core/styles';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';



import { headerAction, headerSearchAction, CountryAction, CityAction, UpdateLanguage } from '../actions/header';
import Login from '../screens/Login'
import { getcartAction } from '../actions/cart';

import { getter, getObject,localClear, setter,languageForAPI} from '../commonfunction';


import {notify} from 'react-notify-toast';
import { InitailStateAction, InitailState_ACTION, showLogin } from '../actions/login';
import local,{deafultLanguage} from '../localization';
import { languageReducer } from '../reducers/header';
import { MicroSiteUrl,GA_ID } from '../constant';
import ReactGA from 'react-ga'

const styles = theme => ({
 
});

const mui = createMuiTheme({
  overrides: {
    MuiMenuItem: {
      selected: {
        color: "#e4b122 !important"
      },
    }
  }
});


class Header extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            headerData: [],
            toggleLogin:false,
            loginstatus : false,
            searchData: {
                isShown: true,
                data: [],
                value: '',
                recentSearches: []
            },
            loginDetails: "",
            cart: "",
            cartCount : 0,
            country : false,
            countryDefaultValue : "SA",
            showCrossButton : true,
            logoutRedirect : ['checkout','myaccount','shopingcart'],
            language: '',
            brandMenuData : "",
            headerBrandMenu: false,
            showRecentSearchesSection:true
        }
    }


    toggleLogin(){
        this.setState({
            toggleLogin: !this.state.toggleLogin,
             
        })
            
        if(getter('userId')){
            this.setState({
                loginstatus : true
            })
        }
        
        if(this.props.history.location.pathname == "/checkout"){
            this.setState({
                showCrossButton : false
            })
        }
    }

    getSearchData(val){
        let searchData = {...this.state.searchData};
        searchData.value = val;
        this.setState({searchData: searchData,showRecentSearchesSection:false});
        this.props.dispatch(headerSearchAction(val));
    }

    componentDidMount(){
        ReactGA.initialize(GA_ID);
        this.setState({
            language: getter('language')?getter('language'):deafultLanguage
        }, () => {
            if(this.state.language == "sar"){
                document.body.classList = "righttoleft-direction";
            }
            else{
                document.body.classList = "";
            }
            this.props.dispatch(headerAction(languageForAPI()));
        });

       // this.props.dispatch(headerAction());

        if(localStorage.getItem('recentSearches')){
            let searchData = {...this.state.searchData};
            searchData.recentSearches = JSON.parse(localStorage.getItem('recentSearches'));
            this.setState({searchData: searchData}); 
        }

        this.props.dispatch(getcartAction())

        this.unlisten = this.props.history.listen((location, action) => {
            $('#login_popup').modal('hide');
            $('.modal-backdrop').remove(); // to remove the popup backdrop when we press browser backbutton
            $('body').removeClass('modal-open');
            $('body').removeAttr('style');
            this.setState({
                toggleLogin : false,
            })
            
        });
        if(getter('userId')){      
            this.setState({
                loginstatus:true,
                loginDetails : getObject('userObject').data
            })
        }
        
        this.props.dispatch(CountryAction());
        let country = getter('country')
        if(country){
            this.setState({
                countryDefaultValue : country
            })
        }
       console.log("calll")
        console.log(this.getCurrentLocation());
    }

    getCurrentLocation = ()=>{
        if (window.navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(
                (position)=> {
                         
                },
                function() {}
            );
        }
    }

    componentWillReceiveProps(nextProps){
        if(nextProps.languageReducer.value){
            this.setState({
                language: nextProps.languageReducer.value
            }, () => {
                if(this.state.language == "sar"){
                    document.body.classList = "righttoleft-direction";
                }
                else{
                    document.body.classList = "";
                }
            })  
            setter('language',nextProps.languageReducer.value)
            // we require timeout for make delay in clean language data
            setTimeout(()=>{
                this.props.dispatch(InitailStateAction('initailLanguageAction'))
            },1000); 
        }

        if(nextProps.InvalidTokenReducer.value){
            this.InvalidTokenLogout();
        }

        if(nextProps.cartReducer.cart !== this.props.cartReducer.cart){
            if(nextProps.cartReducer.cart.response){
                this.setState({
                    cartCount: Object.keys(nextProps.cartReducer.cart.response).length?nextProps.cartReducer.cart.response.count:this.state.cartCount
                })
            }
        }

        if(Object.keys(nextProps.LoginReducer.data).length ){
            this.setState({
                loginDetails : nextProps.LoginReducer.data.data
            })
            if(nextProps.LoginReducer.data.data != this.props.LoginReducer.data.data){
                notify.show(local.headerLoginSuccess[this.state.language])
            }
        }

        if(nextProps.LoginReducer.showLogin){
            if(!nextProps.LoginReducer.data.status){
                this.props.dispatch(InitailStateAction('InitailStateLogin_ACTION'));
                this.toggleLogin();
                setTimeout(()=>{
                    $('#login_popup').modal('show');
                },500)
            }
        }

        if(nextProps.cartReducer.cart.response){
            if(!Object.keys(nextProps.cartReducer.cart.response).length){
                this.setState({
                    cartCount : 0
                })
            }    
        } 
        
        setTimeout(()=>{
            this.formatHeaderData(nextProps.headerData);

            if(this.state.searchData.isShown && nextProps.searchData.length){
                let searchData = {...this.state.searchData};
                searchData.data = nextProps.searchData;
                this.setState({searchData: searchData});
            }
        },500);

        // login task

        if (nextProps.LoginReducer.data.status != this.props.LoginReducer.data.status){
			// if(nextProps.LoginReducer.data.status=='success'){
			// 	$('.modal').modal('hide');
			// 	this.setState({
            //         toggleLogin:!this.state.toggleLogin,
            //         loginstatus : true
			// 	})
				
			// }
			
        }
        
        if(nextProps.countryReducer.data.response != this.props.countryReducer.data.response){
            this.setState({
                    country: nextProps.countryReducer.data.response
                },() => {
                    this.props.dispatch(CityAction(this.state.countryDefaultValue));
                });
        }
        if(nextProps.brandMenuReducer.data.response){
            this.setState({
                brandMenuData : nextProps.brandMenuReducer.data.response, 
                headerBrandMenu: true
            })
        }
    }

    formatHeaderData(data){
        if(data.mainMenu && data.mainMenu.data.response){
            let temp = data.mainMenu.data.response;
            if(data.subMenu && data.subMenu.submenu){
                for (var i = 0; i < temp.length; i++) {
                    data.subMenu.submenu.map((subMenu) => {
                        if(subMenu.index == i){
                            if(subMenu!==undefined && subMenu!="")
                            {
                                subMenu.leaf_nav_list.map((leaf, ind) => {
                                subMenu.leaf_nav_list[ind]['showLeaf'] = 10;
                                });
                            }
                            
                            temp[i].submenu = subMenu;
                        }
                    });
                }
            }
            this.setState({headerData: temp});
        }
    }

    setLeafShownIndex(ind){
        let headerData = [...this.state.headerData];
        if(headerData[ind].submenu!=="" && headerData[ind].submenu!==undefined)
        {
            headerData[ind].submenu.leaf_nav_list.map((leaf, leafIndex) => {
            headerData[ind].submenu.leaf_nav_list[leafIndex].showLeaf = 10;
            });
        }
        
        this.setState({headerData: headerData});
    }

    showMoreLeafs(ind, subMenuInd){
        let headerData = [...this.state.headerData];
        headerData[ind].submenu.leaf_nav_list[subMenuInd].showLeaf += 10;
        this.setState({headerData: headerData});
    }

    showAutosuggest(){
        let searchData = {...this.state.searchData};
        searchData.isShown = true;
        this.setState({searchData: searchData});
    }

    hideAutosuggest(){
        let searchData = {...this.state.searchData};
        searchData.isShown = false;
        // searchData.data = [];
        // searchData.value = '';
        this.setState({searchData: searchData});
    }

    searchSubmit(e){
        let charCode = (typeof e.which === "number") ? e.which : e.keyCode;
        let searchTerm = e.target.value.trim();

        if(charCode == 13 && searchTerm.length > 2){
            let recentSearches = [];
            if(localStorage.getItem('recentSearches')){
                recentSearches = JSON.parse(localStorage.getItem('recentSearches'));
            }
            recentSearches.push(searchTerm);
            localStorage.setItem('recentSearches', JSON.stringify(recentSearches));
            this.props.history.push("/search/?q=" + searchTerm);
        }
    }

    selectSearch(suggestionType, value, id){
        if(suggestionType == "category"){
            this.props.history.push("/ct/" + id);
        }
        else if(suggestionType == "keyword"){
            this.props.history.push("/search/?q=" + value);
        }
    }

    goToRecentSearch(searchTerm){
        this.props.history.push("/search/?q=" + searchTerm);
    }

	goToHome(){
        this.props.history.push("/");
    }

    moveToCart(){
        event.preventDefault();
        
        if(this.state.cartCount)
            this.props.history.push('/shopingcart');
          else{

                notify.show(local.headerBagEmpty[this.state.language],"error"); 
           }  
    }

    goToCategory(catId){    
        this.props.history.push("/ct/"+catId+".html");
    }

    logout(){    
        event.preventDefault();
        localClear('userObject');
        localClear('userId');
        localClear('wishListItem');
        this.props.dispatch(InitailStateAction('InitailStateLogin_ACTION'));
        this.props.dispatch(getcartAction());
        this.setState({
            loginstatus:false,
            loginDetails : false,
            cartCount : 0
        })
        let value = this.props.history.location.pathname.split('/')[1]
        if(this.state.logoutRedirect.includes(value)){
            this.props.history.push('/');
        }
        notify.show(local.headerLogoutSuccess[this.state.language])
    }

    InvalidTokenLogout(){
        localClear('userObject');
        localClear('userId');
        this.props.dispatch(InitailStateAction('InitailInvalidToken_ACTION'));
        this.props.dispatch(getcartAction());
        this.setState({
            loginstatus:false,
            loginDetails : false,
            cartCount : 0
        })
        this.props.dispatch(showLogin())
        this.props.history.push('/');
        notify.show(local.headerSessionTimeout[this.state.language]);
    }

    handleChange = event => {    
    };

    handleCountryChange = event =>{
        this.setState({ [event.target.name]: event.target.value }); 
        setter('country',event.target.value) ; 
    }

    goTOStoreLocator(){
        this.props.history.push('/locator');
    }

    goToMyaccount(page){
        this.props.history.push('/myaccount/' + page);
    }

    manageAccordian(index){    
        for(let i = 0; i <= this.state.headerData.length; i++){
            let elem = document.getElementById("menu_heading"+i);
            if(elem){
                let minus = elem.getElementsByClassName("custom_minus")[0];
                let plus = elem.getElementsByClassName("custom_plus")[0];
                if(i == index){
                    if(minus.style.display == "none"){
                        minus.style.display = "inline";
                        plus.style.display = "none";
                    }
                    else{
                        minus.style.display = "none";
                        plus.style.display = "inline";
                    }
                }
                else{
                    minus.style.display = "none";
                    plus.style.display = "inline";
                }
            }
        }
    }

    changeLanguage(){
        let value = this.state.language =='en'?'sar':'en'
        this.props.dispatch(UpdateLanguage(value));
        if(value=='en')
            document.cookie = 'language=EN';
        else   
            document.cookie = 'language=AR';
        window.location.reload()
    }
    
    redirectToMicrosite(name){
        //console.log("http://"+name+MicroSiteUrl);
        window.open("https://"+name+MicroSiteUrl)
    }

	render() {

    	return ( 
    		<div className="header_contaner">
                <div  className="innercontnt">
                    <div className="subcontent"> 
                        <span>{local.genuineBrands[this.state.language]} </span>
                        <span>{local.sameDayDelivery[this.state.language]}</span>
                        <span>{local.cashOnDelivery[this.state.language]}</span>
                    </div>
                       
    				{ this.state.toggleLogin && <Login showCrossButton= {this.state.showCrossButton} toggleLogin = {this.toggleLogin.bind(this)} />}
                    <div className="top_header text-right d-none d-md-block">

                       {/* <select class="custom-select" id="inputGroupSelect01">
                            <option selected ></option>
                            <option value="1" ></option>
                            <option value="2" ></option>
                        </select>
                        <select class="custom-select" id="inputGroupSelect01">
                            <option selected>DOLLAR</option>
                            <option value="1">USD</option>
                            <option value="2">RUPEE</option>
                        </select>*/}
                        <form autoComplete="off" className="d-md-inline-block">
                        <MuiThemeProvider theme={mui}>
                            <FormControl>
                                <Select
                                    value={this.state.countryDefaultValue}
                                    onChange={this.handleCountryChange.bind(this)}
                                    displayEmpty
                                    
                                    className="selct_outer"
                                    name = "countryDefaultValue"
                                    
                                >
                                    {this.state.country && this.state.country.map((value)=>{
                                        return <MenuItem disabled = {value.country_code =='SA'?false:true} value={value.country_code}>
                                        <img alt="country_img" src={value.country_flag} />{" "+value.country_name} -
                                        <span>{value.country_code =='SA'?" "+value.currency_symbol : " "+value.currency_symbol +"(Coming Soon)"}</span></MenuItem>
                                    })}
                                   

                                </Select>
                            </FormControl>
                            
                        </MuiThemeProvider>
                        </form>
                        <span onClick={this.changeLanguage.bind(this)} className="lang">{local.headerLanguage[this.state.language]}           
                        </span>
                        {/*<span className="lang common_tooltip_outer">{local.headerStoreLocator[this.state.language]}
                            <span className="common_tooltip top">{local.orderComingSoon[this.state.language]}</span>
                        </span>*/}
                        <span className="lang">
                            <a className="lang" href="JavaScript:void(0);" onClick={() => this.goTOStoreLocator()}>
                            {local.headerStoreLocator[this.state.language]}
                            </a>
                            {/*<span className="common_tooltip top">{local.orderComingSoon[this.state.language]}</span>*/}

                        </span>
                    </div>
                </div>
                <nav className="navbar navbar-expand-md navbar-light flex-md-row flex-row-reverse" id="navbar_header">
                    <a className="navbar-brand" href="JavaScript:void(0);" onClick={() => this.goToHome()}>
                        {this.state.language == "sar" ? <img src="/img/logo_arabic.png" alt="logo" /> : <img src="/img/logo.png" alt="logo" />}
                    </a>
                    <button className="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <svg className="icon icon-menu"><use xlinkHref="/img/icon-sprite.svg#icon-menu"></use></svg>
                    </button>
                    <div className="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul className="navbar-nav mr-auto d-none d-md-flex">
                            {/*<li className="nav-item active">
                                <a className="nav-link" href="#">Men <span className="sr-only">(current)</span></a>
                            </li>*/}
                            {
                                this.state.headerData.map((item, index) => (
                                    <li className="nav-item dropdown" onMouseLeave={() => this.setLeafShownIndex(index)}>
                                        <a className="nav-link dropdown-toggle" href="Javascript:void(0);" id="women_navbar" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{item.name}</a>
                                        <div className="dropdown-menu d-flex submenu" aria-labelledby="women_navbar">
                                            {

                                                item.submenu && item.submenu.leaf_nav_list.map((subMenu, subInd) => (
                                                    <div className="col menu_inner">
                                                        {subMenu.leaf_heading_data && <h6 style={{cursor: "pointer"}} onClick={() => this.goToCategory(subMenu.leaf_heading_data.top_heading_url)}>{subMenu.leaf_heading_data.top_heading_name}</h6>}
                                                        <ul>
                                                            {
                                                                subMenu.subcat_data && subMenu.subcat_data.slice(0, subMenu.showLeaf).map(subMenuData => (
                                                                    <li><a href="Javascript:void(0);" onClick={() => this.goToCategory(subMenuData.sub_category_url)}>{subMenuData.sub_category_name}</a></li>
                                                                ))
                                                            }
                                                        </ul>
                                                        { subMenu.subcat_data && subMenu.subcat_data.length > subMenu.showLeaf && <button className="show_more" onClick={() => this.showMoreLeafs(index, subInd)}>{local.headerViewMore[this.state.language]}</button> }
                                                    </div>
                                                ))
                                            }

                                        </div>
                                    </li>
                                ))
                            }
                            {this.state.headerBrandMenu && <li className="nav-item dropdown brand_menu">
                                <a className="nav-link dropdown-toggle" href="Javascript:void(0);" id="brand" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{local.headerBrandMenu[this.state.language]}</a>
                                <div className="dropdown-menu d-flex submenu" aria-labelledby="brand">
                                    <div className="col menu_inner">
                                        <ul className="row">
                                        {this.state.brandMenuData && this.state.brandMenuData.map((value,index)=>{
                                            return <li className="col-md-3" ><a href="Javascript:void(0);" onClick={this.redirectToMicrosite.bind(this,value.seo_name)} ><img src={value.logo} /></a></li>
                                        })}
                                            
                                            
                                        </ul>
                                    </div>
                                </div>
                            </li>}

                        </ul>
                        <div className="mobile_menu_outer d-sm-flex flex-column justify-content-between d-md-none">
                            <span className="close_menu text-right">
                                <a href="JavaScript:void(0);" ><svg className="icon icon-multiply"><use xlinkHref="/img/icon-sprite.svg#icon-multiply"></use></svg>
                                </a>
                            </span>
                            <form autoComplete="off">
                                <MuiThemeProvider theme={mui}>
                                    <FormControl>
                                        <Select
                                            value={this.state.countryDefaultValue}
                                            onChange={this.handleCountryChange.bind(this)}
                                            displayEmpty
                                            
                                            className="selct_outer"
                                            name = "countryDefaultValue"
                                            
                                        >
                                            {this.state.country && this.state.country.map((value)=>{
                                                return <MenuItem disabled = {value.country_code =='SA'?false:true} value={value.country_code}>
                                                <img alt="country_img" src={value.country_flag} />{" "+value.country_name} -
                                                <span>{value.country_code =='SA'?" "+value.currency_symbol : " "+value.currency_symbol +"(Coming Soon)"}</span></MenuItem>
                                            })}
                                           

                                        </Select>
                                    </FormControl>
                                    
                                </MuiThemeProvider>
                            </form>
                            <div id="menu_accordion" className="menu_list">
                                {
                                    this.state.headerData.map((item, index) => (
                                    <div className="card">
                                        <div className="card-header" id={"menu_heading"+index}>
                                            <h5 className="mb-0" onClick={() => this.manageAccordian(index)}>
                                                <button className="btn btn-link text-left" data-toggle="collapse" data-target={ "#menu_collapse" + index } aria-expanded="true" aria-controls={ "menu_collapse" + index }>
                                                        {item.name} <span className=" custom_plus" style={{display: index != 0 ? "inline" : "none"}}>+</span>
                                                        <span className="custom_minus" style={{display: index == 0 ? "inline" : "none"}}>-</span>
                                                </button>
                                            </h5>
                                        </div>
                                        <div id={ "menu_collapse" + index } className={index == 0 ? "collapse show" : "collapse"} aria-labelledby={"menu_heading"+index} data-parent={"#menu_accordion"}>
                                                <div className="card-body">
                                                    {
                                                        item.submenu && item.submenu.leaf_nav_list.map((subMenu, subInd) => (
                                                            <div className="col menu_inner">
                                                                {subMenu.leaf_heading_data && <h6 style={{cursor: "pointer"}} onClick={() => this.goToCategory(subMenu.leaf_heading_data.top_heading_url)}>{subMenu.leaf_heading_data.top_heading_name}</h6>}
                                                                <ul>
                                                                    {
                                                                        subMenu.subcat_data && subMenu.subcat_data.slice(0, subMenu.showLeaf).map(subMenuData => (
                                                                            <li><a href="Javascript:void(0);" onClick={() => this.goToCategory(subMenuData.sub_category_url)}>{subMenuData.sub_category_name}</a></li>
                                                                        ))
                                                                    }
                                                                </ul>
                                                                { subMenu.subcat_data && subMenu.subcat_data.length > subMenu.showLeaf && <button className="show_more" onClick={() => this.showMoreLeafs(index, subInd)}>{local.headerViewMore[this.state.language]}</button> }
                                                            </div>
                                                        ))
                                                    }
                                                </div>
                                        </div>
                                    </div>
                                    ))

                                }
                                <div className="card">
                                        {<div className="card-header" id={"brand_heading" + this.state.headerData.length}>
                                            <h5 className="mb-0" onClick={() => this.manageAccordian(this.state.headerData.length)}>
                                                <button className="btn btn-link text-left" data-toggle="collapse" data-target={"#brand_collapse" + this.state.headerData.length} aria-expanded="true" aria-controls={"brand_collapse" + this.state.headerData.length}>
                                                        {local.headerBrandMenu[this.state.language]}<span className=" custom_plus" style={{display: this.state.headerData.length != 0 ? "inline" : "none"}}>+</span>
                                                        <span className="custom_minus" style={{display: this.state.headerData.length == 0 ? "inline" : "none"}}>-</span>
                                                </button>
                                            </h5>
                                        </div>}
                                        <div id={"brand_collapse" + this.state.headerData.length} className={this.state.headerData.length == 0 ? "collapse show" : "collapse"} aria-labelledby={"brand_heading" + this.state.headerData.length} data-parent={"#menu_accordion"}>
                                                <div className="card-body">
                                                    <ul className="row">
                                                    {this.state.brandMenuData && this.state.brandMenuData.map((value,index)=>{
                                                        return <li className="col-md-" ><a href="Javascript:void(0);" onClick={this.redirectToMicrosite.bind(this,value.seo_name)} ><img src={value.logo} /></a></li>
                                                     })}
                                                      
                                                    </ul>
                                                </div>
                                        </div>
                                </div>
                                <div className="card">
                                        <div className="card-header" id={"menu_heading" + this.state.headerData.length}>
                                            <h5 className="mb-0" onClick={() => this.manageAccordian(this.state.headerData.length)}>
                                                <button className="btn btn-link text-left" data-toggle="collapse" data-target={"#menu_collapse" + this.state.headerData.length} aria-expanded="true" aria-controls={"menu_collapse" + this.state.headerData.length}>
                                                        {local.addrMyAccount[this.state.language]}<span className=" custom_plus" style={{display: this.state.headerData.length != 0 ? "inline" : "none"}}>+</span>
                                                        <span className="custom_minus" style={{display: this.state.headerData.length == 0 ? "inline" : "none"}}>-</span>
                                                </button>
                                            </h5>
                                        </div>
                                        <div id={"menu_collapse" + this.state.headerData.length} className={this.state.headerData.length == 0 ? "collapse show" : "collapse"} aria-labelledby={"menu_heading" + this.state.headerData.length} data-parent={"#menu_accordion"}>
                                                <div className="card-body login_outer">
                                                    {this.state.loginDetails &&  <span className="dropdown-item">{local.headerHi[this.state.language]} {this.state.loginDetails?this.state.loginDetails.firstname:""}</span>}
                                                    {this.state.loginDetails && <a className="dropdown-item" href="JavaScript:void(0);" onClick={() => this.goToMyaccount("profile")}>{local.headerMyProfile[this.state.language]}</a>}
                                                    {this.state.loginDetails &&  <a className="dropdown-item" href="JavaScript:void(0);" onClick={() => this.goToMyaccount("orders")}>{local.headerMyOrders[this.state.language]}</a>}
                                                    {this.state.loginDetails &&  <a className="dropdown-item" href="JavaScript:void(0);" onClick={() => this.goToMyaccount("wishlist")}>{local.headerWishList[this.state.language]}</a>}
                                                      <a className="dropdown-item" href="JavaScript:void(0);">{local.headerHelpAndSupport[this.state.language]}</a>

                                                     {this.state.loginDetails && <a className="dropdown-item" onClick={this.logout.bind(this)} href="#">{local.headerLogout[this.state.language]}</a>} 
                                                     {!this.state.loginDetails && <a className="dropdown-item" onClick = {this.toggleLogin.bind(this)} href="#" data-toggle="modal" data-target="#login_popup">{local.headerLogin[this.state.language]}</a> } 
                                                </div>
                                        </div>
                                </div>
                            </div>
                            <div className="bottom_links">
                                <ul>
                                    <li><a href="Javascript:void(0);" onClick={() => this.goTOStoreLocator()}>{local.headerStoreLocator[this.state.language]}</a></li>
                                    <li><a href="Javascript:void(0);">{local.headerHelpAndSupport[this.state.language]}</a></li>
                                    <li><a href="Javascript:void(0);">{local.headerAboutUs[this.state.language]}</a></li>
                                    <li><a href="Javascript:void(0);">{local.headerBlogs[this.state.language]}</a></li>
                                    <li><a href="Javascript:void(0);">{local.headerFaq[this.state.language]}</a></li>
                                </ul>
                                <p><a href="Javascript:void(0);">{local.headerTermsOfUse[this.state.language]}</a> | <a href="Javascript:void(0);">{local.headerPrivacyPolicy[this.state.language]}</a></p>
                            </div> 
                        </div>
					</div>
                    <div className="navigaton">
							<span onClick={this.changeLanguage.bind(this)} className="lang d-sm-inline-block d-md-none">{local.headerLanguage[this.state.language]}</span>
                            <form className="search_form dropdown align-middle common_dropdown">
                                <button className="btn search align-middle dropdown-toggle" id="search_box" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><svg className="icon icon-magnifying-glass"><use xlinkHref="/img/icon-sprite.svg#icon-magnifying-glass"></use></svg></button>
                                <div className="dropdown-menu" aria-labelledby="search_box">
                                   <div className="inner">
                                        {/*<input className="form-control" type="search" placeholder="Search" aria-label="Search" onChange={() => this.getSearchData()} />*/}
                                        {<input className="form-control" placeholder="Search" value={this.state.searchData.value} aria-label="Search" onChange={e => this.getSearchData(e.target.value)} onKeyDown={(e) => this.searchSubmit(e)} />}
                                        {
                                            this.state.showRecentSearchesSection && this.state.searchData.recentSearches.length ? (<div className="serch_output">
                                                <p>{local.headerRecentSearches[this.state.language]}</p>
                                                <ul>
                                                    {
                                                        this.state.searchData.recentSearches.slice(Math.max(this.state.searchData.recentSearches.length - 5, 0)).reverse().map(data => (
                                                            <li><a href="Javascript:void(0);" onClick={() => this.goToRecentSearch(data)}>{data}</a></li>
                                                        ))
                                                    }
                                                </ul>
                                            </div>) : ""
                                        }
                                        {
                                            this.state.searchData.data.length ? (<div className="serch_output">
                                                <p>{local.headerSearches[this.state.language]}</p>
                                                <ul>
                                                    {
                                                        this.state.searchData.data.map(data => (
                                                            <li><a href="Javascript:void(0);" onClick={() => this.selectSearch(data.suggestion_type, data.value, data.id)} dangerouslySetInnerHTML={{__html:data.label}}></a></li>
                                                        ))
                                                    }
                                                </ul>
                                            </div>) : ""
                                        }
                                   </div>
                                </div>
                            </form>
                            <div className="dropdown align-middle d-none d-md-inline-block login_dropdown common_dropdown">
                                <button className="btn search align-middle dropdown-toggle" id="dropdown_link" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><svg className="icon icon-login"><use xlinkHref="/img/icon-sprite.svg#icon-login"></use></svg></button>
                                <div className="dropdown-menu" aria-labelledby="dropdown_link">
                              {this.state.loginDetails &&  <span className="dropdown-item">{local.headerHi[this.state.language]} {this.state.loginDetails?this.state.loginDetails.firstname:""}</span>}
                                {this.state.loginDetails && <a className="dropdown-item" href="JavaScript:void(0);" onClick={() => this.goToMyaccount("profile")}>{local.headerMyProfile[this.state.language]}</a>}
                                {this.state.loginDetails &&  <a className="dropdown-item" href="JavaScript:void(0);" onClick={() => this.goToMyaccount("orders")}>{local.headerMyOrders[this.state.language]}</a>}
                                {this.state.loginDetails &&  <a className="dropdown-item" href="JavaScript:void(0);" onClick={() => this.goToMyaccount("wishlist")}>{local.headerWishList[this.state.language]}</a>}
                                  <a className="dropdown-item" href="JavaScript:void(0);">{local.headerHelpAndSupport[this.state.language]}</a>

                                 {this.state.loginDetails && <a className="dropdown-item" onClick={this.logout.bind(this)} href="#">{local.headerLogout[this.state.language]}</a>} 
                                 {!this.state.loginDetails && <a className="dropdown-item" onClick = {this.toggleLogin.bind(this)} href="#" data-toggle="modal" data-target="#login_popup">{local.headerLogin[this.state.language]}</a> } 
                                  
                                </div>
                            </div>
                        <a href="#" onClick={this.moveToCart.bind(this)} className="cart_bag align-middle"><svg className="icon icon-shopping-bag"><use xlinkHref="/img/icon-sprite.svg#icon-shopping-bag"></use></svg><span>{this.state.cartCount}</span></a>

                    </div>
                </nav>
    		</div>
    	);
    }
}

function mapStateToProps(state) {
    let headerData = {
        mainMenu: '',
        subMenu: ''
    };
    
    if(state.headerReducer){
        headerData.mainMenu = state.headerReducer;
    }
    if(state.subMenuReducer){
        headerData.subMenu = state.subMenuReducer;
    }
    
    return {
        headerData: headerData,

        searchData: state.headerSearchReducer.data,

        LoginReducer : state.LoginReducer,
        cartReducer : state.cartReducer,
        placeOrderReducer : state.placeOrderReducer,
        countryReducer : state.countryReducer,
        InvalidTokenReducer : state.InvalidTokenReducer,
        languageReducer : state.languageReducer,
        brandMenuReducer : state.brandMenuReducer


    }
}

export default connect(mapStateToProps)(withStyles (styles)(Header));

