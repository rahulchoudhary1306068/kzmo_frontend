import React, { Component } from 'react';
import {connect} from 'react-redux';

export default class Map extends Component {
    
   constructor(props){
       super(props)
       this.state = {
            markers:[],
            mymap :"",
            lat : 23.7224509,
            lng : 46.7893653,
            marker: ""
       }
      
   }         
 

    componentDidMount(){
        
        
        this.setState({
            lat : this.props.lat,
            lng : this.props.lng
        },()=>{
            
            this.initMap()
        })
        //
    }
    componentWillReceiveProps(nextprops){
        if(nextprops.lat!== this.props.lat){
            
            this.setState({
                lat : nextprops.lat,
                lng : nextprops.lng
            },()=>{
                //this.state.marker.setMap(null);
                //this.putMarker({lat:this.state.lat,lng:this.state.lng}, true,'Your Location','Address Loading','');
                this.initMap()
            })

        }
        
    }
    sendAddress(data){
        this.props.address(data);
    }
    geocodePosition=(pos)=>
    {
       let geocoder = new google.maps.Geocoder();
        

        geocoder.geocode({'latLng': pos},(results, status)=> {
            if (status == google.maps.GeocoderStatus.OK) {
               
                if (results[0]) {
                    
                    var city = '';
                    var cityLevel2 = '';
                    var state = '';
                    var country = '';
                    var postalCode = '';
                    var postalCodeSuffix = '';
                    var address = '';
                    var comma = '';
                    var addressComponents = results[0].address_components;
                    for(let i=0;i<addressComponents.length;i++) {
                        if(addressComponents[i].types[0] == "postal_code_suffix") {
                            postalCodeSuffix = addressComponents[i].long_name;
                        } else if (addressComponents[i].types[0] == "postal_code"){
                            postalCode = addressComponents[i].long_name;
                        } else if (addressComponents[i].types[0] == "locality"){
                            city = addressComponents[i].long_name;
                        } else if (addressComponents[i].types[0] == "administrative_area_level_1"){
                            state = addressComponents[i].long_name;
                        } else if (addressComponents[i].types[0] == "administrative_area_level_2"){
                            cityLevel2 = addressComponents[i].long_name;
                        } else if (addressComponents[i].types[0] == "country"){
                            country = addressComponents[i].long_name;
                        } else {
                            address = address + comma + addressComponents[i].long_name;
                            comma = ", ";
                        }
                    }
                    this.updatepositiom({lat:results[0].geometry.location.lat(),lng:results[0].geometry.location.lng(),"city":city,"houseNo":address})
                    console.log(89898,city)
                    let data = {"postCode":postalCode,"state":state,"city":city,"houseNo":address,'country':country}
                   
                   this.sendAddress(data);
                }
            }
            else {console.log("No result available.")}
        });
    }
    
    updatepositiom(data){
        //console.log(data);
        this.props.updateLatLong(data)
    }
      
    
    initMap=()=> {

        let myLatLng = {lat: this.state.lat, lng: this.state.lng}
        
        let value =new google.maps.Map(document.getElementById('Map'), {
            zoom: 5,
            center: myLatLng
        })
        
        this.setState({
            mymap :  value
        })
      
       
        setTimeout(()=>{
            
            this.putMarker(myLatLng, true,'Your Location','Address Loading','');
        },100)
        
        this.geocodePosition(myLatLng);
    }

    putMarker=(myLatLng,draggable,title,info,markerIcon)=> {
        
        let marker = new google.maps.Marker(
                {
                    map:this.state.mymap,
                    draggable:draggable,
                    animation: google.maps.Animation.DROP,
                    position: myLatLng,
                    title: title,
                    
                });
                this.setState({
                    marker:marker
                })
        if(draggable) {
            google.maps.event.addListener(marker, 'dragend', ()=>
            {
                //console.log(marker.getPosition())
                
                this.geocodePosition(marker.getPosition());
            });
        }
       
        this.state.markers.push(marker);
    }



    render() { 
        return (
            <div>
                <div style={{height :"500px"}} className="map_section position-relative" id="Map"></div>
               
            </div>
        )
    }
}