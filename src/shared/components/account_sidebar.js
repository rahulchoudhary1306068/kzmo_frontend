import React from 'react';
import {connect} from 'react-redux';
import Notifications from 'react-notify-toast';
import local, {deafultLanguage} from '../localization';
import {getter} from '../commonfunction';

class Sidebar extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            language: ''
        };
    }
    componentDidMount(){
        this.setState({
            language: getter('language') ? getter('language') : deafultLanguage
        })
    }
    componentWillReceiveProps(nextProps){
        if(nextProps.languageReducer.value){
            this.setState({
                    language: nextProps.languageReducer.value
            })  
        }
    }
    render() {
        return (
            <nav className="sidebar_container">
               <ul className="list">
                    <li className="list_head d-md-none d-sm-block">{local.addrMyAccount[this.state.language]}</li>
                    <li className={this.props.viewType == "profile" ? "item active" : "item"}><a href="Javascript:void(0);" onClick={() => this.props.setView("profile")}>{local.headerMyProfile[this.state.language]}</a></li>
                    <li className={this.props.viewType == "orders" ? "item active" : "item"}><a href="Javascript:void(0);" onClick={() => this.props.setView("orders")}>{local.headerMyOrders[this.state.language]}</a></li>
                    <li className={this.props.viewType == "wishlist" ? "item active" : "item"}><a href="Javascript:void(0);" onClick={() => this.props.setView("wishlist")}>{local.wishliMyWishList[this.state.language]}</a></li>
                    <li className={this.props.viewType == "reward-points" ? "item active" : "item"}><a href="Javascript:void(0);" onClick={() => this.props.setView("reward-points")}>{local.barMyKZMOPoints[this.state.language]}</a></li>
                    <li className={this.props.viewType == "address-book" ? "item active" : "item"}><a href="Javascript:void(0);" onClick={() => this.props.setView("address-book")}>{local.barAddressBook[this.state.language]}</a></li>
                    <li className={this.props.viewType == "rate-purchase" ? "item active" : "item" }><a href="Javascript:void(0);" onClick={() => this.props.setView("rate-purchase")}>{local.barRateYourPurchases[this.state.language]}</a></li>
                    {/*<li className="item"><a href="Javascript:void(0);">My Fav Stores</a></li>*/}
                    <li className="item common_tooltip_outer"><a href="Javascript:void(0);">{local.headerHelpAndSupport[this.state.language]} <span className="common_tooltip down">{local.orderComingSoon[this.state.language]}</span></a></li>
               </ul> 
            </nav>
        );
    }
}

function mapStateToProps(state) {
    return {
		languageReducer : state.languageReducer
    }
}

export default connect(mapStateToProps)(Sidebar);