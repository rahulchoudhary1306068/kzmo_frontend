import React, { Component } from 'react';
import {connect} from 'react-redux'
import local, {deafultLanguage} from '../localization';
import {getter} from '../commonfunction';

class ErrorBoundary extends React.Component {
    constructor(props) {
      super(props);
      this.state = { 
          hasError: false,
          language: ''
       };
    }
  
    // static getDerivedStateFromError(error) {
    //   // Update state so the next render will show the fallback UI.
    //   return { hasError: true };
    //   console.log("call error drived")
    // }
    goHome(){
        this.setState({
            hasError:false
        })
        this.props.history.push('/')
    }
    componentDidMount(){
      this.setState({
				language: getter('language') ? getter('language') : deafultLanguage
			})
    }
    componentWillReceiveProps(nextProps){
      if(nextProps.languageReducer.value){
				this.setState({
						language: nextProps.languageReducer.value
				}) 
			}
    }
    componentDidCatch(error, info) {
        // Update state so the next render will show the fallback UI.
        this.setState({hasError:true})
      // You can also log the error to an error reporting service
     // logErrorToMyService(error, info);
    }
  
    render() {
        
      if (this.state.hasError) {
        // You can render any custom fallback UI

        return<div className="thankyou_outer text-center"> <h1>{local.errorSomethingWentWrong[this.state.language]}</h1><button className=" continue btn" onClick={this.goHome.bind(this)}>{local.errorGoToHomePage[this.state.language]}</button></div>;

      }
  
      return this.props.children; 
    }
  }
function mapStateToProps(state) {
    return {
				userInfo: state.userInfoReducer,
				languageReducer : state.languageReducer
    }
}

export default connect(mapStateToProps)(ErrorBoundary);
