import React, { Component } from 'react';
import {connect} from 'react-redux';

export default class LocatorMap extends Component {

   constructor(props){
       super(props)
       this.state = {
            markers:[],
            mymap :"",
            lat : 23.7224509,
            lng : 46.7893653,
            marker: "",
            addresses:""
       }

   }


    componentDidMount(){


        this.setState({
            lat : this.props.lat,
            lng : this.props.lng
        },()=>{

            this.initMap();
            this.setState({locatorList : this.props.address});

        })
        //
    }
    componentWillReceiveProps(nextprops){
        if(nextprops.lat!== this.props.lat){

            this.setState({
                lat : nextprops.lat,
                lng : nextprops.lng
            },()=>{

                this.initMap();

            })


        }
        this.setState({addresses:nextprops.address },()=>{this.initMap()});

    }
    /*Address(data){
        this.props.address(data);
    }*/
  /*  geocodePosition=(pos)=>
    {
       let geocoder = new google.maps.Geocoder();


        geocoder.geocode({'latLng': pos},(results, status)=> {
            if (status == google.maps.GeocoderStatus.OK) {

                if (results[0]) {
                    this.updatepositiom({lat:results[0].geometry.location.lat(),lng:results[0].geometry.location.lng()})
                    var city = '';
                    var cityLevel2 = '';
                    var state = '';
                    var country = '';
                    var postalCode = '';
                    var postalCodeSuffix = '';
                    var address = '';
                    var comma = '';
                    var addressComponents = results[0].address_components;
                    for(let i=0;i<addressComponents.length;i++) {
                        if(addressComponents[i].types[0] == "postal_code_suffix") {
                            postalCodeSuffix = addressComponents[i].long_name;
                        } else if (addressComponents[i].types[0] == "postal_code"){
                            postalCode = addressComponents[i].long_name;
                        } else if (addressComponents[i].types[0] == "locality"){
                            city = addressComponents[i].long_name;
                        } else if (addressComponents[i].types[0] == "administrative_area_level_1"){
                            state = addressComponents[i].long_name;
                        } else if (addressComponents[i].types[0] == "administrative_area_level_2"){
                            cityLevel2 = addressComponents[i].long_name;
                        } else if (addressComponents[i].types[0] == "country"){
                            country = addressComponents[i].long_name;
                        } else {
                            address = address + comma + addressComponents[i].long_name;
                            comma = ", ";
                        }
                    }
                    console.log(89898,city)
                    let data = {"postCode":postalCode,"state":state,"city":city,"houseNo":address,'country':country}

                   this.sendAddress(data);
                }
            }
            else {console.log("No result available.")}
        });
    }
*/
    /*updatepositiom(data){
        //console.log(data);
        this.props.updateLatLong(data)
    }*/


    initMap=()=> {
        var map;
        
        var mapOptions = {
            mapTypeId: 'roadmap'
        };

        var b = [];
        let myLatLng = {lat: this.state.lat, lng: this.state.lng}
         // Display a map on the web page
        map = new google.maps.Map(document.getElementById("LocatorMap"), mapOptions);
        map.setTilt(50);
        map.setCenter(myLatLng);

        var markers = this.state.addresses.data;

        // Add multiple markers to map
        var infoWindow = new google.maps.InfoWindow(), marker, i;
        var popupData = new Array();
        // Place each marker on the map
        if(markers !== undefined && markers !== null){
            for( i = 0; i < Object.keys(markers).length; i++ ) {
                popupData.push('<p><h6>'+markers[i].name+'</h6></p><p>'+markers[i].address+'</p><p> Opens at '+markers[i].from_time+'</p>');
                //console.log(popupData,'add data');
                var position = new google.maps.LatLng(markers[i].latitude, markers[i].longitude);
                var bounds = new google.maps.LatLngBounds(position);
                bounds.extend(position);
                marker = new google.maps.Marker({
                    position: position,
                    map: map,
                    title: markers[i].name
                });

                // Add info window to marker
                google.maps.event.addListener(marker, 'click', (function(marker, i) {
                    return function() {
                        infoWindow.setContent(popupData[i]);
                        infoWindow.open(map, marker);
                    }
                })(marker, i));

                // Center the map to fit all markers on the screen
               
               
                map.fitBounds(bounds);
            }
        }

        // Set zoom level
        var boundsListener = google.maps.event.addListener((map), 'bounds_changed', function(event) {
            this.setZoom(5);
            google.maps.event.removeListener(boundsListener);
        });
    }

    render() {
        return (
            <div>
                <div style={{height :"500px"}} className="map_section position-relative" id="LocatorMap"></div>

            </div>
        )
    }
}
