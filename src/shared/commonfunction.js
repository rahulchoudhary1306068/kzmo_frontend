import {api_KEY,MicroSiteUrl} from './constant';
import local, { deafultLanguage } from './localization';
 
const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

export const months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
export const days = ["Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"];

export function checkRequired(value, language){
    // console.log(language)
    if(value){
        return [true]
    }
    else{
        return [false, local.addrErrorFieldCannotBeBlank[language]]
    }
}
export function checkCharacterCount(text,count,message){
    if(((text.trim()).length) >= count){
        return [true]
    }
    else{
        return [false,message]
    }
}

export function checkPhonenumber(value, language){
    if(value){
        if(!isNaN(value)){
            if(value.length ==10){
                return [true]
            }
            else{
                return [false, local.logErrorPhoneMustBeGreater[language]]
            }
        }
        else{
            return [false, local.logErrorPhoneMustContain[language]]
        }
        
    }
    else{
        return [false, local.logErrorPhoneCannotBeBlank[language]]
    }
}
export function checkPassword(value, language){
    if(value){
        
        if(value.length >= 6){
            return [true]
        }
        else{
            return [false, local.logErrorPassMustBeGreater[language]]
        }
        
        
        
    }
    else{
        return [false, local.logErrorPassword[language]]
    }
}
export function checkEmail(value, language){
    if(value){
                 
        if(re.test(String(value).toLowerCase())){
            return [true]
        }
        else{
            return [false, local.logErrorEnterValidEmail[language]]
         }
    }
    else{
        return [false, local.logErrorEmailCannotBeBlank[language]]
    }
}
export function checkconfirmPassword(value,newpassword, language){
    if(value){
        
        if(value.length > 6){
            if(value===newpassword){
                return [true]
            }
            else
            {
                return [false, local.logErrorConfirmPass[language]]
            }
        }
        else{
            return [false, local.logErrorPassMustBeGreater[language]]
        }
        
        
        
    }
    else{
        return [false, local.logErrorPassword[language]]
    }

}

export function checkuserName(value, language){
    if(value){
        if(isNaN(value))
        {
            if(re.test(String(value).toLowerCase())){
                return [true]
            }
            else{
                return [false,local.logErrorEnterValidEmail[language]]
             }
        }
        else{
            if(value.length ==10){
                return [true]
            }
            else{
                return [false,local.logErrorPhoneNumber[language]]
            }
            
        }

    }
    else{
        return [false, local.logErrorUsername[language]]
    }
}

export function getUserstatus(){
    if(localStorage.getItem('userId')){
        return ['user_id',localStorage.getItem('userId')]
    }    
    else if(localStorage.getItem('sessionId')){
        return ['session_id',localStorage.getItem('sessionId')]
    }
    else{
        return false
    }    
    
}

export  function getter(key){
   return localStorage.getItem(key)
}

export function setter(key,value){
    return localStorage.setItem(key,value)
}

export function localClear(key){
    return localStorage.removeItem(key);
}

export function createLocalSessionId() {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
  
    for (var i = 0; i < 5; i++)
      text += possible.charAt(Math.floor(Math.random() * possible.length));

    var number1 = Math.floor(Math.random() * 100000);  
  
    return ['session_id',text+number1];
  }

  export function checkPincode(value, language){
      return [true]
    if(value){
        if(!isNaN(value)){
            if(value.length == 5 || value.length == 6){
                return [true]
            }
            else{
                return [false, local.addrErrorPincodeMustBeGreater[language]]
            }
        }
        else{
            return [false, local.addrErrorPinContainNumbers[language]]
        }
        
    }
    else{
        return [false, local.addrErrorPinCannotBeBlank[language]]
    }
}

export function setObject(key,object){
  
    console.log(JSON.stringify(object));
    return localStorage.setItem(key,JSON.stringify(object))
}
export function getObject(key){
    return JSON.parse(localStorage.getItem(key))
}

export function createSessionId() {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
  
    for (var i = 0; i < 5; i++)
      text += possible.charAt(Math.floor(Math.random() * possible.length));

    var number1 = Math.floor(Math.random() * 100000);  
  
    return text+number1;
  }

  export function createToken() {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789dfsdfdsfsf45454";
  
    for (var i = 0; i < 5; i++)
      text += possible.charAt(Math.floor(Math.random() * possible.length));

    var number1 = Math.floor(Math.random() * 100000);  
  
    return text+number1;
    // let rawToken = api_KEY+getter('userId')+createTtl();
    // let hash = CryptoJS.HmacSHA256(rawToken,'123').toString(CryptoJS.enc.Hex);
    // return hash;
  }

  export function createTtl() {
    return Math.round(new Date().getTime()/1000);
  }

  export function getUserAgent(){
      return "navigator.userAgent";
  }

  export function createMD5(value){
    return CryptoJS.MD5(value).toString(CryptoJS.enc.Hex);
  }

export function getUserLoginToken(){
    let userInfo = JSON.parse(localStorage.getItem("userObject"));
    return userInfo.data.token;     
}

export function getUserTTLToken(){
    let userInfo = JSON.parse(localStorage.getItem("userObject"));
    return userInfo.data.ttl;     
}

export function getUserInfo(){
    let userInfo = JSON.parse(localStorage.getItem("userObject"));
    return userInfo;     
}
export function languageForAPI(){
    //var result =document.cookie.split(';');
    //var final = result[1].split('=')[1]
    //return final;
    
    //return "EN";
    let value = getter('language')?getter('language'):deafultLanguage;
    if(value=='en'){
        return 'EN';
    }
    else{
        return 'AR';
    }
}
export function calculateDiscount(price,listprice){
    let discountPercentage = parseInt(((listprice - price) / listprice) * 100);
    
    if(isNaN(discountPercentage)){
        return 
    }
    return discountPercentage;
}
export function convertTimeToAM(timeString){
    let H = +timeString.substr(0, 2);
    let h = (H % 12) || 12;
    let ampm = H < 12 ? " am" : " pm";
    timeString = h + timeString.substr(2, 3) + ampm;

    return timeString;
}

export function redirectToMicrosite(name){
        //console.log("http://"+name+MicroSiteUrl);
       return window.open("http://"+name+MicroSiteUrl);
}

export function getDayIndex(day){
    return day===0?6:day-1;
}