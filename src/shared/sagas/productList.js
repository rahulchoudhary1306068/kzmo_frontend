import 'fetch-everywhere';

import {END} from 'redux-saga';
import { channel } from 'redux-saga'
import {takeEvery, select, call, put,take,fork, actionChannel} from 'redux-saga/effects';
import { base_URL, api_KEY, base_url_shopclues, api_key_shopclues, delivery_detail_base_url} from '../constant'
import {sagaMiddleware} from '../../shared/config';
import rootSaga from './index';


import { PRODUCTLIST_ACTION_RESULT, PRODUCTLIST_ACTION_RETRY, PRODUCTLIST_ACTION_ERROR,PRODUCTID_SEONAME_ACTION_RESULT,PRODUCTID_SEONAME_ACTION_RETRY, PRODUCTID_SEONAME_ACTION_ERROR } from '../actions/productList';

const PRODUCT_LIST_URL = base_URL + "category?key=" + api_KEY + "&cat_id=";

const SEARCH_URL = base_URL + "search?key=" + api_KEY + "&q=";
const SEO_NAME_URL = base_URL + "property?key="+ api_KEY +"&type=seoname";

export const getProductId = (data)=>{
    return fetch(SEO_NAME_URL + "&name="+data.seo_name);
};
export function* fetchProductIdBySeoName(action) {
    try {
        const response = yield call(getProductId, action.payload);
        const productList = yield response.json();
        if(response.status === 200)
        {
            yield put({type: PRODUCTID_SEONAME_ACTION_RESULT, data: productList});
        }        
        else{
            yield put({type: PRODUCTID_SEONAME_ACTION_ERROR, message: productList});
        }
    }
    catch (e) {
        yield put({type: PRODUCTID_SEONAME_ACTION_RETRY, message: e.message});
    }
    yield put(END);
    yield sagaMiddleware.run(rootSaga);
}

export const getProductList = (data) => {
    
    
    if(data.pageType == "plp"){
        return fetch(PRODUCT_LIST_URL + data.catId + "&page=" + data.page + data.paramStr+"&lang_code="+data.lang_code);
    }
    else{
       
        return fetch(SEARCH_URL + data.catId + "&page=" + data.page + data.paramStr+"&lang_code="+data.lang_code);
    }
};

export function* fetchProductListData(action) {
    try {
        const response = yield call(getProductList, action.payload);
        const productList = yield response.json();
        if(response.status === 200)
        {
            yield put({type: PRODUCTLIST_ACTION_RESULT, data: productList});
        }        
        else{
            yield put({type: PRODUCTLIST_ACTION_ERROR, message: productList});
        }
    }
    catch (e) {
        yield put({type: PRODUCTLIST_ACTION_RETRY, message: e.message});
    }
    yield put(END);
    yield sagaMiddleware.run(rootSaga);
}

