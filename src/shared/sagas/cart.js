import 'fetch-everywhere';

import {END} from 'redux-saga';

import { call, put } from 'redux-saga/effects';
import { base_URL, api_KEY, base_url_shopclues, api_key_shopclues, base_URL_SM } from '../constant'
import { sagaMiddleware } from '../config';
import rootSaga from '.';

import { notify } from 'react-notify-toast';

import { getUserstatus, setter, createLocalSessionId, localClear,languageForAPI } from '../commonfunction';

import { addtocart_ACTION_RESULT, addtocart_ACTION_RETRY, addtocart_ACTION_ERROR, getcart_ACTION_RESULT, getcart_ACTION_RETRY, getcart_ACTION_ERROR, deleteproductfromcart_ACTION_RESULT, deleteproductfromcart_ACTION_RETRY, deleteproductfromcart_ACTION_ERROR, cartcouponcode_ACTION_RESULT, cartcouponcode_ACTION_RETRY, cartcouponcode_ACTION_ERROR, addToWishlist_ACTION_RESULT, addToWishlist_ACTION_ERROR, addToWishlist_ACTION_RETRY } from '../actions/cart';

import local from '../localization'

const ADDTOCART_URL = base_URL+"atom/cart/addProductToCart?key="+api_KEY; 

const GETCART_URL = base_URL+"atom/cart/getCart?key="+api_KEY; 

const DELETEFROMCART_URL = base_URL+"atom/cart/deleteProductFromCart?key="+api_KEY; 

const COUPONCODE_URL = base_URL+"atom/cart/applyPromotions?key="+api_KEY;

const ADDTOWISHLIST_URL = base_URL_SM + "wishlist/addProductToWishlist?key=" + api_KEY;

export const getaddToCart = (userStatus,productdata) => {
        
   var data = {
         [userStatus[0]]:userStatus[1],
         "platform":"D",
         "cart_service_id":1,
         "return_cart" :1,
         "get_cod_fee" : 1,
         "product_data":productdata
     }
    // var data = {
    //     [userStatus[0]]:userStatus[1],
    //     "platform":"D",
    // "cart_service_id":1,
    // "return_cart" :1,
    
	   //    "product_data":{"280855":{"product_id":280855,"amount":5,"product_options":{"25067":"431035"},"upsert":1}}
    // }


    return fetch(ADDTOCART_URL,{
        method: "POST",
        body: JSON.stringify(data),
        headers: {
          "Content-Type": "application/json",
        }
    });

    
};


export function* fetchaddtocartData(action) {
    try {
        let userStatus = yield call(getUserstatus);
        if(!userStatus){
            userStatus = yield call(createLocalSessionId);
            yield call(setter,'sessionId',userStatus[1]);
        }
        const response = yield call(getaddToCart,userStatus,action.payload);
        const result = yield response.json();
        
        if(response.status === 200) { 
            if(result.message){
                notify.show(result.message,"error");
                yield put({type: addtocart_ACTION_ERROR, message: result.message});
            }
            else{
                // notify.show("Product added in cart","success");
                yield put({type: addtocart_ACTION_RESULT, data: result});
            }
        }  
        else {
            notify.show(result.message,"error");
            yield put({type: addtocart_ACTION_ERROR, message: result.message});
        }
    }
    catch (e) {
        yield put({type: addtocart_ACTION_RETRY, message: e.message});
    }
    yield put(END);
    yield sagaMiddleware.run(rootSaga);
}

export const getCart = (userStatus,payloadData) => {


    let data;
    if(payloadData){
        data = {
            session_id:payloadData.session_id,
            user_id : payloadData.user_id,
           "cart_service_id":1,
           "platform":"D",
           "payment_option_id":"",
           "get_cod_fee" : 1,
           "lang_code" : languageForAPI()
           
      }
    }
    else{
        data = {
            [userStatus[0]]:userStatus[1],
           "cart_service_id":1,
           "platform":"D",
           "payment_option_id":"",
           "get_cod_fee" : 1,
           "lang_code" : languageForAPI()
           
      }
       
    }
    

    return fetch(GETCART_URL+"&lang_code="+languageForAPI(),{
        method: "POST",
        body: JSON.stringify(data),
        headers: {
          "Content-Type": "application/json",
          
          
        }
    });

    
};


export function* fetchgetcartData(action) {
    try {
        
        let userStatus = yield call(getUserstatus);
        if(!userStatus){
            
            yield put({type: getcart_ACTION_RESULT,data:{},message: "No Product in cart" });
        }
        else{
            const response = yield call(getCart,userStatus,action.payload);
            const result = yield response.json();
            
            if(response.status === 200)
                { 
                    yield put({type: getcart_ACTION_RESULT, data: result});
                    // below we clear guest user session Id
                    if(action.payload){
                        yield call(localClear,'sessionId');
                        yield call (setter,'userId',action.payload.user_id)
                    }                
            
                }
                    
            else {
                yield put({type: getcart_ACTION_ERROR, message: result.data.message});
            }
        }
        
    }
    catch (e) {
        yield put({type: getcart_ACTION_RETRY, message: e.message});
    }
    yield put(END);
    yield sagaMiddleware.run(rootSaga);
}

export const deletefromCart = (userStatus,productdata) => {
        
    
    var data = {
          [userStatus[0]]:userStatus[1],
         "platform":"D",
         "cart_service_id":1,
         "return_cart" :1,
         "product_data":[productdata]
   
         
     }

    return fetch(DELETEFROMCART_URL,{
        method: "POST",
        body: JSON.stringify(data),
        headers: {
          "Content-Type": "application/json",
          
          
        }
    });

    
};
export function* deleteproductfromcart(action){
    try {
        
        let userStatus = yield call(getUserstatus);
        if(!userStatus){
           // userStatus = yield call(createLocalSessionId);
           // yield call(setter,'sessionId',userStatus[1]);
        }
        const response = yield call(deletefromCart,userStatus,action.payload);
        const result = yield response.json();
        
        if(response.status === 200)
            { 
                yield put({type: deleteproductfromcart_ACTION_RESULT, data: result});
                yield put({type:"getcart_ACTION"});
                
        
            }
                
        else {
            yield put({type: deleteproductfromcart_ACTION_ERROR, message: result});
        }
    }
    catch (e) {
        yield put({type: deleteproductfromcart_ACTION_RETRY, message: e.message});
    }
    yield put(END);
    yield sagaMiddleware.run(rootSaga);
}



export const couponCodeCart = (userStatus,value) => {
        
    var data = {
      
          [userStatus[0]]:userStatus[1],
         "platform":"D",
         "cart_service_id":1,
         "return_cart" :1,
         "coupon_codes":value,
         "get_cod_fee" : 1,
         
   
         
     }

    return fetch(COUPONCODE_URL,{
        method: "POST",
        body: JSON.stringify(data),
        headers: {
          "Content-Type": "application/json",
          
          
        }
    });

    
};
export function* cartcouponcode(action){
    try {
        
        let userStatus = yield call(getUserstatus);
        if(!userStatus){
           // userStatus = yield call(createLocalSessionId);
           // yield call(setter,'sessionId',userStatus[1]);
        }
        const response = yield call(couponCodeCart,userStatus,action.payload);
        const result = yield response.json();
        
        if(response.status === 200)
            { 
                yield put({type: cartcouponcode_ACTION_RESULT, data: result});
                
        
            }
                
        else {
            yield put({type: cartcouponcode_ACTION_ERROR, message: result});
        }
    }
    catch (e) {
        yield put({type: cartcouponcode_ACTION_RETRY, message: e.message});
    }
    yield put(END);
    yield sagaMiddleware.run(rootSaga);
}

export const addToWishlist = (userStatus, productdata) => {
        
    let data = {
        [userStatus[0]]:userStatus[1],
        "platform": "D",
        "cart_service_id": 2,
        "product_data": productdata
    }
    console.log("datadatawishlist", data);
    return fetch(ADDTOWISHLIST_URL,{
        method: "POST",
        body: JSON.stringify(data),
        headers: {
          "Content-Type": "application/json",
        }
    });

    
};


export function* fetchaddToWishlist(action) {
    // console.log("rajat", Object.keys(action.payload)[0]);
    let key = Object.keys(action.payload)[0];
    let lang = action.payload[key].language;
    try {
        let userStatus = yield call(getUserstatus);
        if(!userStatus){
            userStatus = yield call(createLocalSessionId);
            yield call(setter, 'sessionId', userStatus[1]);
        }
        const response = yield call(addToWishlist, userStatus, action.payload);
        const result = yield response.json();

        if(response.status === 200) { 
            if(result.message){
                yield put({type: addToWishlist_ACTION_ERROR, message: result.message});
            }
            else{
                notify.show(local.wishliAddedInWishlist[lang], "success");
                yield put({type: addToWishlist_ACTION_RESULT, data: result});
            }
        }  
        else {
            yield put({type: addToWishlist_ACTION_ERROR, message: result.message});
        }
    }
    catch (e) {
        yield put({type: addToWishlist_ACTION_RETRY, message: e.message});
    }
    yield put(END);
    yield sagaMiddleware.run(rootSaga);
}