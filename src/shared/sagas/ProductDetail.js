import 'fetch-everywhere';

import {END} from 'redux-saga';
import { channel } from 'redux-saga'
import {takeEvery, select, call, put,take,fork, actionChannel} from 'redux-saga/effects';
import { base_URL, api_KEY, base_url_shopclues, api_key_shopclues, delivery_detail_base_url,base_URL_SM} from '../constant'
import {sagaMiddleware} from '../../shared/config';
import rootSaga from './index';

import { notify } from 'react-notify-toast';

import { PRODUCTDETAIL_ACTION_RESULT, PRODUCTDETAIL_ACTION_RETRY, PRODUCTDETAIL_ACTION_ERROR 
,   GETNEARSTORE_ACTION_RESULT,  GETNEARSTORE_ACTION_RETRY,  GETNEARSTORE_ACTION_ERROR
,GETEDD_ACTION,GETEDD_ACTION_RESULT,GETEDD_ACTION_ERROR,GETEDD_ACTION_RETRY
} from '../actions/ProductDetail';

import { PINCODE_ACTION_RESULT, PINCODE_ACTION_RETRY, PINCODE_ACTION_ERROR } from '../actions/ProductDetail';

import { GET_SIBLING_ID_RESULT, GET_SIBLING_ID_RETRY, GET_SIBLING_ID_ERROR } from '../actions/ProductDetail';

const PRODUCT_URL = base_URL + "products_info?key=" + api_KEY + "&product_id=";

const PINCODE_URL = delivery_detail_base_url + "?pincode_no=";

const SPECIFICATION_URL = base_URL + "product_features_new?key=" + api_KEY;

const REVIEW_URL = base_URL + "reviewdetails?key=" + api_KEY + "&object_type=P&useful=1&rated=1&detail_rating=1&pid=";

const ADD_PRODUCT_REVIEW_URL = base_URL + "add_review?key=" + api_KEY;

const GET_IMAGE_URL = base_URL + "colorvariant?key=" + api_KEY;

const GET_NEARSTORE_URL = base_URL_SM + "/order/getStorewiseStock?key=" + api_KEY;

const GET_SIBLING_ID_URL = base_URL + "sibling_features?key=" + api_KEY;

// url for edd api - dinesh
const GET_EDD_URL = base_URL + "atom/order/calculateEDDForPDP?key=" + api_KEY;

export const getProductDetail = (data) => {
    return fetch(PRODUCT_URL + data.prodId+"&lang_code="+data.lang);
};

export const getDeliveryDetail = (data) => {
    return fetch(PINCODE_URL + data.pincode + "&product_ids=" + data.prodId);
};

export const getProductSpecification = (data) => {
    return fetch(SPECIFICATION_URL + "&product_id=" + data.prodId+"&lang_code="+data.lang);
}

export const getProductReviews = (data) => {
    return fetch(REVIEW_URL + data.prodId+"&lang_code="+data.lang);
}

export function* fetchProductDetailData(action) {
    try {
        //console.log(4444);
        //console.log(action.payload);return;
        let prodInfo;
        const response = yield call(getProductDetail, action.payload);
        const resultProductDetailPageStructure = yield response.json();
        if(response.status === 200 && resultProductDetailPageStructure.status == "success")
        {
            let response = resultProductDetailPageStructure.response;
            response['specification'] = [];
            response['reviews'] = [];
            // yield put({type: PRODUCTDETAIL_ACTION_RESULT, data: {type: "details" ,data: response}});
            /*yield call(fetchProductReview, action.payload);
            return yield call(fetchProductSpecification, {payload: action.payload});*/
            let prodInfo = response;

            const specificationResponse = yield call(getProductSpecification, action.payload);
            const productSpecification = yield specificationResponse.json();
            if(specificationResponse.status === 200 && productSpecification.status == 1){
                /*yield put({type: PRODUCTDETAIL_ACTION_RESULT, data: {type: "specification" ,data: productSpecification.data}});*/
                prodInfo["specification"] = productSpecification.data;
            }

            const reviewsResponse = yield call(getProductReviews, action.payload);
            const productReviews = yield reviewsResponse.json();
            if(reviewsResponse.status === 200 && productReviews.status == 1){
                /*yield put({type: PRODUCTDETAIL_ACTION_RESULT, data: {type: "reviews" ,data: productReviews.data}});*/
                prodInfo["reviews"] = productReviews.data;
            }

            yield put({type: PRODUCTDETAIL_ACTION_RESULT, data: prodInfo});
        }        
        else{
            yield put({type: PRODUCTDETAIL_ACTION_ERROR, message: resultProductDetailPageStructure});
        }
    }
    catch (e) {
        yield put({type: PRODUCTDETAIL_ACTION_RETRY, message: e.message});
    }
    yield put(END);
    yield sagaMiddleware.run(rootSaga);
}

export function* fetchProductSpecification(action) {
    try {
        const response = yield call(getProductSpecification, action.payload);
        const productSpecification = yield response.json();
        if(response.status === 200 && productSpecification.status == 1){
            yield put({type: PRODUCTDETAIL_ACTION_RESULT, data: {type: "specification" ,data: productSpecification.data}});
        }
        else{
            yield put({type: PRODUCTDETAIL_ACTION_ERROR, message: productSpecification});
        }
    }
    catch (e) {
        yield put({type: PRODUCTDETAIL_ACTION_RETRY, message: e.message});
    }
    yield put(END);
    yield sagaMiddleware.run(rootSaga);
}

export function* fetchProductReview(prodId) {
    try {
        const response = yield call(getProductReviews, prodId);
        const productReviews = yield response.json();
        if(response.status === 200 && productReviews.status == 1){
            yield put({type: PRODUCTDETAIL_ACTION_RESULT, data: {type: "reviews" ,data: productReviews.data}});
        }
        else{
            yield put({type: PRODUCTDETAIL_ACTION_ERROR, message: productReviews});
        }
    }
    catch (e) {
        yield put({type: PRODUCTDETAIL_ACTION_RETRY, message: e.message});
    }
    yield put(END);
    yield sagaMiddleware.run(rootSaga);
}

export function* fetchDeliveryDetailData(action) {
    try {
        const response = yield call(getDeliveryDetail, action.payload);
        const resultDeliveryDetail = yield response.json();
        if(response.status === 200)
        {
            return yield put({type: PINCODE_ACTION_RESULT, data: resultDeliveryDetail});
        }        
        else{
            yield put({type: PINCODE_ACTION_ERROR, message: resultDeliveryDetail});
        }
    }
    catch (e) {
        yield put({type: PINCODE_ACTION_RETRY, message: e.message});
    }
    yield put(END);
    yield sagaMiddleware.run(rootSaga);
}

export const addProductReview = (data) => {
    return fetch(ADD_PRODUCT_REVIEW_URL, {
        method: "POST",
        body: JSON.stringify(data),
        headers: {
          "Content-Type": "application/json",
        }
    });
};

export function* fetchAddProductReview(action) {
    try {
        const response = yield call(addProductReview, action.payload);
        const addReviewDetail = yield response.json();
        if(response.status === 200)
        {
            notify.show(addReviewDetail.response.message, "success");
            return yield call(fetchProductReview, action.payload.object_id)
        }        
        else{
            notify.show(addReviewDetail.response.message, "error");
        }
    }
    catch (e) {
        //error
    }
    yield put(END);
    yield sagaMiddleware.run(rootSaga);
}

export const getProductImage = (data) => {
    return fetch(GET_IMAGE_URL + "&product_id=" + data.product_id + "&option_id=" + data.option_id + "&variant_id=" + data.variant_id);
}

export function* fetchGetProductImage(action) {
    try {
        const response = yield call(getProductImage, action.payload);
        const imageGallery = yield response.json();
        if(response.status === 200)
        {
            yield put({type: PRODUCTDETAIL_ACTION_RESULT, data: {type: "images", data: imageGallery.data}});
        }        
        else{
            yield put({type: PRODUCTDETAIL_ACTION_ERROR, message: imageGallery});
        }
    }
    catch (e) {
        yield put({type: PRODUCTDETAIL_ACTION_RETRY, message: e.message});
    }
    yield put(END);
    yield sagaMiddleware.run(rootSaga);
}

export const getNearStore = (value) => {
 let data = {
    "product_id" : value.prodId,
    "combination" : value.combination,
    "platform":"D",
    "lang_code":value.lang_code
 }
    return fetch(GET_NEARSTORE_URL, {
        method: "POST",
        body: JSON.stringify(data),
        headers: {
          "Content-Type": "application/json",
        }
    });
};

export function* fetchNearStore(action) {
    try {
        const response = yield call(getNearStore, action.payload);
        const nearstore = yield response.json();
        if(response.status === 200)
        {
            yield put({type: GETNEARSTORE_ACTION_RESULT, data: nearstore});
        }        
        else{
            yield put({type: GETNEARSTORE_ACTION_ERROR, message: "error"});
        }
    }
    catch (e) {
        yield put({type: GETNEARSTORE_ACTION_RETRY, message: e.message});
    }
    yield put(END);
    yield sagaMiddleware.run(rootSaga);
}

export const getSiblingId = (data) => {
    return fetch(GET_SIBLING_ID_URL + "&product_id=" + data.productId + "&getSiblingProduct=1&selectedSfString=" + data.str);
};

export function* fetchSiblingId(action) {
    try {
        const response = yield call(getSiblingId, action.payload);
        const siblingData = yield response.json();
        if(response.status === 200 && siblingData.status == "1")
        {
            yield put({type: GET_SIBLING_ID_RESULT, data: siblingData.data});
        }        
        else{
            yield put({type: GET_SIBLING_ID_ERROR, message: "error"});
        }
    }
    catch (e) {
        yield put({type: GET_SIBLING_ID_RETRY, message: e.message});
    }
    yield put(END);
    yield sagaMiddleware.run(rootSaga);
}

//EDD api - Dinesh /
export const getEdd = (value) => {
    let data = {
        "product_id" : value.prodId,
        "combination" : value.combination,
        "to_city" :value.to_city,
        "platform":"D",
        "lang_code":value.lang_code,
        "device_id":"12312132333"
    }

    return fetch(GET_EDD_URL, {
        method: "POST",
        body: JSON.stringify(data),
        headers: {
            "Content-Type": "application/json",
        }
    });
};

export function* fetchEdd(action) {
    try {
        const response = yield call(getEdd, action.payload);
        const edd = yield response.json();
        if(response.status === 200)
        {
            yield put({type: GETEDD_ACTION_RESULT, data: edd});
        } 
        else{
            yield put({type: GETEDD_ACTION_ERROR, message: "error"});
        }
    }
    catch (e) {
        yield put({type: GETEDD_ACTION_RETRY, message: e.message});
    }
    yield put(END);
    yield sagaMiddleware.run(rootSaga);
}