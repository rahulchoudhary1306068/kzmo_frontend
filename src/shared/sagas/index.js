import {takeEvery} from 'redux-saga/effects';

import { EXAMPLE_ACTION, second_ACTION } from '../actions/example';
import { HOME_ACTION } from '../actions/home';

import { PRODUCTDETAIL_ACTION, PINCODE_ACTION, ADD_PRODUCT_REVIEW_ACTION, GET_PRODUCT_IMAGE_ACTION,GETNEARSTORE_ACTION, GET_SIBLING_ID_ACTION,GETEDD_ACTION } from '../actions/ProductDetail';

import { PRODUCTLIST_ACTION,PRODUCTID_SEONAME_ACTION } from '../actions/productList';

import { fetchLoginData,OTPVerify,ResendOTP,LoginOTP,ForgotOTP,ForgotVerifyOTP,UpdatePassword, InvalidToken } from './login';
import { Login_ACTION, OTPVerify_ACTION,ResendOTP_ACTION,LoginOTP_ACTION,ForgotOTP_ACTION,ForgotVerifyOTP_ACTION, UpdatePassword_ACTION, InvalidToken_ACTION } from '../actions/login'

import { HEADER_ACTION, HEADER_SEARCH, Country_ACTION, CITY_ACTION, STORE_ACTION,UPDATE_LANG_ACTION } from '../actions/header';

import { fetchExample, fetchExample_second } from './example';
import { fetchHomeData } from './home';
import { fetchHeaderSearchData, fetchHeaderData, fetchCountryData, fetchCityData, fetchStoreData, UpdateLangaugeData } from './header';
import { fetchProductDetailData, fetchDeliveryDetailData, fetchAddProductReview, fetchGetProductImage,fetchNearStore, fetchSiblingId,fetchEdd } from './ProductDetail';
import { fetchProductListData,fetchProductIdBySeoName } from './productList';


import { fetchMallList, fetchLocator } from './locator';
import { MALL_ACTION ,LOCATOR_ACTION} from '../actions/locator';



//amit section start here
import { addtocart_ACTION, getcart_ACTION, deleteproductfromcart_ACTION, cartcouponcode_ACTION, addToWishlist_ACTION } from '../actions/cart';
import { fetchaddtocartData, fetchgetcartData, deleteproductfromcart, cartcouponcode, fetchaddToWishlist } from './cart';
import { Checkout_ACTION, PaymentOption_ACTION,placeOrder_ACTION,codOTP_ACTION,orderConfirm_ACTION } from '../actions/checkout';
import { fetchCheckout,fetchPaymentOption,placeOrder,getCodOTP,orderConfirm } from './checkout';

import { UserProfile_ACTION,UserBank_ACTION,UserBank_ADD_ACTION,UserBank_EDIT_ACTION,EditProfile_ACTION,RemoveProfile_ACTION,UserBank_All_Bank_ACTION } from '../actions/userprofile';

import { fetchUserProfile, editUserProfile,removeUserProfile,fetchUserBank,addUserBankGenerator,editUserBankGenerator,fetchAllBanks } from './userprofile';

import { USER_INFO_ACTION, EDIT_USER_INFO_ACTION, REWARD_POINTS_ACTION, ORDER_ACTION, WISHLIST_ACTION,
 WISHLIST_REMOVE_ACTION, ADD_TO_CART_FROM_WISHLIST_ACTION, CANCEL_ORDER_ACTION, RATE_PURCHASE_ACTION,
  CHANGE_USER_PASSWORD_ACTION, ORDER_DETAIL_ACTION, ORDER_REVIEW_SUBMIT_ACTION, HELP_ISSUE_ACTION, 
  HELP_SUB_ISSUE_ACTION, HELP_SUB_SUB_ISSUE_ACTION, SUBMIT_NEED_HELP_ACTION, ORDER_STATUS_ACTION, 
  RETURN_LIST_ACTION, CANCEL_RETURN_ACTION, RETURN_DETAIL_ACTION, REFUND_DETAIL_ACTION, 
  CREATE_RETURN_DETAIL_ACTION, SUBMIT_RETURN_ACTION, CANCEL_ORDER_FROM_DETAIL_ACTION } from '../actions/myAccount';

import { fetchUserInfo, fetchEditUserInfo, fetchRewardPoints, fetchUserOrders, fetchWishlist, fetchRemoveWishlist, fetchaddtocartFromWishlist, fetchCancelOrder, fetchUserReviews, fetchChangeUserPassword, fetchOrderDetail, fetchSubmitOrderReviews, fetchHelpIssues, fetchHelpSubIssues, fetchHelpSubSubIssues, fetchSubmitNeedHelp, fetchOrderStatus, fetchReturnList, fetchCancelReturn, fetchReturnDetail, fetchRefundDetail, fetchCreateReturnDetail, fetchSubmitReturnDetail } from './myAccount';


//amit section end

export default function* rootSaga() {
	// combine all saga functions here
    yield takeEvery(HOME_ACTION, fetchHomeData);
    yield takeEvery(Login_ACTION, fetchLoginData);
    yield takeEvery(EXAMPLE_ACTION, fetchExample);
    yield takeEvery(second_ACTION, fetchExample_second);
    yield takeEvery(HEADER_ACTION, fetchHeaderData);
    yield takeEvery(HEADER_SEARCH, fetchHeaderSearchData);

    yield takeEvery(PRODUCTDETAIL_ACTION, fetchProductDetailData);
    yield takeEvery(GET_SIBLING_ID_ACTION, fetchSiblingId);
    yield takeEvery(PINCODE_ACTION, fetchDeliveryDetailData);
    yield takeEvery(ADD_PRODUCT_REVIEW_ACTION, fetchAddProductReview);

    yield takeEvery(PRODUCTLIST_ACTION, fetchProductListData);
    // amit section
    yield takeEvery(addtocart_ACTION, fetchaddtocartData);
    yield takeEvery(getcart_ACTION, fetchgetcartData);
    yield takeEvery(deleteproductfromcart_ACTION, deleteproductfromcart);
    yield takeEvery(cartcouponcode_ACTION, cartcouponcode);
    yield takeEvery(Checkout_ACTION, fetchCheckout);
    yield takeEvery(UserProfile_ACTION, fetchUserProfile);
    yield takeEvery(PaymentOption_ACTION,fetchPaymentOption);
    yield takeEvery(EditProfile_ACTION, editUserProfile);
    yield takeEvery(placeOrder_ACTION, placeOrder);
    yield takeEvery(Country_ACTION, fetchCountryData);
    yield takeEvery(CITY_ACTION, fetchCityData);
    yield takeEvery(STORE_ACTION, fetchStoreData);

    yield takeEvery(OTPVerify_ACTION, OTPVerify);
    yield takeEvery(ResendOTP_ACTION,ResendOTP);
    yield takeEvery(LoginOTP_ACTION,LoginOTP);

    yield takeEvery(addToWishlist_ACTION, fetchaddToWishlist);

    yield takeEvery(USER_INFO_ACTION, fetchUserInfo);
    yield takeEvery(EDIT_USER_INFO_ACTION, fetchEditUserInfo);
    yield takeEvery(REWARD_POINTS_ACTION, fetchRewardPoints);
    yield takeEvery(codOTP_ACTION, getCodOTP);

    yield takeEvery(RemoveProfile_ACTION, removeUserProfile);

	yield takeEvery(ORDER_ACTION, fetchUserOrders);

    yield takeEvery(WISHLIST_ACTION, fetchWishlist);
    yield takeEvery(WISHLIST_REMOVE_ACTION, fetchRemoveWishlist);
    yield takeEvery(ADD_TO_CART_FROM_WISHLIST_ACTION, fetchaddtocartFromWishlist);
	yield takeEvery(CANCEL_ORDER_ACTION, fetchCancelOrder);
    yield takeEvery(RATE_PURCHASE_ACTION, fetchUserReviews);
    yield takeEvery(CHANGE_USER_PASSWORD_ACTION, fetchChangeUserPassword);
    yield takeEvery(ORDER_DETAIL_ACTION, fetchOrderDetail);
    yield takeEvery(ForgotOTP_ACTION, ForgotOTP);
    yield takeEvery(ForgotVerifyOTP_ACTION, ForgotVerifyOTP);
    yield takeEvery(UpdatePassword_ACTION, UpdatePassword);
    yield takeEvery(GET_PRODUCT_IMAGE_ACTION, fetchGetProductImage);

    yield takeEvery(InvalidToken_ACTION,InvalidToken);
    yield takeEvery(ORDER_REVIEW_SUBMIT_ACTION, fetchSubmitOrderReviews);
    yield takeEvery(HELP_ISSUE_ACTION, fetchHelpIssues);
    yield takeEvery(HELP_SUB_ISSUE_ACTION, fetchHelpSubIssues);
    yield takeEvery(HELP_SUB_SUB_ISSUE_ACTION, fetchHelpSubSubIssues);
    yield takeEvery(SUBMIT_NEED_HELP_ACTION, fetchSubmitNeedHelp);
    yield takeEvery(UPDATE_LANG_ACTION, UpdateLangaugeData);
    yield takeEvery(ORDER_STATUS_ACTION, fetchOrderStatus);
    yield takeEvery(RETURN_LIST_ACTION, fetchReturnList);
    yield takeEvery(CANCEL_RETURN_ACTION, fetchCancelReturn);
    yield takeEvery(RETURN_DETAIL_ACTION, fetchReturnDetail);
    yield takeEvery(REFUND_DETAIL_ACTION, fetchRefundDetail);
    yield takeEvery(orderConfirm_ACTION, orderConfirm);
    yield takeEvery(CREATE_RETURN_DETAIL_ACTION, fetchCreateReturnDetail);

    yield takeEvery(SUBMIT_RETURN_ACTION, fetchSubmitReturnDetail);

    yield takeEvery(GETNEARSTORE_ACTION, fetchNearStore);

    yield takeEvery(UserBank_ACTION, fetchUserBank);
    yield takeEvery(UserBank_ADD_ACTION, addUserBankGenerator);
    yield takeEvery(UserBank_EDIT_ACTION, editUserBankGenerator);
    yield takeEvery(UserBank_All_Bank_ACTION, fetchAllBanks);
    // new binding for edd - dinesh
    //yield takeEvery(GETEDD_ACTION, fetchEdd);
    yield takeEvery(LOCATOR_ACTION, fetchLocator);
    yield takeEvery(MALL_ACTION, fetchMallList);

    // end here
    yield takeEvery(CANCEL_ORDER_FROM_DETAIL_ACTION, fetchCancelOrder);
    yield takeEvery(GETEDD_ACTION, fetchEdd);
    yield takeEvery(PRODUCTID_SEONAME_ACTION, fetchProductIdBySeoName);
}
