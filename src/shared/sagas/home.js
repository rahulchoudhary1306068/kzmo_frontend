import 'fetch-everywhere';

import {END} from 'redux-saga';
import { channel } from 'redux-saga'
import {takeEvery, select, call, put,take,fork, actionChannel} from 'redux-saga/effects';
import {base_URL,api_KEY} from '../constant'
import {sagaMiddleware} from '../../shared/config';
import rootSaga from './index';

import { getter } from '../commonfunction';
import { deafultLanguage } from '../localization';

import { HOME_ACTION_RESULT, HOME_ACTION_RETRY, HOME_ACTION_ERROR } from '../actions/home';
import { HOME_SECTION_RESULT, HOME_SECTION_RETRY, HOME_SECTION_ERROR } from '../actions/home';
//
const EXAMPLE_URL = base_URL+"desktop_structure?type=H&key="+api_KEY; 
//`http://api.jazle.com/api/v11/desktop_structure?type=H&key=JD5646trt5gdff3dinesh098uhg&type=H`;
const Section_URL = base_URL+"new_deals?key="+api_KEY+"&gid=";
//`http://api.jazle.com/api/v11/new_deals?key=JD5646trt5gdff3dinesh098uhg&gid=`;
export const getExample = (language) => {
    return fetch(EXAMPLE_URL+"&lang_code="+language);
};
export const getSections = (id,language) => {
    return fetch(Section_URL + id+"&lang_code="+language);
};

export function* fetchHomeData(action) {
    try {
        const response = yield call(getExample,action.payload);
        const resultHomePageStructure = yield response.json();
        if(response.status === 200 && resultHomePageStructure.status===1)
        { 
            // yield put({type: HOME_ACTION_RESULT, data: resultHomePageStructure});
            const responses = yield resultHomePageStructure.data.map((value,index) => {
                return call(fetchHomeSectionData, value,index,action.payload)
            })
        }
        else {
            yield put({type: HOME_ACTION_ERROR, message: resultHomePageStructure});
        }
    }
    catch (e) {
        yield put({type: HOME_ACTION_RETRY, message: e.message});
    }
    yield put(END);
    yield sagaMiddleware.run(rootSaga);
}

export function* fetchHomeSectionData(data,index,language) {
    try {
        const response = yield call(getSections,data.id,language);
        const result = yield response.json();
        result[0]['index']=index; // Appened array index in result.
        result[0]['section']=data.position; // append section type in 0th result always
        console.log("myfinal result",result);
        yield put({type: HOME_ACTION_RESULT, data: result})
    }
    catch (e) { 
        yield put({type: HOME_ACTION_RETRY, message: e.message});
    }
    yield put(END);
    yield sagaMiddleware.run(rootSaga);
}