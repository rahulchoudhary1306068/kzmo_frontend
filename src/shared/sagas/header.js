import 'fetch-everywhere';

import {END} from 'redux-saga';
import { channel } from 'redux-saga'
import {takeEvery, select, call, put,take,fork, actionChannel} from 'redux-saga/effects';


import {sagaMiddleware} from '../../shared/config';
import rootSaga from './index';

import { base_URL, api_KEY, base_url_shopclues, api_key_shopclues, developerUrl, token_URL } from '../constant'

import {languageForAPI} from '../commonfunction'

import { HEADER_ACTION_RESULT, HEADER_ACTION_RETRY, HEADER_ACTION_ERROR,UPDATELANG_ACTION_RESULT
    ,BrandMenu_ACTION_RESULT, BrandMenu_ACTION_RETRY, BrandMenu_ACTION_ERROR
} from '../actions/header';
import { HEADER_SUBMENU_RESULT, HEADER_SUBMENU_RETRY, HEADER_SUBMENU_ERROR } from '../actions/header';
import { HEADER_SEARCH_RESULT, HEADER_SEARCH_RETRY, HEADER_SEARCH_ERROR, Country_ACTION_RESULT, Country_ACTION_RETRY, Country_ACTION_ERROR, CITY_ACTION_RESULT, CITY_ACTION_ERROR, CITY_ACTION_RETRY, STORE_ACTION_RESULT, STORE_ACTION_ERROR, STORE_ACTION_RETRY } from '../actions/header';

const HEADER_URL = base_URL + "group_list?key=" + api_KEY;

const SUB_MENU_URL = base_URL + "navigation_list?key=" + api_KEY + "&gid=";

const SEARCH_URL = base_URL + "autosuggest?key=" + api_KEY + "&z=1&term=";
const Country_URL = base_URL + "atom/getcountrycity/getlist?key=" + api_KEY + "&error=1&platform=D";

const CITY_URL = base_URL + "atom/getcountrycity/getlist?key=" + api_KEY + "&error=1&platform=D&country_code=";

const STORE_URL = developerUrl + "merchant/offline/address";

const Token_URL = token_URL;
const brandMenuUrl = base_URL+"brand_list?key="+api_KEY;

export const getHeader = (language) => {

    return fetch(HEADER_URL+"&lang_code="+languageForAPI());
};

export const getSubmenu = (id) => {
    
    return fetch(SUB_MENU_URL+id+"&lang_code="+languageForAPI());
};

export const getSearchData = (searchTerm) => {
    
    return fetch(SEARCH_URL + searchTerm+"&lang_code="+languageForAPI());
};
export const getBrandMenu = () => {
    return fetch(brandMenuUrl+"&lang_code="+languageForAPI())
}

export function* fetchHeaderData(action) {
    try {
        const response = yield call(getHeader,action.payload);
        const resultHeaderStructure = yield response.json();
        if(response.status === 200)
            { 
                yield put({type: HEADER_ACTION_RESULT, data: resultHeaderStructure});
                const responses = yield resultHeaderStructure.response.map((value,index) => {
                    return call(fetchHeaderSubmenuData, value,index)
                })

                const brandMenuResponse = yield call(getBrandMenu);
                const brandMenuResult = yield brandMenuResponse.json();
               if(brandMenuResult.status == 200){
                   yield put({type : BrandMenu_ACTION_RESULT,data:brandMenuResult})
               }
               else{
                yield put({type : BrandMenu_ACTION_RESULT,message:brandMenuResult.message})
               }
            }
                
        else {
            yield put({type: HEADER_ACTION_ERROR, message: resultHeaderStructure});
        }
    }
    catch (e) {
        yield put({type: HEADER_ACTION_RETRY, message: e.message});
    }
    yield put(END);
    yield sagaMiddleware.run(rootSaga);
}

export function* fetchHeaderSubmenuData(data,index) {
    try {
        const response = yield call(getSubmenu, data.menu_group_id);
        const temp = yield response.json();
        const result = yield temp['response'];
        result['index']=index;
        yield put({type: HEADER_SUBMENU_RESULT, data: result})
        
    }
    catch (e) { 
        yield put({type: HEADER_SUBMENU_RETRY, message: e.message});
    }
    yield put(END);

    yield sagaMiddleware.run(rootSaga);
}

export function* fetchHeaderSearchData(action) {
    try {
        const response = yield call(getSearchData, action.payload);
        const resultHeaderStructure = yield response.json();
        if(response.status === 200)
        {
            yield put({type: HEADER_SEARCH_RESULT, data: resultHeaderStructure});
        }       
        else {
            yield put({type: HEADER_SEARCH_ERROR, message: resultHeaderStructure});
        }
    }
    catch (e) {
        yield put({type: HEADER_SEARCH_RETRY, message: e.message});
    }
    yield put(END);
	yield sagaMiddleware.run(rootSaga);
}

export const getCountryData = () => {
    return fetch(Country_URL+"&lang_code="+languageForAPI());
};

export function* fetchCountryData(action) {
    try {
        const response = yield call(getCountryData);
        const result = yield response.json();
        if(result.status == 200)
            { 
                yield put({type: Country_ACTION_RESULT, data: result});
       
            }
                
        else {
            yield put({type: Country_ACTION_ERROR, message: result});
        }
    }
    catch (e) {
        yield put({type: Country_ACTION_RETRY, message: e.message});
    }
    yield put(END);
    yield sagaMiddleware.run(rootSaga);
}

export const getCityData = (countryId) => {
    
    return fetch(CITY_URL + countryId+"&lang_code="+languageForAPI());
};

export function* fetchCityData(action) {
    try {
        const response = yield call(getCityData, action.payload);
        const result = yield response.json();
        if(result.status == 200)
            { 
                yield put({type: CITY_ACTION_RESULT, data: result});
       
            }
                
        else {
            yield put({type: CITY_ACTION_ERROR, message: result});
        }
    }
    catch (e) {
        yield put({type: CITY_ACTION_RESULT, message: e.message});
    }
    yield put(END);
    yield sagaMiddleware.run(rootSaga);
}

export const getToken = () =>  {
    var data = JSON.stringify({
        "client_id": "01RX33H3LTQIUZXSAPJ1AFUU34K1X6OW",
        "client_secret": "_X7HMB78MMV&!VPAIQH9^_1HIDFZ8+WI75X8RI+33^RBI",
        "grant_type": "client_credentials"
      });
    return fetch(Token_URL,{
        method: "POST",
        body: data,
        
        headers: {
          "Content-Type": "application/json",
        }
    });
}


export const getStoreData = (cityId, tokenValue, tokenType) => {
    let data = {
        "city_id": cityId,
        "lang_code" : languageForAPI()
    }

    return fetch(STORE_URL,{
        method: "POST",
        body: JSON.stringify(data),
        headers: {
          "Content-Type": "application/json",
          "Authorization": tokenType + " " + tokenValue
        }
    });
};

export function* fetchStoreData(action) {
    try {
        const tokenResponse = yield call(getToken);
        const token = yield tokenResponse.json();
        console.log("tokannnnj", token);
        if(tokenResponse.status === 200)
        { 
            const tokenValue=token.access_token;
            const tokenType= token.token_type;

            const response = yield call(getStoreData, action.payload, tokenValue, tokenType);
            const result = yield response.json();
            if(result.code == 200)
                { 
                    yield put({type: STORE_ACTION_RESULT, data: result});
                }
                    
            else {
                yield put({type: STORE_ACTION_ERROR, message: result});
            }
        }
    }
    catch (e) {
        yield put({type: STORE_ACTION_RESULT, message: e.message});
    }
    yield put(END);
    yield sagaMiddleware.run(rootSaga);
}
export function* UpdateLangaugeData(action) {
    
    yield put({type: UPDATELANG_ACTION_RESULT, data: action.payload})
    yield put(END);
    yield sagaMiddleware.run(rootSaga);
}