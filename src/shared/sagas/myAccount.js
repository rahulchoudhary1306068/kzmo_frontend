import 'fetch-everywhere';

import { END } from 'redux-saga';
import { channel } from 'redux-saga'
import { takeEvery, select, call, put, take, fork, actionChannel } from 'redux-saga/effects';
import { sagaMiddleware } from '../../shared/config';
import rootSaga from './index';

import { notify } from 'react-notify-toast';

import { base_URL, api_KEY, base_URL_SM, base_url_SM_API, developerUrlSec, client_id, client_secret, REFRESH_TOKEN_URL } from '../constant';

import { getUserLoginToken, getUserInfo, getter, setter , languageForAPI } from '../commonfunction';

import { USER_INFO_RESULT, USER_INFO_RETRY, USER_INFO_ERROR } from '../actions/myAccount';
import { REWARD_POINTS_RESULT, REWARD_POINTS_RETRY, REWARD_POINTS_ERROR } from '../actions/myAccount';
import { ORDER_ACTION_RESULT, ORDER_ACTION_ERROR, ORDER_ACTION_RETRY, CANCEL_ORDER_FROM_DETAIL_ACTION } from '../actions/myAccount';
import { ORDER_DETAIL_RESULT, ORDER_DETAIL_ERROR, ORDER_DETAIL_RETRY } from '../actions/myAccount';
import { WISHLIST_ACTION_RESULT, WISHLIST_ACTION_ERROR, WISHLIST_ACTION_RETRY } from '../actions/myAccount';
import { RATE_PURCHASE_ACTION_RESULT, RATE_PURCHASE_ACTION_ERROR, RATE_PURCHASE_ACTION_RETRY } from '../actions/myAccount';
import { ORDER_REVIEW_SUBMIT_ACTION_RESULT, ORDER_REVIEW_SUBMIT_ACTION_ERROR, ORDER_REVIEW_SUBMIT_ACTION_RETRY } from '../actions/myAccount';

import { HELP_ISSUE_ACTION_RESULT, HELP_ISSUE_ACTION_ERROR, HELP_ISSUE_ACTION_RETRY } from '../actions/myAccount';
import { HELP_SUB_ISSUE_ACTION_RESULT, HELP_SUB_ISSUE_ACTION_ERROR, HELP_SUB_ISSUE_ACTION_RETRY } from '../actions/myAccount';
import { HELP_SUB_SUB_ISSUE_ACTION_RESULT, HELP_SUB_SUB_ISSUE_ACTION_ERROR, HELP_SUB_SUB_ISSUE_ACTION_RETRY } from '../actions/myAccount';
import { SUBMIT_NEED_HELP_ACTION_RESULT, SUBMIT_NEED_HELP_ACTION_ERROR, SUBMIT_NEED_HELP_ACTION_RETRY } from '../actions/myAccount';

import { ORDER_STATUS_ACTION_RESULT, ORDER_STATUS_ACTION_ERROR, ORDER_STATUS_ACTION_RETRY } from '../actions/myAccount';

import { RETURN_LIST_ACTION_RESULT, RETURN_LIST_ACTION_ERROR, RETURN_LIST_ACTION_RETRY } from '../actions/myAccount';

import { RETURN_DETAIL_ACTION_RESULT, RETURN_DETAIL_ACTION_ERROR, RETURN_DETAIL_ACTION_RETRY } from '../actions/myAccount';

import { REFUND_DETAIL_ACTION_RESULT, REFUND_DETAIL_ACTION_ERROR, REFUND_DETAIL_ACTION_RETRY } from '../actions/myAccount';

import { CREATE_RETURN_DETAIL_RESULT, CREATE_RETURN_DETAIL_ERROR, CREATE_RETURN_DETAIL_RETRY } from '../actions/myAccount';

import { SUBMIT_RETURN_RESULT, SUBMIT_RETURN_ERROR, SUBMIT_RETURN_RETRY } from '../actions/myAccount';

import { addtocart_ACTION_RESULT, addtocart_ACTION_RETRY, addtocart_ACTION_ERROR } from '../actions/cart';
import local from '../localization';
const USER_INFO_URL = base_url_SM_API + "myaccount?key=" + api_KEY + "&user_id=";

const EDIT_USER_INFO_URL = base_url_SM_API + "profile_details";

const CHANGE_USER_PASSWORD_URL = base_url_SM_API + "password";

const REWARD_POINT_URL = base_URL_SM + "CluesBucks/getCbHistory?key=" + api_KEY + "&platform=D&user_id=";

const ORDER_URL = base_url_SM_API + "orders/?key=" + api_KEY;

const ORDER_DETAIL_URL = base_url_SM_API + "trackingorder?key=" + api_KEY;

const WISHLIST_URL = base_URL_SM + "wishlist/getWishlist?key=" + api_KEY;

const WISHLIST_REMOVE_URL = base_URL_SM + "wishlist/removeProductFromWishlist?key=" + api_KEY+"&platform=D";

const ADDTOCART_URL = base_URL+"atom/cart/addProductToCart?key="+api_KEY;

const CANCEL_ORDER_URL = base_url_SM_API + "orders";

const GET_REVIEW_URL = base_URL_SM + "feedbacksAndRatings/getfeedbacks?key=" + api_KEY;
// const GET_REVIEW_URL = "https://test-api.jazle.com/api/v11/atom/feedbacksAndRatings/getfeedbacks?key=d42121c70dda5edfgd1df6633fdb36c0";

const SUBMIT_ORDER_REVIEW_URL = base_URL_SM + "feedbacksAndRatings/addfeedbacks?key=" + api_KEY;

const HELP_ISSUE_URL = base_url_SM_API + "customercare?key=" + api_KEY + "&user_id=";

const HELP_SUB_ISSUE_URL = base_url_SM_API + "customercare?key=" + api_KEY + "&user_id=";

const HELP_SUB_SUB_ISSUE_URL = base_url_SM_API + "customercare?key=" + api_KEY + "&user_id=";

const SUBMIT_NEED_HELP_URL = base_url_SM_API + "customercare";

const ORDER_STATUS_URL = developerUrlSec + "ordertracking/orderstatus?order_id=";

const RETURN_LIST_URL = base_url_SM_API + "returnrequestlist?key=" + api_KEY;

const CANCEL_RETURN_URL = base_url_SM_API + "cancelreturn";

const RETURN_DETAIL_URL = base_url_SM_API + "return_form_action?key=" + api_KEY + "&mode=returndetails";

const REFUND_DETAIL_URL = base_URL_SM + "Refund/getRefundDetails?key=" + api_KEY + "&mode=returndetails&platform=D";

const CREATE_RETURN_DETAIL_URL = base_url_SM_API + "returnform?key=" + api_KEY;

const SUBMIT_RETURN_URL = developerUrlSec + "v1/Returns";

export const getUserProfileInfo = (userId, userInfo) => {
    return fetch(USER_INFO_URL + userId + "&ttl=" + userInfo.data.ttl + "&token=" + userInfo.data.token);
};

export function* fetchUserInfo(action) {
    try {
        const userInfo = yield call(getUserInfo);
        const response = yield call(getUserProfileInfo, action.payload, userInfo);
        const resultUserInfo = yield response.json();
        if(response.status === 200)
        {
            return yield put({type: USER_INFO_RESULT, data: resultUserInfo});
        }        
        else{
            yield put({type: USER_INFO_ERROR, message: resultUserInfo});
        }
    }
    catch (e) {
        yield put({type: USER_INFO_RETRY, message: e.message});
    }
    yield put(END);
    yield sagaMiddleware.run(rootSaga);
}

export const editUserInfo = (data, userInfo) => {
    data["key"] = api_KEY;
    data["ttl"] = userInfo.data.ttl;
    data["token"] = userInfo.data.token;

    return fetch(EDIT_USER_INFO_URL, {
        method: "PUT",
        body: JSON.stringify(data),
        headers: {
          "Content-Type": "application/json",
        }
    });
};

export function* fetchEditUserInfo(action, ) {
    try {
        const userInfo = yield call(getUserInfo);
        const response = yield call(editUserInfo, action.payload, userInfo);
        const resultUserInfo = yield response.json();
        if(response.status === 200 && resultUserInfo.status != "failed")
        {
            notify.show(resultUserInfo.status,"success");
        }        
        else{
            notify.show(resultUserInfo.msg,"error");
        }
    }
    catch (e) {
        //error
    }
    yield put(END);
    yield sagaMiddleware.run(rootSaga);
}

export const changeUserPassword = (data) => {
    data["key"] = api_KEY;

    return fetch(CHANGE_USER_PASSWORD_URL, {
        method: "PUT",
        body: JSON.stringify(data),
        headers: {
          "Content-Type": "application/json",
        }
    });
};

export function* fetchChangeUserPassword(action) {
    try {
        const response = yield call(changeUserPassword, action.payload);
        const resultUserPass = yield response.json();
        if(response.status === 200 && resultUserPass.status == "Successful.")
        {
            notify.show(resultUserPass.msg, "success");
        }
        else{
            notify.show(resultUserPass.msg, "error");
        }        
        
    }
    catch (e) {
        //error
    }
    yield put(END);
    yield sagaMiddleware.run(rootSaga);
}

export const getRewardPoints = (data) => {
    return fetch( REWARD_POINT_URL + data.userId + "&per_page_record=10&page_number=" + data.currentPage+"&platform =D" );
};

export function* fetchRewardPoints(action) {
    try {
        const response = yield call(getRewardPoints, action.payload);
        const resultUserInfo = yield response.json();
        if(response.status === 200)
        {
            return yield put({type: REWARD_POINTS_RESULT, data: resultUserInfo});
        }        
        else{
            yield put({type: REWARD_POINTS_ERROR, message: resultUserInfo});
        }
    }
    catch (e) {
        yield put({type: REWARD_POINTS_RETRY, message: e.message});
    }
    yield put(END);
    yield sagaMiddleware.run(rootSaga);
}

export const getUserOrders = (userInfo, pageNo) => {
    return fetch( ORDER_URL + "&page=" + pageNo + "&user_id=" + userInfo.data.user_id + "&ttl=" + userInfo.data.ttl + "&token=" + userInfo.data.token);
}

export function* fetchUserOrders(action) {
    try {
        const userInfo = yield call(getUserInfo);
        const response = yield call(getUserOrders, userInfo, action.payload);
        const userOrderInfo = yield response.json();
        if(response.status === 200 && userOrderInfo.status != "failed")
        {
            return yield put({type: ORDER_ACTION_RESULT, data: userOrderInfo});
        }        
        else{
            yield put({type: ORDER_ACTION_ERROR, message: userOrderInfo});
        }
    }
    catch (e) {
        yield put({type: ORDER_ACTION_RETRY, message: e.message});
    }
    yield put(END);
    yield sagaMiddleware.run(rootSaga);
}

export const getOrderDetail = (orderId, userInfo) => {
    return fetch( ORDER_DETAIL_URL + "&order_id=" + orderId + "&email_id=" + userInfo.data.email + "&ttl=" + userInfo.data.ttl + "&token=" + userInfo.data.token);
}

export function* fetchOrderDetail(action) {
    try {
        const userInfo = yield call(getUserInfo);
        const response = yield call(getOrderDetail, action.payload, userInfo);
        const orderDetail = yield response.json();
        if(response.status === 200)
        {
            return yield put({type: ORDER_DETAIL_RESULT, data: orderDetail});
        }        
        else{
            yield put({type: ORDER_DETAIL_ERROR, message: orderDetail});
        }
    }
    catch (e) {
        yield put({type: ORDER_DETAIL_RETRY, message: e.message});
    }
    yield put(END);
    yield sagaMiddleware.run(rootSaga);
}

export const getWishlist = (userId) => {
    let data = {
        "user_id": userId,
        "cart_service_id": 2,
        "platform": "D"
    }
    let language=languageForAPI();
    return fetch(WISHLIST_URL+"&lang_code="+language, {
        method: "POST",
        body: JSON.stringify(data),
        headers: {
          "Content-Type": "application/json",
        }
    });
}

export function* fetchWishlist(action) {
    try {
        const response = yield call(getWishlist, action.payload);
        const resultWishlist = yield response.json();
        if(response.status === 200)
        {
            return yield put({type: WISHLIST_ACTION_RESULT, data: resultWishlist});
        }        
        else{
            yield put({type: WISHLIST_ACTION_ERROR, message: resultWishlist});
        }
    }
    catch (e) {
        yield put({type: WISHLIST_ACTION_RETRY, message: e.message});
    }
    yield put(END);
    yield sagaMiddleware.run(rootSaga);
}

export const removeWishlist = (data) => {
    return fetch(WISHLIST_REMOVE_URL, {
        method: "POST",
        body: JSON.stringify(data),
        headers: {
          "Content-Type": "application/json",
        }
    });
}

export function* fetchRemoveWishlist(action) {
    try {
        const response = yield call(removeWishlist, action.payload);
        const resultWishlist = yield response.json();
        if(response.status === 200)
        { 
            //notify.show(local.removeWishlistMessage[language],"success");
            return yield call(fetchWishlist, {payload: action.payload.user_id}); 
            //yield put({type: WISHLIST_ACTION_RESULT, data: resultWishlist});
        }        
        else{
            yield put({type: WISHLIST_ACTION_ERROR, message: resultWishlist});
        }
    }
    catch (e) {
        yield put({type: WISHLIST_ACTION_RETRY, message: e.message});
    }
    yield put(END);
    yield sagaMiddleware.run(rootSaga);
}

export const getaddToCartFromWishlist = (data) => {

    return fetch(ADDTOCART_URL,{
        method: "POST",
        body: JSON.stringify(data),
        headers: {
          "Content-Type": "application/json",
        }
    });
};

export function* fetchaddtocartFromWishlist(action) {
    try {
        const response = yield call(getaddToCartFromWishlist, action.payload.addToCart);
        const result = yield response.json();
        
        if(response.status === 200) { 
            if(result.message){
                notify.show(result.message,"error");
                yield put({type: WISHLIST_ACTION_ERROR, message: result.message});
            }
            else{ 
                let lang = languageForAPI(),language="";
                if(lang=="EN") language="en";
                else language="sar";
                notify.show(local.wishliAddedInCart[language]);
                yield put({type: addtocart_ACTION_RESULT, data: result});
                yield call(fetchRemoveWishlist, {"payload": action.payload.removeFromWishlist})
            }
        }  
        else {
            notify.show(result.message,"error");
            yield put({type: WISHLIST_ACTION_ERROR, message: result.message});
        }
    }
    catch (e) {
        yield put({type: WISHLIST_ACTION_RETRY, message: e.message});
    }
    yield put(END);
    yield sagaMiddleware.run(rootSaga);
}

export const cancelOrder = (data, loginToken, userInfo) => {
    data["key"] = api_KEY;
    data["ttl"] = userInfo.data.ttl;
    data["token"] = userInfo.data.token;

    return fetch(CANCEL_ORDER_URL,{
        method: "PUT",
        body: JSON.stringify(data),
        headers: {
          "Content-Type": "application/json",
          "Authorization": "Bearer " + loginToken
        }
    });
};

export function* fetchCancelOrder(action) {
    try {
        const userInfo = yield call(getUserInfo);
        let loginToken = yield call(getUserLoginToken);
        const response = yield call(cancelOrder, action.payload, loginToken, userInfo);
        const resultUserInfo = yield response.json();

        if(response.status === 200)
        {
            //return yield call(fetchUserOrders, {payload: action.payload.user_id})
            //by dinesh
            if(action.type === CANCEL_ORDER_FROM_DETAIL_ACTION){
                return yield call(fetchOrderDetail, {payload: action.payload.order_id})
            }else{
                return yield call(fetchUserOrders, {payload: action.payload.user_id})
            }
        }        
        else{
            yield put({type: ORDER_ACTION_ERROR, message: resultUserInfo});
        }
    }
    catch (e) {
        yield put({type: ORDER_ACTION_RETRY, message: e.message});
    }
    yield put(END);
    yield sagaMiddleware.run(rootSaga);
}

export const getUserReviews = (data) => {

    let params = {
        "platform": "D",
        "type": data.type,
        "user_id": data.user_id,
        "info_type": "custom", 
        "field_required": [
            "order_id",
            "status",
            "product_id",
            "product_name",
            "product_image_path",
            "shipping_time",
            "shipping_cost",
            "product_quality",
            "value_for_money",
            "review_merchant",
            "product_rating",
            "review",
            "creation_date",
            "company"
        ],
        "limit":10,
        "page":1
    }

    return fetch(GET_REVIEW_URL,{
        method: "POST",
        body: JSON.stringify(params),
        headers: {
          "Content-Type": "application/json"
        }
    });
};

export function* fetchUserReviews(action) {
    try {
        const response = yield call(getUserReviews, action.payload);
        const resultReviews = yield response.json();
        if(response.status === 200)
        {
            return yield put({type: RATE_PURCHASE_ACTION_RESULT, data: {"type": action.payload.type, "data":resultReviews.response}});
        }        
        else{
            yield put({type: RATE_PURCHASE_ACTION_ERROR, message: resultReviews});
        }
    }
    catch (e) {
        yield put({type: RATE_PURCHASE_ACTION_RETRY, message: e.message});
    }
    yield put(END);
    yield sagaMiddleware.run(rootSaga);
}

export const submitOrderReview = (data) => {

    return fetch(SUBMIT_ORDER_REVIEW_URL,{
        method: "POST",
        body: JSON.stringify(data),
        headers: {
          "Content-Type": "application/json"
        }
    });
};

export function* fetchSubmitOrderReviews(action) {
    try {
        const response = yield call(submitOrderReview, action.payload);
        const submitReviews = yield response.json();
        if(response.status === 200)
        {
            yield put({type: ORDER_REVIEW_SUBMIT_ACTION_RESULT, data: submitReviews.response});
            const userInfo = yield call(getUserInfo);
            yield call(fetchUserReviews, {"payload": {"user_id": userInfo.data.user_id, "type": "completed"}});
            yield call(fetchUserReviews, {"payload": {"user_id": userInfo.data.user_id, "type": "pending"}});
        }        
        else{
            yield put({type: ORDER_REVIEW_SUBMIT_ACTION_ERROR, message: submitReviews});
        }
    }
    catch (e) {
        yield put({type: ORDER_REVIEW_SUBMIT_ACTION_RETRY, message: e.message});
    }
    yield put(END);
    yield sagaMiddleware.run(rootSaga);
}

export const getHelpIssues = (userInfo, orderId) => {
    return fetch( HELP_ISSUE_URL + userInfo.data.user_id + "&ttl=" + userInfo.data.ttl + "&token=" + userInfo.data.token +  "&order_id=" + orderId);
};

export function* fetchHelpIssues(action) {
    try {
        const userInfo = yield call(getUserInfo);
        const response = yield call(getHelpIssues, userInfo, action.payload);
        const helpIssues = yield response.json();
        if(response.status === 200)
        {
            yield put({type: HELP_ISSUE_ACTION_RESULT, data: helpIssues.response})
        }        
        else{
            yield put({type: HELP_ISSUE_ACTION_ERROR, message: helpIssues});
        }
    }
    catch (e) {
        yield put({type: HELP_ISSUE_ACTION_RETRY, message: e.message});
    }
    yield put(END);
    yield sagaMiddleware.run(rootSaga);
}

export const getHelpSubIssues = (userInfo, issueId) => {
    return fetch( HELP_SUB_ISSUE_URL +  userInfo.data.user_id + "&ttl=" + userInfo.data.ttl + "&token=" + userInfo.data.token + "&parent_id=" + issueId);
};

export function* fetchHelpSubIssues(action) {
    try {
        const userInfo = yield call(getUserInfo);
        const response = yield call(getHelpSubIssues, userInfo, action.payload);
        const helpSubIssues = yield response.json();
        if(response.status === 200)
        {
            yield put({type: HELP_SUB_ISSUE_ACTION_RESULT, data: helpSubIssues.response})
        }        
        else{
            yield put({type: HELP_SUB_ISSUE_ACTION_ERROR, message: helpSubIssues});
        }
    }
    catch (e) {
        yield put({type: HELP_SUB_ISSUE_ACTION_RETRY, message: e.message});
    }
    yield put(END);
    yield sagaMiddleware.run(rootSaga);
}

export const getHelpSubSubIssues = (userInfo, subIssueId) => {
    return fetch( HELP_SUB_SUB_ISSUE_URL + userInfo.data.user_id + "&ttl=" + userInfo.data.ttl + "&token=" + userInfo.data.token + "&child_id=" + subIssueId );
};

export function* fetchHelpSubSubIssues(action) {
    try {
        const userInfo = yield call(getUserInfo);
        const response = yield call(getHelpSubSubIssues, userInfo, action.payload);
        const helpSubSubIssues = yield response.json();
        if(response.status === 200 && helpSubSubIssues.status === "success")
        {
            yield put({type: HELP_SUB_SUB_ISSUE_ACTION_RESULT, data: helpSubSubIssues.response})
        }        
        else{
            yield put({type: HELP_SUB_SUB_ISSUE_ACTION_ERROR, message: helpSubSubIssues});
        }
    }
    catch (e) {
        yield put({type: HELP_SUB_SUB_ISSUE_ACTION_RETRY, message: e.message});
    }
    yield put(END);
    yield sagaMiddleware.run(rootSaga);
}

export const submitNeedHelp = (userInfo, data) => {
    let params = {
      "subject": data.issueId,
      "subissues": data.subIssueId,
      "sub_subissues": data.subSubIssueId,
      "message": data.helpText,
      "email": userInfo.data.email,
      "orderid": data.orderId,
      "callme_input": userInfo.data.phone,
      "scumd_m": "0",
      "image": "",
      "callme": "N",
      "user_id": userInfo.data.user_id,
      "ttl": userInfo.data.ttl,
      "token": userInfo.data.token,
      "key": api_KEY
    }
    return fetch(SUBMIT_NEED_HELP_URL,{
        method: "POST",
        body: JSON.stringify(params),
        headers: {
          "Content-Type": "application/json"
        }
    });
};

export function* fetchSubmitNeedHelp(action) {
    try {
        const userInfo = yield call(getUserInfo);
        const response = yield call(submitNeedHelp, userInfo, action.payload);
        const needHelpSubmitData = yield response.json();
        if(response.status === 200){
            yield put({type: SUBMIT_NEED_HELP_ACTION_RESULT, data: needHelpSubmitData.response})
        }        
        else{
            yield put({type: SUBMIT_NEED_HELP_ACTION_ERROR, message: needHelpSubmitData});
        }
    }
    catch (e) {
        yield put({type: SUBMIT_NEED_HELP_ACTION_RETRY, message: e.message});
    }
    yield put(END);
    yield sagaMiddleware.run(rootSaga);
}

export function* fetchOrderStatus(action) {
    try {
        let userInfo = yield call(getUserInfo);
        const response = yield call(getSoaToken, userInfo.data.soa_refresh_token);
        const tokenData = yield response.json();

        if(response.status === 200){
            userInfo.data.soa_token = tokenData.access_token;
            userInfo.data.soa_refresh_token = tokenData.refresh_token;

            yield call(setter, "userObject", JSON.stringify(userInfo));
            // yield call(fetchOrderStatusWithToken, tokenData.access_token, data);

            const orderResponse = yield call(getOrderStatus, tokenData.access_token, action.payload);
            const orderStatus = yield orderResponse.json();
            if(orderResponse.status === 200 && orderStatus.status === 1){
                yield put({type: ORDER_STATUS_ACTION_RESULT, data: orderStatus.data})
            }
            else{
                yield put({type: ORDER_STATUS_ACTION_ERROR, message: orderStatus});
            }
        }
        else{
            yield put({type: ORDER_STATUS_ACTION_ERROR, message: tokenData});
        }
    }
    catch (e) {
        yield put({type: ORDER_STATUS_ACTION_RETRY, message: e});
    }
    yield put(END);
    yield sagaMiddleware.run(rootSaga);
}

export const getSoaToken = (refreshToken) => {
    let data = {
        "client_id": client_id, 
        "client_secret": client_secret, 
        "refresh_token": refreshToken,
        "grant_type": "refresh_token"
    }

    return fetch(REFRESH_TOKEN_URL, {
        method: "POST",
        body: JSON.stringify(data),
        headers: {
          "Content-Type": "application/json"
        }
    });
};

export function* fetchSoaToken(refreshToken, data) {
    try {
        const response = yield call(getSoaToken, refreshToken);
        const tokenData = yield response.json();
        if(response.status === 200)
        {console.log("in success");
            let userInfo = yield call(getter, "userObject");
            userInfo.data.soa_token = tokenData.access_token;
            userInfo.data.soa_refresh_token = tokenData.refresh_token;
            // userInfo.data.soa_token_expiry = new Date().getTime() + tokenData.expires_in;
            console.log("uderObjectttt", userInfo);
            yield call(setter, "userObject", JSON.stringify(userInfo));
            yield call(fetchOrderStatusWithToken, tokenData.access_token, data);
        }        
        else{
            console.log("in error");
        }
    }
    catch (e) {

    }
    yield put(END);
    yield sagaMiddleware.run(rootSaga);
}

export const getOrderStatus = (loginToken, orderId) => {
    return fetch(ORDER_STATUS_URL + orderId, {
        method: "GET",
        headers: {
          "Content-Type": "application/json",
          "Authorization": "Bearer " + loginToken
        }
    });
};

export function* fetchOrderStatusWithToken(loginToken, orderId) {console.log("in fetch status");
    try {
        const response = yield call(getOrderStatus, loginToken, orderId);
        const orderStatus = yield response.json();
        if(response.status === 200 && orderStatus.status === 1){
            yield put({type: ORDER_STATUS_ACTION_RESULT, data: orderStatus.data})
        }
        else{
            yield put({type: ORDER_STATUS_ACTION_ERROR, message: orderStatus});
        }
    }
    catch (e) {
        yield put({type: ORDER_STATUS_ACTION_RETRY, message: e.message});
    }
    yield put(END);
    yield sagaMiddleware.run(rootSaga);
}

export const getReturnList = (userInfo) => {
    return fetch(RETURN_LIST_URL + "&user_id=" + userInfo.data.user_id + "&ttl=" + userInfo.data.ttl + "&token=" + userInfo.data.token);
};

export function* fetchReturnList(action) {
    try {
        const userInfo = yield call(getUserInfo);
        const response = yield call(getReturnList, userInfo);
        const returnList = yield response.json();
        if(response.status === 200 && returnList.status === "success")
        {
            yield put({type: RETURN_LIST_ACTION_RESULT, data: returnList.response})
        }        
        else{
            yield put({type: RETURN_LIST_ACTION_ERROR, message: returnList});
        }
    }
    catch (e) {
        yield put({type: RETURN_LIST_ACTION_RETRY, message: e.message});
    }
    yield put(END);
    yield sagaMiddleware.run(rootSaga);
}

export const cancelReturn = (data) => {
    let params = {
        "orderid": data.orderId,
        "returnid": data.returnId,
        "key": api_KEY
    }

    return fetch(CANCEL_RETURN_URL, {
        method: "POST",
        body: JSON.stringify(params),
        headers: {
          "Content-Type": "application/json"
        }
    });
};

export function* fetchCancelReturn(action) {
    try {
        const userInfo = yield call(getUserInfo);
        const response = yield call(cancelReturn, action.payload);
        const cancelResponse = yield response.json();
        if(response.status === 200 && cancelResponse.status === "success")
        {
            notify.show(cancelResponse.response.success_msg, "success");
            yield call(fetchReturnList, {});
            // yield put({type: RETURN_LIST_ACTION_RESULT, data: returnList.response})
        }        
        else{
            // yield put({type: RETURN_LIST_ACTION_ERROR, message: returnList});
        }
    }
    catch (e) {
        // yield put({type: RETURN_LIST_ACTION_RETRY, message: e.message});
    }
    yield put(END);
    yield sagaMiddleware.run(rootSaga);
}

export const getReturnDetail = (returnId) => {
    return fetch(RETURN_DETAIL_URL + "&return_id=" + returnId);
};

export function* fetchReturnDetail(action) {
    try {
        const response = yield call(getReturnDetail, action.payload);
        const returnDetail = yield response.json();
        if(response.status === 200 && returnDetail.status === "success")
        {
            yield put({type: RETURN_DETAIL_ACTION_RESULT, data: returnDetail.response})
        }        
        else{
            yield put({type: RETURN_DETAIL_ACTION_ERROR, message: returnDetail});
        }
    }
    catch (e) {
        yield put({type: RETURN_DETAIL_ACTION_RETRY, message: e.message});
    }
    yield put(END);
    yield sagaMiddleware.run(rootSaga);
}

export const getRefundDetail = (data, userInfo) => {
    return fetch(REFUND_DETAIL_URL + "&return_id=" + data.returnId + "&order_id=" + data.orderId + "&token=" + userInfo.data.token + "&ttl=" + userInfo.data.ttl);
};

export function* fetchRefundDetail(action) {
    try {
        const userInfo = yield call(getUserInfo);
        const response = yield call(getRefundDetail, action.payload, userInfo);
        const refundDetail = yield response.json();
        if(response.status === 200)
        {
            yield put({type: REFUND_DETAIL_ACTION_RESULT, data: refundDetail.response})
        }        
        else{
            yield put({type: REFUND_DETAIL_ACTION_ERROR, message: refundDetail});
        }
    }
    catch (e) {
        yield put({type: REFUND_DETAIL_ACTION_RETRY, message: e.message});
    }
    yield put(END);
    yield sagaMiddleware.run(rootSaga);
}

export const getCreateReturnDetail = (data, userInfo) => {
    return fetch(CREATE_RETURN_DETAIL_URL + "&order_id=" + data.orderId + "&item_id=" + data.itemId + "&user_id=" + userInfo.data.user_id + "&ttl=" + userInfo.data.ttl + "&token=" + userInfo.data.token);
};

export function* fetchCreateReturnDetail(action) {
    try {
        const userInfo = yield call(getUserInfo);
        const response = yield call(getCreateReturnDetail, action.payload, userInfo);
        const createReturnDetail = yield response.json();
        if(response.status === 200 && createReturnDetail.status == "success")
        {   
            yield put({type: CREATE_RETURN_DETAIL_RESULT, data: createReturnDetail.response})
        }        
        else{
            yield put({type: CREATE_RETURN_DETAIL_ERROR, message: createReturnDetail});
        }
    }
    catch (e) {
        yield put({type: CREATE_RETURN_DETAIL_RETRY, message: e.message});
    }
    yield put(END);
    yield sagaMiddleware.run(rootSaga);
}

export const getSubmitReturnDetail = (data, userInfo) => {
    data["soa_token"] = "Bearer " + userInfo.data.soa_token;
    data["key"] = api_KEY;

    return fetch(SUBMIT_RETURN_URL, {
        method: "POST",
        body: JSON.stringify(data),
        headers: {
          "Content-Type": "application/json",
          "Authorization": "Bearer " + userInfo.data.soa_token
        }
    });
};

export function* fetchSubmitReturnDetail(action) {
    try {
        let userInfo = yield call(getUserInfo);

        /*const soaResponse = yield call(getSoaToken, userInfo.data.soa_refresh_token);
        const soaResult = yield soaResponse.json();console.log("soaResultsoaResult", soaResult);
        if(soaResponse.status === 200){
            userInfo.data.soa_token = soaResult.access_token;
            userInfo.data.soa_refresh_token = soaResult.refresh_token;
            yield call(setter, "userObject", JSON.stringify(userInfo));*/

            const response = yield call(getSubmitReturnDetail, action.payload, userInfo);
            const submitReturnDetail = yield response.json();

            if(response.status === 200)
            {   
                yield put({type: SUBMIT_RETURN_RESULT, data: submitReturnDetail.data})
            }        
            else{
                yield put({type: SUBMIT_RETURN_ERROR, message: submitReturnDetail});
            }
        /*}
        else{
            yield put({type: SUBMIT_RETURN_ERROR, message: soaResult});
        }*/
    }
    catch (e) {
        yield put({type: SUBMIT_RETURN_RETRY, message: e.message});
    }
    yield put(END);
    yield sagaMiddleware.run(rootSaga);
}
