import 'fetch-everywhere';

import {END} from 'redux-saga';
import { call, put} from 'redux-saga/effects';
import {sagaMiddleware} from '../../shared/config';
import rootSaga from './index';


import {userProfile, userProfile_key, base_url_shopclues, base_url_SM_API,developerUrlSec} from '../constant'
import {UserProfile_ACTION_RESULT, UserProfile_ACTION_RETRY, UserProfile_ACTION_ERROR,RemoveProfile_ACTION_RESULT, RemoveProfile_ACTION_RETRY, RemoveProfile_ACTION_ERROR,UserBank_ACTION_RESULT,UserBank_ACTION, UserBank_ACTION_RETRY, UserBank_ACTION_ERROR,UserBank_ADD_ACTION_RESULT, UserBank_ADD_ACTION_RETRY, UserBank_ADD_ACTION_ERROR,UserBank_EDIT_ACTION_RESULT, UserBank_EDIT_ACTION_RETRY, UserBank_EDIT_ACTION_ERROR,UserBank_All_Bank_ACTION_RESULT, UserBank_All_Bank_ACTION_RETRY, UserBank_All_Bank_ACTION_ERROR} from '../actions/userprofile';
import {getter,getObject,getUserLoginToken,getUserInfo,languageForAPI} from '../commonfunction';


const UserProfile_URL = userProfile+"atom/user/getprofile?key="+userProfile_key+"&platform=D&user_id=";

const EditProfile_URL = userProfile+"atom/user/edituser?key="+userProfile_key;

let RemoveProfile_URL =  base_url_SM_API+"addressbook?key="+userProfile_key;



export const getUserProfile = (userId) => {

    return fetch(UserProfile_URL+userId);
};

export const getUserBank = (userId) => {
    
    let soa_token = getUserInfo();
    return fetch(developerUrlSec+"bankdetails?user_id="+userId,{
        method: "GET",
        headers: {
          "Content-Type": "application/json",
          "Authorization": "Bearer "+soa_token.data.soa_token
        }
    });
};


export const getAllBanks = (lang_code) => {
    
    let soa_token = getUserInfo();
    return fetch(developerUrlSec+"v1/banklist?lang_code="+lang_code,{
        method: "GET",
        headers: {
          "Content-Type": "application/json",
          "Authorization": "Bearer "+soa_token.data.soa_token
        }
    });
};

export const addUserBank = (data,userId) => {
    
    let soa_token = getUserInfo();
    
    return fetch(developerUrlSec+"bankdetails?user_id="+userId,{
        method: "POST",
        body: JSON.stringify(data),
        headers: {
          "Content-Type": "application/json",
          "Authorization": "Bearer "+soa_token.data.soa_token
        }
    });
};


export const editUserBank = (data,userId) => {
    
    let soa_token = getUserInfo();
    
    return fetch(developerUrlSec+"bankdetails?user_id="+userId,{
        method: "PUT",
        body: JSON.stringify(data),
        headers: {
          "Content-Type": "application/json",
          "Authorization": "Bearer "+soa_token.data.soa_token
        }
    });
};

export const editProfile = (data, userId) => {
   data["user_id"] = parseInt(userId);
      

    return fetch(EditProfile_URL,{
        method: "POST",
        body: JSON.stringify(data),
        headers: {
          "Content-Type": "application/json",
        }
    });
};


export function* fetchUserProfile(action) {
    try {
        const value = yield call(getter,'userId');
        
      
        const response = yield call(getUserProfile,value);
        const result = yield response.json();
        
        if (response.status === 200) {
            yield put({type: UserProfile_ACTION_RESULT, data: result});
        }
        else {
            yield put({type: UserProfile_ACTION_ERROR, message: result});
        }
    }
    catch (e) {
        yield put({type: UserProfile_ACTION_RETRY, message: e.message});
    }
    yield put(END);
    yield sagaMiddleware.run(rootSaga);
}


export function* fetchUserBank(action) {
    try {
        const value = yield call(getter,'userId');
        const response = yield call(getUserBank,value);
        const result = yield response.json();
     
        if (response.status === 200) {

            yield put({type: UserBank_ACTION_RESULT, data: result});
        }
        else {
            yield put({type: UserBank_ACTION_ERROR, message: result});
        }
    }
    catch (e) {
        yield put({type: UserBank_ACTION_RETRY, message: e.message});
    }
    yield put(END);
    yield sagaMiddleware.run(rootSaga);
}


export function* fetchAllBanks(action) {
    try {
        const lang_code = languageForAPI();
        const response = yield call(getAllBanks,lang_code);
        const result = yield response.json();
        
        if (response.status === 200) {
           
            yield put({type: UserBank_All_Bank_ACTION_RESULT, data: result});
        }
        else {
            yield put({type: UserBank_All_Bank_ACTION_ERROR, message: result});
        }
    }
    catch (e) {
        yield put({type: UserBank_All_Bank_ACTION_RETRY, message: e.message});
    }
    yield put(END);
    yield sagaMiddleware.run(rootSaga);
}

export function* addUserBankGenerator(action) {
    try {
        const value = yield call(getter,'userId');
        const response = yield call(addUserBank,action.payload,value);
        const result = yield response.json();
     
        if (response.status === 200) {
        
            yield call(fetchUserBank,{type: UserBank_ACTION,data: action.payload});
            yield put({type: UserBank_ADD_ACTION_RESULT, data: result});
        }
        else {
            yield put({type: UserBank_ADD_ACTION_ERROR, message: result});
        }
    }
    catch (e) {
        yield put({type: UserBank_ADD_ACTION_RETRY, message: e.message});
    }
    yield put(END);
    yield sagaMiddleware.run(rootSaga);
}

export function* editUserBankGenerator(action) {
    try {
        const value = yield call(getter,'userId');
        const response = yield call(editUserBank,action.payload,value);
        const result = yield response.json();
     
        if (response.status === 200) {
            const banks_response = yield call(fetchUserBank);
            const banks_result = yield response.json();
            if (banks_response.status === 200) {
           
                yield put({type: UserBank_All_Bank_ACTION_RESULT, data: banks_result});
            }
            else {
                yield put({type: UserBank_All_Bank_ACTION_ERROR, message: banks_result});
            }
            yield put({type: UserBank_EDIT_ACTION_RESULT, data: result});
        }
        else {
            yield put({type: UserBank_EDIT_ACTION_ERROR, message: result});
        }
    }
    catch (e) {
        yield put({type: UserBank_EDIT_ACTION_RETRY, message: e.message});
    }
    yield put(END);
    yield sagaMiddleware.run(rootSaga);
}


export function* editUserProfile(action) {
    try {
         const userId = yield call(getter,'userId')
      
        const response = yield call(editProfile, action.payload,userId);
        const result = yield response.json();
        
        if (response.status === 200) {
            // yield put({type: UserProfile_ACTION_RESULT, data: result});
            return yield call(fetchUserProfile);
        }
        else {
            yield put({type: UserProfile_ACTION_ERROR, message: result});
        }
    }
    catch (e) {
        yield put({type: UserProfile_ACTION_RETRY, message: e.message});
    }
    yield put(END);
    yield sagaMiddleware.run(rootSaga);
}

export const removeProfile = (data,userID,ttl,token) => {
    RemoveProfile_URL = RemoveProfile_URL+"&profile_id="+data+"&user_id="+userID+"&ttl="+ttl+"&token="+token
       
 
     return fetch(RemoveProfile_URL,{
         method: "DELETE",
         body: JSON.stringify(data),
         headers: {
           "Content-Type": "application/json",
         }
     });
 };
 
export function* removeUserProfile(action) {
    try {
         const userObject = yield call(getObject,'userObject')
         console.log(56565);
         console.log(userObject.data)
      
        const response = yield call(removeProfile, action.payload,userObject.data.user_id,userObject.data.ttl,userObject.data.token);
        const result = yield response.json();
        
        if (response.status === 200) {
             yield put({type: RemoveProfile_ACTION_RESULT, data: result});
             yield call(fetchUserProfile);
        }
        else {
            yield put({type: RemoveProfile_ACTION_ERROR, message: result});
        }
    }
    catch (e) {
        yield put({type: RemoveProfile_ACTION_RETRY, message: e.message});
    }
    yield put(END);
    yield sagaMiddleware.run(rootSaga);
}

