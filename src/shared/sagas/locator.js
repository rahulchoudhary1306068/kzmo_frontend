import 'fetch-everywhere';

import {END} from 'redux-saga';
import { channel } from 'redux-saga'
import {takeEvery, select, call, put,take,fork, actionChannel} from 'redux-saga/effects';
import { base_URL, api_KEY, base_url_shopclues, api_key_shopclues, delivery_detail_base_url,base_URL_SM,developerUrl ,developerUrlSec,token_URL} from '../constant'
import {sagaMiddleware} from '../../shared/config';
import rootSaga from './index';

import { LOCATOR_ACTION_RESULT, LOCATOR_ACTION_RETRY, LOCATOR_ACTION_ERROR, MALL_ACTION_RESULT, MALL_ACTION_RETRY, MALL_ACTION_ERROR } from '../actions/locator';

import { notify } from 'react-notify-toast';

const STORE_LOCATOR_URL = developerUrl + "merchant/offline/address";

export const getLocatorList = (value,token,toke_type) => {
 let data = {
    "city_id" : value.city_id,
    "company_id" : value.company_id,
    "mall_id" :value.mall_id,
    "lang_code":value.lang_code
 }

    return fetch(STORE_LOCATOR_URL+'?'+'lang_code='+value.lang_code, {
        method: "POST",
        body: JSON.stringify(data),
        headers: {
          "Content-Type": "application/json",
          "Authorization" : toke_type+" "+token,
          'Access-Control-Allow-Origin':'*'
        }
    });
};

export function* fetchLocator(action) {
    try {
        const response = yield call(tokenRequest);
        const token = yield response.json();
        
        //yield put({type: Login_ACTION_RESULT});
        if(response.status === 200)
            { 
                const tokenValue=token.access_token;
                const tokenType= token.token_type;
                const response = yield call(getLocatorList, action.payload,tokenValue,tokenType);
                const edd = yield response.json();
                if(response.status === 200)
                {
                    yield put({type: LOCATOR_ACTION_RESULT, data: edd});
                }        
                else{
                    yield put({type: LOCATOR_ACTION_ERROR, message: "error"});
                }
            }
                
        else {
            yield put({type: Login_ACTION_ERROR, message: token});
        }
    }
    catch (e) {
        yield put({type: LOCATOR_ACTION_RETRY, message: e.message});
    }
    yield put(END);
    yield sagaMiddleware.run(rootSaga);
}

//mall

const Mall_URL = developerUrlSec + "v1/malls";

export const getMallList = (value,token,toke_type) => {

    return fetch(Mall_URL+"?lang_code="+value.lang_code, {
        method: "GET",
        //body: JSON.stringify(data),
        headers: {
          "Content-Type": "application/json",
          "Authorization" : toke_type+" "+token,
          'Access-Control-Allow-Origin':'*'
        }
    });
};

export function* fetchMallList(action) {
    
    try {
       
        const response = yield call(tokenRequest);
        const token = yield response.json();
        
        //yield put({type: Login_ACTION_RESULT});
        if(response.status === 200)
            { 
                const tokenValue=token.access_token;
                const tokenType= token.token_type;
                const MallResponse = yield call(getMallList,action.payload,tokenValue,tokenType);
                
                const mallList = yield MallResponse.json();
                if(MallResponse.status === 200)
                {
                    yield put({type: MALL_ACTION_RESULT, data: mallList});
                }        
                else{
                    yield put({type: MALL_ACTION_ERROR, message: "error"});
                }
                
                        
            }
                
        else {
            yield put({type: Login_ACTION_ERROR, message: token});
        }
    }
    catch (e) {
        yield put({type: Login_ACTION_RETRY, message: e.message});
    }
    yield put(END);
    yield sagaMiddleware.run(rootSaga);


}

const Token_URL = token_URL;
export const tokenRequest = () => {

    var data = JSON.stringify({
        "client_id": "01RX33H3LTQIUZXSAPJ1AFUU34K1X6OW",
        "client_secret": "_X7HMB78MMV&!VPAIQH9^_1HIDFZ8+WI75X8RI+33^RBI",
        "grant_type": "client_credentials"
      });
    return fetch(Token_URL,{
        method: "POST",
        body: data,
        
        headers: {
          "Content-Type": "application/json",
          
          
          
        }
    });
};
