import 'fetch-everywhere';
import {END} from 'redux-saga';
import {takeEvery, call, put} from 'redux-saga/effects';
import {login_base_URL,login_api_KEY,token_URL,base_URL_CODOTP,api_KEY} from '../constant'
import {sagaMiddleware} from '../../shared/config';
import rootSaga from './index';
import {getcartAction} from '../actions/cart'



import { Login_ACTION_RESULT, Login_ACTION_RETRY, Login_ACTION_ERROR
    ,OTPVerify_ACTION_RESULT, OTPVerify_ACTION_RETRY, OTPVerify_ACTION_ERROR
    ,ResendOTP_ACTION_RESULT, ResendOTP_ACTION_RETRY, ResendOTP_ACTION_ERROR
    ,LoginOTP_ACTION_RESULT, LoginOTP_ACTION_RETRY, LoginOTP_ACTION_ERROR,
    ForgotOTP_ACTION_RESULT, ForgotOTP_ACTION_RETRY, ForgotOTP_ACTION_ERROR,
    ForgotVerifyOTP_ACTION_RESULT, ForgotVerifyOTP_ACTION_RETRY, ForgotVerifyOTP_ACTION_ERROR
    ,UpdatePassword_ACTION_RESULT, UpdatePassword_ACTION_RETRY, UpdatePassword_ACTION_ERROR,
    InvalidToken_ACTION_RESULT

} from '../actions/login';
import {getUserstatus, setter, createLocalSessionId,setObject} from '../commonfunction';

const Login_URL = login_base_URL+"login";
const Signup_URL = login_base_URL+"signup"
const Token_URL = token_URL;
const OTPVerify_URL = login_base_URL+"signup/verifyotp";
const OTPResend_URL = login_base_URL+"signup/resendotp";
const LoginOTP_URL =  login_base_URL+"loginviaotp"
const LoginOTPVerify_URL = login_base_URL+"loginviaotp/verifyotp"
const codOTPVerify_URL = base_URL_CODOTP+"otpConfirmation/verifyOtp?key="+api_KEY
const ForgotOTP_URL = login_base_URL+"forgotpassword";
const ForgotVerifyOTP_URL = login_base_URL+"forgotpassword/verifyotp";
const UpdatePassword_URL = login_base_URL+"resetpassword";

export const loginRequest = ( data,token,toke_type) => {
 
    let url = ""
    if(data.otp){
        url = LoginOTPVerify_URL
    }
    else if(data.signup){
        url = Signup_URL
    }
    else{
        url = Login_URL
    }

    return fetch(url,{
        method: "POST",
        body: JSON.stringify(data),
        headers: {
          "Content-Type": "application/json",
          "Authorization" : toke_type+" "+token,
          'Access-Control-Allow-Origin':'*'
        }
    });
};
export const tokenRequest = () => {

    var data = JSON.stringify({
        "client_id": "01RX33H3LTQIUZXSAPJ1AFUU34K1X6OW",
        "client_secret": "_X7HMB78MMV&!VPAIQH9^_1HIDFZ8+WI75X8RI+33^RBI",
        "grant_type": "client_credentials"
      });
    return fetch(Token_URL,{
        method: "POST",
        body: data,
        
        headers: {
          "Content-Type": "application/json",
          
          
          
        }
    });
};


export function* fetchLoginData(action) {
    try {
       
        const response = yield call(tokenRequest);
        const token = yield response.json();
        
        //yield put({type: Login_ACTION_RESULT});
        if(response.status === 200)
            { 
                const tokenValue=token.access_token;
                const tokenType= token.token_type;
                const LoginResponse = yield call(loginRequest,action.payload,tokenValue,tokenType);
                const loginResult = yield LoginResponse.json();
                
                yield call(setObject,'userObject',loginResult)
                
                yield put({type: Login_ACTION_RESULT, data: loginResult});
                
                let userStatus = yield call(getUserstatus);
                if(userStatus){ // means user have something in cart as guest user

                    // here we need to write cart merge action and delete session ID from localstorge
                   // userStatus = yield call(createLocalSessionId);
                   // yield call(setter,'sessionId',userStatus[1]);
                   // here 1 need to replace with realuserid
                   let mergeCartData = { session_id:userStatus[1],
                    user_id:loginResult.data.user_id // 1 need to change actual id
                    };
                   // this.props.dispatch(getcartAction(mergeCartData))
                   yield call(setter,'userId',loginResult.data.user_id)
                   yield put({type:"getcart_ACTION",payload:mergeCartData});
                   yield put({type:"UserProfile_ACTION"});
                   
                }
                else{
                    if(loginResult.status === 'success'){
                        yield call(setter,'userId',loginResult.data.user_id);// here 1 need to replace with realuserid
                        yield put({type:"getcart_ACTION"});
                        yield put({type:"UserProfile_ACTION"});
                    }
                    
                    
                }
                
                        
            }
                
        else {
            yield put({type: Login_ACTION_ERROR, message: token});
        }
    }
    catch (e) {
        yield put({type: Login_ACTION_RETRY, message: e.message});
    }
    yield put(END);
    yield sagaMiddleware.run(rootSaga);
}

export const callOTP = (data,token,toke_type)=>{
   
    if(data['cod']){
        return fetch(codOTPVerify_URL,{
            method: "POST",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
                "Authorization" : toke_type+" "+token
                
              }
        });
    }
    else{
        return fetch(OTPVerify_URL,{
            method: "POST",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
                "Authorization" : toke_type+" "+token
                
              }
        });
    }
    

}


export function* OTPVerify(action) {
    try {
        const response = yield call(tokenRequest);
        const token = yield response.json();
        
        //yield put({type: Login_ACTION_RESULT});
        if(response.status === 200)
            { 
                const tokenValue=token.access_token;
                const tokenType= token.token_type;
                const response = yield call(callOTP,action.payload,tokenValue,tokenType);
                const result = yield response.json();
                
                if (response.status === 200) {
                    yield put({type: OTPVerify_ACTION_RESULT, data: result});
                }
                else {
                    yield put({type: OTPVerify_ACTION_ERROR, message: result});
                }
            }
            else{
                yield put({type: OTPVerify_ACTION_ERROR, message: result});

            }    
        }
    catch (e) {
        yield put({type: OTPVerify_ACTION_RETRY, message: e.message});
    }
    yield put(END);
    yield sagaMiddleware.run(rootSaga);
}

export const callResendOTP = (data,token,toke_type)=>{
    let url = ""
    if(data.type){
        url = LoginOTP_URL
    }
    else{
        url = OTPResend_URL
    }

    return fetch(url,{
        method: "POST",
        body: JSON.stringify(data),
        headers: {
            "Content-Type": "application/json",
            "Authorization" : toke_type+" "+token
            
          }
    });

}


export function* ResendOTP(action) {
    try {
        const response = yield call(tokenRequest);
        const token = yield response.json();
        
        //yield put({type: Login_ACTION_RESULT});
        if(response.status === 200)
            { 
                const tokenValue=token.access_token;
                const tokenType= token.token_type;
                const response = yield call(callResendOTP,action.payload,tokenValue,tokenType);
                const result = yield response.json();
                
                if (response.status === 200) {
                    yield put({type: ResendOTP_ACTION_RESULT, data: result});
                }
                else {
                    yield put({type: ResendOTP_ACTION_ERROR, message: result});
                }
            }
            else{
                yield put({type: ResendOTP_ACTION_ERROR, message: result});

            }    
        }
    catch (e) {
        yield put({type: ResendOTP_ACTION_RETRY, message: e.message});
    }
    yield put(END);
    yield sagaMiddleware.run(rootSaga);
}
export const callLoginOTP = (data,token,toke_type)=>{
   

    return fetch(LoginOTP_URL,{
        method: "POST",
        body: JSON.stringify(data),
        headers: {
            "Content-Type": "application/json",
            "Authorization" : toke_type+" "+token
            
          }
    });

}


export function* LoginOTP(action) {
    try {
        const response = yield call(tokenRequest);
        const token = yield response.json();
        
        //yield put({type: Login_ACTION_RESULT});
        if(response.status === 200)
            { 
                const tokenValue=token.access_token;
                const tokenType= token.token_type;
                const response = yield call(callLoginOTP,action.payload,tokenValue,tokenType);
                const result = yield response.json();
                
                if (response.status === 200) {
                    yield put({type: LoginOTP_ACTION_RESULT, data: result});
                }
                else {
                    yield put({type: LoginOTP_ACTION_ERROR, message: result});
                }
            }
            else{
                yield put({type: LoginOTP_ACTION_ERROR, message: result});

            }    
        }
    catch (e) {
        yield put({type: LoginOTP_ACTION_RETRY, message: e.message});
    }
    yield put(END);
    yield sagaMiddleware.run(rootSaga);
}
export const callForgotOTP = (data,token,toke_type)=>{
   

    return fetch(ForgotOTP_URL,{
        method: "POST",
        body: JSON.stringify(data),
        headers: {
            "Content-Type": "application/json",
            "Authorization" : toke_type+" "+token
            
          }
    });

}


export function* ForgotOTP(action) {
    try {
        const response = yield call(tokenRequest);
        const token = yield response.json();
        
        //yield put({type: Login_ACTION_RESULT});
        if(response.status === 200)
            { 
                const tokenValue=token.access_token;
                const tokenType= token.token_type;
                const response = yield call(callForgotOTP,action.payload,tokenValue,tokenType);
                const result = yield response.json();
                
                if (response.status === 200) {
                    yield put({type: ForgotOTP_ACTION_RESULT, data: result});
                }
                else {
                    yield put({type: ForgotOTP_ACTION_ERROR, message: result});
                }
            }
            else{
                yield put({type: ForgotOTP_ACTION_ERROR, message: result});

            }    
        }
    catch (e) {
        yield put({type: ForgotOTP_ACTION_RETRY, message: e.message});
    }
    yield put(END);
    yield sagaMiddleware.run(rootSaga);
}
export const callForgotVerifyOTP = (data,token,toke_type)=>{
   

    return fetch(ForgotVerifyOTP_URL,{
        method: "POST",
        body: JSON.stringify(data),
        headers: {
            "Content-Type": "application/json",
            "Authorization" : toke_type+" "+token
            
          }
    });

}


export function* ForgotVerifyOTP(action) {
    try {
        const response = yield call(tokenRequest);
        const token = yield response.json();
        
        
        if(response.status === 200)
            { 
                const tokenValue=token.access_token;
                const tokenType= token.token_type;
                const response = yield call(callForgotVerifyOTP,action.payload,tokenValue,tokenType);
                const result = yield response.json();
                
                if (response.status === 200) {
                    yield put({type: ForgotVerifyOTP_ACTION_RESULT, data: result});
                }
                else {
                    yield put({type: ForgotVerifyOTP_ACTION_ERROR, message: result});
                }
            }
            else{
                yield put({type: ForgotVerifyOTP_ACTION_ERROR, message: result});

            }    
        }
    catch (e) {
        yield put({type: ForgotVerifyOTP_ACTION_RETRY, message: e.message});
    }
    yield put(END);
    yield sagaMiddleware.run(rootSaga);
}

export const callUpdatePassword = (data,token,toke_type)=>{
   

    return fetch(UpdatePassword_URL,{
        method: "POST",
        body: JSON.stringify(data),
        headers: {
            "Content-Type": "application/json",
            "Authorization" : toke_type+" "+token
            
          }
    });

}


export function* UpdatePassword(action) {
    try {
        const response = yield call(tokenRequest);
        const token = yield response.json();
        
        
        if(response.status === 200)
            { 
                const tokenValue=token.access_token;
                const tokenType= token.token_type;
                const response = yield call(callUpdatePassword,action.payload,tokenValue,tokenType);
                const result = yield response.json();
                
                if (response.status === 200) {
                    yield put({type: UpdatePassword_ACTION_RESULT, data: result});
                }
                else {
                    yield put({type: UpdatePassword_ACTION_ERROR, message: result});
                }
            }
            else{
                yield put({type: UpdatePassword_ACTION_ERROR, message: result});

            }    
        }
    catch (e) {
        yield put({type: UpdatePassword_ACTION_RETRY, message: e.message});
    }
    yield put(END);
    yield sagaMiddleware.run(rootSaga);
}
export function* InvalidToken(action) {
        yield put({type: InvalidToken_ACTION_RESULT});

    yield put(END);
    yield sagaMiddleware.run(rootSaga);
}