import 'fetch-everywhere';

import {END} from 'redux-saga';
import { call, put} from 'redux-saga/effects';
import {sagaMiddleware} from '../../shared/config';
import rootSaga from './index';
import {userProfile,userProfile_key, base_URL, api_KEY,token_URL,base_URL_CODOTP,base_URL_SM } from '../constant';

import {Checkout_ACTION_RESULT, Checkout_ACTION_RETRY, 
    Checkout_ACTION_ERROR,PaymentOption_ACTION_RESULT, PaymentOption_ACTION_RETRY, 
    PaymentOption_ACTION_ERROR,placeOrder_ACTION_RESULT, placeOrder_ACTION_RETRY, 
    placeOrder_ACTION_ERROR,codOTP_ACTION_RESULT, codOTP_ACTION_RETRY, 
    codOTP_ACTION_ERROR,orderConfirm_ACTION_RESULT, orderConfirm_ACTION_RETRY, 
    orderConfirm_ACTION_ERROR} from '../actions/checkout';
import { getter, getObject, languageForAPI } from '../commonfunction';

const Checkout_URL = "";
const Token_URL = token_URL;
const codOTP_URL = base_URL_CODOTP+"otpConfirmation/generateOtp?key="+api_KEY
const  orderConfirmUrl = base_URL_SM+"order/postOrderProcessing?key="+api_KEY
export const getCheckout = () => {

    return fetch(Checkout_URL+"&platform=D");
};


export function* fetchCheckout(action) {
    try {

        const response = yield call(getCheckout);
        const result = yield response.json();
        
        if (response.status === 200) {
            yield put({type: Checkout_ACTION_RESULT, data: result});
        }
        else {
            yield put({type: Checkout_ACTION_ERROR, message: result});
        }
    }
    catch (e) {
        yield put({type: Checkout_ACTION_RETRY, message: e.message});
    }
    yield put(END);
    yield sagaMiddleware.run(rootSaga);
}

const GetPaymentOption_URL =  userProfile+"atom/GetPayment/getpaymentmethod?key="+userProfile_key+"&platform=D";

export const GetPaymentOption = () => {

    return fetch(GetPaymentOption_URL+"&lang_code="+languageForAPI());
};


export function* fetchPaymentOption(action) {
    try {

        const response = yield call(GetPaymentOption);
        const result = yield response.json();
        
        if (response.status === 200) {
            yield put({type: PaymentOption_ACTION_RESULT, data: result});
        }
        else {
            yield put({type: PaymentOption_ACTION_ERROR, message: result});
        }
    }
    catch (e) {
        yield put({type: PaymentOption_ACTION_RETRY, message: e.message});
    }
    yield put(END);
    yield sagaMiddleware.run(rootSaga);
}

const placeOrder_URL = base_URL+"atom/order/placeorder?key="+api_KEY;

export const Order = (value,userId) => {
  var data  =  {
    "user_id" : userId,
    "cart_service_id" : 1,
    "platform" : "D",
    "payment_option_id" : value.payment_option_id,
    "profile_id": value.profileId,
    "pincode": "431001",
    "lang_code" : languageForAPI()
    }
    if(value.coupon_codes){
        data['coupon_codes'] = [value.coupon_codes];
    }
    else{
        data['coupon_codes']=[];
    }
    
    return fetch(placeOrder_URL,{
        method: "POST",
        body: JSON.stringify(data),
        headers: {
          "Content-Type": "application/json",
          
          
        }
    });
};


export function* placeOrder(action) {
    try {

        const userId = yield call(getter,'userId')
        const response = yield call(Order,action.payload,userId);
        const result = yield response.json();
        
        if (response.status === 200) {
            yield put({type: placeOrder_ACTION_RESULT, data: result});
            yield put({type:"getcart_ACTION"});
            
        }
        else {
            yield put({type: placeOrder_ACTION_ERROR, message: result});
        }
    }
    catch (e) {
        yield put({type: placeOrder_ACTION_RETRY, message: e.message});
    }
    yield put(END);
    yield sagaMiddleware.run(rootSaga);
}
export const tokenRequest = () => {

    var data = JSON.stringify({
        "client_id": "01RX33H3LTQIUZXSAPJ1AFUU34K1X6OW",
        "client_secret": "_X7HMB78MMV&!VPAIQH9^_1HIDFZ8+WI75X8RI+33^RBI",
        "grant_type": "client_credentials"
      });
    return fetch(Token_URL,{
        method: "POST",
        body: data,
        
        headers: {
          "Content-Type": "application/json",
          
          
          
        }
    });
};

export const callcodOTP = (data,token,toke_type,userToken,ttl)=>{
   

    return fetch(codOTP_URL+"&token="+userToken+"&ttl="+ttl+"&platform=D",{
        method: "POST",
        body: JSON.stringify(data),
        headers: {
            "Content-Type": "application/json",
            "Authorization" : toke_type+" "+token
            
          }
    });

}


export function* getCodOTP(action) {
    try {
        const response = yield call(tokenRequest);
        const token = yield response.json();
        
        //yield put({type: Login_ACTION_RESULT});
        if(response.status === 200)
            { 
                const tokenValue=token.access_token;
                const tokenType= token.token_type;
                const userObject = yield call(getObject,'userObject');
                const response = yield call(callcodOTP,action.payload,tokenValue,tokenType,userObject.data.token,userObject.data.ttl);
                const result = yield response.json();
                
                if (response.status === 200) {
                    yield put({type: codOTP_ACTION_RESULT, data: result});
                }
                else {
                    yield put({type: codOTP_ACTION_ERROR, message: result});
                }
            }
            else{
                yield put({type: codOTP_ACTION_ERROR, message: result});

            }    
        }
    catch (e) {
        yield put({type: codOTP_ACTION_RETRY, message: e.message});
    }
    yield put(END);
    yield sagaMiddleware.run(rootSaga);
}

function getOrderConfirm(orderId){
    var data  =  {
        "order_id" : orderId,
        'lang_code' : languageForAPI(),
        "platform" : "D"
        }
        return fetch(orderConfirmUrl,{
            method: "POST",
            body: JSON.stringify(data),
            headers: {
              "Content-Type": "application/json",
              
              
            }
        });

}
export function* orderConfirm(action) {
    try {

        const response = yield call(getOrderConfirm,action.payload);
        const result = yield response.json();
        
        if (response.status === 200) {
            yield put({type: orderConfirm_ACTION_RESULT, data: result});
        }
        else {
            yield put({type: orderConfirm_ACTION_ERROR, message: result});
        }
    }
    catch (e) {
        yield put({type: orderConfirm_ACTION_RETRY, message: e.message});
    }
    yield put(END);
    yield sagaMiddleware.run(rootSaga);
}