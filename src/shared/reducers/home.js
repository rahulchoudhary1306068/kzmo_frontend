import { HOME_ACTION, HOME_ACTION_RESULT, HOME_ACTION_ERROR, HOME_ACTION_RETRY, HOME_SECTION_RESULT } from '../actions/home';


const initialState = {
    Section: [],
    isFetching: false,
    retry: false,
    error: false,
    message: null,
};


const homeReducer = (state = initialState, action) => {

    switch (action.type) {

        case HOME_ACTION:
            return {...state, isFetching: true, error: false, message: null, retry: false};

        /*case HOME_ACTION_RESULT:
            return {...state, isFetching: false, data: action.data};*/

        case HOME_ACTION_RESULT:
            {
                let stateData = {...state};
                let sectionArr = Object.assign([], stateData.Section);
                sectionArr[action.data[0].index]=action.data;
                return {...state, isFetching: false, Section: sectionArr};
            }

        case HOME_ACTION_ERROR:
            return {...state, isFetching: false, message: action.message, retry: false};

        case HOME_ACTION_RETRY:
            return {...state, isFetching: false, message: action.message, retry: false};

        default:
            return state;
    }
};

export default homeReducer;

/*import { HOME_SECTION, HOME_SECTION_RESULT, HOME_SECTION_ERROR, HOME_SECTION_RETRY } from '../actions/home';


const initialStateSection = {
    Section: [],
    isFetching: false,
    retry: false,
    error: false,
    message: null,
};


const sectionReducer = (state = initialStateSection, action) => {

    switch (action.type) {

        case HOME_SECTION:
            return {...state, isFetching: true, error: false, message: null, retry: false};

        case HOME_SECTION_RESULT:
             {  
                state.Section[action.data.index]=action.data;

                isFetching: false;
                return state;
             };

        case HOME_SECTION_ERROR:
            return {...state, isFetching: false, message: action.message, retry: false};

        case HOME_SECTION_RETRY:
            return {...state, isFetching: false, message: action.message, retry: false};

        default:
            return state;
    }
};

export {sectionReducer};*/