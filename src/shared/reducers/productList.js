import { PRODUCTLIST_ACTION, PRODUCTLIST_ACTION_RESULT, PRODUCTLIST_ACTION_ERROR, PRODUCTLIST_ACTION_RETRY,PRODUCTID_SEONAME_ACTION, PRODUCTID_SEONAME_ACTION_RESULT, PRODUCTID_SEONAME_ACTION_ERROR, PRODUCTID_SEONAME_ACTION_RETRY } from '../actions/productList';


const initialState = {
    data: {},
    isFetching: false,
    retry: false,
    error: false,
    message: null,
    result: false
};


const productListReducer = (state = initialState, action) => {

    switch (action.type) {

        case PRODUCTLIST_ACTION:
            return {...state, isFetching: true, error: false, message: null, retry: false, result: false};

        case PRODUCTLIST_ACTION_RESULT:
            return {...state, isFetching: false, data: action.data, result: true};

        case PRODUCTLIST_ACTION_ERROR:
            return {...state, isFetching: false, message: action.message, retry: false, result: false};

        case PRODUCTLIST_ACTION_RETRY:
            return {...state, isFetching: false, message: action.message, retry: false, result: false};

        case 'Initail_PRODUCT_LIST_ACTION':
            return {...state, isFetching: false, data: {}, retry: false, error: false, message: null, result: false};

        default:
            return state;
    }
};

export default productListReducer;

const productIdReducer = (state = initialState, action) => {

    switch (action.type) {

        case PRODUCTID_SEONAME_ACTION:
            return {...state, isFetching: true, error: false, message: null, retry: false, result: false};

        case PRODUCTID_SEONAME_ACTION_RESULT:
            return {...state, isFetching: false, data: action.data, result: true};

        case PRODUCTID_SEONAME_ACTION_ERROR:
            return {...state, isFetching: false, message: action.message, retry: false, result: false};

        case PRODUCTID_SEONAME_ACTION_RETRY:
            return {...state, isFetching: false, message: action.message, retry: false, result: false};

        case 'Initail_PRODUCT_LIST_ACTION':
            return {...state, isFetching: false, data: {}, retry: false, error: false, message: null, result: false};

        default:
            return state;
    }
};

export {productIdReducer};