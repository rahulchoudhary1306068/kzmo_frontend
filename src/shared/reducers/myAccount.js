import { USER_INFO_ACTION, USER_INFO_RESULT, USER_INFO_ERROR, USER_INFO_RETRY } from '../actions/myAccount';

const initialState = {
    data: {},
    isFetching: false,
    retry: false,
    error: false,
    message: null,
};

const userInfoReducer = (state = initialState, action) => {

    switch (action.type) {

        case USER_INFO_ACTION:
            return {...state, isFetching: true, error: false, message: null, retry: false};

        case USER_INFO_RESULT:
            return {...state, isFetching: false, data: action.data};

        case USER_INFO_ERROR:
            return {...state, isFetching: false, message: action.message, retry: false};

        case USER_INFO_RETRY:
            return {...state, isFetching: false, message: action.message, retry: false};

        default:
            return state;
    }
};

export { userInfoReducer };

import { REWARD_POINTS_ACTION, REWARD_POINTS_RESULT, REWARD_POINTS_ERROR, REWARD_POINTS_RETRY } from '../actions/myAccount';

const initialRewardPointState = {
    data: {},
    isFetching: false,
    retry: false,
    error: false,
    message: null,
};

const rewardPointReducer = (state = initialRewardPointState, action) => {

    switch (action.type) {

        case REWARD_POINTS_ACTION:
            return {...state, isFetching: true, error: false, message: null, retry: false};

        case REWARD_POINTS_RESULT:
            return {...state, isFetching: false, data: action.data};

        case REWARD_POINTS_ERROR:
            return {...state, isFetching: false, message: action.message, retry: false};

        case REWARD_POINTS_RETRY:
            return {...state, isFetching: false, message: action.message, retry: false};

        default:
            return state;
    }
};

export { rewardPointReducer };

import { ORDER_ACTION, ORDER_ACTION_RESULT, ORDER_ACTION_ERROR, ORDER_ACTION_RETRY, CANCEL_ORDER_ACTION, CANCEL_ORDER_FROM_DETAIL_ACTION } from '../actions/myAccount';

const initialOrderState = {
    data: {},
    isFetching: false,
    retry: false,
    error: false,
    message: null,
    result: false
};

const orderReducer = (state = initialOrderState, action) => {

    switch (action.type) {

        case ORDER_ACTION:
            return {...state, isFetching: true, error: false, message: null, retry: false, result: false};

        case CANCEL_ORDER_ACTION:
            return {...state, isFetching: true, error: false, message: null, retry: false, result: false};

        //dinesh for cancal order from order detail
        case CANCEL_ORDER_FROM_DETAIL_ACTION:
            return {...state, isFetching: true, error: false, message: null, retry: false, result: false};
        
        case ORDER_ACTION_RESULT:
            return {...state, isFetching: false, data: action.data, result: true};

        case ORDER_ACTION_ERROR:
            return {...state, isFetching: false, message: action.message, retry: false, result: false};

        case ORDER_ACTION_RETRY:
            return {...state, isFetching: false, message: action.message, retry: false, result: false};

        default:
            return state;
    }
};

export { orderReducer };

import { ORDER_DETAIL_ACTION, ORDER_DETAIL_RESULT, ORDER_DETAIL_ERROR, ORDER_DETAIL_RETRY } from '../actions/myAccount';

const initialOrderDetailState = {
    data: {},
    isFetching: false,
    retry: false,
    error: false,
    message: null,
};

const orderDetailReducer = (state = initialOrderDetailState, action) => {

    switch (action.type) {

        case ORDER_DETAIL_ACTION:
            return {...state, isFetching: true, error: false, message: null, retry: false};

        case ORDER_DETAIL_RESULT:
            return {...state, isFetching: false, data: action.data};

        case ORDER_DETAIL_ERROR:
            return {...state, isFetching: false, message: action.message, retry: false};

        case ORDER_DETAIL_RETRY:
            return {...state, isFetching: false, message: action.message, retry: false};

        default:
            return state;
    }
};

export { orderDetailReducer };

import { WISHLIST_ACTION, WISHLIST_ACTION_RESULT, WISHLIST_ACTION_ERROR, WISHLIST_ACTION_RETRY, WISHLIST_REMOVE_ACTION, ADD_TO_CART_FROM_WISHLIST_ACTION } from '../actions/myAccount';

const initialWishlistState = {
    data: {},
    isFetching: false,
    retry: false,
    error: false,
    message: null,
};

const getWishlistReducer = (state = initialWishlistState, action) => {

    switch (action.type) {

        case WISHLIST_ACTION:
            return {...state, isFetching: true, error: false, message: null, retry: false};

        case WISHLIST_REMOVE_ACTION:
            return {...state, isFetching: true, error: false, message: null, retry: false};

        case ADD_TO_CART_FROM_WISHLIST_ACTION:
            return {...state, isFetching: true, error: false, message: null, retry: false};

        case WISHLIST_ACTION_RESULT:
            return {...state, isFetching: false, data: action.data};

        case WISHLIST_ACTION_ERROR:
            return {...state, isFetching: false, message: action.message, retry: false};

        case WISHLIST_ACTION_RETRY:
            return {...state, isFetching: false, message: action.message, retry: false};

        default:
            return state;
    }
};

export { getWishlistReducer };

import { RATE_PURCHASE_ACTION, RATE_PURCHASE_ACTION_RESULT, RATE_PURCHASE_ACTION_ERROR, RATE_PURCHASE_ACTION_RETRY } from '../actions/myAccount';

const initialReviewState = {
    data: {
        "complete": "",
        "pending": ""
    },
    isFetching: false,
    retry: false,
    error: false,
    message: null,
};

const getReviewsReducer = (state = initialReviewState, action) => {

    switch (action.type) {

        case RATE_PURCHASE_ACTION:
            return {...state, isFetching: true, error: false, message: null, retry: false};

        case RATE_PURCHASE_ACTION_RESULT:
            {
                let stateData = {...state};
                if(action.data.type == "completed"){
                    stateData.data.complete = action.data.data;
                }
                else if(action.data.type == "pending"){
                    stateData.data.pending = action.data.data;   
                }

                return {...state, isFetching: false, data: stateData.data};
            }

        case RATE_PURCHASE_ACTION_ERROR:
            return {...state, isFetching: false, message: action.message, retry: false};

        case RATE_PURCHASE_ACTION_RETRY:
            return {...state, isFetching: false, message: action.message, retry: false};

        default:
            return state;
    }
};

export { getReviewsReducer };

import { ORDER_REVIEW_SUBMIT_ACTION, ORDER_REVIEW_SUBMIT_ACTION_RESULT, ORDER_REVIEW_SUBMIT_ACTION_ERROR, ORDER_REVIEW_SUBMIT_ACTION_RETRY } from '../actions/myAccount';

const initialSubmitOrderReviewState = {
    isFetching: false,
    retry: false,
    error: false,
    message: null,
    result: false
};

const submitOrderReviewsReducer = (state = initialSubmitOrderReviewState, action) => {

    switch (action.type) {

        case ORDER_REVIEW_SUBMIT_ACTION:
            return {...state, isFetching: true, error: false, message: null, retry: false, result: false};

        case ORDER_REVIEW_SUBMIT_ACTION_RESULT:
            return {...state, isFetching: false, error: false, message: null, retry: false, result: true};

        case ORDER_REVIEW_SUBMIT_ACTION_ERROR:
            return {...state, isFetching: false, message: action.message, retry: false, result: false};

        case ORDER_REVIEW_SUBMIT_ACTION_RETRY:
            return {...state, isFetching: false, message: action.message, retry: false, result: false};

        default:
            return state;
    }
};

export { submitOrderReviewsReducer };

import { HELP_ISSUE_ACTION, HELP_ISSUE_ACTION_RESULT, HELP_ISSUE_ACTION_ERROR, HELP_ISSUE_ACTION_RETRY } from '../actions/myAccount';

const initialHelpIssueState = {
    data: [],
    isFetching: false,
    retry: false,
    error: false,
    message: null
};

const helpIssueReducer = (state = initialHelpIssueState, action) => {

    switch (action.type) {

        case HELP_ISSUE_ACTION:
            return {...state, isFetching: true, error: false, message: null, retry: false};

        case HELP_ISSUE_ACTION_RESULT:
            return {...state, isFetching: false, data: action.data, error: false, message: null, retry: false};

        case HELP_ISSUE_ACTION_ERROR:
            return {...state, isFetching: false, message: action.message, retry: false};

        case HELP_ISSUE_ACTION_RETRY:
            return {...state, isFetching: false, message: action.message, retry: false};

        default:
            return state;
    }
};

export { helpIssueReducer };

import { HELP_SUB_ISSUE_ACTION, HELP_SUB_ISSUE_ACTION_RESULT, HELP_SUB_ISSUE_ACTION_ERROR, HELP_SUB_ISSUE_ACTION_RETRY } from '../actions/myAccount';

const initialHelpSubIssueState = {
    data: [],
    isFetching: false,
    retry: false,
    error: false,
    message: null
};

const helpSubIssueReducer = (state = initialHelpSubIssueState, action) => {

    switch (action.type) {

        case HELP_SUB_ISSUE_ACTION:
            return {...state, isFetching: true, error: false, message: null, retry: false};

        case HELP_SUB_ISSUE_ACTION_RESULT:
            return {...state, isFetching: false, data: action.data, error: false, message: null, retry: false};

        case HELP_SUB_ISSUE_ACTION_ERROR:
            return {...state, isFetching: false, message: action.message, retry: false};

        case HELP_SUB_ISSUE_ACTION_RETRY:
            return {...state, isFetching: false, message: action.message, retry: false};

        default:
            return state;
    }
};

export { helpSubIssueReducer };

import { HELP_SUB_SUB_ISSUE_ACTION, HELP_SUB_SUB_ISSUE_ACTION_RESULT, HELP_SUB_SUB_ISSUE_ACTION_ERROR, HELP_SUB_SUB_ISSUE_ACTION_RETRY } from '../actions/myAccount';

const initialHelpSubSubIssueState = {
    data: [],
    isFetching: false,
    retry: false,
    error: false,
    message: null
};

const helpSubSubIssueReducer = (state = initialHelpSubSubIssueState, action) => {

    switch (action.type) {

        case HELP_SUB_SUB_ISSUE_ACTION:
            return {...state, isFetching: true, error: false, message: null, retry: false};

        case HELP_SUB_SUB_ISSUE_ACTION_RESULT:
            return {...state, isFetching: false, data: action.data, error: false, message: null, retry: false};

        case HELP_SUB_SUB_ISSUE_ACTION_ERROR:
            return {...state, isFetching: false, message: action.message, retry: false};

        case HELP_SUB_SUB_ISSUE_ACTION_RETRY:
            return {...state, isFetching: false, message: action.message, retry: false};

        default:
            return state;
    }
};

export { helpSubSubIssueReducer };

import { SUBMIT_NEED_HELP_ACTION, SUBMIT_NEED_HELP_ACTION_RESULT, SUBMIT_NEED_HELP_ACTION_ERROR, SUBMIT_NEED_HELP_ACTION_RETRY } from '../actions/myAccount';

const initialSubmitNeedHelpState = {
    data: "",
    isFetching: false,
    retry: false,
    error: false,
    message: null,
    result: false
};

const submitNeedHelpReducer = (state = initialSubmitNeedHelpState, action) => {

    switch (action.type) {

        case SUBMIT_NEED_HELP_ACTION:
            return {...state, isFetching: true, error: false, message: null, retry: false, result: false};

        case SUBMIT_NEED_HELP_ACTION_RESULT:
            return {...state, isFetching: false, data: action.data, error: false, message: null, retry: false, result: true};

        case SUBMIT_NEED_HELP_ACTION_ERROR:
            return {...state, isFetching: false, message: action.message, retry: false, result: false};

        case SUBMIT_NEED_HELP_ACTION_RETRY:
            return {...state, isFetching: false, message: action.message, retry: false, result: false};

        default:
            return state;
    }
};

export { submitNeedHelpReducer };

import { ORDER_STATUS_ACTION, ORDER_STATUS_ACTION_RESULT, ORDER_STATUS_ACTION_ERROR, ORDER_STATUS_ACTION_RETRY } from '../actions/myAccount';

const initialOrderStatusState = {
    data: {},
    isFetching: false,
    retry: false,
    error: false,
    message: null
};

const orderStatusReducer = (state = initialOrderStatusState, action) => {

    switch (action.type) {

        case ORDER_STATUS_ACTION:
            return {...state, isFetching: true, error: false, message: null, retry: false};

        case ORDER_STATUS_ACTION_RESULT:
            return {...state, isFetching: false, data: action.data, error: false, message: null, retry: false};

        case ORDER_STATUS_ACTION_ERROR:
            return {...state, isFetching: false, message: action.message, retry: false, error: true};

        case ORDER_STATUS_ACTION_RETRY:
            return {...state, isFetching: false, message: action.message, retry: false};

        default:
            return state;
    }
};

export { orderStatusReducer };

import { RETURN_LIST_ACTION, RETURN_LIST_ACTION_RESULT, RETURN_LIST_ACTION_ERROR, RETURN_LIST_ACTION_RETRY } from '../actions/myAccount';

const initialReturnListState = {
    data: {},
    isFetching: false,
    retry: false,
    error: false,
    message: null
};

const returnListReducer = (state = initialReturnListState, action) => {

    switch (action.type) {

        case RETURN_LIST_ACTION:
            return {...state, isFetching: true, error: false, message: null, retry: false};

        case RETURN_LIST_ACTION_RESULT:
            return {...state, isFetching: false, data: action.data, error: false, message: null, retry: false};

        case RETURN_LIST_ACTION_ERROR:
            return {...state, isFetching: false, message: action.message, retry: false};

        case RETURN_LIST_ACTION_RETRY:
            return {...state, isFetching: false, message: action.message, retry: false};

        default:
            return state;
    }
};

export { returnListReducer };

import { RETURN_DETAIL_ACTION, RETURN_DETAIL_ACTION_RESULT, RETURN_DETAIL_ACTION_ERROR, RETURN_DETAIL_ACTION_RETRY } from '../actions/myAccount';

const initialReturnDetailState = {
    data: {},
    isFetching: false,
    retry: false,
    error: false,
    message: null
};

const returnDetailReducer = (state = initialReturnDetailState, action) => {

    switch (action.type) {

        case RETURN_DETAIL_ACTION:
            return {...state, isFetching: true, error: false, message: null, retry: false};

        case RETURN_DETAIL_ACTION_RESULT:
            return {...state, isFetching: false, data: action.data, error: false, message: null, retry: false};

        case RETURN_DETAIL_ACTION_ERROR:
            return {...state, isFetching: false, message: action.message, retry: false};

        case RETURN_DETAIL_ACTION_RETRY:
            return {...state, isFetching: false, message: action.message, retry: false};

        default:
            return state;
    }
};

export { returnDetailReducer };

import { REFUND_DETAIL_ACTION, REFUND_DETAIL_ACTION_RESULT, REFUND_DETAIL_ACTION_ERROR, REFUND_DETAIL_ACTION_RETRY } from '../actions/myAccount';

const initialRefundDetailState = {
    data: {},
    isFetching: false,
    retry: false,
    error: false,
    message: null
};

const refundDetailReducer = (state = initialRefundDetailState, action) => {

    switch (action.type) {

        case REFUND_DETAIL_ACTION:
            return {...state, isFetching: true, error: false, message: null, retry: false};

        case REFUND_DETAIL_ACTION_RESULT:
            return {...state, isFetching: false, data: action.data, error: false, message: null, retry: false};

        case REFUND_DETAIL_ACTION_ERROR:
            return {...state, isFetching: false, message: action.message, retry: false};

        case REFUND_DETAIL_ACTION_RETRY:
            return {...state, isFetching: false, message: action.message, retry: false};

        default:
            return state;
    }
};

export { refundDetailReducer };

import { CREATE_RETURN_DETAIL_ACTION, CREATE_RETURN_DETAIL_RESULT, CREATE_RETURN_DETAIL_ERROR, CREATE_RETURN_DETAIL_RETRY } from '../actions/myAccount';

const initialCreateReturnDetailState = {
    data: {},
    isFetching: false,
    retry: false,
    error: false,
    message: null,
    result: false
};

const createReturnDetailReducer = (state = initialCreateReturnDetailState, action) => {

    switch (action.type) {

        case CREATE_RETURN_DETAIL_ACTION:
            return {...state, isFetching: true, error: false, message: null, retry: false, result: false};

        case CREATE_RETURN_DETAIL_RESULT:
            return {...state, isFetching: false, data: action.data, error: false, message: null, retry: false, result: true};

        case CREATE_RETURN_DETAIL_ERROR:
            return {...state, isFetching: false, message: action.message, retry: false, result: false};

        case CREATE_RETURN_DETAIL_RETRY:
            return {...state, isFetching: false, message: action.message, retry: false, result: false};

        case 'Initail_CREATE_RETURN_ACTION':
            return {...state, isFetching: false, data: {}, message: null, retry: false, result: false};

        default:
            return state;
    }
};

export { createReturnDetailReducer };


import { SUBMIT_RETURN_ACTION, SUBMIT_RETURN_RESULT, SUBMIT_RETURN_ERROR, SUBMIT_RETURN_RETRY } from '../actions/myAccount';

const initialSubmitReturnState = {
    data: {},
    isFetching: false,
    retry: false,
    error: false,
    message: null,
    result: false
};

const submitReturnReducer = (state = initialSubmitReturnState, action) => {

    switch (action.type) {

        case SUBMIT_RETURN_ACTION:
            return {...state, isFetching: true, error: false, message: null, retry: false, result: false};

        case SUBMIT_RETURN_RESULT:
            return {...state, isFetching: false, data: action.data, error: false, message: null, retry: false, result: true};

        case SUBMIT_RETURN_ERROR:
            return {...state, isFetching: false, message: action.message, retry: false, result: false};

        case SUBMIT_RETURN_RETRY:
            return {...state, isFetching: false, message: action.message, retry: false, result: false};

        case 'Initail_SUBMIT_RETURN_ACTION':
            return {...state, isFetching: false, data: {}, message: null, retry: false, result: false};

        default:
            return state;
    }
};

export { submitReturnReducer };