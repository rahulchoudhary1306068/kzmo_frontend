import {UserProfile_ACTION, UserProfile_ACTION_RESULT,
    UserProfile_ACTION_ERROR, UserProfile_ACTION_RETRY,RemoveProfile_ACTION, RemoveProfile_ACTION_RESULT,
    RemoveProfile_ACTION_ERROR, RemoveProfile_ACTION_RETRY,UserBank_ACTION, UserBank_ACTION_RESULT,
    UserBank_ACTION_ERROR, UserBank_ACTION_RETRY,UserBank_ADD_ACTION, UserBank_ADD_ACTION_RESULT,
    UserBank_ADD_ACTION_ERROR, UserBank_ADD_ACTION_RETRY,UserBank_EDIT_ACTION, UserBank_EDIT_ACTION_RESULT,
    UserBank_EDIT_ACTION_ERROR, UserBank_EDIT_ACTION_RETRY,UserBank_All_Bank_ACTION, UserBank_All_Bank_ACTION_RESULT,
    UserBank_All_Bank_ACTION_ERROR, UserBank_All_Bank_ACTION_RETRY} 
    from '../actions/userprofile';
   
   const initialState = {
       data: {},
       isFetching: false,
       retry: false,
       error: false,
       message: null,
       result: false
   };
   
   
   const UserProfileReducer = (state = initialState, action) => {

       switch (action.type) {
   
           case UserProfile_ACTION:
               return {...state, isFetching: true, error: false, message: null, retry: false, result: false};
   
           case UserProfile_ACTION_RESULT: 
           

               return {...state, isFetching: false, data: action.data, result: true};
   
           case UserProfile_ACTION_RETRY:
               return {...state, isFetching: false, message: action.message, retry: false, result: false};
   
           case UserProfile_ACTION_ERROR:
               return {...state, isFetching: false, message: action.message, retry: false, result: false};
   
           default:
               return state;
       }
   
   };

   export default UserProfileReducer;


   
   const initialStateRemove = {
    data: {},
    isFetching: false,
    retry: false,
    error: false,
    message: null,
};


const RemoveProfileReducer = (state = initialStateRemove, action) => {

    switch (action.type) {

        case RemoveProfile_ACTION:
            return {...state, isFetching: true, error: false, message: null, retry: false};

        case RemoveProfile_ACTION_RESULT:
            return {...state, isFetching: false, data: action.data};

        case RemoveProfile_ACTION_RETRY:
            return {...state, isFetching: false, message: action.message, retry: false};

        case RemoveProfile_ACTION_ERROR:
            return {...state, isFetching: false, message: action.message, retry: false};
        
        case 'InitailStateRemoveProfile_ACTION':
        return {...state, isFetching: false, message: null, retry: false,error:false,data:{}};
    

        default:
            return state;
    }

};


export {RemoveProfileReducer};

const initialStateBank = {
       data: {data:[]},
       isFetching: false,
       retry: false,
       error: false,
       message: null,
       result: false
   };

const UserBankReducer = (state = initialStateBank, action) => {
   
   
       switch (action.type) {
   
           case UserBank_ACTION:
               return {...state, isFetching: true, error: false, message: null, retry: false, result: false};
   
           case UserBank_ACTION_RESULT:
           
           
               return {...state, isFetching: false, data: action.data, result: true};
   
           case UserBank_ACTION_RETRY:
           
           
               return {...state, isFetching: false, message: action.message, retry: false, result: false};
   
           case UserBank_ACTION_ERROR:
               return {...state, isFetching: false, message: action.message, retry: false, result: false};
   
           default:
               return state;
       }
   
  } 
  export {UserBankReducer};


const UserBankAddReducer = (state = initialStateBank, action) => {
  
       switch (action.type) {
   
           case UserBank_ADD_ACTION:
               return {...state, isFetching: true, error: false, message: null, retry: false, result: false};
   
           case UserBank_ADD_ACTION_RESULT:
               return {...state, isFetching: false, data: action.data, result: true};
   
           case UserBank_ADD_ACTION_RETRY:
           
           
               return {...state, isFetching: false, message: action.message, retry: false, result: false};
   
           case UserBank_ADD_ACTION_ERROR:
               return {...state, isFetching: false, message: action.message, retry: false, result: false};
   
           default:
               return state;
       }
   
  } 
  export {UserBankAddReducer};

  const UserBankEditReducer = (state = initialStateBank, action) => {
  
       switch (action.type) {
   
           case UserBank_EDIT_ACTION:
               return {...state, isFetching: true, error: false, message: null, retry: false, result: false};
   
           case UserBank_EDIT_ACTION_RESULT:
               return {...state, isFetching: false, data: action.data, result: true};
   
           case UserBank_EDIT_ACTION_RETRY:
           
           
               return {...state, isFetching: false, message: action.message, retry: false, result: false};
   
           case UserBank_EDIT_ACTION_ERROR:
               return {...state, isFetching: false, message: action.message, retry: false, result: false};
   
           default:
               return state;
       }
   
  } 
  export {UserBankEditReducer};



    const AllBankReducer = (state = initialStateBank, action) => {
  
       switch (action.type) {
   
           case UserBank_All_Bank_ACTION:
               return {...state, isFetching: true, error: false, message: null, retry: false, result: false};
   
           case UserBank_All_Bank_ACTION_RESULT:
               return {...state, isFetching: false, data: action.data, result: true};
   
           case UserBank_All_Bank_ACTION_RETRY:
           
               return {...state, isFetching: false, message: action.message, retry: false, result: false};
   
           case UserBank_All_Bank_ACTION_ERROR:
               return {...state, isFetching: false, message: action.message, retry: false, result: false};
   
           default:
               return state;
       }
   
  } 
  export {AllBankReducer};
