import {Checkout_ACTION, Checkout_ACTION_RESULT,
    Checkout_ACTION_ERROR, Checkout_ACTION_RETRY,PaymentOption_ACTION, PaymentOption_ACTION_RESULT,
    PaymentOption_ACTION_ERROR, PaymentOption_ACTION_RETRY,placeOrder_ACTION, placeOrder_ACTION_RESULT,
    placeOrder_ACTION_ERROR, placeOrder_ACTION_RETRY,codOTP_ACTION, codOTP_ACTION_RESULT,
    codOTP_ACTION_ERROR, codOTP_ACTION_RETRY,orderConfirm_ACTION, orderConfirm_ACTION_RESULT,
    orderConfirm_ACTION_ERROR, orderConfirm_ACTION_RETRY} 
    from '../actions/checkout';
   
   
   const initialState = {
       data: {},
       isFetching: false,
       retry: false,
       error: false,
       message: null,
   };
   
   
   const checkoutReducer = (state = initialState, action) => {
   
       switch (action.type) {
   
           case Checkout_ACTION:
               return {...state, isFetching: true, error: false, message: null, retry: false};
   
           case Checkout_ACTION_RESULT:
               return {...state, isFetching: false, data: action.data};
   
           case Checkout_ACTION_RETRY:
               return {...state, isFetching: false, message: action.message, retry: false};
   
           case Checkout_ACTION_ERROR:
               return {...state, isFetching: false, message: action.message, retry: false};
   
           default:
               return state;
       }
   
   };

   export default checkoutReducer;


   const initialStatePayment = {
    data: {},
    isFetching: false,
    retry: false,
    error: false,
    message: null,
};


const PaymentOptionReducer = (state = initialStatePayment, action) => {

    switch (action.type) {

        case PaymentOption_ACTION:
            return {...state, isFetching: true, error: false, message: null, retry: false};

        case PaymentOption_ACTION_RESULT:
            return {...state, isFetching: false, data: action.data};

        case PaymentOption_ACTION_RETRY:
            return {...state, isFetching: false, message: action.message, retry: false};

        case PaymentOption_ACTION_ERROR:
            return {...state, isFetching: false, message: action.message, retry: false};

        default:
            return state;
    }

};

export  {PaymentOptionReducer};


const initialStatePlaceOrder = {
    data: {},
    isFetching: false,
    retry: false,
    error: false,
    message: null,
};


const placeOrderReducer = (state = initialStatePlaceOrder, action) => {

    switch (action.type) {

        case placeOrder_ACTION:
            return {...state, isFetching: true, error: false, message: null, retry: false};

        case placeOrder_ACTION_RESULT:
            return {...state, isFetching: false, data: action.data};

        case placeOrder_ACTION_RETRY:
            return {...state, isFetching: false, message: action.message, retry: false};

        case placeOrder_ACTION_ERROR:
            return {...state, isFetching: false, message: action.message, retry: false};
        
        case 'InitailStatePlaceOrder_ACTION':    
            return {...state,data: {},isFetching: false,retry: false,error: false,message: null};

        default:
            return state;
    }

};

export {placeOrderReducer};


const initialStateCodOTP = {
    data: {},
    isFetching: false,
    retry: false,
    error: false,
    message: null,
};


const codOTPReducer = (state = initialStateCodOTP, action) => {

    switch (action.type) {

        case codOTP_ACTION:
            return {...state, isFetching: true, error: false, message: null, retry: false};

        case codOTP_ACTION_RESULT:
            return {...state, isFetching: false, data: action.data};

        case codOTP_ACTION_RETRY:
            return {...state, isFetching: false, message: action.message, retry: false};

        case codOTP_ACTION_ERROR:
            return {...state, isFetching: false, message: action.message, retry: false};
        
        case 'InitailStatecodOTP_ACTION':    
            return {...state,data: {},isFetching: false,retry: false,error: false,message: null};

        default:
            return state;
    }

};

export {codOTPReducer};

const orderConfirmState = {
    data: {},
    isFetching: false,
    retry: false,
    error: false,
    message: null,
};


const orderConfirmReducer = (state = orderConfirmState, action) => {

    switch (action.type) {

        case orderConfirm_ACTION:
            return {...state, isFetching: true, error: false, message: null, retry: false};

        case orderConfirm_ACTION_RESULT:
            return {...state, isFetching: false, data: action.data};

        case orderConfirm_ACTION_RETRY:
            return {...state, isFetching: false, message: action.message, retry: false};

        case orderConfirm_ACTION_ERROR:
            return {...state, isFetching: false, message: action.message, retry: false};

        default:
            return state;
    }

};

export {orderConfirmReducer};