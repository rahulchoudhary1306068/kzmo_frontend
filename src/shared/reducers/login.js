import { Login_ACTION, Login_ACTION_RESULT, Login_ACTION_ERROR, 
    Login_ACTION_RETRY, Showlogin_ACTION,OTPVerify_ACTION, OTPVerify_ACTION_RESULT, OTPVerify_ACTION_ERROR, 
    OTPVerify_ACTION_RETRY,ResendOTP_ACTION, ResendOTP_ACTION_RESULT, ResendOTP_ACTION_ERROR, 
    ResendOTP_ACTION_RETRY,LoginOTP_ACTION, LoginOTP_ACTION_RESULT, LoginOTP_ACTION_ERROR, 
    LoginOTP_ACTION_RETRY,ForgotOTP_ACTION, ForgotOTP_ACTION_RESULT, ForgotOTP_ACTION_ERROR, 
    ForgotOTP_ACTION_RETRY,ForgotVerifyOTP_ACTION, ForgotVerifyOTP_ACTION_RESULT, ForgotVerifyOTP_ACTION_ERROR, 
    ForgotVerifyOTP_ACTION_RETRY,UpdatePassword_ACTION, UpdatePassword_ACTION_RESULT, UpdatePassword_ACTION_ERROR, 
    UpdatePassword_ACTION_RETRY,InvalidToken_ACTION_RESULT} from '../actions/login';


const initialState = {
    data: {},
    isFetching: false,
    retry: false,
    error: false,
    message: null,
    showLogin: false
};


const LoginReducer = (state = initialState, action) => {

    switch (action.type) {

        case Login_ACTION:
            return {...state, isFetching: true, error: false, message: null, retry: false};

        case Login_ACTION_RESULT:
            return {...state, isFetching: false, data: action.data,showLogin:false};

        case Login_ACTION_ERROR:
            return {...state, isFetching: false, message: action.message, retry: false};

        case Login_ACTION_RETRY:
            return {...state, isFetching: false, message: action.message, retry: false};

        case Showlogin_ACTION:
            return {...state, isFetching: false, showLogin:true}; 
        case 'InitailStateLogin_ACTION':
        return {...state, data: {},isFetching: false,retry: false,error: false,message: null,showLogin: false};     
        default:
            return state;
    }
};

export default LoginReducer;

const initialStateOTP = {
    data: {},
    isFetching: false,
    retry: false,
    error: false,
    message: null,
    showLogin: false
};


const OTPReducer = (state = initialStateOTP, action) => {

    switch (action.type) {

        case OTPVerify_ACTION:
            return {...state, isFetching: true, error: false, message: null, retry: false};

        case OTPVerify_ACTION_RESULT:
            return {...state, isFetching: false, data: action.data};

        case OTPVerify_ACTION_ERROR:
            return {...state, isFetching: false, message: action.message, retry: false};

        case OTPVerify_ACTION_RETRY:
            return {...state, isFetching: false, message: action.message, retry: false};
        
        case 'InitailStateOTPVerify_ACTION':
        return {...state, isFetching: false, message: null, retry: false,data:{}};    

          
        default:
            return state;
    }
};

export  {OTPReducer};

const initialStateResendOTP = {
    data: {},
    isFetching: false,
    retry: false,
    error: false,
    message: null,
    showLogin: false
};


const OTPResendReducer = (state = initialStateResendOTP, action) => {

    switch (action.type) {

        case ResendOTP_ACTION:
            return {...state, isFetching: true, error: false, message: null, retry: false};

        case ResendOTP_ACTION_RESULT:
            return {...state, isFetching: false, data: action.data};

        case ResendOTP_ACTION_ERROR:
            return {...state, isFetching: false, message: action.message, retry: false};

        case ResendOTP_ACTION_RETRY:
            return {...state, isFetching: false, message: action.message, retry: false};
        case 'InitailStateResendOTP_ACTION':
            return {...state, data: {},isFetching: false,retry: false,error: false,message: null,showLogin: false}

          
        default:
            return state;
    }
};

export  {OTPResendReducer};
const initialStateLoginOTP = {
    data: {},
    isFetching: false,
    retry: false,
    error: false,
    message: null,
    
};


const LoginOTPReducer = (state = initialStateLoginOTP, action) => {

    switch (action.type) {

        case LoginOTP_ACTION:
            return {...state, isFetching: true, error: false, message: null, retry: false};

        case LoginOTP_ACTION_RESULT:
            return {...state, isFetching: false, data: action.data};

        case LoginOTP_ACTION_ERROR:
            return {...state, isFetching: false, message: action.message, retry: false};

        case LoginOTP_ACTION_RETRY:
            return {...state, isFetching: false, message: action.message, retry: false};
        
        case 'InitailStateLoginOTP_ACTION':
            return{...state, isFetching:false,message:null,data:{},retry:false,error:false}    

          
        default:
            return state;
    }
};

export  {LoginOTPReducer};

const initialStateForgotOTP = {
    data: {},
    isFetching: false,
    retry: false,
    error: false,
    message: null,
    showLogin: false
};


const ForgotOTPReducer = (state = initialStateForgotOTP, action) => {

    switch (action.type) {

        case ForgotOTP_ACTION:
            return {...state, isFetching: true, error: false, message: null, retry: false};

        case ForgotOTP_ACTION_RESULT:
            return {...state, isFetching: false, data: action.data};

        case ForgotOTP_ACTION_ERROR:
            return {...state, isFetching: false, message: action.message, retry: false};

        case ForgotOTP_ACTION_RETRY:
            return {...state, isFetching: false, message: action.message, retry: false};

       
        case 'InitailStateForgotOTP_ACTION':
        return {...state, data: {},isFetching: false,retry: false,error: false,message: null};     
        default:
            return state;
    }
};
export {ForgotOTPReducer}

const initialStateForgotVerifyOTP = {
    data: {},
    isFetching: false,
    retry: false,
    error: false,
    message: null,
    showLogin: false
};


const ForgotVerifyOTPReducer = (state = initialStateForgotVerifyOTP, action) => {

    switch (action.type) {

        case ForgotVerifyOTP_ACTION:
            return {...state, isFetching: true, error: false, message: null, retry: false};

        case ForgotVerifyOTP_ACTION_RESULT:
            return {...state, isFetching: false, data: action.data};

        case ForgotVerifyOTP_ACTION_ERROR:
            return {...state, isFetching: false, message: action.message, retry: false};

        case ForgotVerifyOTP_ACTION_RETRY:
            return {...state, isFetching: false, message: action.message, retry: false};

       
        case 'InitailStateForgotVerifyOTP_ACTION':
        return {...state, data: {},isFetching: false,retry: false,error: false,message: null};     
        default:
            return state;
    }
};
export {ForgotVerifyOTPReducer}

const initialStateUpdatePassword = {
    data: {},
    isFetching: false,
    retry: false,
    error: false,
    message: null,
    showLogin: false
};


const UpdatePasswordReducer = (state = initialStateUpdatePassword, action) => {

    switch (action.type) {

        case UpdatePassword_ACTION:
            return {...state, isFetching: true, error: false, message: null, retry: false};

        case UpdatePassword_ACTION_RESULT:
            return {...state, isFetching: false, data: action.data};

        case UpdatePassword_ACTION_ERROR:
            return {...state, isFetching: false, message: action.message, retry: false};

        case UpdatePassword_ACTION_RETRY:
            return {...state, isFetching: false, message: action.message, retry: false};

       
        case 'InitailStateUpdatePassword_ACTION':
        return {...state, data: {},isFetching: false,retry: false,error: false,message: null};     
        default:
            return state;
    }
};
export {UpdatePasswordReducer}


const InvalidTokeinitialState = {
    value: false,
    
};


const InvalidTokenReducer = (state = InvalidTokeinitialState, action) => {

    switch (action.type) {

        case InvalidToken_ACTION_RESULT:
            return {...state, value:true};
         
        case 'InitailInvalidToken_ACTION':
            return {...state, value:false};     
        default:
            return state;
    }
};

export{InvalidTokenReducer}