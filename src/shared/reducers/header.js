import { HEADER_ACTION, HEADER_ACTION_RESULT, HEADER_ACTION_ERROR, HEADER_ACTION_RETRY, UPDATE_LANG_ACTION, UPDATELANG_ACTION_RESULT,
    BrandMenu_ACTION, BrandMenu_ACTION_RESULT, BrandMenu_ACTION_ERROR, BrandMenu_ACTION_RETRY
} from '../actions/header';


const initialState = {
    data: {},
    isFetching: false,
    retry: false,
    error: false,
    message: null,
};


const headerReducer = (state = initialState, action) => {

    switch (action.type) {

        case HEADER_ACTION:
            return {...state, isFetching: true, error: false, message: null, retry: false};

        case HEADER_ACTION_RESULT:
            return {...state, isFetching: false, data: action.data};

        case HEADER_ACTION_ERROR:
            return {...state, isFetching: false, message: action.message, retry: false};

        case HEADER_ACTION_RETRY:
            return {...state, isFetching: false, message: action.message, retry: false};

        default:
            return state;
    }
};

export default headerReducer;


import { HEADER_SUBMENU, HEADER_SUBMENU_RESULT, HEADER_SUBMENU_ERROR, HEADER_SUBMENU_RETRY } from '../actions/header';

const initialStateSection = {
    submenu: [],
    isFetching: false,
    retry: false,
    error: false,
    message: null,
};


const subMenuReducer = (state = initialStateSection, action) => {

    switch (action.type) {

        case HEADER_SUBMENU:
            return {...state, isFetching: true, error: false, message: null, retry: false};

        case HEADER_SUBMENU_RESULT:
             {  
                state.submenu[action.data.index]=action.data;

                state.isFetching = false;
                return state;
             };

        case HEADER_SUBMENU_ERROR:
            return {...state, isFetching: false, message: action.message, retry: false};

        case HEADER_SUBMENU_RETRY:
            return {...state, isFetching: false, message: action.message, retry: false};

        default:
            return state;
    }
};

export {subMenuReducer};


import { HEADER_SEARCH, HEADER_SEARCH_RESULT, HEADER_SEARCH_ERROR, HEADER_SEARCH_RETRY } from '../actions/header';

const initialStateSearch = {
    data: [],
    isFetching: false,
    retry: false,
    error: false,
    message: null,
};


const headerSearchReducer = (state = initialStateSearch, action) => {

    switch (action.type) {

        case HEADER_SEARCH:
            return {...state, isFetching: true, error: false, message: null, retry: false};

        case HEADER_SEARCH_RESULT:
            return {...state, isFetching: false, data: action.data};

        case HEADER_SEARCH_ERROR:
            return {...state, isFetching: false, message: action.message, retry: false};

        case HEADER_SEARCH_RETRY:
            return {...state, isFetching: false, message: action.message, retry: false};

        default:
            return state;
    }
};

export { headerSearchReducer };


import { Country_ACTION, Country_ACTION_RESULT, Country_ACTION_ERROR, Country_ACTION_RETRY } from '../actions/header';
const initialCountry = {
    data: [],
    isFetching: false,
    retry: false,
    error: false,
    message: null,
};


const countryReducer = (state = initialCountry, action) => {

    switch (action.type) {

        case Country_ACTION:
            return {...state, isFetching: true, error: false, message: null, retry: false};

        case Country_ACTION_RESULT:
            return {...state, isFetching: false, data: action.data};

        case Country_ACTION_ERROR:
            return {...state, isFetching: false, message: action.message, retry: false};

        case Country_ACTION_RETRY:
            return {...state, isFetching: false, message: action.message, retry: false};

        default:
            return state;
    }
};

export { countryReducer };


import { CITY_ACTION, CITY_ACTION_RESULT, CITY_ACTION_ERROR, CITY_ACTION_RETRY } from '../actions/header';
const initialCity = {
    data: [],
    isFetching: false,
    retry: false,
    error: false,
    message: null,
};

const cityReducer = (state = initialCity, action) => {

    switch (action.type) {

        case CITY_ACTION:
            return {...state, isFetching: true, error: false, message: null, retry: false};

        case CITY_ACTION_RESULT:
            return {...state, isFetching: false, data: action.data};

        case CITY_ACTION_ERROR:
            return {...state, isFetching: false, message: action.message, retry: false};

        case CITY_ACTION_RETRY:
            return {...state, isFetching: false, message: action.message, retry: false};

        default:
            return state;
    }
};

export { cityReducer };

import { STORE_ACTION, STORE_ACTION_RESULT, STORE_ACTION_ERROR, STORE_ACTION_RETRY } from '../actions/header';
const initialStore = {
    data: [],
    isFetching: false,
    retry: false,
    error: false,
    message: null,
};

const storeReducer = (state = initialStore, action) => {

    switch (action.type) {

        case STORE_ACTION:
            return {...state, isFetching: true, error: false, message: null, retry: false};

        case STORE_ACTION_RESULT:
            return {...state, isFetching: false, data: action.data};

        case STORE_ACTION_ERROR:
            return {...state, isFetching: false, message: action.message, retry: false};

        case STORE_ACTION_RETRY:
            return {...state, isFetching: false, message: action.message, retry: false};

        default:
            return state;
    }
};

export { storeReducer };

const initialLanguage= {
    value: ''
};

const languageReducer = (state = initialLanguage, action) => {

    switch (action.type) {

        case UPDATE_LANG_ACTION:
            return {...state};
        case UPDATELANG_ACTION_RESULT:
            return {...state, value:action.data};
        case 'initailLanguageAction':
            return {...state,value:""}
        default:
            return state;
    }
};

export { languageReducer };


const BrandMenuinitialState = {
    data: {},
    isFetching: false,
    retry: false,
    error: false,
    message: null,
};


const brandMenuReducer = (state = BrandMenuinitialState, action) => {

    switch (action.type) {

        case BrandMenu_ACTION:
            return {...state, isFetching: true, error: false, message: null, retry: false};

        case BrandMenu_ACTION_RESULT:
            return {...state, isFetching: false, data: action.data};

        case BrandMenu_ACTION_ERROR:
            return {...state, isFetching: false, message: action.message, retry: false};

        case BrandMenu_ACTION_RETRY:
            return {...state, isFetching: false, message: action.message, retry: false};

        default:
            return state;
    }
};

export {brandMenuReducer};
