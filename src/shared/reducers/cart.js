import { addtocart_ACTION, addtocart_ACTION_RESULT,
     addtocart_ACTION_ERROR, addtocart_ACTION_RETRY,
     getcart_ACTION, getcart_ACTION_RESULT,
     getcart_ACTION_ERROR, getcart_ACTION_RETRY,
     deleteproductfromcart_ACTION, deleteproductfromcart_ACTION_RESULT,
     deleteproductfromcart_ACTION_ERROR, deleteproductfromcart_ACTION_RETRY,
     cartcouponcode_ACTION, cartcouponcode_ACTION_RESULT,
     cartcouponcode_ACTION_ERROR, cartcouponcode_ACTION_RETRY,
    addToWishlist_ACTION, addToWishlist_ACTION_RESULT, addToWishlist_ACTION_ERROR, addToWishlist_ACTION_RETRY
    } from '../actions/cart';


const initialState = {
    cart: {},
    cartCount:0,
    addResult: false,
    isFetching: false,
    retry: false,
    error: false,
    message: null,
};


const cartReducer = (state = initialState, action) => {

    switch (action.type) {

        case addtocart_ACTION:
            return {...state, isFetching: true, error: false, message: null, retry: false, addResult: false};

        case addtocart_ACTION_RESULT:
            return {...state, isFetching: false, cart: action.data, addResult: true};

        case addtocart_ACTION_ERROR:
            return {...state, isFetching: false, message: action.message, retry: false, addResult: false};

        case addtocart_ACTION_RETRY:
            return {...state, isFetching: false, message: action.message, retry: false, addResult: false};

        case getcart_ACTION:
            return {...state, isFetching: true, error: false, message: null, retry: false, addResult: false};

        case getcart_ACTION_RESULT:
            return {...state, isFetching: false,message:action.message, cart: action.data, addResult: false};

        case getcart_ACTION_ERROR:
            return {...state, isFetching: false, message: action.message, retry: false, addResult: false};

        case getcart_ACTION_RETRY:
            return {...state, isFetching: false, message: action.message, retry: false, addResult: false};
        case deleteproductfromcart_ACTION:
            return {...state, isFetching: true, error: false, message: null, retry: false, addResult: false};

        case deleteproductfromcart_ACTION_RESULT:
            return {...state, isFetching: false,message:action.message, cart: action.data, addResult: false};

        case deleteproductfromcart_ACTION_ERROR:
            return {...state, isFetching: false, message: action.message, retry: false, addResult: false};

        case deleteproductfromcart_ACTION_RETRY:
            return {...state, isFetching: false, message: action.message, retry: false, addResult: false};
        case cartcouponcode_ACTION:
            return {...state, isFetching: true, error: false, message: null, retry: false, addResult: false};

        case cartcouponcode_ACTION_RESULT:
            return {...state, isFetching: false,message:action.message, cart: action.data, addResult: false};

        case cartcouponcode_ACTION_ERROR:
            return {...state, isFetching: false, message: action.message, retry: false, addResult: false};

        case cartcouponcode_ACTION_RETRY:
            return {...state, isFetching: false, message: action.message, retry: false, addResult: false};        
        default:
            return state;
    }
};

const initialWishlistState = {
    wishlist: {},
    isFetching: false,
    retry: false,
    error: false,
    message: null,
};

export default cartReducer;

const wishlistReducer = (state = initialWishlistState, action) => {

    switch (action.type) {

        case addToWishlist_ACTION:
            return {...state, isFetching: true, error: false, message: null, retry: false};

        case addToWishlist_ACTION_RESULT:
            return {...state, isFetching: false, cart: action.data};

        case addToWishlist_ACTION_ERROR:
            return {...state, isFetching: false, message: action.message, retry: false};

        case addToWishlist_ACTION_RETRY:
            return {...state, isFetching: false, message: action.message, retry: false};

        default:
            return state;
    }
};

export { wishlistReducer };