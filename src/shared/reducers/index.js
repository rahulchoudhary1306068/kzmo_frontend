import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';

// Import your reducers here
import example, {reducer1} from './example';

import homeReducer, {sectionReducer} from './home'
import LoginReducer , {OTPReducer,OTPResendReducer,LoginOTPReducer,ForgotOTPReducer,ForgotVerifyOTPReducer,UpdatePasswordReducer,InvalidTokenReducer} from './login';
import productDetailReducer, { deliveryDetailReducer, nearStoreReducer, siblingIdReducer, eDDReducer } from './ProductDetail';
import productListReducer,{productIdReducer} from './productList';
import headerReducer, { subMenuReducer, headerSearchReducer, countryReducer, cityReducer, storeReducer,languageReducer,brandMenuReducer } from './header';
import cartReducer, { wishlistReducer } from './cart'
import checkoutReducer, {PaymentOptionReducer, placeOrderReducer,codOTPReducer,orderConfirmReducer}from './checkout'

import UserProfileReducer,{RemoveProfileReducer,UserBankReducer,UserBankAddReducer,UserBankEditReducer,AllBankReducer} from './userprofile';
import { userInfoReducer, rewardPointReducer, orderReducer, getWishlistReducer, getReviewsReducer, orderDetailReducer, submitOrderReviewsReducer, helpIssueReducer, helpSubIssueReducer, helpSubSubIssueReducer, submitNeedHelpReducer, orderStatusReducer, returnListReducer, returnDetailReducer, refundDetailReducer, createReturnDetailReducer, submitReturnReducer } from './myAccount';

import locatorReducer,{ mallReducer} from './locator';


const allReducers = combineReducers({
    // add your reducers here
   // routing: routerReducer,
    example,
    reducer1,
    homeReducer,
    // sectionReducer,
    headerReducer,
    subMenuReducer,

    productDetailReducer,
    siblingIdReducer,
    deliveryDetailReducer,
    productListReducer,
    productIdReducer,
    LoginReducer,

    headerSearchReducer,
    cartReducer,
    checkoutReducer,
    UserProfileReducer,
    UserBankReducer,
    UserBankAddReducer,
    UserBankEditReducer,
    AllBankReducer,
    PaymentOptionReducer,
    placeOrderReducer,
    countryReducer,
    cityReducer,
    storeReducer,
	rewardPointReducer,
    OTPReducer,
    OTPResendReducer,
    LoginOTPReducer,
    wishlistReducer,
    userInfoReducer,
    codOTPReducer,
	orderReducer,
    getWishlistReducer,
    RemoveProfileReducer,
	getReviewsReducer,
    orderDetailReducer,
    ForgotOTPReducer,
    ForgotVerifyOTPReducer,
    UpdatePasswordReducer,
    InvalidTokenReducer,

    submitOrderReviewsReducer,
    helpIssueReducer,
    helpSubIssueReducer,
    helpSubSubIssueReducer,
    submitNeedHelpReducer,
	languageReducer,
    orderStatusReducer,
    returnListReducer,
    returnDetailReducer,
    refundDetailReducer,
    orderConfirmReducer,
    brandMenuReducer,
    createReturnDetailReducer,

    submitReturnReducer,
    nearStoreReducer,

    eDDReducer,
    locatorReducer,
    mallReducer
});

export default allReducers;
