import { PRODUCTDETAIL_ACTION, PRODUCTDETAIL_ACTION_RESULT, PRODUCTDETAIL_ACTION_ERROR, PRODUCTDETAIL_ACTION_RETRY,
    GETNEARSTORE_ACTION, GETNEARSTORE_ACTION_RESULT, GETNEARSTORE_ACTION_ERROR, GETNEARSTORE_ACTION_RETRY,
GETEDD_ACTION,GETEDD_ACTION_RESULT,GETEDD_ACTION_ERROR,GETEDD_ACTION_RETRY
} from '../actions/ProductDetail';

import { PINCODE_ACTION, PINCODE_ACTION_RESULT, PINCODE_ACTION_RETRY, PINCODE_ACTION_ERROR } from '../actions/ProductDetail';


const initialState = {
    data: {},
    isFetching: false,
    retry: false,
    error: false,
    message: null,
    result: false
};


const productDetailReducer = (state = initialState, action) => {

    switch (action.type) {

        case PRODUCTDETAIL_ACTION:
            return {...state, isFetching: true, error: false, message: null, retry: false, result: false};

        case PRODUCTDETAIL_ACTION_RESULT:
            return {...state, isFetching: false, data: action.data, error: false, message: null, retry: false, result: true};

        case PRODUCTDETAIL_ACTION_ERROR:
            return {...state, isFetching: false, message: action.message, retry: false, result: false};

        case PRODUCTDETAIL_ACTION_RETRY:
            return {...state, isFetching: false, message: action.message, retry: false, result: false};

        case "Initail_Product_Detail_ACTION":
            return {...state, isFetching: false, message: null, retry: false, result: false, error: false, data: {}};

        default:
            return state;
    }
};

export default productDetailReducer;

const initialNearStore = {
    data: {},
    isFetching: false,
    retry: false,
    error: false,
    message: null,
    result: false
};


const nearStoreReducer = (state = initialNearStore, action) => {

    switch (action.type) {

        case GETNEARSTORE_ACTION:
            return {...state, isFetching: true, error: false, message: null, retry: false, result: false, initial: false};

        case GETNEARSTORE_ACTION_RESULT:
            return {...state, isFetching: false, data: action.data, result: true, initial: false};

        case GETNEARSTORE_ACTION_ERROR:
            return {...state, isFetching: false, message: action.message, retry: false, result: false, initial: false};

        case GETNEARSTORE_ACTION_RETRY:
            return {...state, isFetching: false, message: action.message, retry: false, result: false, initial: false};

        case "Initail_Near_Store_ACTION":
            return {...state, isFetching: false, message: null, retry: false, result: false, error: false, data: {}, initial: true};

        default:
            return state;
    }
}

export {nearStoreReducer};


import { GET_SIBLING_ID_ACTION, GET_SIBLING_ID_RESULT, GET_SIBLING_ID_RETRY, GET_SIBLING_ID_ERROR } from '../actions/ProductDetail';

const initialSiblingState = {
    data: {},
    isFetching: false,
    retry: false,
    error: false,
    message: null,
    result: false
};

const siblingIdReducer = (state = initialSiblingState, action) => {

    switch (action.type) {

        case GET_SIBLING_ID_ACTION:
            return {...state, isFetching: true, error: false, message: null, retry: false, result: false};

        case GET_SIBLING_ID_RESULT:
            return {...state, isFetching: false, data: action.data, result: true};

        case GET_SIBLING_ID_ERROR:
            return {...state, isFetching: false, message: action.message, retry: false, result: false};

        case GET_SIBLING_ID_RETRY:
            return {...state, isFetching: false, message: action.message, retry: false, result: false};

        case "Initail_SIBLING_ID_ACTION":
            return {...state, isFetching: false, message: null, retry: false, result: false, error: false, data: {}};

        default:
            return state;
    }
}

export {siblingIdReducer};

const initialDeliveryState = {
data: {},
isFetching: false,
retry: false,
error: false,
message: null,
};


const eDDReducer = (state = initialDeliveryState, action) => {

switch (action.type) {

case GETEDD_ACTION:
return {...state, isFetching: true, error: false, message: null, retry: false};

case GETEDD_ACTION_RESULT:
return {...state, isFetching: false, data: action.data};

case GETEDD_ACTION_ERROR:
return {...state, isFetching: false, message: action.message, retry: false};

case GETEDD_ACTION_RETRY:
return {...state, isFetching: false, message: action.message, retry: false};

default:
return state;
}
}

export {eDDReducer};