import { LOCATOR_ACTION, LOCATOR_ACTION_RESULT, LOCATOR_ACTION_RETRY, LOCATOR_ACTION_ERROR,
MALL_ACTION, MALL_ACTION_RESULT, MALL_ACTION_RETRY, MALL_ACTION_ERROR
 } from '../actions/locator';


const initialState = {
    data: {},
    isFetching: false,
    retry: false,
    error: false,
    message: null,
    result: false
};


const locatorReducer = (state = initialState, action) => {

    switch (action.type) {

        case LOCATOR_ACTION:
            return {...state, isFetching: true, error: false, message: null, retry: false, result: false};

        case LOCATOR_ACTION_RESULT:
            return {...state, isFetching: false, data: action.data, error: false, message: null, retry: false, result: true};

        case LOCATOR_ACTION_ERROR:
            return {...state, isFetching: false, message: action.message, retry: false, result: false};

        case LOCATOR_ACTION_RETRY:
            return {...state, isFetching: false, message: action.message, retry: false, result: false};

        //case "Initail_Product_Detail_ACTION":
          //  return {...state, isFetching: false, message: null, retry: false, result: false, error: false, data: {}};

        default:
            return state;
    }
};

export default locatorReducer;

const initialMallState = {
    data: {},
    isFetching: false,
    retry: false,
    error: false,
    message: null,
    result: false
};


const mallReducer = (state = initialMallState, action) => {

    switch (action.type) {

        case MALL_ACTION:
            return {...state, isFetching: true, error: false, message: null, retry: false, result: false};

        case MALL_ACTION_RESULT:
            return {...state, isFetching: false, data: action.data, error: false, message: null, retry: false, result: true};

        case MALL_ACTION_ERROR:
            return {...state, isFetching: false, message: action.message, retry: false, result: false};

        case MALL_ACTION_RETRY:
            return {...state, isFetching: false, message: action.message, retry: false, result: false};

        //case "Initail_Product_Detail_ACTION":
          //  return {...state, isFetching: false, message: null, retry: false, result: false, error: false, data: {}};

        default:
            return state;
    }
};

export {mallReducer};



