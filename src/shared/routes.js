/**
 * Created by amit on 4/24/18.
 */

// Import your components here.
//example imports
import { Home } from './screens';
import { ProductDetail } from './screens';
import { ProductListing } from './screens';
import { ShopingCart } from './screens';
import { Checkout } from './screens';
import { ThankYou } from './screens';
import { LandingPage } from './screens';
import { SelectLocator } from './screens';
import { MyAccount } from './screens';
import { NeedHelp } from './screens';
import { OrderDetail } from './screens';
import { ReturnDetail } from './screens';
import { OrderFail } from './screens';
import { Brand } from './screens';
import { ReturnRequest } from './screens';
import { ReturnThankyou } from './screens';
import { ReinitiateReturn } from './screens';

export default [
    {
        exact: true,
        component: Home,
        path: "/"
    },
    {
        exact: true,
        component: ProductDetail,
        path: "/pd/:product_id"
    },
    {
        // exact: true,
        component: ProductListing,
        path: "/search/:pathParam1?"

    },
    {
        exact: true,
        component: ProductListing,
        path: "/ct/:category_id"

    },
    {
        exact: true,
        component: ShopingCart,
        path: "/shopingcart"

    },
    {
        exact: true,
        component: Checkout,
        path: "/checkout"
    },
    {
        exact: true,
        component: ThankYou,
        path: "/thankyou/:pathParam1?/:pathParam2?"
    },
    {
        exact: true,
        component: OrderFail,
        path: "/orderfail/:pathParam1?/:pathParam2?"
    },
    {
        exact: true,
        component: LandingPage,
        path: "/landingpage"

    },

    {
        exact: true,
        component: SelectLocator,
        path: "/locator"

    },
    {
        
        component: ThankYou,
        path: "/orderConfirmation/:pathParam1?"

    },
    {
        exact: true,
        component: OrderDetail,
        path: "/myaccount/order/:orderId"
    },
    {
        exact: true,
        component: OrderDetail,
        path: "/myaccount/need-help/:orderId"
    },
    {
        exact: true,
        component: OrderDetail,
        path: "/myaccount/track-order/:orderId"
    },
    {
        exact: true,
        component: ReturnDetail,
        path: "/myaccount/return/:orderId/:returnId"
    },
    {
        exact: true,
        component: MyAccount,
        path: "/myaccount/:viewType"
    },
    {
        exact: true,
        component: MyAccount,
        path: "/myaccount"
    },
    {
        exact: true,
        component: ReturnRequest,
        path: "/myaccount/create-return/:orderId/:itemId"
    },
    {
        exact: true,
        component: NeedHelp,
        path: "/needhelp"
    },
    {
        exact: true,
        component: ReturnThankyou,
        path: "/returnthankyou"
    },
    {
        exact: true,
        component: Brand,
        path: "/brand"
    },
    {
        exact: true,
        component: ReinitiateReturn,
        path: "/reinitiate"
    }
    

];
