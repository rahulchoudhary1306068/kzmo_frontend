$(document).ready(function() {
    /*navbar toggle*/
    $(".header_contaner .navbar .dropdown-toggle").hover(function () {
       $(this).parent().toggleClass("show");
       $(this).parent().find(".dropdown-menu").toggleClass("show"); 
     });

    $( ".header_contaner .navbar .dropdown-menu" ).mouseleave(function() {
      $(this).removeClass("show");  
    });
    $(".header_contaner .search_form .dropdown-toggle").hover(function () {
        if($(this).hasClass("hover_outer")){
            $(this).removeClass("hover_outer");
        }
        else{
            $(this).addClass("hover_outer");
        }
    });
    $( ".header_contaner .search_form .dropdown-menu" ).hover(function() {
       $(this).parents(".search_form").find(".dropdown-toggle").addClass("hover_outer");
    });
    $( ".header_contaner .search_form .dropdown-menu" ).mouseleave(function() {
       $(this).parents(".search_form").find(".dropdown-toggle").removeClass("hover_outer");
    });
    /* owl carusel slider for home page*/

    $('.just_arrived_loop').owlCarousel({
        center: true,
        loop:true,
        margin:20,
        responsive:{
            0:{
                items:2
            },
            768:{
                mouseDrag : false,
                items:3
            }
        }
        
    });
    $('.trending_near_loop').owlCarousel({
        center: false,
        loop:true,
        margin:40,
        responsive:{
            0:{
                items:2
            },
            768:{
                mouseDrag : false,
                items:4
            }
        }
        
    });
    $('.may_like_loop').owlCarousel({
        center: false,
        loop:true,
        margin:40,
        responsive:{
            0:{
                items:2
            },
            768:{
                mouseDrag : false,
                items:4
            }
        }
        
    });
    $('.follow_insta_loop').owlCarousel({
        center: false,
        loop:true,
        margin:0,
        nav: true,
        dots: false,
        autoplay: true,
        responsive:{
            0:{
                items:2
            },
            768:{
                items:6
            }
        } 
    });

    /*view more/less in menu*/
    $(".menu_inner").each(function(){
        var list = $(this).find("ul li");
        var numToShow = 10;
        var show_more = $(this).find(".show_more");
        var numInList = list.length;
        list.hide();
        show_more.hide();
        if (numInList > numToShow) {
            show_more.show();
        }
        list.slice(0, numToShow).show();
        show_more.click(function(){
            var showing = list.filter(':visible').length;
            list.slice(showing - 1, showing + numToShow).fadeIn();
            var nowShowing = list.filter(':visible').length;
            if (nowShowing >= numInList) {
                show_more.hide();
            }
        });
        
    });
    
    /*sticly header*/
    console.log($('.sort_filter_outer'));
    $(window).scroll(function(){
        if ($(window).scrollTop() >= 35) {
            $('#navbar_header').addClass('sticky');
        }
        else {
            $('#navbar_header').removeClass('sticky');
        }
        if ($(window).scrollTop() >= 50) {
            $('.sort_filter_outer').addClass('sticky_filter');
        }
        else {
            $('.sort_filter_outer').removeClass('sticky_filter');
        }
    });
    /* navigation menu*/
    $(".header_contaner .navbar-toggler,.header_contaner .close_menu").on("click",function(){
        if ($('html').hasClass('nav-open')) {
            $('html').removeClass('nav-open');
            $('html').removeClass('nav-before-open');
        } else {
            $('html').addClass('nav-before-open');
            $('html').addClass('nav-open');
           
        }
    });
    $("body").on("click",".mobile_menu_outer .menu_list a,.bottom_links,.mobile_menu_outer .card-body h6",function(){
         $(".navbar-collapse").collapse('hide');
         if ($('html').hasClass('nav-open')) {
            $('html').removeClass('nav-open');
            $('html').removeClass('nav-before-open');
        } else {
            $('html').addClass('nav-before-open');
            $('html').addClass('nav-open');
           
        }
    });
    $(".header_contaner .close_menu").on("click",function(){
         $(".navbar-collapse").collapse('hide');
    });
    /*plp filter meenu*/
   $("body").on("click", ".sort_filter_outer .filter_btn",function(){
        if($(".filter_outer").hasClass("filter_open")) {
            $(".filter_outer").removeClass("filter_open");
            $('html').removeClass('filterouter-open');
        }
        else{
            $(".filter_outer").addClass("filter_open");
            $('html').addClass('filterouter-open');

        }
    });
    $("body").on("click", ".filter_outer .cross_icon",function(){
        $(".filter_outer").removeClass("filter_open");
        $('html').removeClass('filterouter-open');
    });
    $("body").on("click",function(){
        $(".common_tooltip").hide();
    });
    $("body").on("click", ".common_tooltip_outer",function(e){
        e.stopPropagation();
        $(".common_tooltip").hide();
        $(this).find(".common_tooltip").show();
    });
    
    /*$(".product_carosel .carousel-slider .slide.selected").hover(function () {
        console.log("showww");
        $(this).parents(".product_carosel").find(".zoom_wrapper").show();

    });
    $(".product_carosel .carousel-slider .slide.selected").mouseleave(function () {
        console.log("hidddeee");
        $(this).parents(".product_carosel").find(".zoom_wrapper").hide();

    });*/
    if ($(window).width() <= 1024) {
        $("body").on( "click",'.product_carosel .carouseldata-cont', function() {
           $('#image_popup').modal('show');
           console.log("open");
        });
    }
});
